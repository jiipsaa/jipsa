// Copyright Epic Games, Inc. All Rights Reserved.

#include "MultiplayMapPlayerController.h"
#include "Kismet/GameplayStatics.h"

void AMultiplayMapPlayerController::RequestSendChat_Implementation(const FText& message)
{
	TArray<AActor*> actors;
	UGameplayStatics::GetAllActorsOfClass(GetPawn()->GetWorld(), APlayerController::StaticClass(), actors);

	for (AActor* actor : actors)
	{
		AMultiplayMapPlayerController* playerController = Cast<AMultiplayMapPlayerController>(actor);
		if (playerController)
		{
			playerController->ResponseChat(message);
		}
	}
}

void AMultiplayMapPlayerController::ResponseChat_Implementation(const FText& message)
{
	FromClient_ResponseChat(message);
}