// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "MultiplayMapGameState.generated.h"

/**
 * 
 */
UCLASS()
class LISTENSERVER_API AMultiplayMapGameState : public AGameStateBase
{
	GENERATED_BODY()

public:
	virtual void AddPlayerState(APlayerState* PlayerState) override
	{
		Super::AddPlayerState(PlayerState);

		FromClient_AddPlayerState();
	}
	
	virtual void RemovePlayerState(APlayerState* PlayerState) override
	{
		Super::RemovePlayerState(PlayerState);

		FromClient_RemovePlayerState();
	}

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void FromClient_AddPlayerState();

	UFUNCTION(BlueprintImplementableEvent)
	void FromClient_RemovePlayerState();
};