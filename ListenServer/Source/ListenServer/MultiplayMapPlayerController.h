// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MultiplayMapPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class LISTENSERVER_API AMultiplayMapPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	UFUNCTION(Server, Unreliable)
	void RequestSendChat(const FText& message);

	UFUNCTION(Client, Unreliable)
	void ResponseChat(const FText& message);

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void FromClient_ResponseChat(const FText& message);
};
