// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ListenServer : ModuleRules
{
	public ListenServer(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] {
			"Core"
			,"CoreUObject"
			,"Engine"
			,"InputCore"
			,"HeadMountedDisplay"
			,"UMG"
			//,"OnlineSubsystem"
			//,"OnlineSubsystemUtils"
            ,"Sockets"
			//,"Networking"
			,"Http"
		});

		PrivateDependencyModuleNames.AddRange(new string[] {
			"Slate"
			,"SlateCore"
		});
	}
}
