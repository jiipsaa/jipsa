// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ListenServerGameMode.generated.h"

UCLASS(minimalapi)
class AListenServerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AListenServerGameMode();
};



