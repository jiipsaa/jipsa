// Copyright Epic Games, Inc. All Rights Reserved.

#include "ListenServer.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ListenServer, "ListenServer" );
 