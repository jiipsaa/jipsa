﻿#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "HttpModule.h"

#include "LobbyMenuWindow.generated.h"

UCLASS()
class LISTENSERVER_API ULobbyMenuWindow : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UFUNCTION(BlueprintCallable, Category = "ToClient")
	FString ToClient_GetInternalIPAddress();

	UFUNCTION(BlueprintCallable, Category = "ToClient")
	void ToClient_RequestExternalIPAddress();

	UFUNCTION(BlueprintImplementableEvent, Category = "FromClient")
	void FromClient_ResponseExternalIPAddress(const FString& s);

private:
	void ResponseExternalIPAddress(FHttpRequestPtr request, FHttpResponsePtr response, bool bConnectedSuccessfully);
};