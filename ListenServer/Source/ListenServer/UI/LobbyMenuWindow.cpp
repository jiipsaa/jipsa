﻿#include "LobbyMenuWindow.h"

#include "Interfaces/IHttpResponse.h"
//#include "Interfaces/IPv4/IPv4Endpoint.h"
#include "SocketSubsystem.h"

FString ULobbyMenuWindow::ToClient_GetInternalIPAddress()
{
	// 방법 1.
	//TArray<TSharedPtr<FInternetAddr>> localAddresses;
	//if (true == ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->GetLocalAdapterAddresses(localAddresses))
	//{
	//	if (0 < localAddresses.Num())
	//	{
	//		FString ip;
	//		for (const auto& localAddress : localAddresses)
	//		{
	//			FIPv4Endpoint endpoint(localAddress);
	//			ip.Appendf(TEXT("%s\n"), *endpoint.Address.ToString());
	//		}
	//		return ip;
	//	}
	//}

	// 방법 2.
	bool bCanBindAll;
	TSharedRef<FInternetAddr> localAddresses = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->GetLocalHostAddr(*GLog, bCanBindAll);
	if (true == localAddresses->IsValid())
	{
		return localAddresses->ToString(false);
	}

	return FString("Error");
}

void ULobbyMenuWindow::ToClient_RequestExternalIPAddress()
{
	FHttpModule* httpModule = &FHttpModule::Get();
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> httpRequest = httpModule->CreateRequest();

	httpRequest->SetURL(FString("https://api.ipify.org/"));

	httpRequest->OnProcessRequestComplete().BindUObject(this, &ULobbyMenuWindow::ResponseExternalIPAddress);
	if (false == httpRequest->ProcessRequest())
	{
		httpRequest->OnProcessRequestComplete().Unbind();
		UE_LOG(LogTemp, Warning, TEXT("ToClient_RequestExternalIPAddress() 실패했습니다. url=%s"), *httpRequest->GetURL());
	}
}

void ULobbyMenuWindow::ResponseExternalIPAddress(FHttpRequestPtr request, FHttpResponsePtr response, bool bConnectedSuccessfully)
{
	if (false == bConnectedSuccessfully)
	{
		UE_LOG(LogTemp, Warning, TEXT("ResponseExternalIPAddress() 실패했습니다. url=%s"), *request.Get()->GetURL());
		return;
	}

	if (false == response.IsValid())
	{
		UE_LOG(LogTemp, Warning, TEXT("ResponseExternalIPAddress() 실패했습니다. HttpResponsePtr 가 InValid 합니다. url=%s"), *request.Get()->GetURL());
		return;
	}

	FromClient_ResponseExternalIPAddress(response.Get()->GetContentAsString());
}