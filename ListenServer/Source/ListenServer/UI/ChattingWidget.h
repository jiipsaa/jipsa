// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/EditableTextBox.h"
#include "Components/ScrollBox.h"
#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"

#include "../MultiplayMapPlayerController.h"

#include "ChattingWidget.generated.h"

/**
 * 
 */
UCLASS()
class LISTENSERVER_API UChattingWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(Meta = (BindWidget))
	class UScrollBox* chatScrollBox;

	UPROPERTY(Meta = (BindWidget))
	class UEditableTextBox* inputBox;

public:
	UFUNCTION(BlueprintCallable)
	void AddChatToScrollBox(const FText& message)
	{
		if (true == message.IsEmpty())
		{
			return;
		}

		UTextBlock* chatText = NewObject<UTextBlock>(chatScrollBox);
		chatText->SetText(message);

		chatScrollBox->AddChild(chatText);
		chatScrollBox->ScrollToEnd();
	}

protected:
	virtual void NativeConstruct() override
	{
		Super::NativeConstruct();

		inputBox->OnTextCommitted.AddDynamic(this, &UChattingWidget::OnTextCommitted_Chat);
	}

	UFUNCTION()
	void OnTextCommitted_Chat(const FText& message, ETextCommit::Type eTextCommit)
	{
		if (ETextCommit::OnEnter == eTextCommit)
		{
			if (true == message.IsEmpty())
			{
				return;
			}

			AMultiplayMapPlayerController* playerController = Cast<AMultiplayMapPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
			if (nullptr == playerController)
			{
				return;
			}

			playerController->RequestSendChat(message);

			inputBox->SetText(FText::GetEmpty());

			// todo: focus to game.
		}

	}
};