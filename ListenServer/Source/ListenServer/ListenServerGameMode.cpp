// Copyright Epic Games, Inc. All Rights Reserved.

#include "ListenServerGameMode.h"
#include "ListenServerCharacter.h"
#include "UObject/ConstructorHelpers.h"

AListenServerGameMode::AListenServerGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/Actor/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}