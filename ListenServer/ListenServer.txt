[TODO: 나중에]
WidgetRef를 GameInstance에서 관리하니까, Map이동 했을때 IsShow 플래그가 초기화 안된다. -> UI 레퍼런스 유지하려면 레벨위젯이나 플레이어 컨트롤러처럼 맵이동시 없어지는곳에서 관리해야 한다.
확인팝업 추가 (정말로 나가시겠습니까?, OK 버튼에 function 연결)
SetState대신에 Ack를 주고 싶은데.. 함수 반환으로 되려나?
WaitingResponseWindow 20초 후에 닫을때는 eMultiplayState를 초기화 하고, Destroy Session하는게 어떻까?
WaitingResponseWindow에 취소 버튼이 있어서 직전 요청을 취소할 수 있었으면 좋겠다. (네트워크쪽 Async 로직을 완전히 제어할 수 있어야함...)
팝업 매니저에서 InputMode랑 ShowMouseCursor 제어 (팝업 단위고 관리하면 팝업이 겹쳐 떴다 하나 닫을때 오작동함)
서버 이름 설정? (중복은?) (~의 월드? 첫번째 플레이어 이름으로?)
=> Advanced Session Plugin 필요 (https://www.youtube.com/watch?v=rWs6SyyVpTE)
에디터에서는 맨처음 Authority 가졌던 유저는 IP로 Join이 실패한다. 실제 빌드해서 확인이 필요하다.
차이점 확인 Add to Viewport vs Add to player screen
HUD off 시트를 만든다?
개발전용 BP?
? Set Player Name
리스폰
PlayerState.h/uint8 bUseCustomPlayerNames : 1; => 이름을 바꿔본다.
(코드로 한다) 유저 목록 및 추방
1. 새로운 정보를 넣을껍니다. 이름을 순서대로 다시 매길껍니다.
2. 추방을 해본다.
* https://docs.unrealengine.com/4.27/en-US/API/Runtime/Engine/GameFramework/AGameSession/KickPlayer/
* https://answers.unrealengine.com/questions/163468/possible-to-kick-players-andor-set-player-limit.html
* https://unreal.gg-labs.com/wiki-archives/networking/how-to-use-sessions-in-c++
채팅에 유저 이름을 표시한다. 접속하면 이전 채팅도 받을 수 있게 수정. CustomEvent가 아니라 UIManager 같은것 타도록 수정. 엔터 누르면 채팅창 포커스
일시정지 팝업 -> 일시정지를 하면 모든 유저 정지하나?
- 다 함께 다른 Map으로 이동할수도 있네 (키워드: ServerTravel, https://docs.unrealengine.com/4.27/ko/InteractiveExperiences/Networking/Travelling/)
이미 세션 연결이 됬는지 확인하는 방법이 있었으면 좋겠다.
호스트가 나가서 방이 터질때 알림 팝업을 띄우고 싶다.
서버가 갑자기 종료됬을때 클라가 감지할 수 있는가?
클라가 갑자기 종료됬을때 서버가 감지할 수 있는가?
IP Address로도 Destroy Session이 되는가? 아니면 다른 방법을 사용해야 하는가?
관람자 추가 (채팅으로 분위기를 잡는역할을 할 수 있으면 좋겠다)
GameModeBase/PreLogin 에서 접속 거부를 해보자. (블랙리스트에 있으면 접속 거부 -> 대상자한테는 메시지가 갔으면 하는데..)

* 네트워크 에러 예외처리 참고
void UMyGameInstance::HandleNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString) {
     if (FailureType == ENetworkFailure::FailureReceived || FailureType == ENetworkFailure::ConnectionLost) {
         APlayerController* PlayerController = GetFirstLocalPlayerController();
         if (!ensure(PlayerController != nullptr)) return;
 
         PlayerController->ClientTravel("/Game/Maps/MainMenu", ETravelType::TRAVEL_Absolute);
     }
 }


[스펙 확인]
//서버가 Destroy Session 해도 연결된것처럼 작동한다. 맵 이동을 한다면 사라지는가?
//-> 로비/인게임 맵이 분리되니까, 서버가 Destroy Session하면 클라도 로비로 튕긴다. 정상 스펙.

//서버가 Destroy Session하면 클라는 튕기는데, 다시 Join이 가능한가?
//-> Find Session이 되지만 Join Session 되지 않는다. 클라에서 Destroy Session후에 다시 시도하니까 접속이 가능하다.
//-> 로비로 오면 Destroy Session을 시키자.

//서버만 Destroy Session 후에 Join을 하면 클라가 있는 방으로 다시 들어가는가? Find Session이 성공하지만 갯수가 0개다. 즉 이전에 생성했던 Session을 찾지 못한다. 정상 스펙.

//서버만 Destroy Session 후에 Host를 하면 어떻게 되는가?
//-> 정상적으로 세션이 다시 만들어진다. 그렇지만 클라는 Join Session에서 진행하지 못한다. 성공도 실패도 하지 않으므로 상태 관리가 반드시 필요하다.
//-> 클라가 Destroy Session 후에 다시 Join 시도하면 정상 작동한다.

//클라가 Destroy Session을 하면?
//- Join 전엔 실패한다.
//- Join 후엔 성공한다.. 클라는 단순히 방만 나가고 싶은데..
//- 서버가 Destroy Session을 한 이후, 클라도 Destroy Session이 가능하다. 따로 관리되는듯?

//클라만 Destroy Session 후에 다시 Join이 되는가? Find 갯수가 0개로 나온다. 재진입이 가능한 방법을 찾아야 한다.
//-> 로비와 인게임맵 두개를 이동하니까 재진입이 가능했다. 해결됨.

//Destroy Session 후 다시 Destroy Session시 실패하는가? 실패한다. 정상 스펙.

//서버와 클라 모두가 Destroy Session 후에 Join을 하면 실패하는가? Find까지 성공하지만 세션 갯수가 0개로 나온다. 정상 스펙.
//서버와 클라 모두가 Destroy Sessino 후에 Host를 하면 새로운 방이 만들어지는가? 새로운 방이 만들어지며 진입도 잘 된다.


[참고]
유튜브 / 리슨 서버 기본 설정
https://www.youtube.com/watch?v=wvbFdSIPCHA

공식 문서 / 온라인 세션 노드: https://docs.unrealengine.com/4.27/ko/ProgrammingAndScripting/Blueprints/UserGuide/OnlineNodes/


[에디터에서의 설정]
Play / Multiplayer Options / Number of Players: 2~
Play / Multiplayer Options / Net Mode: Play As Listen Server
(선택) Play / Modes / New Editoer Window
(선택) Play / Advanced Settings  / Multiplayer Options / Server / Server Port: 7777


[참고]
To start as a listen server you need
-> MapName?Listen

To start as a dedicated server you need
-> MapName -server

To start as a client you need
-> ServerIP

액터 설정
스폰되는 액터 클래스는 리플리케이트 설정이 되어있어야 한다.
- Class Defaults / Replication / Replicate Movement, Replicates: 체크	// 참고: SetReplicates() // Replicate Movement
- 서버에 스폰되면 클라에도 스폰된다.

서버인지 확인하는 방법
- Switch Has Authority 또는 Has Autority

변수 설정
1. Details / Variable / Replication: Replicated 또는 RepNotify
- Replicated: 실시간으로 값을 반영합니다.
- RepNotify: 변수가 바뀌면 노티파이 이벤트가 클라이언트와 서버 양쪽에서 호출됩니다. BP에서는 OnRep_(VariableName) 함수가 자동으로 추가됩니다.
2. 서버에서 변경되면 클라이언트에 반영됩니다.

커스텀 이벤트 설정
Details / Graph / Replicates: Multicast 또는 Run on Server 또는 Run on owning Client
- Multicast: Replicate this event from the server to everyone else. Server executes this event lacally too. Only call this from the server.
- Run on Server: Replicate this event from net owning client to server.
- Run on owning Client: Repliate this event from the server to owning client.
* Reliable: 체크하면 반영 확인까지 되는것 같다. 이펙트 같은건 설정 꺼버리자.

GameInstance 이벤트
- Event NetworkError: Opportunity for blueprints to handle network errors.
- Event TravelError: Opportunity for blueprints to handle travel errors.
- Event Init: Opportunity for blueprints to handle the game instance being initialized.
- Event Shutdown: Opportunity for blueprints to handle the game instance being shutdown.

GameMode
- ex) 게임의 규칙이나 승리 조건
- 맵에 종속된다. UGameEngine::LoadMap() 에서 맵을 초기화 시킬때 인스턴스가 생성된다.
- 맵 마다 게임 모드를 다르게 사용하는것도 좋은 방법이 된다.
- GameMode를 사용할때는 GameState를, GameModeBase를 사용할때는 GameStateBase를 짝 맞춰서 사용하자. (GameMode에서 MatchStateMachine 를 따로 관리한다. 요 상태머신은 GameState와 연계되도록 구현되어 있다.)
- 게임 모드는 서버에만 존재한다. 클라에는 리플리케이트되지 않는다.
* AGameMode/EnteringMap, WaitingToStart, InProgress, WaitingPostMatch, LeavingMap, Aborted 참고해서 GameModeBase에 재구현 해보자.

GameState
- ex) 접속된 플레이어 목록, 점수, 체크 게임에서 말들의 위치, 오픈 월드 게임에서 완료한 퀘스트 목록
- 게임 모드에서 관리하는 값중에서 클라이언트들이 다같이 확인해야되는 정보를 관리할 수 있게 구현해야 한다.
- PlayerState의 목록을 들고있다. GameState중에서 개별 유저로 구분해서 관리하고싶은 값은 PlayerState에 갱신하면 된다.
- 게임 스테이트는 서버에 존재하며, 수정 내용이 클라들에게 리플리케이트된다.

PlayerState
- ex) 플레이어 이름, 점수, MOBA 게임류에서의 대전상대 레벨, CTF 게임에서 플레이어가 현재 깃발을 운반중인지 여부, 팀에 속한 유저의 개인 점수
- 동기화 상태 유지를 위해 자유로이 리플리케이트 가능
- AI 에는 PlayerState 가 없습니다.
* PlayerController에서 플레이어 정보를 얻는 것은 올바르지 않은 접근 방식이라고 볼 수 있으며, GameState를 통하여 PlayerState를 알아내어 얻는 것이 바른 방법이라고 한다.





