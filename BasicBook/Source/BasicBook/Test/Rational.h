﻿#pragma once

#include "../Jipsa/Log.h"

struct ZeroBottomException
{
	int32 top, bottom;
	ZeroBottomException(const int32& _top, const int32& _bottom) : top(_top), bottom(_bottom) {}
};

struct NegativeBottomException
{
	int32 top, bottom;
	NegativeBottomException(const int32& _top, const int32& _bottom) : top(_top), bottom(_bottom) {}
};

class Rational
{
private:
	int32 top;
	int32 bottom;

public:
	Rational(const int32& _top = 0, const int32& _bottom = 1) : top(_top), bottom(_bottom) {}

	// ex) ++(++r)
	Rational& operator++()
	{
		top += bottom;
		return *this;
	}

	// ex) r++
	Rational operator++(int32 temp)
	{
		top += bottom;
		return Rational(top - bottom, bottom);
	}

	// ex) (float)r
	operator float() const
	{
		if (0 == bottom)
		{
			PRINT_WARNING("분모가 0 입니다. 계산할 수 없습니다. %d / %d", top, bottom);
			return 0.0f;
		}

		return static_cast<float>(top) / bottom;
	}
	float GetRealValue(bool& isError) const
	{
		if (0 == bottom)
		{
			PRINT_WARNING("분모가 0 입니다. 계산할 수 없습니다. top=%d, bottom=%d", top, bottom);

			isError = true;
			return 0.0f;
		}

		isError = false;
		return static_cast<float>(top) / bottom;
	}
	float GetRealValue() const
	{
		// 어썰트 참고. AssertionMacros.h
		// 디파인 참고. Build.h 에서 DO_GUARD_SLOW, DO_CHECK, DO_ENSURE 확인.
		// 1. DO_GUARD_SLOW		If true, then checkSlow, checkfSlowand verifySlow are compiled into the executable.
		// 2. DO_CHECK			If true, then checkCode, checkf, verify, check, checkNoEntry, checkNoReentry, checkNoRecursion, verifyf, checkf are compiled into the executables
		// 3. DO_ENSURE			If true, then ensure, ensureAlways, ensureMsgfand ensureAlwaysMsgf are compiled into the executables
		// 4. STATS				If true, then the stats system is compiled into the executable.
		// 5. ALLOW_DEBUG_FILES	If true, then debug files like screen shotsand profiles can be saved from the executable.
		// 6. NO_LOGGING		If true, then no logs or text output will be produced

		// o, 콜스택 지원해줌. 합격.
		//if (true == ensureMsgf(0 != bottom, TEXT("분모가 0 입니다. 계산할 수 없습니다. top=%d, bottom=%d"), top, bottom))
		//{
		//	return 0.0f;
		//}

		// o, 콜스택 따로 표시해야 하니까 탈락.
		//checkCode(
		//	if (0 == bottom)
		//	{
		//		PRINT_WARNING("분모가 0 입니다. 계산할 수 없습니다.(2) top=%d, bottom=%d", top, bottom);
		//		return 0.0f;
		//	}
		//);

		// o, 콜스택 지원. 그치만 에디터 종료하니까 개발용으론 탈락.
		//verifyf(0 != bottom, TEXT("분모가 0 입니다. 계산할 수 없습니다.(3) top=%d, bottom=%d"), top, bottom);

		// o, 콜스택 지원. 그치만 에디터 종료하니까 개발용으론 탈락.
		//checkf(0 != bottom, TEXT("분모가 0 입니다. 계산할 수 없습니다.(4) top=%d, bottom=%d"), top, bottom);

		// x
		//checkfSlow(0 != bottom, TEXT("분모가 0 입니다. 계산할 수 없습니다.(5) top=%d, bottom=%d"), top, bottom);

		// o, 그 함수 안에서 처리할 수 있다면 예외 처리 기법을 사용하지 않는 것이 좋다.
		//try
		//{
		//	if (0 == bottom)
		//	{
		//		throw('Z');
		//	}
		//	else if (bottom < 0)
		//	{
		//		//throw(bottom);
		//		//throw(NegativeBottomException(top, bottom));
		//	}
		//}
		//catch (char c)
		//{
		//	PRINT_WARNING("분모가 0 입니다. 계산할 수 없습니다.(6) error=%c, top=%d, bottom=%d", c, top, bottom);
		//	return 0.0f;
		//}
		////catch (int32 b)
		////{
		////	PRINT_WARNING("분모가 음수 입니다. 예외처리 테스트 중입니다. bottom=%d", b);
		////}
		////catch (NegativeBottomException e)
		////{
		////	PRINT_WARNING("분모가 음수 입니다. 예외처리 테스트 중입니다. bottom=%d", e.bottom);
		////}
		//catch (...)
		//{
		//	PRINT_WARNING("알 수 없는 문제가 발생했습니다. 계산할 수 없습니다. top=%d, bottom=%d", top, bottom);
		//	return 0.0f;
		//}

		// o, 예외의 전달. 함수의 호출자마다 예외를 처리하는 방법이 다르다면 최대한 예외 처리를 이용하는 것이 좋다.
		if (0 == bottom)
		{
			//throw(ZeroBottomException(top, bottom));
			return -1.0f;
		}

		return static_cast<float>(top) / bottom;
	}

	// ex) int32 top = r[0];
	// ex) r[0] = 1; r[1] = 2; (참조형이 리턴됬기 때문에 외부에서 초기화 할 수 있다)
	int32& operator[](const int32& index)
	{
		if (0 == index)
		{
			return top;
		}
		else
		{
			return bottom;
		}
	}

	// ex) int32 top = r(0);
	// ex) r(0) = 1; r(1) = 2;
	int32& operator()(const int32& index)
	{
		return (*this)[index];
	}
};