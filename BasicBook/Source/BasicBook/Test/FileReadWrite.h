﻿#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

#include "../Jipsa/Log.h"

// NOTE: 작동하는것 처럼 보이지만 '사용불가'.
// 언리얼에서는 파일 내용 변경이 안됬음.
// Test01.txt에 값을 직접 넣고 read를 해도 값이 다르게 나옴. 이전 테스트에서 코드로 write한 내용이 읽혔음. 가상의 저장소에서 실행되는것처럼 동작함;;
class FileReadWrite
{
public:
	void Test01()
	{
		//FILE* fp;
		//int a[10], b[10];

		//for (int i = 0; i < 10; ++i)
		//{
		//	a[i] = rand();
		//}

		//fp = fopen("Test01.txt", "w");
		//if (NULL != fp)
		//{
		//	FPrintArray(a, 10, fp);
		//	fclose(fp);

		//	PRINT("Success write");
		//}
		//else
		//{
		//	PRINT("Fail write");
		//}

		//fp = fopen("Test01.txt", "r");
		//if (NULL != fp)
		//{
		//	for (int i = 0; i < 10; ++i)
		//	{
		//		fscanf(fp, "%d", &(b[i]));
		//		PRINT("%d", b[i]);
		//	}

		//	fclose(fp);

		//	PRINT("Success read");
		//}
		//else
		//{
		//	PRINT("Fail read");
		//}
	}

	void Test01_V2()
	{
		//FILE* fp;
		//errno_t err;

		//err = fopen_s(&fp, "Test01.txt", "w");
		//if (0 == err)
		//{
		//	int result = fprintf(fp, "%d ", 1111);
		//	fclose(fp);

		//	PRINT("Success write: %d byte", result);
		//}
		//else
		//{
		//	PRINT("Fail write");
		//}

		//err = fopen_s(&fp, "Test01.txt", "r");
		//if (0 == err)
		//{
		//	int temp;
		//	fscanf_s(fp, "%d", &temp);
		//	fclose(fp);

		//	PRINT("Success read: %d", temp);
		//}
		//else
		//{
		//	PRINT("Fail read");
		//}
	}

	void Test01_V3()
	{
		std::ofstream outputFileStream("Test01.txt");
		if (outputFileStream)
		{
			int x = 1111;
			int y = 2222;
			outputFileStream << x << " " << y << endl;
			outputFileStream << "Game Over !\n\n";
			outputFileStream.close();

			PRINT("Success write");
		}
		else
		{
			PRINT("Fail write");
		}

		std::ifstream inputFileStream("Test01.txt");
		if (inputFileStream)
		{
			int x;
			int y;
			char s[100];
			inputFileStream >> x >> y;
			inputFileStream >> s; // NOTE: 공백 문자 앞까지만 읽힌다. "Game" 이 얻어진다. getline()을 사용해야 한다.
			inputFileStream.close();

			PRINT("Success read: %d %d %s", x, y, UTF8_TO_TCHAR(s));
		}
		else
		{
			PRINT("Fail read");
		}
	}

protected:
	void FPrintArray(int a[], int len, FILE* fp)
	{
		for (int i = 0; i < len; ++i)
		{
			fprintf(fp, "%d ", a[i]);
		}
		fprintf(fp, "\n");
	}
};