﻿#pragma once

#include "../Jipsa/Log.h"

class ComplexV2
{
private:
	float real;
	float imaginary;

public:
	ComplexV2(const float& _real = 0.0f, const float& _imaginary = 0.0f) : real(_real), imaginary(_imaginary) {}

	void Set(const float& _real, const float& _imaginary)
	{
		real = _real;
		imaginary = _imaginary;
	}
	void Reset()
	{
		real = imaginary = 0.0f;
	}
	void SetRandom()
	{
		real = FMath::FRandRange(0.0f, 10.0f);
		imaginary = FMath::FRandRange(0.0f, 100.0f);
	}
	void Print(const char* msg = "복소수")
	{
		PRINT("%s %4.2f + %4.2fi", UTF8_TO_TCHAR(msg), real, imaginary);
	}

	// 멤버 함수. (추천.)
	void Add(const ComplexV2& a, const ComplexV2& b)
	{
		real = a.real + b.real;
		imaginary = a.imaginary + b.imaginary;
	}
	// 멤버 함수. (비추천. '반환형'이 있기 때문에 반드시 '복사 생성자'가 호출된다. 또한 실제로 값을 이용하려면 '대입 연산자'도 사용할 필요가 생긴다.)
	ComplexV2 Add(const ComplexV2& b)
	{
		return ComplexV2(real + b.real, imaginary + b.imaginary);
	}
	// 프랜드 함수. (프랜드 선언을 하면 '일반 함수'가 된다. 일반 함수지만, 프랜드 선언됬기 때문에 private로 선언한 real과 imaginary 에 접근이 가능하다.)
	friend ComplexV2 Add(const ComplexV2& a, const ComplexV2& b)
	{
		return ComplexV2(a.real + b.real, a.imaginary + b.imaginary);
	}
	// 멤버 함수 + 연산자 오버로딩. (ComplexV2 Add(const ComplexV2& b) 와 구현이 동일하다)
	ComplexV2 operator+(const ComplexV2& b)
	{
		return ComplexV2(real + b.real, imaginary + b.imaginary);
	}
	// 프랜드 함수 + 연산자 오버로딩. (friend ComplexV2 Add(const ComplexV2& a, const ComplexV2& b) 와 구현이 동일하다)
	friend ComplexV2 operator+(const ComplexV2& a, const ComplexV2& b)
	{
		return ComplexV2(a.real + b.real, a.imaginary + b.imaginary);
	}

	// 멤버 함수 + 연산자 오버로딩. (이항 연산자이다)
	ComplexV2 operator-(const ComplexV2& b)
	{
		return ComplexV2(real - b.real, imaginary - b.imaginary);
	}
	// 멤버 함수 + 연산자 오버로딩. (단항 연산자이다)
	ComplexV2 operator-()
	{
		return ComplexV2(-real, -imaginary);
	}

	// 멤버 함수 + 연산자 오버로딩.
	ComplexV2 operator*(const float& b)
	{
		return ComplexV2(real * b, imaginary * b);
	}
	// 멤버 함수로는 왼쪽 피연산자가 float이 되는 연산자 오버로딩을 할 수 없다.
	//ComplexV2 operator*(const ComplexV2& b)
	//{
	//	return ComplexV2(a * b.real, a * b.imaginary);
	//}
	// 프랜드 함수 + 연산자 오버로딩.
	friend ComplexV2 operator*(const ComplexV2& a, const float& b)
	{
		return ComplexV2(a.real * b, a.imaginary * b);
	}
	// 프랜드 함수 + 연산자 오버로딩. (피연산자의 순서를 자유롭게 하려면 이처럼 모든 경우의 수를 오버로딩 하면된다)
	friend ComplexV2 operator*(const float& a, const ComplexV2& b)
	{
		return ComplexV2(a * b.real, a * b.imaginary);
	}

	// 반대되는 연산자 또는 유사한 연산자라고 생각되지만 모두 오버로딩 해야한다.
	bool operator==(const ComplexV2& b)
	{
		return ((real == b.real) && (imaginary == b.imaginary));
	}
	bool operator!=(const ComplexV2& b)
	{
		return ((real != b.real) || (imaginary != b.imaginary));
	}

	// 대입 연산자 오버로딩. (멤버 함수로만 구현이 가능하다)
	ComplexV2& operator=(const ComplexV2& b)
	{
		real = b.real;
		imaginary = b.imaginary;
		return *this;
	}
	// 멤버 함수로는 대입 연산자 오버로딩을 할 수 없다. (컴파일 에러 발생함 -> 'operator =' has too many parameters)
	//void operator=(const ComplexV2& a, const ComplexV2& b)
	//{
	//	a.real = b.real;
	//	a.imaginary = b.imaginary;
	//}
	ComplexV2& operator+=(const ComplexV2& b)
	{
		real += b.real;
		imaginary += b.imaginary;
		return *this;
	}
	ComplexV2& operator-=(const ComplexV2& b)
	{
		real -= b.real;
		imaginary -= b.imaginary;
		return *this;
	}

	float GetMagnitude()
	{
		return sqrt(real * real + imaginary * imaginary);
	}
	operator float() const
	{
		return sqrt(real * real + imaginary * imaginary);
	}
};