﻿#pragma once

#include "Shapes.h"

class GraphicEditor
{
	// TODO: 게임 내에서 사생 대회가 열려서 유저가 직접 그린 그림으로 참가하면 재밌겠다.
	// 도트 그림을 그릴 수 있도록 지원해보자.
	// 기본 도형을 드래그해서 그릴 수 있게 하자.
	// 마우스 드래그로 도트를 찍을 수 있게 하자.
	// 색칠을 할 수 있게 하자.
	// 색깔을 고를 수 있게 하자.

public:
	void Test7()
	{
		PointV3<int32> arr[5];
		for (int32 i = 0; i < 5; ++i)
		{
			arr[i] = PointV3<int32>(FMath::RandRange(0, 9), FMath::RandRange(0, 9));
		}

		PointV3<float> avg = GetAverage(arr, 5);
		avg.PrintFloatValue();
	}

	void Test6()
	{
		// 무게중심 구하기.
		int32 image[6][6] = {
			 { 0, 0, 0, 0, 0, 0 }
			,{ 0, 0, 1, 1, 1, 0 }
			,{ 0, 0, 1, 1, 1, 0 }
			,{ 0, 0, 1, 1, 0, 0 }
			,{ 0, 0, 1, 1, 0, 0 }
			,{ 0, 0, 0, 0, 0, 0 }
		};

		PointV3<float> sumOfPixel(0.0f, 0.0f);
		int pixelCount = 0;

		for (int32 y = 0; y < 6; ++y)
		{
			for (int32 x = 0; x < 6; ++x)
			{
				if (1 == image[y][x])
				{
					PointV3<float> pixel(static_cast<float>(x), static_cast<float>(y));
					sumOfPixel.Add(pixel);
					++pixelCount;
				}
			}
		}

		PointV3<float> centerOfPixel = (1.0f / pixelCount) * sumOfPixel;
		centerOfPixel.PrintFloatValue();
	}

	void Test5()
	{
		// 클래스 템플릿 테스트.
		PointV3<int32> p1(1, 2), p2(3, 4);
		PointV3<float> p3 = 0.5f * p2;
		p1.PrintIntegerValue();
		p2.PrintIntegerValue();
		p3.PrintFloatValue();
		
		float magnitude = p2.GetMagnitude();
		PRINT("%f", magnitude);

		PointV3<float> q1(5.0f, 6.0f), q2(7.0f, 8.0f);
		PointV3<float> q3 = AddPoint(q1, q2);
		q1.PrintFloatValue();
		q2.PrintFloatValue();
		q3.PrintFloatValue();
	}

	void Test4()
	{
		// 프랜드 함수 테스트.
		PointV2 pointList[5] = { PointV2(0, 1), PointV2(2, 3), PointV2(4, 5) };
		pointList[3] = AddPoint(pointList[0], pointList[1]);
		pointList[4] = SubPoint(pointList[2], pointList[1]);
		for (int32 i = 0; i < 5; ++i)
		{
			pointList[i].Print();
		}

		float avg = AvgPointX(pointList, 5);
		PRINT("Average X: %f", avg);
	}

	void Test3()
	{
		// 도형들을 관리할 변수.
		ShapeV2* shapes[100];
		int32 shapeCount = 0;

		// 캔버스 생성.
		int32 width = 25, height = 15;
		MonsterWorldCanvas canvas(width, height);
		canvas.Print("[GraphicEditor] (1)");

		// 도형들을 동적으로 생성.
		int32 x, y, x2, y2, w, h;

		x = FMath::RandRange(0, width - 1);
		y = FMath::RandRange(0, height - 1);
		x2 = FMath::RandRange(0, width - 1);
		y2 = FMath::RandRange(0, height - 1);
		shapes[shapeCount++] = new LineV2(x, y, x2, y2);

		x = FMath::RandRange(0, width - 1);
		y = FMath::RandRange(0, height - 1);
		w = FMath::RandRange(2, 5);
		shapes[shapeCount++] = new HLineV2(x, y, w);

		x = FMath::RandRange(0, width - 1);
		y = FMath::RandRange(0, height - 1);
		w = FMath::RandRange(2, 5);
		shapes[shapeCount++] = new VLineV2(x, y, w);

		x = FMath::RandRange(0, width - 1);
		y = FMath::RandRange(0, height - 1);
		w = FMath::RandRange(2, 5);
		h = FMath::RandRange(2, 5);
		shapes[shapeCount++] = new RectangleV2(x, y, w, h);

		for (int32 i = 0; i < 3; ++i)
		{
			x = FMath::RandRange(0, width - 1);
			y = FMath::RandRange(0, height - 1);
			w = FMath::RandRange(2, 5);
			shapes[shapeCount++] = new SquareV2(x, y, w);
		}

		x = FMath::RandRange(0, width - 1);
		y = FMath::RandRange(0, height - 1);
		w = FMath::RandRange(2, 5);
		shapes[shapeCount++] = new CircleV2(x, y, w);

		// 도형들을 캔버스에 갱신.
		canvas.Clear();
		for (int32 i = 0; i < shapeCount; ++i)
		{
			shapes[i]->Draw(canvas);
		}

		// 캔버스를 페인트.
		canvas.Print("[GraphicEditor] (2)");

		// 도형들을 동적으로 해제.
		//delete[] lines;
		for (int32 i = 0; i < shapeCount; ++i)
		{
			delete shapes[i];
		}
	}

	void Test2()
	{
		// 도형들을 관리할 변수.
		LineV2* lines[100];
		HLineV2* hLines[100];
		VLineV2* vLines[100];
		RectangleV2* rectangles[100];
		SquareV2* squares[100];
		CircleV2* circles[100];
		int32 lineCount = 0, hLineCount = 0, vLineCount = 0;
		int32 rectangleCount = 0, squareCount = 0, circleCount = 0;

		// 캔버스 생성.
		int32 width = 25, height = 15;
		MonsterWorldCanvas canvas(width, height);
		canvas.Print("[GraphicEditor] (1)");

		// 도형들을 동적으로 생성.
		int32 x, y, x2, y2, w, h;
		
		x = FMath::RandRange(0, width - 1);
		y = FMath::RandRange(0, height - 1);
		x2 = FMath::RandRange(0, width - 1);
		y2 = FMath::RandRange(0, height - 1);
		lines[lineCount++] = new LineV2(x, y, x2, y2);

		x = FMath::RandRange(0, width - 1);
		y = FMath::RandRange(0, height - 1);
		w = FMath::RandRange(2, 5);
		hLines[hLineCount++] = new HLineV2(x, y, w);

		x = FMath::RandRange(0, width - 1);
		y = FMath::RandRange(0, height - 1);
		w = FMath::RandRange(2, 5);
		vLines[vLineCount++] = new VLineV2(x, y, w);

		x = FMath::RandRange(0, width - 1);
		y = FMath::RandRange(0, height - 1);
		w = FMath::RandRange(2, 5);
		h = FMath::RandRange(2, 5);
		rectangles[rectangleCount++] = new RectangleV2(x, y, w, h);

		for (int32 i = 0; i < 3; ++i)
		{
			x = FMath::RandRange(0, width - 1);
			y = FMath::RandRange(0, height - 1);
			w = FMath::RandRange(2, 5);
			squares[squareCount++] = new SquareV2(x, y, w);
		}

		x = FMath::RandRange(0, width - 1);
		y = FMath::RandRange(0, height - 1);
		w = FMath::RandRange(2, 5);
		circles[circleCount++] = new CircleV2(x, y, w);

		// 도형들을 캔버스에 갱신.
		canvas.Clear();

		for (int32 i = 0; i < lineCount; ++i)
		{
			lines[i]->Draw(canvas);
		}

		for (int32 i = 0; i < hLineCount; ++i)
		{
			hLines[i]->Draw(canvas);
		}

		for (int32 i = 0; i < vLineCount; ++i)
		{
			vLines[i]->Draw(canvas);
		}

		for (int32 i = 0; i < rectangleCount; ++i)
		{
			rectangles[i]->Draw(canvas);
		}

		for (int32 i = 0; i < squareCount; ++i)
		{
			squares[i]->Draw(canvas);
		}

		for (int32 i = 0; i < circleCount; ++i)
		{
			circles[i]->Draw(canvas);
		}

		// 캔버스를 페인트.
		canvas.Print("[GraphicEditor] (2)");

		// 도형들을 동적으로 해제.
		//delete[] lines;
		for (int32 i = 0; i < lineCount; ++i)
		{
			delete lines[i];
		}

		for (int32 i = 0; i < hLineCount; ++i)
		{
			delete hLines[i];
		}

		for (int32 i = 0; i < vLineCount; ++i)
		{
			delete vLines[i];
		}

		for (int32 i = 0; i < rectangleCount; ++i)
		{
			delete rectangles[i];
		}

		for (int32 i = 0; i < squareCount; ++i)
		{
			delete squares[i];
		}

		for (int32 i = 0; i < circleCount; ++i)
		{
			delete circles[i];
		}
	}

	void Test()
	{
		// 각 도형을 그린다.
		LineV2 line(2, 2, 8, 4);
		RectangleV2 rectangle(2, 9, 6, 3);
		HLineV2 hLine(1, 7, 23);
		VLineV2 vLine(12, 1, 13);
		SquareV2 square(17, 1, 4);
		CircleV2 circle(19, 11, 2);

		MonsterWorldCanvas canvas(25, 15);
		line.Draw(canvas);
		rectangle.Draw(canvas);
		hLine.Draw(canvas);
		vLine.Draw(canvas);
		square.Draw(canvas);
		circle.Draw(canvas);
		canvas.Print("[GraphicEditor] (1)");

		// Line을 우하단으로, Circle을 좌상단으로 이동 + 그린다.
		line.Move(1, 1);
		circle.Move(-1, -1);

		canvas.Clear();
		line.Draw(canvas);
		rectangle.Draw(canvas);
		hLine.Draw(canvas);
		vLine.Draw(canvas);
		square.Draw(canvas);
		circle.Draw(canvas);
		canvas.Print("[GraphicEditor] (2)");
	}
};