﻿#pragma once

template <typename T1 = int32, typename T2 = int32>
class Pair
{
private:
	T1 data1;
	T2 data2;

public:
	Pair() {}

	void Set(const T1& _data1, const T2& _data2)
	{
		data1 = _data1;
		data2 = _data2;
	}

	T1 GetData1()
	{
		return data1;
	}

	T2 GetData2()
	{
		return data2;
	}
};

// 템플릿도 상속할 수 있다.
class WordPair : public Pair<string, string>
{
};