#pragma once

#include "../Jipsa/Singleton.h"

class TestSingleton_2 : public Singleton<TestSingleton_2> {
private:
	int value;

public:
	TestSingleton_2() : value(2) {}

	int GetValue() {
		return value;
	}

	void SetValue(int _value) {
		value = _value;
	}
};