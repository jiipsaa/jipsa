#pragma once

#define LABELLING_WIDTH 17
#define LABELLING_HEIGHT 5

class ComponentLabelling
{
public:
	void PrintImage(unsigned char img[LABELLING_HEIGHT][LABELLING_WIDTH], char* msg);
	void BlobColoring(unsigned char img[LABELLING_HEIGHT][LABELLING_WIDTH]);

private:
	void Label(unsigned char img[LABELLING_HEIGHT][LABELLING_WIDTH], int x, int y, int color);
};