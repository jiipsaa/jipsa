﻿#pragma once

#include "../MiniGame/MonsterWorldCanvas.h"

inline int32 Abs(int32 x)
{
	return ((0 < x) ? x : -x);
}

inline int32 Max(int32 x, int32 y)
{
	return ((y < x) ? x : y);
}

inline int32 Round(float x)
{
	return static_cast<int32>(x + 0.5f);
}

template <typename T>
class PointV3
{
	// 프랜드 클래스 테스트 위해 private로 변경.
	// public:
private:
	T x, y;

	// 프랜드 클래스 선언. 접근 지정자에 관계없이 멤버에 접근 가능해진다.
	friend class ShapeV2;

	// 프랜드 선언은 상속되지 않는다. 자식 클래스도 프랜드 선언이 필요하다.
	friend class LineV2;
	friend class RectangleV2;
	friend class CircleV2;

public:
	PointV3(const T& _x = 0, const T& _y = 0) : x(_x), y(_y) {}

	// TODO: PRINT 는 템플릿에 사용할 수 없다. 
	void PrintIntegerValue()
	{
		PRINT("PointV3: (%d, %d)", x, y);
	}
	void PrintFloatValue()
	{
		PRINT("PointV3: (%f, %f)", x, y);
	}

	void Add(const PointV3& q)
	{
		x += q.x;
		y += q.y;
	}

	void Move(const T& dX, const T& dY)
	{
		x += dX;
		y += dY;
	}

	// 프랜드 함수 1. 멤버 함수가 아닌 일반 함수가 된다.
	friend float AvgPointX(const PointV3* pointList, T listCount);

	// 프랜드 함수 2.멤버 함수가 아닌 일반 함수가 된다.
	friend PointV3 AddPoint(const PointV3& p, const PointV3& q)
	{
		PointV3 r;
		r.x = p.x + q.x;
		r.y = p.y + q.y;
		return r;
	}

	// 프랜드 함수 3. 멤버 함수가 아닌 일반 함수가 된다.
	friend PointV3 SubPoint(const PointV3& p, const PointV3& q)
	{
		return PointV3(p.x - q.x, p.y - q.y);
	}

	float GetMagnitude()
	{
		return sqrt(static_cast<float>((x * x) + (y * y)));
	}

	// 항상 PointV3<float> 형으로 결과가 나오도록 제한 할 수 있다.
	friend PointV3<float> operator*(const float& a, const PointV3& b)
	{
		return PointV3<float>(a * b.x, a * b.y);
	}
};

template <typename T>
float AvgPointX(const PointV3<T>* pointList, T listCount)
{
	float sum = 0;
	for (T i = 0; i < listCount; ++i)
	{
		sum += pointList[i].x;
	}
	return (sum / listCount);
}

PointV3<float> GetAverage(PointV3<int32> arr[], int32 len)
{
	PointV3<int32> sumOfPoint(0, 0);
	for (int32 i = 0; i < len; ++i)
	{
		sumOfPoint.Add(arr[i]);
	}
	return (1.0f / len) * sumOfPoint;
}

class PointV2
{
// 프랜드 클래스 테스트 위해 private로 변경.
// public:
private:
	int32 x, y;

	// 프랜드 클래스 선언. 접근 지정자에 관계없이 멤버에 접근 가능해진다.
	friend class ShapeV2;

	// 프랜드 선언은 상속되지 않는다. 자식 클래스도 프랜드 선언이 필요하다.
	friend class LineV2;
	friend class RectangleV2;
	friend class CircleV2;

public:
	PointV2(const int32& _x = 0, const int32& _y = 0) : x(_x), y(_y) {}

	void Print()
	{
		PRINT("PointV2: (%d, %d)", x, y);
	}

	void Move(const int32& dX, const int32& dY)
	{
		x += dX;
		y += dY;
	}

	// 프랜드 함수 1. 멤버 함수가 아닌 일반 함수가 된다.
	friend float AvgPointX(const PointV2* pointList, int32 listCount);

	// 프랜드 함수 2.멤버 함수가 아닌 일반 함수가 된다.
	friend PointV2 AddPoint(const PointV2& p, const PointV2& q)
	{
		PointV2 r;
		r.x = p.x + q.x;
		r.y = p.y + q.y;
		return r;
	}

	// 프랜드 함수 3. 멤버 함수가 아닌 일반 함수가 된다.
	friend PointV2 SubPoint(const PointV2& p, const PointV2& q)
	{
		return PointV2(p.x - q.x, p.y - q.y);
	}

	float GetMagnitude()
	{
		return sqrt(static_cast<float>((x * x) + (y * y)));
	}

	friend PointV2 operator*(const float& a, const PointV2& b)
	{
		return PointV2(static_cast<int32>(a * b.x), static_cast<int32>(a * b.y));
	}
};

// 프랜드 함수 1. 멤버 함수가 아닌 일반 함수가 된다.
float AvgPointX(const PointV2* pointList, int32 listCount)
{
	float sum = 0;
	for (int32 i = 0; i < listCount; ++i)
	{
		sum += pointList[i].x;
	}
	return (sum / listCount);
}

class ShapeV2
{
protected:
	PointV2 point;
	char color;

public:
	ShapeV2(const int32& _x = 0, const int32& _y = 0, const char& _color = 'S')
		: point(_x, _y), color(_color) {}

	// NOTE: 자식 클래스에 동적 할당된 멤버가 추가된다면 부모 클래스의 소멸자는 반드시 가상 함수로 선언해야 한다.
	// 당장 필요 없을지 모르지만, 다른 작업자가 자식 클래스에 동적 할당을 사용할 수 있다.
	// 아무리 자식 클래스에 소멸자를 잘 구현해놔도 호출 자체가 안되는 문제가 생기는 것이다.
	// 한편, 부모 클래스에 가상 함수가 하나도 없다면, 위의 문제가 발생할 확률이 현저히 적어진다.
	// 하지만, 다른 작업자가 부모 클래스에 가상 함수를 추가할 수 있다. 간과해서는 안된다.
	// 결론 -> 부모 클래스가 되야 한다면, 가능한한 소멸자를 가상 함수로 선언하는 것이 안전하다. (트레이드오프: 안전성 vs 메모리)
	virtual ~ShapeV2() {}

	// 순수 가상 함수(pure virtual function): 몸체 없이 virtual function을 선언한 함수. (이제 ShapeV2는 추상 클래스로 분류된다)
	virtual void Draw(MonsterWorldCanvas& canvas) = 0;

	virtual void Move(const int32& dX, const int32& dY)
	{
		point.Move(dX, dY);
	}
};

class LineV2 : public ShapeV2
{
protected:
	PointV2 endPoint;

public:
	LineV2(const int32& startX = 0, const int32& startY = 0, const int32& endX = 0, const int32& endY = 0, const char& color = 'L')
		: ShapeV2(startX, startY, color), endPoint(endX, endY) {}

	void Draw(MonsterWorldCanvas& canvas)
	{
		// NOTE: 두 점을 잇는 방법. 여기서는 DDA(Digital Differential Analyzer) 알고리즘을 사용함. Bresenham 알고리즘도 있다.
		int32 length = Max(Abs(endPoint.x - point.x), Abs(endPoint.y - point.y));
		float dX = static_cast<float>(endPoint.x - point.x) / length;
		float dY = static_cast<float>(endPoint.y - point.y) / length;
		//PRINT("%d, %f, %f", length, dX, dY);

		float x = static_cast<float>(point.x);
		float y = static_cast<float>(point.y);
		for (int32 i = 0; i <= length; ++i)
		{
			//PRINT("%f->%d, %f->%d", x, Round(x), y, Round(y));
			canvas.Draw(Round(x), Round(y), color);
			x += dX;
			y += dY;
		}
	}

	void Move(const int32& dX, const int32& dY)
	{
		ShapeV2::Move(dX, dY); //point.Move(dX, dY);
		endPoint.Move(dX, dY);
	}
};

class HLineV2 : public LineV2
{
public:
	HLineV2(const int32& startX = 0, const int32& startY = 0, const int32& length = 0, const char& color = 'H')
		: LineV2(startX, startY, startX + length, startY, color) {}
};

class VLineV2 : public LineV2
{
public:
	VLineV2(const int32& startX = 0, const int32& startY = 0, const int32& length = 0, const char& color = 'V')
		: LineV2(startX, startY, startX, startY + length, color) {}
};

class RectangleV2 : public ShapeV2
{
protected:
	int32 width, height;

public:
	RectangleV2(const int32& _x = 0, const int32& _y = 0, const int32& _width = 0, const int32& _height = 0, const char& _color = 'R')
		: ShapeV2(_x, _y, _color), width(_width), height(_height) {}

	void Draw(MonsterWorldCanvas& canvas)
	{
		// NOTE: 직사각형을 그리는 방법.
		int32 maxX = point.x + width;
		int32 maxY = point.y + height;
		for (int32 x = point.x; x <= maxX; ++x)
		{
			canvas.Draw(x, point.y, color);	// 윗변.
			canvas.Draw(x, maxY, color);	// 아랫변.
		}
		for (int32 y = point.y; y <= maxY; ++y)
		{
			canvas.Draw(point.x, y, color);	// 좌변.
			canvas.Draw(maxX, y, color);	// 우변.
		}

		// NOTE: LineV2 를 이용하는 방법.
		//LineV2(point.x, point.y, point.x + width, point.y, color).Draw(canvas);					// 윗변.
		//LineV2(point.x, point.y + height, point.x + width, point.y + height, color).Draw(canvas);	// 아랫변.
		//LineV2(point.x, point.y, point.x, point.y + height, color).Draw(canvas);					// 좌변.
		//LineV2(point.x + width, point.y, point.x + width, point.y + height, color).Draw(canvas);	// 우변.
	}
};

class SquareV2 : public RectangleV2
{
public:
	SquareV2(const int32& _x = 0, const int32& _y = 0, const int32& _width = 0, const char& _color = 'S')
		: RectangleV2(_x, _y, _width, _width, _color) {}
};

class CircleV2 : public ShapeV2
{
protected:
	int32 radius;

public:
	CircleV2(const int32& _x = 0, const int32& _y = 0, const int32& _radius = 0, const char& _color = 'C')
		: ShapeV2(_x, _y, _color), radius(_radius) {}

	void Draw(MonsterWorldCanvas& canvas)
	{
		// TODO: 원을 그리기 위해 Midpoint Circle 알고리즘을 사용해보자.
		LineV2(point.x, point.y, point.x, point.y + radius, color).Draw(canvas);
		LineV2(point.x, point.y, point.x, point.y - radius, color).Draw(canvas);
		LineV2(point.x, point.y, point.x + radius, point.y, color).Draw(canvas);
		LineV2(point.x, point.y, point.x - radius, point.y, color).Draw(canvas);
	}
};