﻿//#pragma once
//
//template <typename T = float>
//class VectorContainer
//{
//private:
//	int32 maxCapacity;
//	int32 curCapacity;
//	T* dynamicArray;
//
//public:
//	VectorContainer(const int32& capacity = 0) : maxCapacity(0), curCapacity(0), dynamicArray(nullptr)
//	{
//		Resize((4 < capacity) ? capacity : 4);
//		curCapacity = capacity;
//	}
//
//	VectorContainer(VectorContainer& originVector) : dynamicArray(nullptr)
//	{
//		Clone(originVector);
//	}
//
//	~VectorContainer()
//	{
//		if (nullptr != dynamicArray)
//		{
//			delete[] dynamicArray;
//		}
//	}
//
//	void Resize(const int32& newMaxCapacity)
//	{
//		if (newMaxCapacity <= maxCapacity)
//		{
//			return;
//		}
//
//		T* oldDynamicArray = dynamicArray;
//
//		maxCapacity = newMaxCapacity;
//		dynamicArray = new T[maxCapacity];
//
//		for (int32 i = 0; i < curCapacity; ++i)
//		{
//			dynamicArray[i] = oldDynamicArray[i];
//		}
//
//		delete[] oldDynamicArray;
//	}
//
//	VectorContainer& Clone(VectorContainer& originVector)
//	{
//		if (nullptr != dynamicArray)
//		{
//			delete[] dynamicArray;
//		}
//
//		maxCapacity = originVector.maxCapacity;
//		dynamicArray = new T[maxCapacity];
//
//		curCapacity = originVector.curCapacity;
//		for (int32 i = 0; i < curCapacity; ++i)
//		{
//			dynamicArray[i] = originVector[i];
//		}
//
//		return *this;
//	}
//	VectorContainer& operator=(VectorContainer& originVector)
//	{
//		return Clone(originVector);
//	}
//
//	int32 GetMaxCapacity() const
//	{
//		return maxCapacity;
//	}
//	int32 GetCurCapacity() const
//	{
//		return curCapacity;
//	}
//	bool IsEmpty() const
//	{
//		return (0 == curCapacity);
//	}
//
//	void Clear()
//	{
//		curCapacity = 0;
//	}
//
//	T& At(const int32 index)
//	{
//		return dynamicArray[index];
//	}
//	T& operator[](const int32 index)
//	{
//		return dynamicArray[index];
//	}
//
//	void PopBack()
//	{
//		if (false == IsEmpty())
//		{
//			--curCapacity;
//		}
//	}
//
//	void PushBack(const T& value)
//	{
//		if (maxCapacity <= curCapacity)
//		{
//			Resize(maxCapacity * 2);
//		}
//
//		dynamicArray[curCapacity] = value;
//		++curCapacity;
//	}
//};