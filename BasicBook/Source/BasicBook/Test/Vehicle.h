﻿#pragma once

#include "../Jipsa/Log.h"

class Vehicle
{
public:
	int32 price;

protected:
	int32 speed;

private:
	int32 serial;

public:
	Vehicle(int32 _serial = 0) : price(0), speed(0), serial(_serial) {}

	void SpeedUp()
	{
		++speed;
	}
	void SpeedDown()
	{
		--speed;
	}

	void Print()
	{
		PRINT("[Vehicle] serial=%d, speed=%d, price=%d", serial, speed, price);
	}
};

class Car : public Vehicle
{
protected:
	int32 gear;

public:
	Car(int32 _gear = 0) : gear(_gear) {}

	void PushAccel()
	{
		speed += 5;
	}
	void PushBreak()
	{
		SpeedDown();
	}

	void SetGear(int32 _gear)
	{
		gear = _gear;
	}

	void PrintCarInfo()
	{
		PRINT("[Car] gear=%d, speed=%d, price=%d", gear, speed, price);
	}
};