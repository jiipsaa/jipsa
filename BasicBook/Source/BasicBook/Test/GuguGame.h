﻿#pragma once

#include "../Jipsa/Log.h"

class GuguGame
{
private:
	int from;
	int to;

public:
	GuguGame()
	{
		Set(1, 9);
	}

	~GuguGame() {}

	void Set(int _from, int _to)
	{
		from = _from;
		to = _to;
	}

	void Play(int dan)
	{
		PRINT("[구구단 %d 단]", dan);
		for (int i = from; i <= to; ++i)
		{
			PRINT("%2d x %2d = %2d", dan, i, (dan * i));
		}
	}
};