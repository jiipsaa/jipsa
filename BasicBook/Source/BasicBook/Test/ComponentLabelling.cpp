#include "ComponentLabelling.h"

#include "../Jipsa/Log.h"

void ComponentLabelling::PrintImage(unsigned char img[LABELLING_HEIGHT][LABELLING_WIDTH], char* msg)
{
	FString log;
	log.Appendf(TEXT("%s\n"), UTF8_TO_TCHAR(msg));
	for (int y = 0; y < LABELLING_HEIGHT; ++y)
	{
		for (int x = 0; x < LABELLING_WIDTH; ++x)
		{
			if (0 == img[y][x])
			{
				log.Append(".");
			}
			else
			{
				log.Appendf(TEXT("%d"), img[y][x]);
			}
		}
		log.Append("\n");
	}
	PRINT("%s", *log);
}

void ComponentLabelling::BlobColoring(unsigned char img[LABELLING_HEIGHT][LABELLING_WIDTH])
{
	int color = 1;
	for (int y = 0; y < LABELLING_HEIGHT; ++y)
	{
		for (int x = 0; x < LABELLING_WIDTH; ++x)
		{
			if (9 == img[y][x])
			{
				Label(img, x, y, color++);
			}
		}
	}
}

void ComponentLabelling::Label(unsigned char img[LABELLING_HEIGHT][LABELLING_WIDTH], int x, int y, int color)
{
	if ((x < 0)
		|| (y < 0)
		|| (LABELLING_WIDTH <= x)
		|| (LABELLING_HEIGHT <= y)
		|| (9 != img[y][x]))
	{
		return;
	}

	img[y][x] = color;
	Label(img, x - 1, y, color);
	Label(img, x, y - 1, color);
	Label(img, x + 1, y, color);
	Label(img, x, y + 1, color);
}