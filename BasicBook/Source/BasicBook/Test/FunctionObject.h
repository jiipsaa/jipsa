﻿#pragma once

// 함수 객체(Function Object): 함수 호출 연산자를 오버로딩한 클래스의 객체.

class FuncObjRandRange
{
private:
	int32 from, to;

public:
	FuncObjRandRange(const int32& _from = 1, const int32& _to = 6) : from(_from), to(_to) {}

	// 연산자 ()를 오버로딩하면 함수 객체로 사용할 수 있다.
	int32 operator()()
	{
		return FMath::RandRange(from, to);
	}
};

class FuncObjSequenceGenerator
{
private:
	int32 val;

public:
	FuncObjSequenceGenerator(const int32& _val = 1) : val(_val) {}

	int32 operator()()
	{
		return val++;
	}
};

class FuncObjValueFinder
{
private:
	int32 targetValue;

public:
	FuncObjValueFinder(const int32& _targetValue) : targetValue(_targetValue) {}

	bool operator()(const int32& value)
	{
		return (targetValue == value);
	}
};