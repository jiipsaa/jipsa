﻿#pragma once

#include "../Jipsa/Log.h"

class Vector
{
private:
	int32 dimension;
	float* arr;

public:
	const static int32 VERSION;
	static int32 objectCount;

public:

	// TODO: 빌드는 되는데 런타임 에러난다.. 방법이 필요하다. (2/2)
	//int32 GetVersion()
	//{
	//	return VERSION;
	//}

	//static void PrintObjectCount()
	//{
	//	PRINT("%d", objectCount);
	//}

	Vector(int32 _dimension = 0) : dimension(_dimension), arr(NULL)
	{
		// 동적 할당을 한다.
		arr = new float[dimension];

		++objectCount;
	}

	~Vector()
	{
		// 동적 해제를 한다.
		if (NULL != arr)
		{
			delete[] arr;
		}

		--objectCount;
	}

	// 복사 생성자 구현.
	Vector(const Vector& originVector)
	{
		PRINT("복사 생성자 호출");
		
		Clone(originVector);

		++objectCount;
	}

	// 대입 연산자 오버로딩.
	Vector& operator = (const Vector& originVector)
	{
		PRINT("대입 연산자 호출");
		
		Clone(originVector);
		return *this;
	}

	// '대입 연산자' 대신 참조형 매개변수를 사용한 Clone 함수를 구현해서 사용하자.
	void Clone(const Vector& originVector)
	{
		if (NULL != arr)
		{
			delete[] arr;
		}

		dimension = originVector.dimension;
		arr = new float[dimension];
		for (int32 i = 0; i < dimension; ++i)
		{
			arr[i] = originVector.arr[i];
		}
	}

	// '복사 생성자'가 호출되지 않도록, 매개변수는 참조형(또는 포인터)로 선언하자.
	void Add(const Vector& addedVector)
	{
		if (dimension != addedVector.dimension)
		{
			PRINT_WARNING("dimension이 다릅니다. Add 할 수 없습니다 %d != %d", dimension, addedVector.dimension);
			return;
		}

		for (int32 i = 0; i < dimension; ++i)
		{
			arr[i] += addedVector.arr[i];
		}
	}

	float& operator[](const int32& index)
	{
		return arr[index];
	}

	void SetRandom(float max = 100.0f)
	{
		for (int32 i = 0; i < dimension; ++i)
		{
			arr[i] = FMath::RandRange(0.0f, max);
		}
	}

	void PrintMemoryAddress()
	{
		PRINT("Memory Address: %x", this);
	}

	void Print()
	{
		FString log;
		log.Appendf(TEXT("Vector[%d] = <"), dimension);
		
		for (int32 i = 0; i < dimension; ++i)
		{
			log.Appendf(TEXT("%.1f "), arr[i]);
		}
		if (0 < dimension)
		{
			log.RemoveAt(log.Len() - 1, 1);
		}

		log.Append(">");
		PRINT("%s", *log);
	}
};