#pragma once

#include "../Jipsa/Singleton.h"

class TestSingleton_1 : public Singleton<TestSingleton_1> {
private:
	int value;

public:
	TestSingleton_1() : value(1) {}

	int GetValue() {
		return value;
	}

	void SetValue(int _value) {
		value = _value;
	}
};