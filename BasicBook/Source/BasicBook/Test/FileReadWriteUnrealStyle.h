﻿#pragma once

#include "../Jipsa/Log.h"

class FileReadWriteUnrealStyle
{
public:
	void Test01()
	{
		FString filePath = FString::Printf(TEXT("%sData/Test01.txt"), *FPaths::ProjectContentDir());

		FArchive* fileWriter = IFileManager::Get().CreateFileWriter(*filePath);
		if (NULL != fileWriter)
		{
			int32 temp = 1111;
			*fileWriter << temp;
			fileWriter->Close();

			PRINT("Success write");
		}
		else
		{
			PRINT("Fail write");
		}

		TSharedPtr<FArchive> fileReader = MakeShareable(IFileManager::Get().CreateFileReader(*filePath));
		if (NULL != fileReader)
		{
			int32 temp;

			*fileReader.Get() << temp;
			fileReader->Close();

			PRINT("Success read: %d", temp);
		}
		else
		{
			PRINT("Fail read");
		}
	}
};