﻿#pragma once

#include "../Jipsa/Log.h"

class ASCIIArt
{
public:
	void PrintLog()
	{
		// NOTE: 아스키 아트를 출력할땐 고정폭 폰트를 사용하자. 역슬래시는 \\로 사용하자.
		// *용어: 고정폭 폰트(fixed width font), 가변폭 폰트(variable width font)
		PRINT("%s", TEXT("  ____   ____  ___ ___    ___       ___   __ __    ___  ____       __"));
		PRINT("%s", TEXT(" /    | /    ||   |   |  /  _] / \\ |  |  |  /  _] | \\     |  |"));
		PRINT("%s", TEXT("|   __ || o || _   _ | /[_     |     ||  |  | /[_ | D  )    |  |"));
		PRINT("%s", TEXT("|  |  ||     ||  \\_/  ||    _] | O  ||  |  ||    _]|    /     |__|"));
		PRINT("%s", TEXT("|  |_ || _  ||   |   ||[_     |     ||:|| [_ | \\      __"));
		PRINT("%s", TEXT("|     ||  |  ||   |   ||     |    |     | \\   / |     ||  .  \\    |  |"));
		PRINT("%s", TEXT("|___, _ || __ | __ || ___ | ___ || _____ | \\___ / \\_/  |_____ || __ | \\_|    |__ |"));
	}
};