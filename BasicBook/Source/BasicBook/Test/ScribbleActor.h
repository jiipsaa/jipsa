﻿// 낙서장.

// TODO: STL 관련 예시는 모두 STL <-> Unreal STL 테스트 해본다. 전환 불가능한 경우를 정리해둬야 한다.

#pragma once

// temp include.
#include <vector>
#include <list>
#include <map>
#include <set>
#include <algorithm>
#include <time.h>
#include <cstring>

//#include <string>
using namespace std;

#include "../Jipsa/Log.h"
#include "../Jipsa/MessageManager.h"
#include "../Jipsa/PopupManager.h"
#include "../MiniGame/Data/PuzzleRanking.h"
#include "../MiniGame/Hangman.h"
#include "../MiniGame/Monsters.h"
#include "../MiniGame/MonsterWorld.h"
#include "../MiniGame/MonsterWorldMatrix.h"
#include "../MiniGame/Puzzle.h"
#include "../MiniGame/RussianRoulette.h"
#include "../MiniGame/SpeedGugu.h"
#include "../MiniGame/UpAndDownGame.h"
#include "ASCIIArt.h"
#include "Complex.h"
#include "ComponentLabelling.h"
#include "FileReadWriteUnrealStyle.h"
#include "FileReadWrite.h"
#include "FunctionObject.h"
#include "GraphicEditor.h"
#include "GuguGame.h"
#include "Pair.h"
#include "Rational.h"
#include "STLSample.h"
#include "TestSingleton_1.h"
#include "TestSingleton_2.h"
#include "Vector.h"
#include "VectorContainer.h"
#include "Vehicle.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Blueprint/UserWidget.h" // UMG.
#include "ScribbleActor.generated.h"

#pragma region 템플릿
template <typename T1, typename T2>
void CopyArray(const T1 source[], T2 target[], const int32 len)
{
	for (int32 i = 0; i < len; ++i)
	{
		target[i] = static_cast<T2>(source[i]);
	}
}

template <typename T>
T FindMaxValue(const T a[], const int32 len)
{
	T maxVal = a[0];
	for (int32 i = 1; i < len; ++i)
	{
		if (maxVal < a[i])
		{
			maxVal = a[i];
		}
	}
	return maxVal;
}

// 템플릿 Specialization: 특정한 자료형에 대해 다른 로직을 적용하는 방법.
// ex) T 의 자료형이 char 인 경우엔 알파벳 소문자만 FindMaxValue의 대상으로 하고싶다.
template <>
char FindMaxValue(const char a[], const int32 len)
{
	char maxVal = 'a' - 1;
	for (int32 i = 1; i < len; ++i)
	{
		if (maxVal < a[i])
		{
			if ('a' <= a[i] && a[i] <= 'z')
			{
				maxVal = a[i];
			}
		}
	}
	return maxVal;
}

template <typename T>
void PrintContainer(T& container, FString prefix = "")
{
	FString log;

	//if (true == isShowDivisionLine)
	//{
	//	log.Append("-----------------------\n");
	//}

	if (false == prefix.IsEmpty())
	{
		log.Appendf(TEXT("%s: "), *prefix);
	}

	////string temp;
	//for (T::iterator it = container.begin(); it != container.end(); ++it)
	//{
	//	// 두자리 이상의 숫자가 깨져 보이는 현상을 해결하지 못함.
	//	//temp = to_string(*it);
	//	//log.Append(FString::Printf(TEXT("%s, "), temp.c_str()));

	//	//log.Append(FString::Printf(TEXT("%d, "), *it));
	//	log.Appendf(TEXT("%d, "), *it);
	//}

	PRINT("%s", *log);
}
#pragma endregion

UCLASS()
class BASICBOOK_API AScribbleActor : public AActor
{
	GENERATED_BODY()

private:
	Puzzle* puzzle;
	MonsterWorld* monsterWorld;

public:
	AScribbleActor() : puzzle(nullptr), monsterWorld(nullptr) {}

	~AScribbleActor()
	{
		if (nullptr != puzzle)
		{
			delete puzzle;
		}

		if (nullptr != monsterWorld)
		{
			delete monsterWorld;
		}
	}
	
	//virtual void Tick(float DeltaTime) override
	//{
	//	Super::Tick(DeltaTime);
	//}

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override
	{
		Super::BeginPlay();
	}

	UFUNCTION(BlueprintCallable, Category = "ToClient")
	void ToClient_PressLeft()
	{
		if (nullptr != puzzle)
		{
			puzzle->Input(ePuzzleDirection::Left);
		}
		else if (nullptr != monsterWorld)
		{
			monsterWorld->Input(eMonsterWorldDirection::Left);
		}
	}

	UFUNCTION(BlueprintCallable, Category = "ToClient")
	void ToClient_PressRight()
	{
		if (nullptr != puzzle)
		{
			puzzle->Input(ePuzzleDirection::Right);
		}
		else if (nullptr != monsterWorld)
		{
			monsterWorld->Input(eMonsterWorldDirection::Right);
		}
	}

	UFUNCTION(BlueprintCallable, Category = "ToClient")
	void ToClient_PressUp()
	{
		if (nullptr != puzzle)
		{
			puzzle->Input(ePuzzleDirection::Up);
		}
		else if (nullptr != monsterWorld)
		{
			monsterWorld->Input(eMonsterWorldDirection::Up);
		}
	}

	UFUNCTION(BlueprintCallable, Category = "ToClient")
	void ToClient_PressDown()
	{
		if (nullptr != puzzle)
		{
			puzzle->Input(ePuzzleDirection::Down);
		}
		else if (nullptr != monsterWorld)
		{
			monsterWorld->Input(eMonsterWorldDirection::Down);
		}
	}

	UFUNCTION(BlueprintCallable, Category = "ToClient")
	void ToClient_PressEnter()
	{
		if (nullptr != monsterWorld)
		{
			//monsterWorld->Play();
			monsterWorld->AutoPlay();
		}
	}

	UFUNCTION(BlueprintCallable, Category = "ToClient")
	void ToClient_TestAssert(int32 i, bool b)
	{
		PRINT("ToClient_TestAssert(), 시작");

		if (1 == i)
		{
			// 기본적으로 디버그, 개발, 테스트 및 배송 편집기 빌드에서 작동합니다.
			// USE_CHECKS_IN_SHIPPING참 값을 유지하도록 정의 1하면 Check 매크로가 모든 빌드에서 작동합니다.
			// - 에디터에서 크래시
			// - 빌드에서도 크래시
			check(true == b);
		}
		else if (2 == i)
		{
			checkf(b, TEXT("문제가 있다. i=%d, b=%d"), i, b);
		}
		else if (3 == i)
		{
			// 매크로가 비활성화된 빌드에서도 표현식을 평가합니다.

			// 여부터, UI 에 메시지 나오게 변경, 빌드에서 크래시 되는지 아니면 다음라인 실행만 안되는지 확인
			// - 에디터에서는 크래시.
			// - 빌드에서는 다음 라인 호출 안되게 만들어주나? -> 빌드에서도 크래시.
			verify(true == b);
		}
		else if (4 == i)
		{
			verifyf(true == b, TEXT("문제가 있다. i=%d, b=%d"), i, b);
		}
		else if (5 == i)
		{
			// 엔진이 충돌 보고자에게 알리지만 계속 실행됩니다.
			// 에디터 세션당 한 번만 보고하도록 합니다.
			// 식이 false로 평가될 때마다 매크로 확인을 보고해야 하는 경우 매크로의 "항상" 버전을 사용합니다.
			// - 계속 실행되지만 if 처리를 항상 해야한다.
			if (false == ensureAlways(true == b))
			{
				PRINT("문제가 있다. 처리하자. i=%d, b=%d", i, b);
			}
		}
		else if (6 == i)
		{
			if (false == ensureAlwaysMsgf(true == b, TEXT("문제가 있다. i=%d, b=%d"), i, b))
			{
				PRINT("문제가 있다. 처리하자. i=%d, b=%d", i, b);
			}
		}
		else
		{
			ASSERT(true == b, "문제가 있다. i=%d, b=%d", i, b);
		}

		PRINT("ToClient_TestAssert(), 종료");
	}

private:
	UFUNCTION(BlueprintCallable, Category = "Jipsa")
	void Test()
	{
		//TestUnrealString();
		//TestString();

		//TestTArray();
		//TestTArrayAndSortingAlgorithm();
		//TestTArrayAndNonMutationAlgorithm();
		//TestTArrayAndMutationAlgorithm();
		//TestTMap();
		//TestTSet();

		//MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), "안녕");
		//MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), "반가워");

		//PopupManager::GetSingleton()->OpenPopup(GetWorld(), "AsciiCodePopupBP");

		//RunProgram_1_2();
		//RunProgram_1_3();
		//RunProgram_1_6();
		//RunProgram_2_3();
		//RunProgram_3_12();
		//RunProgram_3_13();
		//RunProgram_3_16();
		//RunProgram_4_3();
		//RunProgram_4_4();
		//RunProgram_4_6();
		//RunProgram_4_7();
		//RunProgram_4_8();
		//RunProgram_4_10();
		//RunProgram_4_14();
		//RunProgram_5_4();
		//RunProgram_5_10();
		//RunProgram_5_14();
		//RunProgram_6_11();
		//RunProgram_7_4();
		//RunProgram_8_1();
		//RunProgram_8_9();
		//RunProgram_9_1();
		//RunProgram_9_3();
		RunProgram_9_5();
		//RunProgram_11_6();
		//RunProgram_12_11();
		//RunProgram_13_1();
		//RunProgram_13_3();
		//RunProgram_13_8();
		//RunProgram_13_10();
		//RunProgram_13_13();
		//RunProgram_14_1();
	}

	UFUNCTION(BlueprintCallable, Category = "Jipsa")
	void TestByStringInput(FString stringInput)
	{
	}

	UFUNCTION(BlueprintCallable, Category = "Jipsa")
	void TestByIntegerInput(int integerInput)
	{
		//RunProgram_2_8(integerInput);
		//RunProgram_2_9(integerInput);
		//RunProgram_2_12(integerInput);
	}

#pragma region Basic Book(게임으로 배우는 C++)
	void RunProgram_14_1()
	{
		STLSample stlSample;
		//stlSample.TestVector();
		//stlSample.TestVector2();
		//stlSample.TestDeque();
		//stlSample.TestList();
		//stlSample.TestStack();
		//stlSample.TestQueue();
		//stlSample.TestPriorityQueue();
		//stlSample.TestSet();
		//stlSample.TestMap();
		//stlSample.TestSequenceAlgorithmGenerate();
		//stlSample.TestSequenceAlgorithmFill();
		//stlSample.TestSequenceAlgorithmForEach();
		//stlSample.TestSequenceAlgorithmTransform();
		//stlSample.TestSequenceAlgorithmPartition();
		//stlSample.TestSequenceAlgorithmRotate();
		//stlSample.TestSequenceAlgorithmPermutation();
		//stlSample.TestSequenceAlgorithmFind();
		//stlSample.TestSequenceAlgorithmSearch();
		//stlSample.TestSortingAlgorithmSort();
		//stlSample.TestSortingAlgorithmBinarySearch();
		//stlSample.TestSortingAlgorithmMerge();
		stlSample.TestSortingAlgorithmHeap();
	}

	//void RunProgram_13_13()
	//{
	//	VectorContainer<VectorContainer<int32>> image(6);

	//	int32 row = image.GetCurCapacity(), col = 12;
	//	int32 value = 10;
	//	for (int32 i = 0; i < row; ++i)
	//	{
	//		for (int32 j = 0; j < col; ++j)
	//		{
	//			++value;
	//			image[i].PushBack(value);
	//		}
	//	}

	//	image[2][5] = image[2][6] = image[3][5] = image[3][6] = 99;

	//	FString log;
	//	log.Append("\n");
	//	row = image.GetCurCapacity();
	//	for (int32 i = 0; i < row; ++i)
	//	{
	//		col = image[i].GetCurCapacity();
	//		for (int32 j = 0; j < col; ++j)
	//		{
	//			log.Appendf(TEXT("%d "), image[i][j]);
	//		}
	//		log.Append("\n");
	//	}
	//	PRINT("%s", *log);
	//}

	//void RunProgram_13_10()
	//{
	//	VectorContainer<int64> v;
	//	for (int32 i = 0; i < 25; ++i)
	//	{
	//		v.PushBack(FMath::RandRange(0, 9));
	//	}

	//	FString log;
	//	log.Append(TEXT("\n랜덤으로 입력: "));
	//	int32 curCapacity = v.GetCurCapacity();
	//	for (int32 i = 0; i < curCapacity; ++i)
	//	{
	//		log.Appendf(TEXT("%d "), v[i]);
	//	}

	//	for (int32 i = 0; i < 5; ++i)
	//	{
	//		v.At(i) = 0;
	//		v[10 + i] = 9;
	//	}

	//	log.Append(TEXT("\n중간값을 변경: "));
	//	curCapacity = v.GetCurCapacity();
	//	for (int32 i = 0; i < curCapacity; ++i)
	//	{
	//		log.Appendf(TEXT("%d "), v[i]);
	//	}

	//	for (int32 i = 0; i < 15; ++i)
	//	{
	//		v.PopBack();
	//	}

	//	log.Append(TEXT("\n뒷부분 제거: "));
	//	curCapacity = v.GetCurCapacity();
	//	for (int32 i = 0; i < curCapacity; ++i)
	//	{
	//		log.Appendf(TEXT("%d "), v[i]);
	//	}

	//	VectorContainer<int64> u = v;

	//	log.Append(TEXT("\n깊은 복사: "));
	//	curCapacity = u.GetCurCapacity();
	//	for (int32 i = 0; i < curCapacity; ++i)
	//	{
	//		log.Appendf(TEXT("%d "), u[i]);
	//	}

	//	VectorContainer<string> stringVector;
	//	stringVector.PushBack("안녕");
	//	stringVector.PushBack("반가워");

	//	log.Append(TEXT("\nstring을 담았음: "));
	//	curCapacity = stringVector.GetCurCapacity();
	//	for (int32 i = 0; i < curCapacity; ++i)
	//	{
	//		log.Appendf(TEXT("%s "), UTF8_TO_TCHAR(stringVector[i].c_str()));
	//	}

	//	PRINT("%s", *log);
	//}

	void RunProgram_13_8()
	{
		Pair<int32, double> i2d[3];
		i2d[0].Set(10, 3.14);
		i2d[1].Set(25, 2.71);
		i2d[2].Set(14, 1.41);
		
		FString log;
		for (int32 i = 0; i < 3; ++i)
		{
			log.Appendf(TEXT("(%d, %lf) "), i2d[i].GetData1(), i2d[i].GetData2());
		}
		PRINT("%s", *log);

		Pair<string, double> map[3];
		map[0].Set("Pi, Archimedes' contant", 3.14);
		map[1].Set("Euler's number", 2.71);
		map[2].Set("square root of 2", 1.41);

		log.Empty();
		for (int32 i = 0; i < 3; ++i)
		{
			log.Appendf(TEXT("(%s, %lf) "), UTF8_TO_TCHAR(map[i].GetData1().c_str()), map[i].GetData2());
		}
		PRINT("%s", *log);

		// typedef 키워드를 이용하면 템플릿 이름을 임의로 사용할 수 있다.
		typedef Pair<string, double> PairS2D;
		PairS2D pair;
		pair.Set("Hey", 1);
		PRINT("(%s, %lf)", UTF8_TO_TCHAR(pair.GetData1().c_str()), pair.GetData2());

		// 디폴트 자료형이 있는경우 아래와 같이 사용하면 된다.
		Pair<> pair2;
		pair2.Set(0, 1);
		PRINT("(%d, %d)", pair2.GetData1(), pair2.GetData2());

		// 템플릿을 상속한 클래스이다.
		WordPair wordPair;
		wordPair.Set("영어", "English");
	}

	void RunProgram_13_3()
	{
		int32 iArr[5] = { 1, 5, 2, 4, 3 };
		float fArr[5];
		double dArr[5] = { 3.0, 4.0, 2.0, 5.0, 1.0 };
		
		FString log;

		CopyArray(iArr, fArr, 5);
		
		log.Append("\niArr:");
		for (int32 i = 0; i < 5; ++i)
		{
			log.Appendf(TEXT("%d "), iArr[i]);
		}

		log.Append("\nfArr:");
		for (int32 i = 0; i < 5; ++i)
		{
			log.Appendf(TEXT("%f "), fArr[i]);
		}

		CopyArray(dArr, iArr, 5);

		log.Append("\niArr:");
		for (int32 i = 0; i < 5; ++i)
		{
			log.Appendf(TEXT("%d "), iArr[i]);
		}

		PRINT("%s", *log);
	}

	void RunProgram_13_1()
	{
		int32 iArr[5] = { 1, 5, 2, 4, 3 };
		double dArr[5] = { 3.0, 4.0, 2.0, 5.0, 1.0 };
		char cArr[] = "game over ! {09}";

		int32 iMax = FindMaxValue(iArr, 5);
		double dMax = FindMaxValue(dArr, 5);
		char cMax = FindMaxValue(cArr, strlen(cArr));

		PRINT("%d %lf %c", iMax, dMax, cMax);
	}

	void RunProgram_12_11()
	{
		// 포인터와 const 키워드.
		int x = 10, y = 20, z = 30, w = 40;
		int x2 = 1, y2 = 2, z2 = 3, w2 = 4;

		int* px = &x;
		*px = 100;
		px = &x2;
		PRINT("%d %d", x, *px);

		const int* py = &y;	// const 키워드가 자료형 앞에 오면 값을 바꿀 수 없음.
		//*py = 200;		// 값을 바꿀 수 없음.
		py = &y2;
		PRINT("%d", *py);

		int* const PZ = &z;	// const 키워드가 자료형 뒤에 오면 주소를 다시 넣을 수 없음.
		*PZ = 300;
		//PZ = &z2;			// 다른 주소를 넣을 수 없음.
		PRINT("%d", z);

		const int* const PW = &w;
		//*PW = 400;		// 값을 바꿀 수 없음.
		//PW = &w2;			// 다른 주소를 넣을 수 없음.
	}

	void RunProgram_11_6()
	{
		Rational a(4), b(6, 8);

		float fA = (float)a;
		float fB = (float)b;
		PRINT("%f %f", fA, fB);

		a[0] = 1;
		a[1] = 2;
		b(0) = 1;
		b(1) = 2;
		Rational a2 = ++a;
		Rational b2 = b++;

		fA = (float)a2;
		fB = (float)b2;
		PRINT("%f %f", fA, fB);

		//Rational wrongA(1, 0);
		//fA = wrongA.GetRealValue();

		//Rational wrongB(1, -1);
		//fB = wrongB.GetRealValue();

		Rational wrongA(1, 0);

		//try
		//{
			fA = wrongA.GetRealValue();
		//}
		//catch (ZeroBottomException zeroBottomException)
		//{
		//	// 함수 GetRealValue() 에서 전달받은 예외를 처리한다.
		//	// 예외를 수정했다. 이 경우 GetRealValue() 에서 또다시 예외가 발생하면 대응이 안된다.
		//	// 분모가 0인 경우 더 이상 드리블 되지 않도록 처리하자.
		//	int32 bottom = zeroBottomException.bottom;
		//	PRINT_WARNING("분모가 0 입니다. 강제로 분모를 1로 변환합니다. bottom=%d -> 1", bottom);
		//	
		//	wrongA[1] = 1;
		//	fA = wrongA.GetRealValue();
		//}

		PRINT("%f", fA);
	}

	void RunProgram_9_5()
	{
		int32 width = 16;
		int32 height = 8;
		monsterWorld = new MonsterWorld(GetWorld(), width, height);

		int32 randomX = FMath::RandRange(0, width - 1);
		int32 randomY = FMath::RandRange(0, height - 1);
		monsterWorld->AddMonster(new Zombie("좀비", 'Z', randomX, randomY));

		randomX = FMath::RandRange(0, width - 1);
		randomY = FMath::RandRange(0, height - 1);
		monsterWorld->AddMonster(new Vampire("뱀파이어", 'P', randomX, randomY));

		randomX = FMath::RandRange(0, width - 1);
		randomY = FMath::RandRange(0, height - 1);
		monsterWorld->AddMonster(new Ghost("유령", 'G', randomX, randomY));

		randomX = FMath::RandRange(0, width - 1);
		randomY = FMath::RandRange(0, height - 1);
		monsterWorld->AddMonster(new Jiangshi("수평강시", 'H', randomX, randomY, true));

		randomX = FMath::RandRange(0, width - 1);
		randomY = FMath::RandRange(0, height - 1);
		monsterWorld->AddMonster(new Jiangshi("수직강시", 'V', randomX, randomY, false));

		randomX = FMath::RandRange(0, width - 1);
		randomY = FMath::RandRange(0, height - 1);
		monsterWorld->AddPlayer(new MonsterWorldPlayer("유저", 'U', randomX, randomY));

		monsterWorld->Ready();
	}

	void RunProgram_9_3()
	{
		GraphicEditor graphicEditor;
		//graphicEditor.Test();
		//graphicEditor.Test2();
		//graphicEditor.Test3();
		//graphicEditor.Test4();
		//graphicEditor.Test5();
		//graphicEditor.Test6();
		graphicEditor.Test7();
	}

	void RunProgram_9_1()
	{
		Car myCar(2), yourCar(3);

		myCar.price = 3000;
		yourCar.price = 2500;

		myCar.PushAccel();

		myCar.Print();
		yourCar.Print();

		myCar.PrintCarInfo();
		yourCar.PrintCarInfo();
	}

	int32** Allock2DInt(int32 rows, int32 cols)
	{
		int32** matrix = new int32* [rows];
		for (int32 i = 0; i < rows; ++i)
		{
			matrix[i] = new int32[cols];
			for (int32 j = 0; j < cols; ++j)
			{
				matrix[i][j] = 0;
			}
		}
		return matrix;
	}
	void Free2DInt(int32** matrix, int32 rows)
	{
		for (int32 i = 0; i < rows; ++i)
		{
			delete[] matrix[i];
		}
		delete[] matrix;
	}
	int32 FindMaxPixel(int32** matrix, int32 rows, int32 cols)
	{
		int32 maxPixel = matrix[0][0];
		for (int32 r = 0; r < rows; ++r)
		{
			for (int32 c = 0; c < cols; ++c)
			{
				if (maxPixel < matrix[r][c])
				{
					maxPixel = matrix[r][c];
				}
			}
		}
		return maxPixel;
	}
	void RunProgram_8_9()
	{
		int32 rows = 2;
		int32 cols = 3;
		int32** matrix = Allock2DInt(rows, cols);

		matrix[1][1] = 9;
		int32 maxPixel = FindMaxPixel(matrix, rows, cols);

		Free2DInt(matrix, rows);

		PRINT("maxPixel=%d", maxPixel);

		//MonsterWorldMatrix monsterWorldMatrix(rows, cols);
		//monsterWorldMatrix.SetRandom();
		//monsterWorldMatrix.Print();
		//maxPixel = FindMaxPixel(monsterWorldMatrix.GetMatrix(), monsterWorldMatrix.GetRows(), monsterWorldMatrix.GetCols());
		//PRINT("maxPixel=%d", maxPixel);
	}

	void RunProgram_8_1()
	{
		Vector u(3), v(3);
		u.SetRandom();
		v.SetRandom();
		u.Print();
		v.Print();
		PRINT("u.arr = { %f, %f, %f }", u[0], u[1], u[2]);

		u.Add(v);
		u.Print();

		v.Clone(u);
		v.Print();

		//Vector w(u);
		//w.Print();
		
		//Vector x = v;
		//x.Print();
		//x.PrintMemoryAddress();

		// TODO: 빌드는 되는데 런타임 에러난다.. 방법이 필요하다. (1/2)
		//Vector::PrintObjectCount();

		//PRINT("클래스 버전=%d, 객체 갯수=%d", Vector::VERSION, Vector::objectCount);
		
		//int32 tempVersion = Vector::VERSION;
		//PRINT("%d", tempVersion);

		//int32 tempVersion = x.GetVersion();
		//PRINT("%d", tempVersion);
	}

	struct Point
	{
		int32 x, y;
		Point(int32 _x = 0, int32 _y = 0) : x(_x), y(_y) {}
		
		// 복사 생성자.
		Point(const Point& p) : x(p.x), y(p.y) { PRINT("복사 생성자. 점 (%d, %d)", x, y); }
		
		void Print()
		{
			PRINT("점 (%d, %d)", x, y);
		}
	};

	class Line
	{
	private:
		Point p1, p2;
	public:
		Line(int32 x1, int32 y1, int32 x2, int32 y2): p1(x1, y1), p2(x2, y2) {}
		
		// 복사 생성자는 다른 객체로부터 새로운 객체를 생성할때 호출된다.
		// 복사 생성자.
		Line(const Line& l) : p1(l.p1), p2(l.p2) { PRINT("복사 생성자. 선 (%d, %d) (%d %d)", p1.x, p1.y, p2.x, p2.y); }

		void Print()
		{
			PRINT("선:");
			p1.Print();
			p2.Print();
		}
	};

	// 복사 생성자는 매개변수로 전달할때나 반환될때 호출된다. (로그 안찍힌다?)
	Line GetTestLine()
	{
		Line testLine(1, 2, 3, 4);
		return testLine;
	}

	void RunProgram_7_4()
	{
		PRINT("line");
		Line line(3, 4, 5, 6);
		line.Print();

		PRINT("line2");
		Line line2 = GetTestLine();
		line2.Print();

		// 대입 연산자도 복사 생성자를 호출한다.
		PRINT("line3");
		Line line3 = line2;
		line3.Print();
	}

	void RunProgram_6_11()
	{
		Hangman hangman;
		hangman.Play();
	}

	void RunProgram_5_14()
	{
		unsigned char image[LABELLING_HEIGHT][LABELLING_WIDTH] = {
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
			,{ 0, 9, 9, 9, 9, 0, 0, 0, 9, 0, 0, 0, 0, 0, 9, 0, 0 }
			,{ 9, 9, 0, 0, 0, 0, 9, 9, 9, 9, 9, 0, 9, 9, 9, 9, 9 }
			,{ 0, 9, 9, 9, 9, 0, 0, 0, 9, 0, 0, 0, 0, 0, 9, 0, 0 }
			,{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
		};

		ComponentLabelling componentLabelling;
		//componentLabelling.PrintImage(image, "<Original image>");
		componentLabelling.BlobColoring(image);
		//componentLabelling.PrintImage(image, "<Labelled image>");
	}

	void RunProgram_5_10()
	{
		int fibonacci = Fibonacci(10);
		int fibonacci2 = Fibonacci_2(10);
		
		int factorial = Factorial(10);
		int factorial2 = Factorial_2(10);
		
		PRINT("n=10, fibonacci=%d/%d, factorial=%d/%d", fibonacci, fibonacci2, factorial, factorial2);
	}
	int Fibonacci(int n)
	{
		if (n < 0)
		{
			return -1;
		}

		if (0 == n)
		{
			return 0;
		}
		else if (1 == n)
		{
			return 1;
		}
		else
		{
			return (Fibonacci(n - 1) + Fibonacci(n - 2));
		}
	}
	int Fibonacci_2(int n)
	{
		if (n < 0)
		{
			return -1;
		}

		if (0 == n)
		{
			return 0;
		}
		else if (1 == n)
		{
			return 1;
		}
		else
		{
			int result = 0, n1 = 0, n2 = 1;
			for (int i = 2; i <= n; ++i)
			{
				result = n1 + n2;
				n1 = n2;
				n2 = result;
			}
			return result;
		}
	}
	int Factorial(int n)
	{
		if (n < 1)
		{
			return -1;
		}

		if (1 == n)
		{
			return 1;
		}
		else
		{
			return (n * Factorial(n - 1));
		}
	}
	int Factorial_2(int n)
	{
		if (n < 1)
		{
			return -1;
		}

		int factorial = 1;
		for (int i = n; 0 < i; --i)
		{
			factorial = i * factorial;
		}
		return factorial;
	}

	void RunProgram_5_4()
	{
		int arr[10] = { 0, 1, 2, 30, 4, 5, 6, -7, 8, 9 };
		int min, max;

		FindMinMax(arr, 10, &min, &max);
		//FindMinMax(arr, 10, &min, NULL);
		//FindMinMax(arr, 10, NULL, &max);
		
		FindMinMax(arr, 10, min, max);
		
		PRINT("min: %d, max:%d", min, max);
	}
	void FindMinMax(const int* a, const int len, int* pMin, int* pMax)
	{
		if (NULL != pMin)
		{
			*pMin = a[0];
			for (int i = 1; i < len; ++i)
			{
				if (a[i] < *pMin)
				{
					*pMin = a[i];
				}
			}
		}

		if (NULL != pMax)
		{
			*pMax = a[0];
			for (int i = 1; i < len; ++i)
			{
				if (*pMax < a[i])
				{
					*pMax = a[i];
				}
			}
		}
	}
	void FindMinMax(const int* a, const int len, int& min, int& max)
	{
		min = a[0];
		for (int i = 1; i < len; ++i)
		{
			if (a[i] < min)
			{
				min = a[i];
			}
		}

		max = a[0];
		for (int i = 1; i < len; ++i)
		{
			if (max < a[i])
			{
				max = a[i];
			}
		}
	}

	void RunProgram_4_14()
	{
		puzzle = new Puzzle();
		puzzle->Play();
	}

	void RunProgram_4_10()
	{
		PuzzleRanking puzzleRanking;
		puzzleRanking.Load();
		puzzleRanking.Print();

		for (int i = 0; i < 5; ++i)
		{
			puzzleRanking.Add(FMath::RandRange(1, 500), FMath::FRandRange(10.0f, 100.0f));
		}
		puzzleRanking.Save();
	}

	void RunProgram_4_8()
	{
		FileReadWrite fileReadWrite;
		fileReadWrite.Test01_V3();

		//FileReadWriteUnrealStyle fileReadWrite;
		//fileReadWrite.Test01();
	}

	struct ComplexV1
	{
		float real;
		float imaginary;
	};
	void SetComplex(ComplexV1& c, const float& real, const float& imaginary)
	{
		c.real = real;
		c.imaginary = imaginary;
	}
	void ResetComplex(ComplexV1& c)
	{
		c.real = c.imaginary = 0.0f;
	}
	void SetRandomComplex(ComplexV1& c)
	{
		c.real = FMath::FRandRange(0.0f, 10.0f);
		c.imaginary = FMath::FRandRange(0.0f, 100.0f);
	}
	void PrintComplex(const ComplexV1& c, const char* msg = "복소수")
	{
		PRINT("%s %4.2f + %4.2fi", UTF8_TO_TCHAR(msg), c.real, c.imaginary);
	}
	void AddComplex(ComplexV1& c, const ComplexV1& a, const ComplexV1& b)
	{
		c.real = a.real + b.real;
		c.imaginary = a.imaginary + b.imaginary;
	}
	void RunProgram_4_7()
	{
		// ComplexV1: 복소수 구조체 버전.
		//ComplexV1 a, b, c;
		//SetComplex(a, 1.0f, 1.0f);
		//SetRandomComplex(b);
		//AddComplex(c, a, b);

		//PrintComplex(a, "a=");
		//PrintComplex(b, "b=");
		//PrintComplex(c, "a+b=");

		//ResetComplex(c);
		//PrintComplex(c, "ResetComplex(c)=");

		// ComplexV2: 복소수 클래스로의 전환.
		ComplexV2 a, b, c;
		a.Set(2.1f, 2.1f);
		b.SetRandom();
		c.Add(a, b);

		a.Print("a=");
		b.Print("b=");
		c.Print("Add(a, b)=");

		c = a + b;
		c.Print("a + b =");

		c.Reset();
		c.Print("Reset()=");

		c = b = a;
		c.Print("c = b = a, c=");
	}

	void RunProgram_4_6()
	{
		int score[5];

		for (int i = 0; i < 8; ++i)
		{
			int newScore = FMath::RandRange(0, 100);
			AddDescending(score, 5, newScore);
			PrintArray(score, 5, "순위");
		}
	}
	void AddDescending(int a[], int len, int val)
	{
		if (a[len - 1] < val)
		{
			a[len - 1] = val;
			for (int i = len - 1; 0 < i; --i)
			{
				if (val < a[i - 1])
				{
					return;
				}

				a[i] = a[i - 1];
				a[i - 1] = val;
			}
		}
	}

	void RunProgram_4_4()
	{
		int a[10] = { 1, 2, 3, 4, 5, 6, 7 };
		int b[10];
		//int* b2;
		//int* b3;
		char src[] = "game over !";
		char dst[40];

		ReverseArray(a, b, 10);
		//b2 = ReverseArray_2(a, 10);
		//b3 = ReverseArray_3(a, 10);

		ReverseString(src, dst);

		PrintArray(a, 10, "배열 a");
		PrintArray(b, 10, "배열 b");
		//PrintArray(b2, 10, "배열 b2");
		//PrintArray(b3, 10, "배열 b3");

		PRINT("문자열 src: %s", UTF8_TO_TCHAR(src));
		PRINT("문자열 dst: %s", UTF8_TO_TCHAR(dst));
	}
	void PrintArray(const int a[], int len, const char msg[] = "")
	{
		FString log;
		log.Appendf(TEXT("%s: "), UTF8_TO_TCHAR(msg));
		for (int i = 0; i < len; ++i)
		{
			log.Appendf(TEXT("%3d"), a[i]);
		}
		PRINT("%s", *log);
	}
	void ReverseArray(const int a[], int b[], const int len)
	{
		for (int i = 0; i < len; ++i)
		{
			b[len - i - 1] = a[i];
		}
	}
	//int* ReverseArray_2(const int a[], const int len)
	//{
	//	int b[10];
	//	for (int i = 0; i < len; ++i)
	//	{
	//		b[len - i - 1] = a[i];
	//	}
	//	return b;
	//}
	//int* ReverseArray_3(const int a[], const int len)
	//{
	//	static int b[10];
	//	for (int i = 0; i < len; ++i)
	//	{
	//		b[len - i - 1] = a[i];
	//	}
	//	return b;
	//}
	void ReverseString(const char src[], char dst[])
	{
		int len = strlen(src);
		for (int i = 0; i < len; ++i)
		{
			dst[len - i - 1] = src[i];
		}
		dst[len] = '\0';
	}

	void RunProgram_4_3()
	{
		int arr[10] = { 3, 4, 5, 6 };
		char str[] = "game over !";
		PRINT("%d %c", FindMaxValue3(arr, 10), FindMaxChar(str));
	}
	int FindMaxValue2(const int a[], int len)
	{
		int maxVal = a[0];
		for (int i = 1; i < len; ++i)
		{
			if (maxVal < a[i])
			{
				maxVal = a[i];
			}
		}
		return maxVal;
	}
	int FindMaxValue3(const int* a, int len)
	{
		int maxVal = a[0];
		for (int i = 1; i < len; ++i)
		{
			if (maxVal < a[i])
			{
				maxVal = a[i];
			}
		}
		return maxVal;
	}
	char FindMaxChar(const char a[])
	{
		char maxChar = a[0];
		for (int i = 1; i < strlen(a); ++i)
		{
			if (maxChar < a[i])
			{
				maxChar = a[i];
			}
		}
		return maxChar;
	}

	void RunProgram_3_16()
	{
		SpeedGugu speedGugu;
		speedGugu.Play();
	}

	void RunProgram_3_13()
	{
		RussianRoulette russianRoulette;
		russianRoulette.Play();
	}

	void RunProgram_3_12()
	{
		FString log;
		for (int i = 0; i < 10; ++i)
		{
			log.Appendf(TEXT("%d, "), GetNextPrime());
		}

		// 참조자가 반환되도록 함수를 작성하면, 정적 지역변수를 초기화 할 수 있다.
		// GetNextPrime() = 1;
		int& temp = GetNextPrime();
		temp = 1;

		log.Append("\n");
		for (int i = 0; i < 10; ++i)
		{
			log.Appendf(TEXT("%d, "), GetNextPrime());
		}

		PRINT("소수: %s", *log);
	}
	int& GetNextPrime()
	{
		static int prime = 1;
		while (false == IsPrimeNumber(++prime));
		return prime;
	}
	inline bool IsPrimeNumber(int n)
	{
		for (int i = 2; i < n; ++i)
		{
			if (0 == (n % i))
			{
				return false;
			}
		}

		return true;
	}

	void RunProgram_3_11()
	{
		// #include <time.h>

		clock_t startClock = clock();
		PrintGuguDan(11);
		clock_t endClock = clock();

		double duration = (double)(endClock - startClock) / CLOCKS_PER_SEC;
		PRINT("실행 시간: %lfs", duration);
	}

	void RunProgram_1_2()
	{
		// 구조적 프로그래밍.
		// 반복문이나 함수등을 사용해서 의존성을 줄이는 방법.
		// 여전히 데이터와 로직은 분리되어 있다.
		
		PrintGuguDan(3);
	}
	void PrintGuguDan(int dan)
	{
		PRINT("[구구단 %d 단]", dan);
		for (int i = 1; i <= 9; ++i)
		{
			PRINT("%2d x %2d = %2d", dan, i, (dan * i));
		}
	}

	void RunProgram_1_3()
	{
		// 객체지향 프로그래밍.
		// 데이터와 로직을 클래스에 부여했다.
		// 만약 일부 로직을 수정하고 싶다면, 다른 Play함수를 제작하거나 Set함수로 멤버 변수를 조정해야 한다.
		// 이처럼 클래스 내부에서 수정이 이뤄져야 하기 때문에 외부에서의 데이터 수정이나 로직 수정을 막고, 스파게티 코드의 발생을 줄인다.

		GuguGame guguGame;
		guguGame.Play(3);

		guguGame.Set(2, 9);
		guguGame.Play(3);
	}

	void RunProgram_1_6()
	{
		ASCIIArt asciiArt;
		asciiArt.PrintLog();
	}

	void RunProgram_2_3()
	{
		// 오버플로우와 자료형의 최소/최대값.
		short n = 0;
		while (true)
		{
			short temp = n + 1;
			if (temp < 0)
			{
				PRINT("오버플로우 발생 -> short 최소값 = %d, short 최대값 = %d", temp, n);
				break;
			}

			n = temp;
		}

		// 비트 이동 연산자.
		int val = 5;
		int right = val >> 2;
		int left = val << 2;

		int val2 = -6;
		int right2 = val2 >> 2;
		int left2 = val2 << 2;

		PRINT("비트 이동 연산자 테스트: %d %d %d %d %d %d", val, right, left, val2, right2, left2);

		// nested loop 탈출.
		FString log;
		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 10; ++j)
			{
				log.Appendf(TEXT("%c "), '$');

				if ((2 == i) && (4 == j))
				{
					i = 4;	// 외부 루프의 조건식이 false가 되도록 처리하고 빠져나간다.
					break;
				}
			}
			log.Append("\n");
		}
		PRINT("다중 루프 탈출 테스트\n%s", *log);
	}

	void RunProgram_2_8(int value)
	{
		int k, count = 0;

		FString log;
		for (int i = 2; i <= value; ++i)
		{
			for (k = 2; k < i; ++k)
			{
				if (0 == i % k)
				{
					break;
				}
			}

			if (i == k)
			{
				log.Appendf(TEXT("%4d "), i);
				++count;
			}
		}
		PRINT("2 ~ %d 사이의 소수 갯수는 %d 개 입니다.", value, count);
		PRINT("소수 목록: %s", *log);
	}

	void RunProgram_2_9(int height)
	{
		FString log;
		for (int i = 1; i <= height; ++i)
		{
			for (int j = 0; j < height - i; ++j)
			{
				log.Append("   ");
			}

			for (int j = 0; j < i; ++j)
			{
				log.Appendf(TEXT("%3d"), j * 2 + 1);
			}

			for (int j = i - 2; j >= 0; --j)
			{
				log.Appendf(TEXT("%3d"), j * 2 + 1);
			}

			log.Append("\n");
		}
		PRINT("숫자 피라미드\n%s", *log);
	}

	void RunProgram_2_12(int answer)
	{
		UpAndDownGame upAndDownGame;
		upAndDownGame.Play(answer);
	}
#pragma endregion

#pragma region Programming Challenges

	// The 3n+1 Problem.
	// #include <map>
	UFUNCTION(BlueprintCallable, Category = "Jipsa")
	int GetMaxCycleLength(int min = 1, int max = 10)
	{
		if (min < 1)
		{
			PRINT_WARNING("min 값은 1보다 커야합니다. min=%d", min);
			return -1;
		}
		else if (max < min)
		{
			PRINT_WARNING("max 값은 min 보다 커야합니다. min=%d, max=%d", min, max);
			return -1;
		}

		PRINT("-------------------------------");

		int maxCycleLength = -1;
		map<int, int> cycleLengthMap;

		int cycleLength;
		for (int i = min; i <= max; ++i)
		{
			cycleLength = GetCycleLength(i, cycleLengthMap);
			if (cycleLength <= 0)
			{
				PRINT_WARNING("%d 의 cycleLength가 음수(%d)로 계산됩니다. 로직 확인이 필요합니다.", i, cycleLength);
				continue;
			}

			PRINT("%d -> cycle length: %d", i, cycleLength);
			cycleLengthMap.insert(make_pair(i, cycleLength));

			if (maxCycleLength < cycleLength)
			{
				maxCycleLength = cycleLength;
			}
		}

		PRINT("%d ~ %d -> max cycle length: %d", min, max, maxCycleLength);
		return maxCycleLength;
	}
	int GetCycleLength(int n, const map<int, int>& cycleLengthMap)
	{
		if (n < 0)
		{
			return -1;
		}

		int cycleLength = 1;
		int number = n;

		map<int, int>::const_iterator it;
		while (1 < number)
		{
			it = cycleLengthMap.find(number);
			if (it != cycleLengthMap.end())
			{
				cycleLength = (cycleLength - 1) + it->second;
				break;
			}

			if (0 == (number % 2))
			{
				//PRINT("%d -> %d", number, (number / 2));
				number = number / 2;
			}
			else
			{
				//PRINT("%d -> %d", number, number * 3 + 1);
				number = number * 3 + 1;
			}

			++cycleLength;
		}

		return cycleLength;
	}
#pragma endregion

#pragma region Singleton
	void TestSingleton()
	{
		TestSingleton_1* testSingleton_1 = TestSingleton_1::GetSingleton();
		TestSingleton_2* testSingleton_2 = TestSingleton_2::GetSingleton();
		
		PRINT("[before] %d %d %d %d", testSingleton_1->GetValue(), testSingleton_2->GetValue(), TestSingleton_1::GetSingleton()->GetValue(), TestSingleton_2::GetSingleton()->GetValue());
		
		testSingleton_1->SetValue(11);
		TestSingleton_2::GetSingleton()->SetValue(22);

		PRINT("[after] %d %d", testSingleton_1->GetValue(), testSingleton_2->GetValue());
	}
#pragma endregion

#pragma region Unreal String
	void TestUnrealString()
	{
		FString myFString = TEXT("FString: 바보");
		
		FText fStringToFText = FText::FromString(myFString);
		FName fStringToFName = FName(*myFString);
		
		FString floatToFString = FString::SanitizeFloat(3.0f);
		FString flooredFloeatToFString = FString::Printf(TEXT("%.2f"), 12.34567f); // 소수점 2째자리 까지 표현.

		FString intToFString = FString::FromInt(3);
		UE_LOG(LogTemp, Display, TEXT("%s"), *myFString); // NOTE: Formatting string must be a TCHAR array.
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, myFString);

		int32 myInt32 = 32;
		FString myFString2 = FString::Printf(TEXT("%s %i"), *myFString, myInt32);
		UE_LOG(LogTemp, Display, TEXT("%s"), *myFString2);
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, myFString2);

		// 현지화 텍스트 용이다? 사용법? 번역 시트 관리는?
		FText myFText = NSLOCTEXT("MyNamespace", "MyKey", "FText 바보");
		FString fTextToFString = myFText.ToString();
		UE_LOG(LogTemp, Display, TEXT("%s"), *myFText.ToString());
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, myFText.ToString());

		TCHAR myTChar('A');
		TCHAR myTChar2 = FChar::ToLower(myTChar);
		UE_LOG(LogTemp, Display, TEXT("%c %c"), myTChar, myTChar2);
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, FString::Printf(TEXT("%c %c"), myTChar, myTChar2));

		// [FName]
		// 문자열이 재사용된다 해도 데이터 테이블에 한 번만 저장된다. 반드시 문자열 Compare 연산이 필요한 경우 이용할 수 있다.
		// 대소문자를 구분하지 않는다. FString에서 FName으로 변환이 가능하지만 대소문자 소실이 되는점은 기억해야 한다.
		// 변경이 불가능하다.
		FName myFName = FName(TEXT("MyFName"));
		FString fNameToFString = myFName.ToString();
		FText fNameToFText = FText::FromName(myFName);
		
		// 작으면 음수, 같으면 제로, 크면 양수.
		FName myFName2 = FName(TEXT("MyFName"));
		bool isSame = (0 == myFName.Compare(myFName2));

		PRINT("%s %s %s %d", *myFName.ToString(), *fNameToFString, *fNameToFText.ToString(), isSame);
	}
#pragma endregion

#pragma region std::string
	// TODO: FString으로 변환해봐야 한다.
	void TestString()
	{
		string s1, s2 = "Game";
		PRINT("s1.size=%d, s2.size=%d", s1.size(), s2.size()); // 0 4

		s1 = s2 + ' ' + "Over";
		if ("Game Over" == s1)
		{
			PRINT("+연산자로 만든 글자는 Game Over입니다.");
		}

		// 찾지 못하면, string::npos 반환됨. (= static_cast<size_type>(-1))
		//size_type findIndex = s1.find("Over");
		int32 findIndex = s1.find("Over");
		PRINT("findIndex=%d", findIndex); // 5

		findIndex = s1.find("e", 4);
		PRINT("findIndex=%d", findIndex); // 7

		char firstCharacter = s1[0];
		PRINT("firstCharacter=%c", firstCharacter);

		// string에 있는 문자열의 주소가 반환됨.
		PRINT("%s", UTF8_TO_TCHAR(s1.c_str()));
	}
#pragma endregion

#pragma region Unreal STL
	void TestTArray()
	{
		// TArray ~= std::vector.

		///////////////////////////////////////////////////////////////////////////////
		// std::vector.
		///////////////////////////////////////////////////////////////////////////////
		// 초기화.
		vector<int> vec(10);
		for (int i = 0; i < vec.size(); ++i)
		{
			vec[i] = rand() % 100;
		}
		for (int i = 0; i < 5; ++i)
		{
			vec.push_back(rand() % 100); // 처음 선언한 갯수를 넘어가면 push_back으로 넣어주자. 동적 할당된다.
		}
		PrintContainer(vec, "init vector");

		// 반복자 사용.
		for (auto it = vec.begin(); it != vec.end(); )
			//for (vector<int>::iterator it = vec.begin(); it != vec.end(); )
		{
			if (0 == (*it % 2))
			{
				it = vec.erase(it);
			}
			else
			{
				++it;
			}
		}
		PrintContainer(vec, "erase even number");

		// 초기화 (2).
		int arr[] = { 12, 3, 17, 8 };
		vector<int> vec2(arr, arr + 4);
		PrintContainer(vec2, "init vector by array");

		///////////////////////////////////////////////////////////////////////////////
		// TArray.
		///////////////////////////////////////////////////////////////////////////////
		TArray<int> myTArray;
		for (int i = 0; i < 10; ++i)
		{
			//myTArray.Add(rand() % 100);
			myTArray.Emplace(rand() % 100);
		}

		FString log;
		for (auto& value : myTArray)
		{
			log.Appendf(TEXT("%d, "), value);
		}
		PRINT("init TArray: %s", *log);

		// RemoveSingle() 호출할때 다시 순회할듯.. it를 사용해서 바로 지우는 방법이 없을까?
		//for (auto it = myTArray.CreateConstIterator(); it;)
		//{
		//	if (0 == (*it % 2))
		//	{
		//		myTArray.RemoveSingle(*it); // 다시 순회할듯..
		//	}
		//	else
		//	{
		//		++it;
		//	}
		//}

		for (int i = 0; i != myTArray.Num();)
		{
			if (0 == (myTArray[i] % 2))
			{
				myTArray.RemoveAt(i);
			}
			else
			{
				++i;
			}
		}

		log.Empty();
		for (int i = 0; i != myTArray.Num(); ++i)
		{
			log.Appendf(TEXT("%d, "), myTArray[i]);
		}
		PRINT("remove even number: %s", *log);

		int arr2[] = { 12, 3, 17, 8 };
		TArray<int> myTArray2;
		myTArray2.Append(arr2, UE_ARRAY_COUNT(arr2));
	}

	void TestTArrayAndSortingAlgorithm()
	{
		///////////////////////////////////////////////////////////////////////////////
		// <algorithm>
		///////////////////////////////////////////////////////////////////////////////
		vector<int> u(7), v(7), w(14);

		generate(v.begin(), v.end(), FuncObjRandRange(1, 7));
		PrintContainer(v, "generate");

		sort(v.begin(), v.end());
		PrintContainer(v, "sort");

		bool b6 = binary_search(v.begin(), v.end(), 6);
		PRINT("binary_search 6: %d", b6);

		generate(u.begin(), u.end(), FuncObjSequenceGenerator(3));
		PrintContainer(u, "generate");

		merge(v.begin(), v.end(), u.begin(), u.end(), w.begin());
		PrintContainer(w, "merge");

		make_heap(v.begin(), v.end());
		PrintContainer(v, "make_heap");

		pop_heap(v.begin(), v.end());
		v.pop_back();
		PrintContainer(v, "pop_heap");

		v.push_back(8);
		push_heap(v.begin(), v.end());
		PrintContainer(v, "push_heap");

		sort_heap(v.begin(), v.end());
		PrintContainer(v, "sort_heap");

		///////////////////////////////////////////////////////////////////////////////
		// unreal alrorithm.
		///////////////////////////////////////////////////////////////////////////////
		PRINT("-------------------------------");
		FuncObjRandRange randRange(1, 7);

		TArray<int> myTArray;
		for (int i = 0; i < 7; ++i)
		{
			myTArray.Emplace(randRange());
		}

		FString log;
		for (auto& value : myTArray)
		{
			log.Appendf(TEXT("%d, "), value);
		}
		PRINT("init TArray: %s", *log);

		myTArray.Sort();

		log.Empty();
		for (auto& value : myTArray)
		{
			log.Appendf(TEXT("%d, "), value);
		}
		PRINT("sort TArray: %s", *log);

		myTArray.Heapify();

		log.Empty();
		for (auto& value : myTArray)
		{
			log.Appendf(TEXT("%d, "), value);
		}
		PRINT("make heap: %s", *log);

		int32 popedValue;
		myTArray.HeapPop(popedValue);

		log.Empty();
		for (auto& value : myTArray)
		{
			log.Appendf(TEXT("%d, "), value);
		}
		PRINT("pop from heap: %s", *log);

		int32 indexOf8 = myTArray.HeapPush(8);

		log.Empty();
		for (auto& value : myTArray)
		{
			log.Appendf(TEXT("%d, "), value);
		}
		PRINT("push 8 to heap: %s", *log);
	}

	void TestTArrayAndNonMutationAlgorithm()
	{
		///////////////////////////////////////////////////////////////////////////////
		// <algorithm>
		///////////////////////////////////////////////////////////////////////////////
		vector<int> u(10), v(10), w(10), x(3);
		vector<int>::iterator it;

		generate(v.begin(), v.end(), FuncObjRandRange(1, 7));
		PrintContainer(v, "generate");

		PRINT("count of 3 = %d", (int)count(v.begin(), v.end(), 3));
		PRINT("min_element = %d", *min_element(v.begin(), v.end()));
		PRINT("max_element = %d", *max_element(v.begin(), v.end()));

		it = find(v.begin(), v.end(), 3);
		if (it != v.end())
		{
			int index = it - v.begin();
			PRINT("index of 3 = %d", index);
		}

		for (it = v.begin(); ; ++it)
		{
			it = find_if(it, v.end(), FuncObjValueFinder(6));
			if (it == v.end())
			{
				break;
			}

			int index = it - v.begin();
			PRINT("index of 6 = %d", index);
		}

		copy_n(v.begin() + 3, 3, x.begin());
		PrintContainer(x, "copy_n");

		it = search(v.begin(), v.end(), x.begin(), x.end());
		if (it != v.end())
		{
			int index = it - v.begin();
			PRINT("searched index = %d", index);
		}

		///////////////////////////////////////////////////////////////////////////////
		// unreal alrorithm.
		///////////////////////////////////////////////////////////////////////////////
		PRINT("-------------------------------");
		FuncObjRandRange randRange(1, 7);

		TArray<int> myTArray;
		for (int i = 0; i < 10; ++i)
		{
			myTArray.Emplace(randRange());
		}

		FString log;
		for (auto& value : myTArray)
		{
			log.Appendf(TEXT("%d, "), value);
		}
		PRINT("init TArray: %s", *log);

		bool isContained = myTArray.Contains(5);
		PRINT("is contained 5: %d", isContained);

		int32 indexOf3;
		if (true == myTArray.Find(3, indexOf3))
		{
			PRINT("index of 3 = %d", indexOf3);
		}
	}

	void TestTArrayAndMutationAlgorithm()
	{
		///////////////////////////////////////////////////////////////////////////////
		// <algorithm>
		///////////////////////////////////////////////////////////////////////////////
		vector<int> u(8), v(8), w(8), y(3);
		list<int> a(8);

		generate(u.begin(), u.end(), FuncObjRandRange(1, 9));
		PrintContainer(u, "generate + FuncObjRandRange(1, 9)");

		copy(u.begin(), u.end(), v.begin());
		PrintContainer(v, "copy");

		reverse(v.begin(), v.end());
		PrintContainer(v, "reverse");

		//random_shuffle(v.begin(), v.end());
		//PrintContainer(v, "random shuffle");

		rotate(v.begin(), v.begin() + 2, v.end());
		PrintContainer(v, "rotate");

		transform(v.begin(), v.end(), w.begin(), Square);
		PrintContainer(w, "transform + inline");

		partition(w.begin(), w.end(), IsOdd);
		PrintContainer(w, "partition + inline");

		generate(a.begin(), a.end(), FuncObjSequenceGenerator(1));
		PrintContainer(a, "generate + FuncObjSequenceGenerator(1)");

		auto it = remove(a.begin(), a.end(), 3);
		PrintContainer(a, "remove");

		a.erase(it);
		PrintContainer(a, "erase");

		a.erase(remove_if(a.begin(), a.end(), IsOdd), a.end());
		PrintContainer(a, "remove_if + erase");

		generate(y.begin(), y.end(), FuncObjSequenceGenerator(1));
		PrintContainer(y, "generate + FuncObjSequenceGenerator(1)");

		while (true == next_permutation(y.begin(), y.end()))
		{
			PrintContainer(y, "next_permutation");
		}

		///////////////////////////////////////////////////////////////////////////////
		// unreal alrorithm.
		///////////////////////////////////////////////////////////////////////////////
		PRINT("-------------------------------");
		FuncObjRandRange randRange(1, 9);

		TArray<int> myTArray;
		for (int i = 0; i < 8; ++i)
		{
			myTArray.Emplace(randRange());
		}

		FString log;
		for (auto& value : myTArray)
		{
			log.Appendf(TEXT("%d, "), value);
		}
		PRINT("init TArray: %s", *log);

		if (0 < myTArray.Remove(3))
		{
			PRINT("value 3 is removed");
		}
		else
		{
			PRINT("value 3 isn't removed");
		}

		// TODO: 인덱스 2가 없으면 런타임 오류가 발생한다. 에러 처리 필요.
		myTArray.RemoveAt(2);
		PRINT("index 2 is removed");
	}

	void TestTMap()
	{
		// TMap ~= std::map.

		///////////////////////////////////////////////////////////////////////////////
		// std::map.
		///////////////////////////////////////////////////////////////////////////////
		map<FString, FString> myDictionary;
		map<FString, FString>::iterator it;
		myDictionary["hello"] = TEXT("안녕하세요?");
		myDictionary["world"] = TEXT("아름다운 세상");
		myDictionary["data"] = TEXT("자료");
		myDictionary["structure"] = TEXT("구조");
		myDictionary["list"] = TEXT("리스트");

		FString log;
		for (it = myDictionary.begin(); it != myDictionary.end(); ++it)
		{
			log.Appendf(TEXT("(%s, %s), "), *it->first, *it->second);
		}
		PRINT("init map: %s", *log);

		it = myDictionary.find("structure");
		if (it == myDictionary.end())
		{
			PRINT("can't find structure");
		}
		else
		{
			PRINT("find structure: (%s, %s)", *it->first, *it->second);
		}

		myDictionary.erase("structure");

		log.Empty();
		for (it = myDictionary.begin(); it != myDictionary.end(); ++it)
		{
			log.Appendf(TEXT("(%s, %s), "), *it->first, *it->second);
		}
		PRINT("erase structure: %s", *log);

		///////////////////////////////////////////////////////////////////////////////
		// TMap.
		// *중복 키를 허용하는 TMultiMap도 이용 가능하다.
		///////////////////////////////////////////////////////////////////////////////
		TMap<FString, FString> myTMap;

		myTMap.Add("hello", TEXT("안녕하세요?"));
		myTMap.Emplace("world", TEXT("아름다운 세상"));
		myTMap.Emplace("data", TEXT("자료"));
		myTMap.Emplace("structure", TEXT("구조"));
		myTMap.Emplace("list", TEXT("리스트"));

		log.Empty();
		for (auto& keyValue : myTMap)
		{
			log.Appendf(TEXT("(%s, %s), "), *keyValue.Key, *keyValue.Value);
		}
		PRINT("init map: %s", *log);

		if (true == myTMap.Contains("structure"))
		{
			PRINT("find structure: (%s, %s)", TEXT("structure"), *myTMap["structure"]);
		}
		else
		{
			PRINT("can't find structure");
		}

		int32 removedCount = myTMap.Remove("structure");

		log.Empty();
		for (auto iterator = myTMap.CreateConstIterator(); iterator; ++iterator)
		{
			log.Appendf(TEXT("(%s, %s), "), *iterator.Key(), *iterator.Value());
		}
		PRINT("erase structure: %s", *log);
	}

	void TestTSet()
	{
		// TSet ~= std::set.

		///////////////////////////////////////////////////////////////////////////////
		// std::set.
		///////////////////////////////////////////////////////////////////////////////
		set<int> simple;
		multiset<int> multiple;

		for (int i = 10; i < 15; ++i)
		{
			simple.insert(i);
		}

		multiple.insert(simple.begin(), simple.end());
		for (int i = 13; i < 18; ++i)
		{
			multiple.insert(i);
		}

		PrintContainer(simple, "Simple Set: ");
		PRINT("count of 14: %d", simple.count(14));
		PrintContainer(multiple, "Multiple Set: ");
		PRINT("count of 14: %d", multiple.count(14));

		///////////////////////////////////////////////////////////////////////////////
		// TSet.
		// *multiSet은 없다. 중복 방지를 하고 싶을 때 TArray대신 사용하면 괜찮다.
		///////////////////////////////////////////////////////////////////////////////
		TSet<int> myTSet;
		
		for (int i = 10; i < 15; ++i)
		{
			//myTSet.Add(i);
			myTSet.Emplace(i);
		}

		for (auto iter = myTSet.CreateIterator(); iter; ++iter)
		{
			if (12 == *iter)
			{
				iter.RemoveCurrent();
			}
		}

		FString log;
		for (auto iter = myTSet.CreateIterator(); iter; ++iter)
		{
			log.Appendf(TEXT("%d, "), *iter);
		}
		PRINT("remove 12: %s", *log);

		TArray<int> myTArray = myTSet.Array();
	}
#pragma endregion

#pragma region STL(Standard Template Library)
	/*
	//#include <vector>
	//#include <list>
	//#include <algorithm>

	//inline int randRange1to6() { return ((rand() % 6) + 1); }
	//inline void print(int val) { UE_LOG(LogTemp, Display, TEXT("%d"), val); }
	
	void TestGenerateAlgorithm()
	{
		vector<int> u(6), v(6);
		list<int> a(6), b(6);

		generate(u.begin(), u.end(), rand);
		PrintContainer(u, "vector + rand");

		fill_n(u.begin() + 2, 3, 11);
		PrintContainer(u, "vector + fill_n", true);

		generate(v.begin(), v.end(), RandRange(10, 16));
		PrintContainer(v, "vector + Function Object(RandRange)", true);

		generate(a.begin(), a.end(), randRange1to6);
		PrintContainer(a, "list + inline", true);

		generate(b.begin(), b.end(), FuncObjSequenceGenerator(1));
		PrintContainer(b, "list + Function Object(FuncObjSequenceGenerator)", true);

		ShowLog("", "list + for_each + inline", true);
		for_each(b.begin(), b.end(), print);
	}
	*/

	/*
	// #include <queue>
	void TestPriorityQueue()
	{
		priority_queue<double> pque;

		double val;
		for (int i = 0; i < 10; ++i)
		{
			val = rand() % 1000 * 0.1;
			ShowLog(val);

			pque.push(val);
		}

		ShowLog("", "", true);
		while (false == pque.empty())
		{
			ShowLog(pque.top());
			pque.pop();
		}
	}
	*/

	/*
	// #include <queue>
	void TestQueue(int count)
	{
		queue<int> que;
		que.push(0);
		que.push(1);

		for (int i = 0; i < count; ++i)
		{
			int fibonacci = que.front();
			que.pop();

			ShowLog(fibonacci);

			que.push(fibonacci + que.front());
		}
	}
	*/

	/*
	// #include <stack>
	void TestStack(FString statement)
	{
		stack<char> st;
		for (int i = 0; i < statement.Len(); ++i)
		{
			st.push(statement[i]);
		}

		while (false == st.empty())
		{
			ShowLog(st.top());
			st.pop();
		}
	}
	*/

	/*
	#include <list>
	void TestList()
	{
		list<int> sortList;
		for (int i = 0; i < 10; ++i)
		{
			int val = rand() % 100;
			auto it = sortList.begin();
			for (; it != sortList.end(); ++it)
			{
				if (*it <= val)
				{
					break;
				}
			}
			sortList.insert(it, val);
		}
		PrintContainer(sortList);
	}
	*/

	/*
	// #include <deque>
	void TestDeque()
	{
		deque<int> dq;
		for (int i = 0; i < 10; ++i)
		{
			int val = rand() % 100 + 1;
			if (0 == (val % 2))
			{
				dq.push_back(val);
			}
			else
			{
				dq.push_front(val);
			}
		}
		PrintContainer(dq);
	}
	*/
#pragma endregion

};