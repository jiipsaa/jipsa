﻿#pragma once

#include <deque>
#include <functional> // 내장된 함수 객체.
#include <list>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <vector>
using namespace std;

#include "../Jipsa/Log.h"
#include "FunctionObject.h"

inline int32 Square(const int32& n)
{
	return (n * n);
}

inline bool IsOdd(const int32& n)
{
	return (1 == (n % 2));
}

class STLSample
{
public:
	void TestSortingAlgorithmHeap()
	{
		// TODO: 힙 노드 그리는 연습할때 확인하는 용도로 사용하면 좋겠다.

		FString log;

		vector<int32> v(10);
		generate(v.begin(), v.end(), FuncObjRandRange(1, 100));

		log.Append(TEXT("\n랜덤으로 벡터 초기화: "));
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}

		// 자료구조 힙(heap) 의 룰에 따라 항목들을 재배치한다.
		make_heap(v.begin(), v.end());

		log.Append(TEXT("\n힙의 룰에 따라 재배치: "));
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}

		// 루트 노드를 다운힙(downheap) 과정을 통해 맨 뒤로 이동시킨다. (맨 뒤로 이동한 항목은 삭제하자)
		pop_heap(v.begin(), v.end());
		v.pop_back();

		log.Append(TEXT("\n루트 노드 삭제: "));
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}

		// 마지막 노드를 업힙(upheap) 과정을 통해 적절한 위치로 이동시킨다. (이동시키기 전에 새로운 항목을 삽입하자)
		v.push_back(70);
		push_heap(v.begin(), v.end());

		log.Append(TEXT("\n마지막 노드에 70 삽입: "));
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}

		// 루트 노드부터 순서대로 나오도록 정렬시킨다.
		sort_heap(v.begin(), v.end());

		log.Append(TEXT("\n루트 노드부터 순서대로 정렬: "));
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}

		PRINT("%s", *log);
	}

	void TestSortingAlgorithmMerge()
	{
		vector<int32> v(2), w(3), x(5);
		generate(v.begin(), v.end(), FuncObjSequenceGenerator(10));
		generate(w.begin(), w.end(), FuncObjSequenceGenerator(1));

		// merge는 정렬 되어있는 두 개의 컨테이너를 합치는 알고리즘이다.
		merge(v.begin(), v.end(), w.begin(), w.end(), x.begin());

		FString log;
		for (int32 i = 0; i < x.size(); ++i)
		{
			log.Appendf(TEXT("%d "), x[i]);
		}
		PRINT("%s", *log);
	}

	void TestSortingAlgorithmBinarySearch()
	{
		vector<int32> v(10);
		generate(v.begin(), v.end(), FuncObjSequenceGenerator(1));

		if (true == binary_search(v.begin(), v.end(), 5))
		{
			PRINT("5가 있습니다.");
		}
	}

	void TestSortingAlgorithmSort()
	{
		int32 arr[] = { 1, 3, 2, 4 };
		vector<int32> v(arr, arr + 4);

		// 오름차순 정렬.
		sort(v.begin(), v.end());

		FString log;
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}

		// 내림차순 정렬.
		sort(v.begin(), v.end(), greater<int32>()); // 내장된 함수 객체도 사용할 수 있다.

		log.Append("\n");
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}
		PRINT("%s", *log);
	}

	void TestSequenceAlgorithmSearch()
	{
		vector<int32> v(10);
		generate(v.begin(), v.end(), FuncObjSequenceGenerator(1));

		vector<int32> w(3);
		generate(w.begin(), w.end(), FuncObjSequenceGenerator(5));

		auto it = search(v.begin(), v.end(), w.begin(), w.end());
		if (it != v.end())
		{
			PRINT("{5, 6, 7}이 있습니다. 인덱스는 %d 입니다.", it - v.begin());
		}
	}

	void TestSequenceAlgorithmFind()
	{
		vector<int32> v(10);
		generate(v.begin(), v.end(), FuncObjSequenceGenerator(1));

		auto it = find(v.begin(), v.end(), 3);
		if (it != v.end())
		{
			PRINT("3이 있습니다. 인덱스는 %d 입니다.", it - v.begin());
		}

		FuncObjValueFinder sixValueFinder(6);
		for (it = v.begin(); ; ++it)
		{
			it = find_if(it, v.end(), sixValueFinder);
			if (it == v.end())
			{
				break;
			}

			PRINT("6이 있습니다. 인덱스는 %d 입니다.", it - v.begin());
		}
	}

	void TestSequenceAlgorithmPermutation()
	{
		vector<int32> v(3);
		generate(v.begin(), v.end(), FuncObjSequenceGenerator(1));

		FString log;

		// 가능한 조합을 모두 찾는다.
		do
		{
			for (int32 i = 0; i < v.size(); ++i)
			{
				log.Appendf(TEXT("%d "), v[i]);
			}
			log.Append("\n");
		} while (true == next_permutation(v.begin(), v.end()));

		PRINT("%s", *log);
	}

	void TestSequenceAlgorithmRotate()
	{
		vector<int32> v(10);
		generate(v.begin(), v.end(), FuncObjSequenceGenerator(1));

		rotate(v.begin(), v.begin() + 2, v.end()); // index 2 의 항목이 맨 앞에 오도록 회전시킨다.

		FString log;
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}
		PRINT("%s", *log);
	}

	void TestSequenceAlgorithmPartition()
	{
		vector<int32> v(10);
		generate(v.begin(), v.end(), FuncObjSequenceGenerator(1));

		// partition은 함수가 true를 리턴하는 항목을 앞으로 옮기는 알고리즘이다.
		partition(v.begin(), v.end(), IsOdd);

		FString log;
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}
		PRINT("%s", *log);
	}

	void TestSequenceAlgorithmTransform()
	{
		vector<int32> v(10), w(10);
		generate(v.begin(), v.end(), FuncObjSequenceGenerator(1));

		// transform은 함수를 각 항목에 적용한 결과를 다른 컨테이너에 삽입시키는 알고리즘이다.
		transform(v.begin(), v.end(), w.begin(), Square);

		FString log;
		for (int32 i = 0; i < w.size(); ++i)
		{
			log.Appendf(TEXT("%d "), w[i]);
		}
		PRINT("%s", *log);
	}

	void TestSequenceAlgorithmForEach()
	{
		int32 arr[] = { 1, 2, 3, 4 };
		list<int32> lst(arr, arr + 4);

		// for_each는 함수를 각 항목에 적용하는 알고리즘이다.
		for_each(lst.begin(), lst.end(), [](int32& n) { n = n + 1; });

		FString log;
		for (auto it = lst.begin(); it != lst.end(); ++it)
		{
			log.Appendf(TEXT("%d "), *it);
		}
		PRINT("%s", *log);
	}

	void TestSequenceAlgorithmFill()
	{
		vector<int32> v(10);
		generate(v.begin(), v.end(), FuncObjSequenceGenerator(1));

		FString log;
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}

		// fill은 항목을 특정 값으로 변경하는 알고리즘이다.
		// index 2 부터 5개의 항목을 100으로 변경한다.
		fill_n(v.begin() + 2, 5, 100);

		log.Append("\n");
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}

		PRINT("%s", *log);
	}

	void TestSequenceAlgorithmGenerate()
	{
		vector<int32> v(10), w(10);

		// 함수 객체. 연산자 ()를 오버로딩한 클래스의 객체이다. 알고리즘 함수의 인자로도 사용되고 있다.
		FuncObjRandRange rand1to6(1, 6);
		
		// generate는 특정한 함수로 컨테이너를 초기화하는 알고리즘이다.
		generate(v.begin(), v.end(), rand1to6);
		generate(w.begin(), w.end(), FuncObjRandRange(1, 2));

		FString log;
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}

		log.Append("\n");
		for (int32 i = 0; i < w.size(); ++i)
		{
			log.Appendf(TEXT("%d "), w[i]);
		}

		PRINT("%s", *log);
	}

	void TestMap()
	{
		// 단어장.
		map<string, string> vocabulary;
		vocabulary["hello"] = "안녕하세요";
		vocabulary["world"] = "세계";
		vocabulary["data"] = "자료";
		vocabulary["structure"] = "구조";

		FString log;

		map<string, string>::iterator it;
		for (it = vocabulary.begin(); it != vocabulary.end(); ++it)
		{
			log.Appendf(TEXT("%s == %s\n"), UTF8_TO_TCHAR(it->first.c_str()), UTF8_TO_TCHAR(it->second.c_str()));
		}

		log.Append("\n");

		it = vocabulary.find("structure");
		if (it != vocabulary.end())
		{
			log.Appendf(TEXT("%s 는 %s 이다.\n"), UTF8_TO_TCHAR(it->first.c_str()), UTF8_TO_TCHAR(it->second.c_str()));
		}

		// map은 key로 value를 다룰 수 있다.
		vocabulary.erase("structure");

		it = vocabulary.find("structure");
		if (it != vocabulary.end())
		{
			log.Appendf(TEXT("find %s = %s\n"), UTF8_TO_TCHAR(it->first.c_str()), UTF8_TO_TCHAR(it->second.c_str()));
		}
		else
		{
			log.Append(TEXT("structure 는 삭제됬다.\n"));
			for (it = vocabulary.begin(); it != vocabulary.end(); ++it)
			{
				log.Appendf(TEXT("%s == %s\n"), UTF8_TO_TCHAR(it->first.c_str()), UTF8_TO_TCHAR(it->second.c_str()));
			}
		}

		PRINT("%s", *log);
	}

	void TestSet()
	{
		set<int32> s;
		for (int32 i = 10; i < 15; ++i)
		{
			s.insert(i);
		}

		// MultiSet은 항목의 중복을 허용한다.
		multiset<int32> ms;
		ms.insert(s.begin(), s.end());
		for (int32 i = 13; i < 18; ++i)
		{
			ms.insert(i);
		}

		FString log;

		for (auto it = s.begin(); it != s.end(); ++it)
		{
			log.Appendf(TEXT("%d "), *it);
		}

		// 연관 컨테이너(set, multiset, map, multimap)는 항목들이 정렬되어 있다. 
		log.Append("\n");
		for (auto it = ms.begin(); it != ms.end(); ++it)
		{
			log.Appendf(TEXT("%d "), *it);
		}

		log.Appendf(TEXT("\nSet에서 14의 갯수: %d: "), s.count(14));
		log.Appendf(TEXT("\nMultiSet에서 14의 갯수: %d: "), ms.count(14));

		PRINT("%s", *log);
	}

	void TestPriorityQueue()
	{
		priority_queue<float> pq;

		float val;
		FString log;

		for (int32 i = 0; i < 10; ++i)
		{
			val = FMath::RandRange(0.0f, 99.9f);
			log.Appendf(TEXT("%.2f "), val);
			
			pq.push(val);
		}

		log.Append("\n");
		while (false == pq.empty())
		{
			log.Appendf(TEXT("%.2f "), pq.top());
			pq.pop();
		}

		PRINT("%s", *log);
	}

	void TestQueue()
	{
		// 피보나치수열.
		// 0 1 1 2 3 5 8 13 21
		queue<int32> q;
		q.push(0);
		q.push(1);

		FString log;
		for (int32 i = 0; i < 10; ++i)
		{
			int32 fibonacci = q.front();
			q.pop();

			log.Appendf(TEXT("%d "), fibonacci);

			q.push(fibonacci + q.front());
		}

		PRINT("%s", *log);
	}

	void TestStack()
	{
		stack<TCHAR> st;
		FString line = TEXT("입력된 문장");
		for (int32 i = 0; i < line.Len(); ++i)
		{
			st.push(line[i]);
		}

		FString log = FString::Printf(TEXT("%s\n"), *line);

		// 문장을 뒤집어서 출력한다.
		while (false == st.empty())
		{
			log.Appendf(TEXT("%c"), st.top());
			st.pop();
		}

		PRINT("%s", *log);
	}

	void TestList()
	{
		list<int32> sortList;
		for (int32 i = 0; i < 10; ++i)
		{
			int32 val = FMath::RandRange(0, 99);

			// 내림차순으로 정렬 되도록 insert 위치를 찾는다.
			auto it = sortList.begin();
			for (; it != sortList.end(); ++it)
			{
				if (*it <= val)
				{
					break;
				}
			}

			sortList.insert(it, val);
		}

		FString log;
		for (auto it = sortList.begin(); it != sortList.end(); ++it)
		{
			log.Appendf(TEXT("%d "), *it);
		}
		PRINT("%s", *log);
	}

	void TestDeque()
	{
		// 덱은 맨 앞에서 삽입이나 삭제할 수 있다.
		deque<int32> dq;
		for (int32 i = 0; i < 10; ++i)
		{
			int32 val = FMath::RandRange(1, 100);
			if (0 == (val % 2))
			{
				dq.push_back(val);
			}
			else
			{
				dq.push_front(val);
			}
		}

		FString log;
		for (int32 i = 0; i < dq.size(); ++i)
		{
			log.Appendf(TEXT("%d "), dq[i]);
		}
		
		log.Append("\n");
		for (auto it = dq.begin(); it != dq.end(); ++it)
		{
			log.Appendf(TEXT("%d "), *it);
		}
		
		PRINT("%s", *log);
	}

	void TestVector2()
	{
		// 배열로 초기화.
		int32 arr[] = { 1, 2, 3, 4 };
		vector<int32> v(arr, arr + 4);

		FString log;
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}
		PRINT("%s", *log);
	}

	void TestVector()
	{
		vector<int32> v(10);
		for (int32 i = 0; i < v.size(); ++i)
		{
			v[i] = FMath::RandRange(0, 99);
		}

		FString log;
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}

		// 비효율적이긴 하지만 Vector도 중간에서 삽입이나 삭제가 가능하다.
		for (vector<int32>::iterator it = v.begin(); it != v.end();)
		{
			// 짝수 항목을 삭제.
			if (0 == (*it % 2))
			{
				it = v.erase(it);
			}
			else
			{
				++it;
			}
		}

		log.Append("\n");
		for (int32 i = 0; i < v.size(); ++i)
		{
			log.Appendf(TEXT("%d "), v[i]);
		}
		PRINT("%s", *log);
	}
};