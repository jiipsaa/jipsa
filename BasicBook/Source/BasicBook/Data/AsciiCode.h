﻿#pragma once

#include "../Jipsa/Singleton.h"
#include "../Jipsa/Log.h"

#include "Runtime/XmlParser/Public/XmlFile.h"
#include "Runtime/XmlParser/Public/XmlNode.h"

class AsciiCode {
public:
	int32 decimal;
	FString character;
	FString meaning;

	AsciiCode(int32 _decimal, FString _character, FString _meaning) : decimal(_decimal), character(_character), meaning(_meaning) {}
};

class AsciiCodeList : public Singleton<AsciiCodeList> {
private:
	TArray<AsciiCode> asciiCodeList;

public:
	AsciiCodeList() {}

	const TArray<AsciiCode>& GetData()
	{
		if (0 == asciiCodeList.Num())
		{
			LoadXml();
		}

		return asciiCodeList;
	}

	// TODO: 한동안 사용 안하는 데이터면 자동으로 메모리에서 내리고 싶은데.. 일단 명시적으로 처리해본다..
	void EmptyData()
	{
		if (0 < asciiCodeList.Num())
		{
			asciiCodeList.Empty();
		}
	}

private:
	void LoadXml() {
		FString inFile = FString::Printf(TEXT("%sData/AsciiCode.xml"), *FPaths::ProjectContentDir());
		FXmlFile xmlFile(inFile);
		if (false == xmlFile.IsValid())
		{
			PRINT_ERROR("%s", *xmlFile.GetLastError());
			return;
		}

		FXmlNode* rootNode = xmlFile.GetRootNode();
		if (nullptr == rootNode)
		{
			PRINT_ERROR("fail GetRootNode().. filePath=%s", *inFile);
			return;
		}

		const FXmlNode* childNode = rootNode->GetFirstChildNode();
		while (nullptr != childNode)
		{
			asciiCodeList.Emplace(
				FCString::Atoi( *childNode->GetAttribute(TEXT("Decimal")) )
				,childNode->GetAttribute(TEXT("Character"))
				,childNode->GetAttribute(TEXT("Meaning"))
			);

			childNode = childNode->GetNextNode();
		}
	}
};