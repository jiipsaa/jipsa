﻿#include "Puzzle.h"
#include "../Jipsa/Log.h"

void Puzzle::Play()
{
	isAbleToInput = false;

	// 퍼즐 피스를 순서대로 초기화한다.
	int32 pieceCount = DIM * DIM - 1;
	for (int32 i = 0; i < pieceCount; ++i)
	{
		map[i / DIM][i % DIM] = i + 1;
	}

	// 마지막 피스는 비워둔다.
	x = y = DIM - 1;
	map[x][y] = 0;

	// 퍼즐 피스를 섞는다.
	Suffle(100);

	puzzleRanking = new PuzzleRanking();
	if (nullptr != puzzleRanking)
	{
		puzzleRanking->Load();
		puzzleRanking->Print();
	}

	Display(false);

	moveCount = 0;
	startClock = clock();

	isAbleToInput = true;
}

void Puzzle::Input(const ePuzzleDirection& direction)
{
	if (false == isAbleToInput)
	{
		return;
	}

	if (false == Move(direction))
	{
		return;
	}
	++moveCount;

	if (true == IsDone())
	{
		isAbleToInput = false;

		if (nullptr != puzzleRanking)
		{
			clock_t endClock = clock();
			double playTime = (double)(endClock - startClock) / CLOCKS_PER_SEC;
			puzzleRanking->Add(moveCount, playTime);
			puzzleRanking->Save();
		}
	}

	Display();
}

void Puzzle::Display(bool isShowPlayInfo)
{
	FString log;
	log.Append("\n");

	for (int32 r = 0; r < DIM; ++r)
	{
		for (int32 c = 0; c < DIM; ++c)
		{
			if (0 < map[r][c])
			{
				log.Appendf(TEXT("%3d"), map[r][c]);
			}
			else
			{
				log.Append("   ");
			}
		}
		log.Append("\n");
	}

	if (true == isShowPlayInfo)
	{
		clock_t currentClock = clock();
		double playTime = (double)(currentClock - startClock) / CLOCKS_PER_SEC;
		log.Appendf(TEXT("\n이동 횟수: %6d, 소요 시간: %6.1lf"), moveCount, playTime);
	}

	PRINT("%s", *log);
}

void Puzzle::Suffle(int suffleCount)
{
	const int32 DIRECTION_COUNT = static_cast<int32>(ePuzzleDirection::Count);
	for (int32 i = 0; i < suffleCount; ++i)
	{
		int32 random = FMath::RandRange(0, DIRECTION_COUNT - 1);
		ePuzzleDirection randomDirection = static_cast<ePuzzleDirection>(random);
		
		if (false == Move(randomDirection))
		{
			--i;
			continue;
		}
	}
}

bool Puzzle::Move(const ePuzzleDirection& direction)
{
	if ((ePuzzleDirection::Left == direction) && (x < (DIM - 1)))
	{
		map[y][x] = map[y][x + 1];
		map[y][x + 1] = 0;
		++x;
	}
	else if ((ePuzzleDirection::Right == direction) && (0 < x))
	{
		map[y][x] = map[y][x - 1];
		map[y][x - 1] = 0;
		--x;
	}
	else if ((ePuzzleDirection::Up == direction) && (y < (DIM - 1)))
	{
		map[y][x] = map[y + 1][x];
		map[y + 1][x] = 0;
		++y;
	}
	else if ((ePuzzleDirection::Down == direction) && (0 < y))
	{
		map[y][x] = map[y - 1][x];
		map[y - 1][x] = 0;
		--y;
	}
	else
	{
		return false;
	}

	return true;
}

bool Puzzle::IsDone()
{
	for (int32 r = 0; r < DIM; ++r)
	{
		for (int32 c = 0; c < DIM; ++c)
		{
			if (map[r][c] != (r * DIM + c + 1))
			{
				return ((r == DIM - 1) && (c == DIM - 1));
			}
		}
	}

	return true;
}