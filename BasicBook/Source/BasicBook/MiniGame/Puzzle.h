﻿#pragma once

#include <ctime>

#include "Data/PuzzleRanking.h"

#define DIM 4

enum class ePuzzleDirection : uint8
{
	Left
	,Right
	,Up
	,Down
	,Count
};

class Puzzle
{
	// TODO:
	// Suffle하는 모습을 보여준다.
	// 퍼즐 사이즈를 정할 수 있게 한다.
	// 리플레이 기능을 추가한다.
	// 2D 버전을 만든다.
	// 3D 버전을 만든다.

protected:
	bool isAbleToInput = false;

	int32 map[DIM][DIM];
	int32 x, y; // NOTE: 행 = y, 열 = x.

	int32 moveCount;
	clock_t startClock;
	
	PuzzleRanking* puzzleRanking = nullptr;

public:
	void Play();
	void Input(const ePuzzleDirection& direction);

protected:
	void Display(bool isShowPlayInfo = true);
	void Suffle(int suffleCount);
	bool Move(const ePuzzleDirection& direction);
	bool IsDone();
};