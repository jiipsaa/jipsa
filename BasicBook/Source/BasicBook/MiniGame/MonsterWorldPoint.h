﻿#pragma once

class MonsterWorldPoint
{
private:
	int32 x, y;

	// TODO: x, y 조작하기 위해서 프랜드 선언을 했다.
	// 차라리 Set(x, y) 이나 Move(x, y), Add(dX, dY) 함수를 만들어서 쓰는게 좋다.
	friend class Monster;
	friend class Vampire;
	friend class Jiangshi;
	friend class MonsterWorldPlayer;

public:
	MonsterWorldPoint(const int32& _x = 0, const int32& _y = 0) : x(_x), y(_y) {}

	// 빗변 구하기.
	// TODO: 사용 안하는 방향으로 고려해보자. GetHypotenuse()
	operator float()
	{
		return sqrt(static_cast<float>(x * x) + (y * y));
	}
	float GetHypotenuse()
	{
		return sqrt(static_cast<float>(x * x) + (y * y));
	}

	// TODO: 사용 안하는 방향으로 고려해보자. Clip()
	void operator()(const int32& maxX, const int32& maxY)
	{
		if (x < 0)
		{
			x = 0;
		}
		else if (maxX <= x)
		{
			x = maxX - 1;
		}

		if (y < 0)
		{
			y = 0;
		}
		else if (maxY <= y)
		{
			y = maxY - 1;
		}
	}
	void Clip(const int32& maxX, const int32& maxY)
	{
		if (x < 0)
		{
			x = 0;
		}
		else if (maxX <= x)
		{
			x = maxX - 1;
		}

		if (y < 0)
		{
			y = 0;
		}
		else if (maxY <= y)
		{
			y = maxY - 1;
		}
	}

	MonsterWorldPoint operator-()
	{
		return MonsterWorldPoint(-x, -y);
	}

	bool operator==(const MonsterWorldPoint& p)
	{
		return ((x == p.x) && (y == p.y));
	}

	bool operator!=(const MonsterWorldPoint& p)
	{
		return ((x != p.x) || (y != p.y));
	}

	MonsterWorldPoint operator-(const MonsterWorldPoint& p)
	{
		return MonsterWorldPoint(x - p.x, y - p.y);
	}

	MonsterWorldPoint operator+(const MonsterWorldPoint& p)
	{
		return MonsterWorldPoint(x + p.x, y + p.y);
	}

	//MonsterWorldPoint& operator+=(const MonsterWorldPoint& p)
	void operator+=(const MonsterWorldPoint& p)
	{
		x += p.x;
		y += p.y;
		//return *this;
	}

	//MonsterWorldPoint& operator-=(const MonsterWorldPoint& p)
	void operator-=(const MonsterWorldPoint& p)
	{
		x -= p.x;
		y -= p.y;
		//return *this;
	}
};