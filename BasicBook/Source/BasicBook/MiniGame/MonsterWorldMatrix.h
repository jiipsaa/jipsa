//#pragma once
//
//#include "../Jipsa/Log.h"
//
//class MonsterWorldMatrix
//{
//private:
//	int32 rows, cols;
//	int32** matrix;
//
//public:
//	MonsterWorldMatrix(int32 _rows = 0, int32 _cols = 0) : rows(_rows), cols(_cols), matrix(NULL)
//	{
//		matrix = new int32 * [rows];
//		for (int32 i = 0; i < rows; ++i)
//		{
//			matrix[i] = new int32[cols];
//		}
//	}
//	~MonsterWorldMatrix()
//	{
//		if (NULL != matrix)
//		{
//			for (int32 i = 0; i < rows; ++i)
//			{
//				delete[] matrix[i];
//			}
//			delete[] matrix;
//		}
//	}
//
//	int32& Elem(const int32& x, const int32& y)
//	{
//		return matrix[y][x];
//	}
//	int32& operator()(const int32& x, const int32& y)
//	{
//		return matrix[y][x];
//	}
//
//	int32 GetRows()
//	{
//		return rows;
//	}
//
//	int32 GetCols()
//	{
//		return cols;
//	}
//
//	int32** GetMatrix()
//	{
//		return matrix;
//	}
//
//	void Print()
//	{
//		FString log;
//		log.Appendf(TEXT("Matrix %d x %d\n"), rows, cols);
//		for (int32 r = 0; r < rows; ++r)
//		{
//			for (int32 c = 0; c < cols; ++c)
//			{
//				log.Appendf(TEXT("%4d"), matrix[r][c]);
//			}
//			log.Append("\n");
//		}
//		PRINT("%s", *log);
//	}
//
//	void SetRandom(int32 maxValue = 100)
//	{
//		if (NULL == matrix)
//		{
//			return;
//		}
//
//		for (int32 r = 0; r < rows; ++r)
//		{
//			for (int32 c = 0; c < cols; ++c)
//			{
//				matrix[r][c] = FMath::RandRange(0, maxValue);
//			}
//		}
//	}
//};