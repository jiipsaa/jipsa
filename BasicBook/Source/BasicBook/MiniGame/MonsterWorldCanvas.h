﻿#pragma once

#include <vector>
using namespace std;

#include "../Jipsa/Log.h"

class MonsterWorldCanvas
{
private:
	vector<vector<char>> canvasMap;
	int32 maxX, maxY;

public:
	MonsterWorldCanvas(const int32& _maxX = 10, const int32& _maxY = 10) : canvasMap(_maxY), maxX(_maxX), maxY(_maxY)
	{
		for (int32 y = 0; y < maxY; ++y)
		{
			for (int32 x = 0; x < maxX; ++x)
			{
				canvasMap[y].push_back(' ');
			}
		}
	}

	void Draw(const int32& x, const int32& y, const char& c)
	{
		if (0 <= x && x < maxX && 0 <= y && y < maxY)
		{
			canvasMap[y][x] = c;
		}
		//else
		//{
		//	PRINT_WARNING("잘못된 위치에 Draw() 하려고 합니다. x=%d, y=%d, c=%c", x, y, c);
		//}
	}

	void Clear()
	{
		for (int32 y = 0; y < maxY; ++y)
		{
			for (int32 x = 0; x < maxX; ++x)
			{
				Draw(x, y, ' ');
			}
		}
	}

	void Print(const char* title = "[Title]")
	{
		FString log;

		log.Appendf(TEXT("%s\n"), UTF8_TO_TCHAR(title));

		for (int y = 0; y < maxY; ++y)
		{
			for (int x = 0; x < maxX; ++x)
			{
				log.Appendf(TEXT("%c"), canvasMap[y][x]);
			}
			log.Append("\n");
		}

		PRINT("%s", *log);
	}
};