﻿#include "MiniGameGameInstance.h"

//#include "../Jipsa/Log.h"

void UMiniGameGameInstance::Init()
{
	Super::Init();
	
	SetPlayMode(eMiniGamePlayMode::Count);
}

bool UMiniGameGameInstance::SetPlayMode(const eMiniGamePlayMode& _playMode)
{
	// TODO Level 2: assert 추가
	// ex) 알 수 없는 ENUM 입니다. PlayMode 변경할 수 없습니다.

	if (playMode == _playMode)
	{
		return false;
	}

	playMode = _playMode;
	return true;
}

eMiniGamePlayMode UMiniGameGameInstance::GetPlayMode()
{
	return playMode;
}

void UMiniGameGameInstance::ToClient_ShowSimpleMessage(FString message)
{
	MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), message);
}