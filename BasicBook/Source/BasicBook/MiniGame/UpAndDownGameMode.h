#pragma once

#include "UpAndDownWindow.h"

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UpAndDownGameMode.generated.h"

//UENUM(BlueprintType)
enum class eUpAndDownState : uint8
{
	EnterAnswer
	,PredictAnswer
	,Result
};

UCLASS()
class BASICBOOK_API AUpAndDownGameMode : public AGameModeBase
{
	GENERATED_BODY()

private:
	eUpAndDownState currentState = eUpAndDownState::EnterAnswer;

	class UUpAndDownWindow* window = nullptr;

	const int32 MIN_ANSWER = 1;
	const int32 MAX_ANSWER = 99;
	FText answer = FText::GetEmpty();

	int32 minPrediction = MIN_ANSWER;
	int32 maxPrediction = MAX_ANSWER;
	FText prediction = FText::GetEmpty();

	const int32 LIMIT_PREDICTION_COUNT = 10;
	int32 predictionCount = 0;

public:
	virtual void StartPlay() override;

	UFUNCTION()
	void OnTextChanged_AnswerEditableTextBox(const FText& text);

	UFUNCTION()
	void OnClicked_GenRandomAnswerButton();

	UFUNCTION()
	void OnClicked_EnterAnswerButton();

	UFUNCTION()
	void OnTextChanged_PredictionEditableTextBox(const FText& text);

	UFUNCTION()
	void OnClicked_EnterPredictionButton();

	UFUNCTION()
	void OnClosed_ResultPopup();

protected:
	bool ChangeState(const eUpAndDownState newState);
};