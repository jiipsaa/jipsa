﻿#pragma once

UENUM()
enum class eMiniGamePlayMode : uint8
{
	Single
	,MultiHost
	,MultiGuest
	,Count
};