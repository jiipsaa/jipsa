﻿#include "MineSweeperWindow_ListEntry.h"

#include "../Jipsa/Log.h"
#include "MineSweeperPlayerController.h"
#include "MineSweeperWindow_ListItemObject.h"

#include "Kismet/GameplayStatics.h"

void UMineSweeperWindow_ListEntry::Reset()
{
	NativeOnListItemObjectSet(GetListItem());
}

void UMineSweeperWindow_ListEntry::SetEnableTouch(const bool& isEnable)
{
	button->SetIsEnabled(isEnable);
}

void UMineSweeperWindow_ListEntry::ShowMineImage()
{
	mineImage->SetVisibility(ESlateVisibility::Visible);
}

void UMineSweeperWindow_ListEntry::ShowEmptyImage(const int32& nearMineCount)
{
	emptyImage->SetVisibility(ESlateVisibility::Visible);
	nearMineCountText->SetText(FText::FromString(FString::FromInt(nearMineCount)));
	nearMineCountText->SetVisibility(ESlateVisibility::Visible);
	
	if (ESlateVisibility::Collapsed != flagImage->GetVisibility())
	{
		flagImage->SetVisibility(ESlateVisibility::Collapsed);
	}
}

void UMineSweeperWindow_ListEntry::NativeOnListItemObjectSet(UObject* ListItemObject)
{
	//UMineSweeperWindow_ListItemObject* listItemObject = Cast<UMineSweeperWindow_ListItemObject>(ListItemObject);
	flagImage->SetVisibility(ESlateVisibility::Collapsed);
	mineImage->SetVisibility(ESlateVisibility::Collapsed);
	emptyImage->SetVisibility(ESlateVisibility::Collapsed);
	nearMineCountText->SetVisibility(ESlateVisibility::Collapsed);
}

void UMineSweeperWindow_ListEntry::ToClient_HoldLeftMouseButton()
{
	UMineSweeperWindow_ListItemObject* listItemObject = GetListItem<UMineSweeperWindow_ListItemObject>();
	if (false == IsValid(listItemObject))
	{
		PRINT_ERROR("%s", TEXT("UMineSweeperWindow_ListItemObject 를 얻을 수 없습니다."));
		return;
	}

	AMineSweeperPlayerController* playerController = Cast<AMineSweeperPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if (false == IsValid(playerController))
	{
		PRINT_ERROR("%s", TEXT("AMineSweeperPlayerController 를 얻을 수 없습니다."));
		return;
	}

	ePlayMapState playMapState = playerController->Flag(listItemObject->x, listItemObject->y);
	if (ePlayMapState::Flag == playMapState)
	{
		flagImage->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
	}
	else if (ePlayMapState::Unkown == playMapState)
	{
		flagImage->SetVisibility(ESlateVisibility::Collapsed);
	}
}

void UMineSweeperWindow_ListEntry::ToClient_ClickLeftMouseButton()
{
	UMineSweeperWindow_ListItemObject* listItemObject = GetListItem<UMineSweeperWindow_ListItemObject>();
	if (false == IsValid(listItemObject))
	{
		PRINT_ERROR("%s", TEXT("UMineSweeperWindow_ListItemObject 를 얻을 수 없습니다."));
		return;
	}

	AMineSweeperPlayerController* playerController = Cast<AMineSweeperPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if (false == IsValid(playerController))
	{
		PRINT_ERROR("%s", TEXT("AMineSweeperPlayerController 를 얻을 수 없습니다."));
		return;
	}

	playerController->Dig(listItemObject->x, listItemObject->y);
}