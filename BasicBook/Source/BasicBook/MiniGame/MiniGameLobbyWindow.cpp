﻿#include "MiniGameLobbyWindow.h"

#include "../Jipsa/Log.h"
#include "MiniGameGameInstance.h"

#include "Interfaces/IHttpResponse.h"
#include "Kismet/GameplayStatics.h"

#if WITH_EDITOR
#include "SocketSubsystem.h"
#endif

void UMiniGameLobbyWindow::NativeConstruct()
{
	Super::NativeConstruct();

	SetPlayMode(eMiniGamePlayMode::Single);
	
	singleBtn->OnClicked.AddDynamic(this, &UMiniGameLobbyWindow::SetPlayModeToSingle);
	hostBtn->OnClicked.AddDynamic(this, &UMiniGameLobbyWindow::SetPlayModeToMultiHost);
	guestBtn->OnClicked.AddDynamic(this, &UMiniGameLobbyWindow::SetPlayModeToMultiGuest);
	joinBtn->OnClicked.AddDynamic(this, &UMiniGameLobbyWindow::JoinSessionByIPAddress);
	refreshExternalIPBtn->OnClicked.AddDynamic(this, &UMiniGameLobbyWindow::RequestExternalIPAddress);
	destroySessionBtn->OnClicked.AddDynamic(this, &UMiniGameLobbyWindow::DestroySession);

	RequestExternalIPAddress();
}

void UMiniGameLobbyWindow::ToClient_AsyncLoadMap(const TSoftObjectPtr<UWorld> map)
{
	// TODO Level 2: assert 추가
	// ex) 알 수 없는 ENUM 입니다. 맵 이동할 수 없습니다.

	UMiniGameGameInstance* gameInstance = Cast<UMiniGameGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (nullptr == gameInstance)
	{
		guideText->SetText(FText::FromString(TEXT("GameInstance 찾을 수 없습니다.")));
		return;
	}

	eMiniGamePlayMode playMode = gameInstance->GetPlayMode();

	if (eMiniGamePlayMode::Single == playMode)
	{
		Super::ToClient_AsyncLoadMap(map);
	}
	else if (eMiniGamePlayMode::MultiHost == playMode)
	{
		// TODO Level 2: C++ 로직으로 변경. (FromClient_TryCreateSession, FromClient_TryJoinSession)
		FromClient_TryCreateSession(map);
	}
}

void UMiniGameLobbyWindow::SetPlayMode(const eMiniGamePlayMode& _playMode)
{
	UMiniGameGameInstance* gameInstance = Cast<UMiniGameGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (nullptr == gameInstance)
	{
		guideText->SetText(FText::FromString(TEXT("GameInstance 찾을 수 없습니다.")));
		return;
	}

	if (false == gameInstance->SetPlayMode(_playMode))
	{
		return;
	}

	FLinearColor enableColor(1.0f, 1.0f, 1.0f, 1.0f);
	FLinearColor disableColor(1.0f, 1.0f, 1.0f, 0.5f);

	if (eMiniGamePlayMode::Single == _playMode)
	{
		singleBtn->SetBackgroundColor(enableColor);
		hostBtn->SetBackgroundColor(disableColor);
		guestBtn->SetBackgroundColor(disableColor);

		myIPText->SetIsEnabled(false);
		friendIPEditableTextBox->SetIsEnabled(false);

		joinBtn->SetVisibility(ESlateVisibility::Collapsed);
		enterBtnWrapBox->SetVisibility(ESlateVisibility::Visible);
	}
	else if (eMiniGamePlayMode::MultiHost == _playMode)
	{
		singleBtn->SetBackgroundColor(disableColor);
		hostBtn->SetBackgroundColor(enableColor);
		guestBtn->SetBackgroundColor(disableColor);

		myIPText->SetIsEnabled(true);
		friendIPEditableTextBox->SetIsEnabled(false);

		joinBtn->SetVisibility(ESlateVisibility::Collapsed);
		enterBtnWrapBox->SetVisibility(ESlateVisibility::Visible);
	}
	else if (eMiniGamePlayMode::MultiGuest == _playMode)
	{
		singleBtn->SetBackgroundColor(disableColor);
		hostBtn->SetBackgroundColor(disableColor);
		guestBtn->SetBackgroundColor(enableColor);

		myIPText->SetIsEnabled(false);
		friendIPEditableTextBox->SetIsEnabled(true);

		joinBtn->SetVisibility(ESlateVisibility::Visible);
		enterBtnWrapBox->SetVisibility(ESlateVisibility::Collapsed);
	}
	else
	{
		PRINT_WARNING("알 수 없는 ENUM 입니다. UI 변경할 수 없습니다. *eMiniGamePlayMode=%d", _playMode);
		return;
	}
}

void UMiniGameLobbyWindow::SetPlayModeToSingle()
{
	SetPlayMode(eMiniGamePlayMode::Single);
}

void UMiniGameLobbyWindow::SetPlayModeToMultiHost()
{
	SetPlayMode(eMiniGamePlayMode::MultiHost);
}

void UMiniGameLobbyWindow::SetPlayModeToMultiGuest()
{
	SetPlayMode(eMiniGamePlayMode::MultiGuest);
}

void UMiniGameLobbyWindow::JoinSessionByIPAddress()
{
	// TODO Level 2: assert 추가
	// ex) eMiniGamePlayMode::MultiGuest 가 아닙니다. Join Session 할 수 없습니다.

	FString friendIPAddress = friendIPEditableTextBox->GetText().ToString();

	friendIPAddress.RemoveSpacesInline();

	if (true == friendIPAddress.IsEmpty())
	{
		guideText->SetText(FText::FromString(TEXT("IP Address 입력해 주세요.")));
		return;
	}

	FRegexPattern regixPattern = FRegexPattern(FString(TEXT(
		"(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])"
	)));

	FRegexMatcher regexMatcher(regixPattern, friendIPAddress);
	if (false == regexMatcher.FindNext())
	{
		guideText->SetText(FText::FromString(TEXT("잘못된 IP Address 입니다.")));
		return;
	}

	FromClient_TryJoinSession(friendIPAddress);
}

void UMiniGameLobbyWindow::DestroySession()
{
	FromClient_TryDestroySession();
}

void UMiniGameLobbyWindow::RequestExternalIPAddress()
{
	FHttpModule* httpModule = &FHttpModule::Get();
	TSharedRef<IHttpRequest, ESPMode::ThreadSafe> httpRequest = httpModule->CreateRequest();

	httpRequest->SetURL(FString("https://api.ipify.org/"));

	httpRequest->OnProcessRequestComplete().BindUObject(this, &UMiniGameLobbyWindow::ResponseExternalIPAddress);
	if (false == httpRequest->ProcessRequest())
	{
		httpRequest->OnProcessRequestComplete().Unbind();
		myIPText->SetText(FText::FromString(TEXT("Fail Request")));
		return;
	}

	refreshExternalIPBtn->SetIsEnabled(false);

#if WITH_EDITOR
	// 에디터에서는 친구IP에 InternalIPAddress 가 자동으로 기입되게 한다.
	bool bCanBindAll;
	TSharedRef<FInternetAddr> localAddresses = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->GetLocalHostAddr(*GLog, bCanBindAll);
	if (true == localAddresses->IsValid())
	{
		friendIPEditableTextBox->SetText(FText::FromString(localAddresses->ToString(false)));
	}
#endif
}

void UMiniGameLobbyWindow::ResponseExternalIPAddress(FHttpRequestPtr request, FHttpResponsePtr response, bool bConnectedSuccessfully)
{
	refreshExternalIPBtn->SetIsEnabled(true);

	if (false == bConnectedSuccessfully)
	{
		myIPText->SetText(FText::FromString(TEXT("Fail Response")));
		return;
	}

	if (false == response.IsValid())
	{
		myIPText->SetText(FText::FromString(TEXT("InValid Response")));
		return;
	}

	myIPText->SetText(FText::FromString(response.Get()->GetContentAsString()));
}