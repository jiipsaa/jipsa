﻿#include "UpAndDownGameMode.h"
#include "../Jipsa/Log.h"
#include "../Jipsa/MessageManager.h"
#include "../Jipsa/Popup.h"
#include "../Jipsa/PopupManager.h"

#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"

void AUpAndDownGameMode::StartPlay()
{
	Super::StartPlay();

	UUserWidget* userWidget = PopupManager::GetSingleton()->OpenWindow(GetWorld(), "UpAndDownWindowBP");
	if (nullptr == userWidget)
	{
		return;
	}

	window = Cast<UUpAndDownWindow>(userWidget);
	//window->Init();

	ChangeState(eUpAndDownState::EnterAnswer);
}

void AUpAndDownGameMode::OnTextChanged_AnswerEditableTextBox(const FText& text)
{
	if (eUpAndDownState::EnterAnswer != currentState)
	{
		return;
	}

	if (true == text.IsEmptyOrWhitespace())
	{
		return;
	}

	if (false == text.IsNumeric())
	{
		FString message = TEXT("숫자만 입력 가능합니다.");
		MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), message);
		window->SetAnswerText(answer);
		return;
	}

	int32 temp = FCString::Atoi(*text.ToString());
	if (temp < MIN_ANSWER)
	{
		FString message = FString::Printf(TEXT("최소값은 %d 입니다."), MIN_ANSWER);
		MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), message);
		window->SetAnswerText(answer);
		return;
	}
	else if (MAX_ANSWER < temp)
	{
		FString message = FString::Printf(TEXT("최대값은 %d 입니다."), MAX_ANSWER);
		MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), message);
		window->SetAnswerText(answer);
		return;
	}

	answer = text;
}

void AUpAndDownGameMode::OnClicked_GenRandomAnswerButton()
{
	if (eUpAndDownState::EnterAnswer != currentState)
	{
		PRINT_WARNING("%s", TEXT("정답을 입력할 수 있는 State 가 아닙니다."));
		return;
	}

	int32 randomAnswer = FMath::RandRange(MIN_ANSWER, MAX_ANSWER);
	answer = FText::FromString(FString::Printf(TEXT("%d"), randomAnswer));

	window->SetAnswerText(answer);
}

void AUpAndDownGameMode::OnClicked_EnterAnswerButton()
{
	if (eUpAndDownState::EnterAnswer != currentState)
	{
		PRINT_WARNING("%s", TEXT("정답을 입력할 수 있는 State 가 아닙니다."));
		return;
	}

	if (true == answer.IsEmptyOrWhitespace())
	{
		return;
	}

	if (false == answer.IsNumeric())
	{
		return;
	}

	ChangeState(eUpAndDownState::PredictAnswer);
}

void AUpAndDownGameMode::OnTextChanged_PredictionEditableTextBox(const FText& text)
{
	if (eUpAndDownState::PredictAnswer != currentState)
	{
		return;
	}

	if (true == text.IsEmptyOrWhitespace())
	{
		return;
	}

	if (false == text.IsNumeric())
	{
		FString message = TEXT("숫자만 입력 가능합니다.");
		MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), message);
		window->SetPredictionText(prediction);
		return;
	}

	int32 temp = FCString::Atoi(*text.ToString());
	if (temp < minPrediction)
	{
		FString message = FString::Printf(TEXT("최소값은 %d 입니다."), minPrediction);
		MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), message);
		window->SetPredictionText(prediction);
		return;
	}
	else if (maxPrediction < temp)
	{
		FString message = FString::Printf(TEXT("최대값은 %d 입니다."), maxPrediction);
		MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), message);
		window->SetPredictionText(prediction);
		return;
	}

	prediction = text;
}

void AUpAndDownGameMode::OnClicked_EnterPredictionButton()
{
	if (eUpAndDownState::PredictAnswer != currentState)
	{
		PRINT_WARNING("%s", TEXT("정답을 맞출 수 있는 State 가 아닙니다."));
		return;
	}

	if (true == prediction.IsEmptyOrWhitespace())
	{
		return;
	}

	if (false == prediction.IsNumeric())
	{
		return;
	}

	int32 answerNum = FCString::Atoi(*answer.ToString());
	int32 predictionNum = FCString::Atoi(*prediction.ToString());
	if (predictionNum == answerNum)
	{
		ChangeState(eUpAndDownState::Result);
		return;
	}

	if (predictionNum < answerNum)
	{
		FString message = TEXT("더 큰 숫자입니다!");
		MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), message);
		minPrediction = predictionNum + 1;
	}
	else
	{
		FString message = TEXT("더 작은 숫자입니다!");
		MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), message);
		maxPrediction = predictionNum - 1;
	}

	++predictionCount;
	if (predictionCount < LIMIT_PREDICTION_COUNT)
	{
		ChangeState(eUpAndDownState::PredictAnswer);
	}
	else
	{
		ChangeState(eUpAndDownState::Result);
	}
}

void AUpAndDownGameMode::OnClosed_ResultPopup()
{
	ChangeState(eUpAndDownState::EnterAnswer);
}

bool AUpAndDownGameMode::ChangeState(const eUpAndDownState newState)
{
	if (eUpAndDownState::EnterAnswer == newState)
	{
		window->SetStateText(FText::FromString(TEXT("Enter Answer")));
		window->ChangeStateToEnterAnswer();

		answer = FText::GetEmpty();
		prediction = FText::GetEmpty();
		window->SetAnswerText(answer);
		window->SetPredictionText(prediction);
		window->SetScoreText(FText::GetEmpty());
	}
	else if (eUpAndDownState::PredictAnswer == newState)
	{
		if (eUpAndDownState::PredictAnswer != currentState)
		{
			window->SetStateText(FText::FromString(TEXT("Predict Answer")));
			window->ChangeStateToPredictAnswer();
			
			minPrediction = MIN_ANSWER;
			maxPrediction = MAX_ANSWER;
			predictionCount = 0;
		}

		int32 score = 10 * (LIMIT_PREDICTION_COUNT - predictionCount);
		window->SetScoreText(FText::FromString(FString::Printf(TEXT("Score: %d"), score)));
	}
	else if (eUpAndDownState::Result == newState)
	{
		window->SetStateText(FText::FromString(TEXT("Result")));
		window->ChangeStateToResultAnswer();

		UUserWidget* userWidget = PopupManager::GetSingleton()->OpenResultPopup(GetWorld(), "UpAndDownResultPopupBP");
		if (nullptr == userWidget)
		{
			return false;
		}

		UPopup* popup = Cast<UPopup>(userWidget);
		if (nullptr == popup)
		{
			return false;
		}
		
		// 결과 팝업이 닫히면, 처음 상태로 돌아가도록 델리게이트를 연결해둔다.
		popup->onCloseDelegate.BindUFunction(this, FName("OnClosed_ResultPopup"));

		// GetWidgetFromName() 사용 예시. 원래는 popup->Init(bool) 호출하는게 맞음.
		UTextBlock* resultText = Cast<UTextBlock>(popup->GetWidgetFromName(TEXT("ResultText")));
		if (true == IsValid(resultText))
		{
			int32 answerNum = FCString::Atoi(*answer.ToString());
			int32 predictionNum = FCString::Atoi(*prediction.ToString());
			if (answerNum == predictionNum)
			{
				resultText->SetText(FText::FromString(FString::Printf(TEXT("성공 !!! 정답은 %d"), answerNum)));
			}
			else
			{
				resultText->SetText(FText::FromString(FString::Printf(TEXT("실패.. 정답은 %d"), answerNum)));
			}
		}
		else
		{
		    PRINT_WARNING("%s", TEXT("ResultText(type = UTextBlock) 찾을 수 없습니다. Blueprint에 추가해야 합니다."));
		}
	}
	else
	{
		PRINT_ERROR("처리할 수 없는 Enum 입니다. 코드 확인해야 합니다. eUpAndDownState=%d", currentState);
		return false;
	}

	currentState = newState;
	return true;
}