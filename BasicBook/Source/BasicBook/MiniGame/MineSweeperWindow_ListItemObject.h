#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MineSweeperWindow_ListItemObject.generated.h"

UCLASS()
class BASICBOOK_API UMineSweeperWindow_ListItemObject : public UObject
{
	GENERATED_BODY()

public:
	int32 x;
	int32 y;
};