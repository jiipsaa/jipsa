﻿#pragma once
// TODO:
// string -> FString.
// 단어 목록 xml을 로드하도록 변경하자. HangmanAnswer.h를 만들자.

#include <string>

#include "../Jipsa/Log.h"
#include "Data/HangmanProgress.h"

class Hangman
{
private:
	const int32 ONE_PROGRESS_LINE_COUNT = 8;
	TArray<FString> progressStrings;

	const int32 MAX_FAIL_COUNT = 7;
	int32 failCount;

	string problem;
	string answer;
	string typedAlphabet;

	const string TEST_PROBLEM = "hello";
	//const string TEST_INPUT = "hxelloxxxx";	// 성공 케이스.
	const string TEST_INPUT = "abcdefghij";		// 실패 케이스. 
	int32 testInputCount = 0;

public:
	void Play()
	{
		HangmanProgress* hangmanProgress = new HangmanProgress();
		int32 totalProgressLineCount = ONE_PROGRESS_LINE_COUNT * (MAX_FAIL_COUNT + 1);
		if (false == hangmanProgress->Load(progressStrings, totalProgressLineCount))
		{
			PRINT_ERROR("%s", "HangmanProgress/Load() 실패했습니다.");
			return;
		}

		problem = TEST_PROBLEM;
		testInputCount = 0;

		answer = string(problem.length(), '-');
		typedAlphabet = string(24, '.');

		failCount = 0;

		Print();
		while ((failCount < MAX_FAIL_COUNT) && (answer != problem))
		{
			PRINT("==========================");
			Guess();
			Print();
		}

		if (MAX_FAIL_COUNT == failCount)
		{
			PRINT("실패");
		}
		else
		{
			PRINT("성공 %d/%d", failCount, MAX_FAIL_COUNT);
		}
	}

private:
	void Print()
	{
		FString log;
		for (int32 i = 0; i < ONE_PROGRESS_LINE_COUNT; ++i)
		{
			log.Appendf(TEXT("%s\n"), *progressStrings[failCount * ONE_PROGRESS_LINE_COUNT + i]);
		}
		log.Appendf(TEXT("ANSWER: %s\n"), UTF8_TO_TCHAR(answer.c_str()));
		log.Appendf(TEXT("TYPED ALPHABET: %s\n"), UTF8_TO_TCHAR(typedAlphabet.c_str()));
		PRINT("%s", *log);
	}

	int32 GetMatchedWordCount(char inputChar)
	{
		int32 matchedWordCount = 0;

		for (int32 pos = -1;;)
		{
			pos = problem.find(inputChar, pos + 1);
			if (pos < 0)
			{
				break;
			}

			answer[pos] = inputChar;
			++matchedWordCount;
		}

		return matchedWordCount;
	}

	void Guess()
	{
		if (TEST_INPUT.size() <= testInputCount)
		{
			PRINT_WARNING("%s", "TEST_INPUT 부족합니다. 강제로 게임을 종료합니다.");
			failCount = MAX_FAIL_COUNT;
			return;
		}

		char inputChar = TEST_INPUT[testInputCount];
		++testInputCount;

		if (('a' <= inputChar) && (inputChar <= 'z'))
		{
			int pos = typedAlphabet.find(inputChar);
			if (pos < 0)
			{
				typedAlphabet[inputChar - 'a'] = inputChar;
				if (0 == GetMatchedWordCount(inputChar))
				{
					PRINT("%c, 매칭 실패.", inputChar);
					++failCount;
				}
				else
				{
					PRINT("%c, 매칭 성공", inputChar);
				}
			}
			else
			{
				PRINT("%c, 입력했던 알파벳입니다.", inputChar);
			}
		}
		else
		{
			PRINT("%c, 알파벳 소문자가 아닙니다.", inputChar);
		}
	}
};