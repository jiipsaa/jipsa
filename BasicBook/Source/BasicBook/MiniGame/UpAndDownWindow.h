#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"
#include "UpAndDownWindow.generated.h"

UCLASS()
class BASICBOOK_API UUpAndDownWindow : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* stateText;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* scoreText;

	UPROPERTY(meta = (BindWidget))
	class UEditableTextBox* answerEditableTextBox;

	UPROPERTY(meta = (BindWidget))
	class UButton* genRandomAnswerButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* enterAnswerButton;
	
	UPROPERTY(meta = (BindWidget))
	class UEditableTextBox* predictionEditableTextBox;

	UPROPERTY(meta = (BindWidget))
	class UButton* enterPredictionButton;

public:
	void SetStateText(const FText& InText);
	void SetScoreText(const FText& InText);

	void SetAnswerText(const FText& InText);
	void SetPredictionText(const FText& InText);

	void ChangeStateToEnterAnswer();
	void ChangeStateToPredictAnswer();
	void ChangeStateToResultAnswer();

protected:
	virtual void NativeConstruct() override;
};