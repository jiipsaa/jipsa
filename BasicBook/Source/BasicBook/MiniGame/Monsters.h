﻿#pragma once

#include "Monster.h"

class Zombie : public Monster
{
public:
	Zombie(const string& _name = "좀비", const char& _icon = 'Z', const int32& _x = 0, const int32& _y = 0)
		: Monster(_name, _icon, _x, _y) {}
};

class Vampire : public Monster
{
public:
	Vampire(const string& _name = "뱀파이어", const char& _icon = 'V', const int32& _x = 0, const int32& _y = 0)
		: Monster(_name, _icon, _x, _y) {}

	void Move(vector<vector<int32>>& map, const int32& maxX, const int32& maxY)
	{
		if (true == IsSteelSleeping())
		{
			return;
		}

		int32 randomDirection = FMath::RandRange(1, 4);
		switch (randomDirection)
		{
		case 1:
			--currPos.x;
			break;
		case 2:
			++currPos.x;
			break;
		case 3:
			--currPos.y;
			break;
		case 4:
			++currPos.y;
			break;
		default:
			PRINT_WARNING("잘못된 방향입니다. 디버깅 필요합니다. randomDirection=%d", randomDirection);
			break;
		}

		Clip(maxX, maxY);
		Eat(map);
	}
};

class Ghost : public Monster
{
public:
	Ghost(const string& _name = "유령", const char& _icon = 'G', const int32& _x = 0, const int32& _y = 0)
		: Monster(_name, _icon, _x, _y) {}

	void Move(vector<vector<int32>>& map, const int32& maxX, const int32& maxY)
	{
		if (true == IsSteelSleeping())
		{
			return;
		}

		currPos = MonsterWorldPoint(FMath::RandRange(0, maxX - 1), FMath::RandRange(0, maxY - 1));

		Clip(maxX, maxY);
		Eat(map);
	}
};

class Jiangshi : public Monster
{
protected:
	bool isHorizontalMovement;

public:
	Jiangshi(const string& _name = "강시", const char& _icon = 'J', const int32& _x = 0, const int32& _y = 0, const bool& _isHorizontalMovement = true)
		: Monster(_name, _icon, _x, _y), isHorizontalMovement(_isHorizontalMovement) {}

	void Move(vector<vector<int32>>& map, const int32& maxX, const int32& maxY)
	{
		if (true == IsSteelSleeping())
		{
			return;
		}

		bool isDirectionAdded = FMath::RandBool();
		int32 randomDistance = FMath::RandRange(1, 2); // 최대 2칸까지 이동 가능.

		if (true == isHorizontalMovement)
		{
			if (true == isDirectionAdded)
			{
				currPos.x += randomDistance;
			}
			else
			{
				currPos.x -= randomDistance;
			}
		}
		else
		{
			if (true == isDirectionAdded)
			{
				currPos.y += randomDistance;
			}
			else
			{
				currPos.y -= randomDistance;
			}
		}

		Clip(maxX, maxY);
		Eat(map);
	}
};