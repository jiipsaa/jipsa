#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Components/TileView.h"
#include "MineSweeperWindow.generated.h"

UCLASS()
class BASICBOOK_API UMineSweeperWindow : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	class UTileView* playerZeroTileView;

	UPROPERTY(meta = (BindWidget))
	class UTileView* playerOneTileView;

	UPROPERTY(meta = (BindWidget))
	class UTileView* playerTwoTileView;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* messageText;

	UPROPERTY(meta = (BindWidget))
	class UButton* startBtn;
	
	UPROPERTY(meta = (BindWidget))
	class UButton* restartBtn;

	TArray<UTileView*> otherPlayerTileViewArray;

public:
	void Init(const int32& width, const int32& height);
	void InitOtherTileView(const int32& width, const int32& height, const int32& otherPlayerIdx);
	
	void ShowMineImage(const int32& index);
	void ShowOtherMineImage(const int32& index, const int32& otherPlayerIdx);
	void ShowEmptyImage(const int32& index, const int32& nearMineCount);
	void ShowOtherEmptyImage(const int32& index, const int32& nearMineCount, const int32& otherPlayerIdx);
	
	void SetEnableTouch(const bool& isEnable);
	void SetEnableStartBtn(const bool& isEnable);
	void SetEnableRestartBtn(const bool& isEnable);

	void SetMessage(const char* message);

protected:
	void NativeConstruct() override;
};
