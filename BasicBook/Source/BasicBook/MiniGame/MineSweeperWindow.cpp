﻿#include "MineSweeperWindow.h"

#include "../Jipsa/Log.h"
#include "MineSweeperPlayerController.h"
#include "MineSweeperWindow_ListEntry.h"
#include "MineSweeperWindow_ListItemObject.h"

#include "Kismet/GameplayStatics.h"

void UMineSweeperWindow::Init(const int32& width, const int32& height)
{
	// 전 판에서 생성한 타일이 있다면 재활용하고, 추가 생성하지 않는다.
	const TArray<UUserWidget*>& entryWidgets = playerZeroTileView->GetDisplayedEntryWidgets();
	if (0 < entryWidgets.Num())
	{
		int32 entryWidgetCount = entryWidgets.Num();
		UMineSweeperWindow_ListEntry* listEntry = nullptr;
		for (int32 index = 0; index < entryWidgetCount; ++index)
		{
			listEntry = Cast<UMineSweeperWindow_ListEntry>(entryWidgets[index]);
			if (false == IsValid(listEntry))
			{
				return;
			}

			listEntry->Reset();
		}
		return;
	}

	// 데이터를 넣어서, 타일을 생성하도록 요청한다.
	UMineSweeperWindow_ListItemObject* listItemObject = nullptr;
	for (int32 y = 0; y < height; ++y)
	{
		for (int32 x = 0; x < width; ++x)
		{
			listItemObject = NewObject<UMineSweeperWindow_ListItemObject>();
			listItemObject->x = x;
			listItemObject->y = y;
			playerZeroTileView->AddItem(listItemObject);
		}
	}
}

void UMineSweeperWindow::InitOtherTileView(const int32& width, const int32& height, const int32& otherPlayerIdx)
{
	// TODO Level 2: assert로 변경.
	if (otherPlayerTileViewArray.Num() <= otherPlayerIdx)
	{
		PRINT_WARNING("다른 플레이어의 TileView 를 초기화 할 수 없습니다. otherPlayerTileViewArray 초기화가 됬는지 코드 확인해야 합니다. otherPlayerIdx=%d, arrayLength=%d", otherPlayerIdx, otherPlayerTileViewArray.Num());
		SetMessage("다른 플레이어의 TileView 를 초기화 할 수 없습니다. otherPlayerTileViewArray 초기화가 됬는지 코드 확인해야 합니다.");
		return;
	}

	// 전 판에서 생성한 타일이 있다면 재활용하고, 추가 생성하지 않는다.
	const TArray<UUserWidget*>& createdEntryWidgets = otherPlayerTileViewArray[otherPlayerIdx]->GetDisplayedEntryWidgets();
	if (0 < createdEntryWidgets.Num())
	{
		int32 createdEntryWidgetCount = createdEntryWidgets.Num();
		UMineSweeperWindow_ListEntry* createdListEntry = nullptr;
		for (int32 index = 0; index < createdEntryWidgetCount; ++index)
		{
			createdListEntry = Cast<UMineSweeperWindow_ListEntry>(createdEntryWidgets[index]);
			if (false == IsValid(createdListEntry))
			{
				return;
			}

			createdListEntry->Reset();
		}
		return;
	}

	// 데이터를 넣어서, 타일을 생성하도록 요청한다.
	UMineSweeperWindow_ListItemObject* listItemObject = nullptr;
	for (int32 y = 0; y < height; ++y)
	{
		for (int32 x = 0; x < width; ++x)
		{
			listItemObject = NewObject<UMineSweeperWindow_ListItemObject>();
			listItemObject->x = x;
			listItemObject->y = y;
			otherPlayerTileViewArray[otherPlayerIdx]->AddItem(listItemObject);
		}
	}
}

void UMineSweeperWindow::ShowMineImage(const int32& index)
{
	const TArray<UUserWidget*>& entryWidgets = playerZeroTileView->GetDisplayedEntryWidgets();
	if ((index < 0) || (entryWidgets.Num() <= index))
	{
		return;
	}

	UMineSweeperWindow_ListEntry* listEntry = Cast<UMineSweeperWindow_ListEntry>(entryWidgets[index]);
	if (false == IsValid(listEntry))
	{
		return;
	}

	listEntry->ShowMineImage();
}

void UMineSweeperWindow::ShowEmptyImage(const int32& index, const int32& nearMineCount)
{
	const TArray<UUserWidget*>& entryWidgets = playerZeroTileView->GetDisplayedEntryWidgets();
	if ((index < 0) || (entryWidgets.Num() <= index))
	{
		return;
	}

	UMineSweeperWindow_ListEntry* listEntry = Cast<UMineSweeperWindow_ListEntry>(entryWidgets[index]);
	if (false == IsValid(listEntry))
	{
		return;
	}

	listEntry->ShowEmptyImage(nearMineCount);
}

void UMineSweeperWindow::ShowOtherMineImage(const int32& index, const int32& otherPlayerIdx)
{
	// TODO Level 2: assert로 변경.
	if (otherPlayerTileViewArray.Num() <= otherPlayerIdx)
	{
		PRINT_WARNING("index 가 배열 범위를 넘습니다. ShowOtherMineImage() 실패합니다. otherPlayerIdx=%d, arrayLength=%d", otherPlayerIdx, otherPlayerTileViewArray.Num());
		return;
	}

	const TArray<UUserWidget*>& entryWidgets = otherPlayerTileViewArray[otherPlayerIdx]->GetDisplayedEntryWidgets();
	if ((index < 0) || (entryWidgets.Num() <= index))
	{
		return;
	}

	UMineSweeperWindow_ListEntry* listEntry = Cast<UMineSweeperWindow_ListEntry>(entryWidgets[index]);
	if (false == IsValid(listEntry))
	{
		return;
	}

	listEntry->ShowMineImage();
}

void UMineSweeperWindow::ShowOtherEmptyImage(const int32& index, const int32& nearMineCount, const int32& otherPlayerIdx)
{
	// TODO Level 2: assert로 변경.
	if (otherPlayerTileViewArray.Num() <= otherPlayerIdx)
	{
		PRINT_WARNING("index 가 배열 범위를 넘습니다. ShowOtherEmptyImage() 실패합니다. otherPlayerIdx=%d, arrayLength=%d", otherPlayerIdx, otherPlayerTileViewArray.Num());
		return;
	}

	const TArray<UUserWidget*>& entryWidgets = otherPlayerTileViewArray[otherPlayerIdx]->GetDisplayedEntryWidgets();
	if ((index < 0) || (entryWidgets.Num() <= index))
	{
		return;
	}

	UMineSweeperWindow_ListEntry* listEntry = Cast<UMineSweeperWindow_ListEntry>(entryWidgets[index]);
	if (false == IsValid(listEntry))
	{
		return;
	}

	listEntry->ShowEmptyImage(nearMineCount);
}

void UMineSweeperWindow::SetEnableTouch(const bool& isEnable)
{
	const TArray<UUserWidget*>& entryWidgets = playerZeroTileView->GetDisplayedEntryWidgets();
	if (0 < entryWidgets.Num())
	{
		int32 entryWidgetCount = entryWidgets.Num();
		UMineSweeperWindow_ListEntry* listEntry = nullptr;
		for (int32 index = 0; index < entryWidgetCount; ++index)
		{
			listEntry = Cast<UMineSweeperWindow_ListEntry>(entryWidgets[index]);
			if (false == IsValid(listEntry))
			{
				continue;
			}

			listEntry->SetEnableTouch(isEnable);
		}
	}
}

void UMineSweeperWindow::SetEnableStartBtn(const bool& isEnable)
{
	startBtn->SetIsEnabled(isEnable);
}

void UMineSweeperWindow::SetEnableRestartBtn(const bool& isEnable)
{
	restartBtn->SetIsEnabled(isEnable);
}

void UMineSweeperWindow::SetMessage(const char* message)
{
	messageText->SetText(FText::FromString(UTF8_TO_TCHAR(message)));
}

void UMineSweeperWindow::NativeConstruct()
{
	Super::NativeConstruct();

	otherPlayerTileViewArray.Emplace(playerOneTileView);
	otherPlayerTileViewArray.Emplace(playerTwoTileView);

	AMineSweeperPlayerController* controller = Cast<AMineSweeperPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if (nullptr == controller)
	{
		PRINT_ERROR("%s", TEXT("AMineSweeperPlayerController 얻을 수 없습니다. 시작 버튼 초기화에 실패했습니다."));
		return;
	}

	if (true == controller->HasAuthority())
	{
		startBtn->OnClicked.AddDynamic(controller, &AMineSweeperPlayerController::ServerStartGame);
		SetEnableStartBtn(true);
	}
	else
	{
		SetEnableStartBtn(false);
	}
}