﻿#include "UpAndDownWindow.h"
#include "UpAndDownGameMode.h"
#include "../Jipsa/Log.h"

#include "Kismet/GameplayStatics.h"

void UUpAndDownWindow::SetStateText(const FText& InText)
{
	stateText->SetText(InText);
}

void UUpAndDownWindow::SetScoreText(const FText& InText)
{
	scoreText->SetText(InText);
}

void UUpAndDownWindow::SetAnswerText(const FText& InText)
{
	answerEditableTextBox->SetText(InText);
}

void UUpAndDownWindow::SetPredictionText(const FText& InText)
{
	predictionEditableTextBox->SetText(InText);
}

void UUpAndDownWindow::ChangeStateToEnterAnswer()
{
	answerEditableTextBox->SetIsEnabled(true);
	genRandomAnswerButton->SetIsEnabled(true);
	enterAnswerButton->SetIsEnabled(true);

	predictionEditableTextBox->SetIsEnabled(false);
	enterPredictionButton->SetIsEnabled(false);
}

void UUpAndDownWindow::ChangeStateToPredictAnswer()
{
	answerEditableTextBox->SetIsEnabled(false);
	genRandomAnswerButton->SetIsEnabled(false);
	enterAnswerButton->SetIsEnabled(false);

	predictionEditableTextBox->SetIsEnabled(true);
	enterPredictionButton->SetIsEnabled(true);
}

void UUpAndDownWindow::ChangeStateToResultAnswer()
{
	answerEditableTextBox->SetIsEnabled(false);
	genRandomAnswerButton->SetIsEnabled(false);
	enterAnswerButton->SetIsEnabled(false);

	predictionEditableTextBox->SetIsEnabled(false);
	enterPredictionButton->SetIsEnabled(false);
}

void UUpAndDownWindow::NativeConstruct()
{
	Super::NativeConstruct();

	AUpAndDownGameMode* gameMode = Cast<AUpAndDownGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (false == IsValid(gameMode))
	{
		PRINT_ERROR("%s", TEXT("UpAndDownGameMode 를 얻을 수 없습니다."));
		return;
	}

	answerEditableTextBox->OnTextChanged.AddDynamic(gameMode, &AUpAndDownGameMode::OnTextChanged_AnswerEditableTextBox);
	genRandomAnswerButton->OnClicked.AddDynamic(gameMode, &AUpAndDownGameMode::OnClicked_GenRandomAnswerButton);
	enterAnswerButton->OnClicked.AddDynamic(gameMode, &AUpAndDownGameMode::OnClicked_EnterAnswerButton);

	predictionEditableTextBox->OnTextChanged.AddDynamic(gameMode, &AUpAndDownGameMode::OnTextChanged_PredictionEditableTextBox);
	enterPredictionButton->OnClicked.AddDynamic(gameMode, &AUpAndDownGameMode::OnClicked_EnterPredictionButton);
}