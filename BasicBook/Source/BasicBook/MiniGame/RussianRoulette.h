﻿#pragma once

#include "../Jipsa/Log.h"

class RussianRoulette
{
public:

	// TODO: 
	// 100% 확률로 죽는다면. 장송곡을 미리 틀어주자 ㅋㅋ 웅장하게!
	// 다른 유저가 방아쇠에 터치하는 모습을 같이 구경하면 좋겠다. 들었다 놨다 들었다 놨다. 3초 터치유지하면 발사. 그동안 애니메이션 재생.
	// 총알 장전할때도 1발 장전하면 next 버튼이 나오는데, 탄창 돌려서 더 넣을수도 있게 하자.
	// 장전은 순서대로가 아니라 원하는 곳으로 넣을 수 있게 하자.

	int32 Play()
	{
		int32 playerCount = 2; // 2 ~

		const int32 MAX_BULLET_COUNT = 6;
		int32 loadedBulletCount = 1; // 1 ~ (MAX_BULLET_COUNT - 1)

		int32 playerIndex = FMath::RandRange(0, playerCount - 1);

		PRINT("%d발 장전 됬습니다. %d번 플레이어 부터 시작합니다.", loadedBulletCount, playerIndex + 1);
		while (true)
		{
			int loadedIndex = FMath::RandRange(0, MAX_BULLET_COUNT - 1);
			PRINT("탄창을 다시 돌렸습니다. [%d] 에게 격발하겠습니까?", playerIndex + 1);

			PRINT("빵~!!!");
			if (loadedIndex < loadedBulletCount)
			{
				PRINT("(...) 내 딸을 부탁한다.");
				break;
			}
			else
			{
				PRINT("휴.. 살았습니다!!");
				playerIndex = (playerIndex + 1) % playerCount;
			}
		}

		return playerIndex;
	}
};