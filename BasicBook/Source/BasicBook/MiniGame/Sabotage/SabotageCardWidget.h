﻿#pragma once

#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/CanvasPanel.h"
#include "Components/Image.h"
#include "Components/SizeBox.h"
#include "Components/TextBlock.h"
#include "CoreMinimal.h"

#include "SabotageCardWidget.generated.h"

DECLARE_DELEGATE(OnClickDelegate)

UCLASS()
class BASICBOOK_API USabotageCardWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	OnClickDelegate onClickDelegate;

	UPROPERTY(meta = (BindWidget)) USizeBox* cardSizeBox;

	UPROPERTY(meta = (BindWidget)) UImage* bgImage;

	UPROPERTY(meta = (BindWidget)) UCanvasPanel* directionPanel;
	UPROPERTY(meta = (BindWidget)) UTextBlock* top;
	UPROPERTY(meta = (BindWidget)) UTextBlock* bottom;
	UPROPERTY(meta = (BindWidget)) UTextBlock* left;
	UPROPERTY(meta = (BindWidget)) UTextBlock* right;

	UPROPERTY(meta = (BindWidget)) UCanvasPanel* breakingTargetPanel;
	UPROPERTY(meta = (BindWidget)) UTextBlock* pick;
	UPROPERTY(meta = (BindWidget)) UTextBlock* lantern;
	UPROPERTY(meta = (BindWidget)) UTextBlock* wagon;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget)) UImage* backImage;

	UPROPERTY(meta = (BindWidget)) UImage* addCardEffectImage;

	UPROPERTY(meta = (BindWidget)) UButton* touchButton;

public:
	float GetSizeBoxWidth();
	float GetSizeBoxHeight();

	void Reset();

	void SetBGImage(UTexture2D* texture);

	void TurnOver();

protected:
	void NativeConstruct() override;

	UFUNCTION()
	void OnClickedCard();

	UFUNCTION(BlueprintImplementableEvent, Category = "FromClient")
	void FromClient_PlayTurnOverAnimation();
};
