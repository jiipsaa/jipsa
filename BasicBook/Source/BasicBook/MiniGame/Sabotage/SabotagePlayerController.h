﻿#pragma once

#include "SabotageWindow.h"

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "SabotagePlayerController.generated.h"

UCLASS()
class BASICBOOK_API ASabotagePlayerController : public APlayerController
{
	GENERATED_BODY()

protected:
	class USabotageWindow* window = nullptr;

public:
	void InitPlayerState() override;

	UFUNCTION(Client, Reliable)
	void InitBoard(const TArray<int32>& initialBoard, int32 const& playerID);

	UFUNCTION(Client, Reliable)
	void FromServer_PostLogin(const int32& playerID, const int32& handCount, bool isEnablePick, bool isEnableLantern, bool isEnableWagon);

	UFUNCTION(Client, Reliable)
	void FromServer_Logout(const int32& playerID);
	
	UFUNCTION(Server, Reliable)
	void ServerRequestReady(const int32& playerID);

	UFUNCTION(Client, Reliable)
	void ClientResponseReady();

	UFUNCTION(Server, Reliable)
	void ToServer_StartGame();

	UFUNCTION(Client, Reliable)
	void FromServer_StartGame(const bool isAllReady);

	UFUNCTION(Client, Reliable)
	void FromServer_StartPick(const int32& characterCount);

	UFUNCTION(Server, Reliable)
	void ToServer_PickCharacter(const int32& pickIdx, const int32& playerID);

	UFUNCTION(Client, Reliable)
	void FromServer_PickCharacter(const int32& pickIdx, const int32& cardIdx, const int32& fakeCardIdx, const int32& playerID);

	UFUNCTION(Client, Reliable)
	void FromServer_InitHand(const TArray<int32>& tunnelAndActionCardArr);

	UFUNCTION(Client, Reliable)
	void FromServer_UpdateHandCount(const int32& handCount, const int32& playerID);

	UFUNCTION(Client, Reliable)
	void FromServer_PlayTurn();

	void OnClickedHandCard(const int32& cardIdx);
	void OnClickedTunnelCard(const int32& cardIdx);
	void OnClickedAddPointCard(const int32& cardIdx);
	void OnClickedCharacterCard(const int32& cardIdx);

	UFUNCTION(Server, Reliable)
	void ToServer_RequestAddTunnelCard(const int32& cardIdx, const FIntPoint& point);

	UFUNCTION(Client, Reliable)
	void FromServer_AddTunnelCard(const int32& cardIdx, const FIntPoint& point, const int32& playerID);

	UFUNCTION(Client, Reliable)
	void FromServer_OpenArrivalPointCard(const int32& cardIdx, const FIntPoint& point);

	UFUNCTION(Server, Reliable)
	void ToServer_TurnOverArrivalPointCard(const int32& cardIdx, const FIntPoint& point);

	UFUNCTION(Client, Reliable)
	void FromServer_TurnOverArrivalPointCard(const int32& cardIdx, const FIntPoint& point, const int32& playerID);

	UFUNCTION(Server, Reliable)
	void ToServer_RemoveBoardCard(const int32& rockfallCardIdx, const FIntPoint& point);

	UFUNCTION(Client, Reliable)
	void FromServer_RemoveBoardCard(const int32& rockfallCardIdx, const FIntPoint& point);

	UFUNCTION(Client, Reliable)
	void FromServer_AddHandCard(const int32& cardIdx);

	UFUNCTION(Server, Reliable)
	void ToServer_RemoveHandCard(const int32& cardIdx);

	UFUNCTION(Client, Reliable)
	void FromServer_RemoveHandCard(const int32& cardIdx);

	UFUNCTION(Server, Reliable)
	void ToServer_BreakEquipment(const int32& breakCardIdx, const int32& targetPlayerID);

	UFUNCTION(Server, Reliable)
	void ToServer_RepairEquipment(const int32& repairCardIdx, const int32& targetPlayerID);

	UFUNCTION(Client, Reliable)
	void FromServer_UpdateEquipment(const bool isEnablePick, const bool isEnableLantern, const bool isEnableWagon, const int32& targetPlayerID);

	UFUNCTION(Client, Reliable)
	void FromServer_EndGame(const bool isSaboteurWin, const bool isSaboteur);

	UFUNCTION(Server, Reliable)
	void ToServer_ResetGame();

	//UFUNCTION(BlueprintCallable, Category = "ToClient")
	//void ToClient_PrintLog();

	UFUNCTION(Client, Reliable)
	void FromServer_SetMessage(const FString& message);
};