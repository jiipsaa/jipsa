﻿#pragma once

#include "Data/SabotageData.h"
#include "SabotagePlayerController.h"

#include "Containers/Queue.h"
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "SabotageGameMode.generated.h"

using namespace NSSabotage;

namespace NSSabotage
{
	//#define MESSAGE_HAS_PROBLEM TEXT("알 수 없는 문제가 발생했습니다.")
	#define MESSAGE_IS_NOT_PLAY_STATE TEXT("진행중이 게임이 없습니다.")
	#define MESSAGE_IS_NOT_YOUR_TURN TEXT("당신의 턴이 아닙니다.")
	#define MESSAGE_CANT_FIND_TARGET_PLAYER TEXT("대상 플레이어를 찾을 수 없습니다.")
	#define MESSAGE_CANT_FIND_CARD TEXT("카드를 찾을 수 없습니다.")
	#define MESSAGE_CANT_USE_CARD TEXT("카드를 사용할 수 없습니다.")
	#define MESSAGE_CANT_ADD_TUNNEL_CARD TEXT("굴 카드를 배치할 수 없습니다.")

	enum class eState : uint8
	{
		Wait	// 방장의 요청도 받을 수 없는 상태. 정산 중이거나, GameMode가 생성되서 초기화중일때 Wait 상태여야 한다.
		,Ready	// 참가자가 입장할 수 있는 상태. 방장이 시작 요청을 했을때 Play 상태로 전환한다.
		,Play	// 게임이 진행중인 상태. 방장이 재시작 요청을 했을때 or 결과가 나왔을때 Ready 상태로 전환한다.
	};
}

USTRUCT()
struct FSabotagePlayerInfo
{
	GENERATED_BODY()

	int32 playerID;
	ASabotagePlayerController* playerController;

	bool isReady;
	
	int32 characterCardIdx;
	TSet<int32> hand;
	TArray<int32> goldCardSet;

	bool isEnablePick;
	bool isEnableLantern;
	bool isEnableWagon;
	
	FSabotagePlayerInfo(const int32& _playerID = -1, ASabotagePlayerController* _playerController = nullptr)
		: playerID(_playerID), playerController(_playerController), isReady(false), characterCardIdx(-1)
		,isEnablePick(true), isEnableLantern(true), isEnableWagon(true) {}

	void Reset(const bool& isNextGame)
	{
		isReady = false;
		characterCardIdx = -1;
		
		hand.Reset();
		if (false == isNextGame)
		{
			goldCardSet.Reset();
		}
		
		isEnablePick = true;
		isEnableLantern = true;
		isEnableWagon = true;
	}

	friend bool operator==(const FSabotagePlayerInfo& LHS, const FSabotagePlayerInfo& RHS)
	{
		return (LHS.playerID == RHS.playerID);
	}

	friend uint32 GetTypeHash(const FSabotagePlayerInfo& RHS)
	{
		return GetTypeHash(RHS.playerID);
	}
};

UCLASS()
class BASICBOOK_API ASabotageGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	eState state;

	// 플레이어 정보.
	TSet<FSabotagePlayerInfo> playerInfoSet;

	// 턴 순서.
	TArray<int32> turnOrder;
	int32 turnIndex;

	// 전체 카드 정보.
	TArray<USabotageCard*> cardList;
	ExtraCardInfo extraCardInfo;

	// Pick 캐릭터 목록.
	TArray<int32> characterList;
	TArray<int32> fakeCharacterList;
	
	// 보드판.
	TMap<FIntPoint, int32> board;

	// 카드 더미.
	TQueue<int32> tunnelAndActionCardQueue;
	TQueue<int32> goldCardQueue;

public:
	void InitGame(const FString& mapName, const FString& options, FString& errorMessage) override;
	void ResetGame(const bool& isNextGame);
	~ASabotageGameMode();

	void PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage) override;
	void PostLogin(APlayerController* newPlayer) override;
	void Logout(AController* exiting) override;

	void ReadyGame(FSabotagePlayerInfo* playerInfo = nullptr);
	void SetReady(const int32& playerID);

	void StartGame();
	
	void PickCharacter(const int32& pickIdx, const int32& playerID);
	
	void AddTunnelCard(const int32& cardIdx, const FIntPoint& point, const int32& playerID);
	void TurnOverArrivalPointCard(const int32& cardIdx, const FIntPoint& point, const int32& playerID);
	void RemoveBoardCard(const int32& rockfallCardIdx, const FIntPoint& point, const int32& playerID);
	void RemoveHandCard(const int32& cardIdx, const int32& playerID);
	void BreakEquipment(const int32& breakCardIdx, const int32& targetPlayerID, const int32& playerID);
	void RepairEquipment(const int32& repairCardIdx, const int32& targetPlayerID, const int32& playerID);

protected:
	bool IsConnected();

	void ResetTurnOrder();
	void SetNextTurn();

	void EndTurn(FSabotagePlayerInfo* playerInfo);

	void EndGame(const bool isSaboteurWin);
};
