﻿#include "SabotageGameMode.h"

#include "../../Jipsa/Log.h"
#include "../MiniGameGameInstance.h"
#include "Data/SabotageData.h"

#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"

void ASabotageGameMode::InitGame(const FString& mapName, const FString& options, FString& errorMessage)
{
	Super::InitGame(mapName, options, errorMessage);

	playerInfoSet.Reset();
	turnOrder.Reset();

	cardList.Reset();
	SabotageData::GetSingleton()->LoadCardList(cardList, &extraCardInfo);
	
	ResetGame(false);
}

void ASabotageGameMode::ResetGame(const bool& isNextGame)
{
	state = eState::Wait;

	for (auto& card : cardList)
	{
		card->SetIsOpened(false);
	}

	board.Reset();

	{// (0, 0) 에 출발지 카드를 놓는다.
		if (extraCardInfo.startingPointCardIndices.Num() < 1)
		{
			PRINT_ERROR("%s", TEXT("출발지 카드는 1장 이상 있어야 합니다. Sabotage.xml 데이터 확인해주세요."));
			return;
		}

		extraCardInfo.startingPointCardIndices.Sort([this](const int32 left, const int32 right) {
			return (FMath::FRand() < 0.5f);
		});

		int32 cardIdx = extraCardInfo.startingPointCardIndices[0];
		cardList[cardIdx]->SetIsOpened(true);

		board.Emplace(FIntPoint(0, 0), cardIdx);
	}

	{// (8, 0) (8, 2) (8, -2) 에 도착지 카드를 섞어서 놓는다.
		if (extraCardInfo.arrivalPointCardIndices.Num() < 3)
		{
			PRINT_ERROR("%s", TEXT("도착지 카드는 3장 이상 있어야 합니다. Sabotage.xml 데이터 확인해주세요."));
			return;
		}

		extraCardInfo.arrivalPointCardIndices.Sort([this](const int32 left, const int32 right) {
			return (FMath::FRand() < 0.5f);
		});

		int32 cardIdx = extraCardInfo.arrivalPointCardIndices[0];
		board.Emplace(FIntPoint(8, 0), cardIdx);

		cardIdx = extraCardInfo.arrivalPointCardIndices[1];
		board.Emplace(FIntPoint(8, 2), cardIdx);

		cardIdx = extraCardInfo.arrivalPointCardIndices[2];
		board.Emplace(FIntPoint(8, -2), cardIdx);
	}

	for (auto& info : playerInfoSet)
	{
		info.Reset(isNextGame);
	}

	if (true == isNextGame)
	{// 플레이어를 추가로 받지 않고, 다음 게임을 진행한다.		
		state = eState::Wait;
	}
	else
	{// 플레이어를 추가로 받을 수 있도록, 상태를 변경한다.
		state = eState::Ready;
	}

	ReadyGame();
}

ASabotageGameMode::~ASabotageGameMode()
{
	//PRINT("ASabotageGameMode 소멸자 호출");

	for (auto& card : cardList)
	{
		card->RemoveFromRoot();
	}
	cardList.Empty();
}

void ASabotageGameMode::PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
	UMiniGameGameInstance* gameInstance = Cast<UMiniGameGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (nullptr == gameInstance)
	{
		PRINT_ERROR("%s", TEXT("UMiniGameGameInstance 를 얻을 수 없습니다. PostLogin() 실패합니다."));
		return;
	}

	if ((eMiniGamePlayMode::MultiHost == gameInstance->GetPlayMode()) || (eMiniGamePlayMode::MultiGuest == gameInstance->GetPlayMode()))
	{
		// TODO: 세션의 최대 플레이어 수. 세션의 관전자 수는 0으로 설정하고 예외처리 하지 말자.

		// Ready 상태에서만 입장이 가능하다.
		if (eState::Ready != state)
		{
			ErrorMessage = TEXT("게임이 진행중입니다.");
			FGameModeEvents::GameModePreLoginEvent.Broadcast(this, UniqueId, ErrorMessage);
			return;
		}
	}

	Super::PreLogin(Options, Address, UniqueId, ErrorMessage);
}

void ASabotageGameMode::PostLogin(APlayerController* newPlayer)
{
	Super::PostLogin(newPlayer);

	UMiniGameGameInstance* gameInstance = Cast<UMiniGameGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (nullptr == gameInstance)
	{
		PRINT_ERROR("%s", TEXT("UMiniGameGameInstance 를 얻을 수 없습니다. PostLogin() 실패합니다."));
		return;
	}

	if (eMiniGamePlayMode::Single == gameInstance->GetPlayMode())
	{
		ASabotagePlayerController* newPlayerController = Cast<ASabotagePlayerController>(newPlayer);
		if (nullptr == newPlayerController->PlayerState)
		{
			PRINT_ERROR("PlayerState 얻을 수 없습니다. PostLogin() 실패합니다.");
			return;
		}

		int32 newPlayerID = newPlayerController->PlayerState->GetPlayerId();

		TArray<int32> initialBoard;
		for (auto& keyValue : board)
		{
			FIntPoint& point = keyValue.Key;
			int32& cardIdx = keyValue.Value;
			//PRINT("[%d, %d] %d", point.X, point.Y, cardIdx);

			initialBoard.Emplace(point.X);
			initialBoard.Emplace(point.Y);
			initialBoard.Emplace(cardIdx);
		}

		// 플레이어에게, 램덤으로 만든 기본 배치를 공유한다.
		newPlayerController->InitBoard(initialBoard, newPlayerID);
	}
	else if (eMiniGamePlayMode::MultiHost == gameInstance->GetPlayMode())
	{
		ASabotagePlayerController* newPlayerController = Cast<ASabotagePlayerController>(newPlayer);
		if (nullptr == newPlayerController->PlayerState)
		{
			PRINT_ERROR("PlayerState 얻을 수 없습니다. PostLogin() 실패합니다.");
			return;
		}

		int32 newPlayerID = newPlayerController->PlayerState->GetPlayerId();
		playerInfoSet.Emplace(FSabotagePlayerInfo(newPlayerID, newPlayerController));

		FSabotagePlayerInfo* newPlayerInfo = playerInfoSet.Find(newPlayerID);
		
		for (auto& info : playerInfoSet)
		{
			// 새로 들어온 플레이어에게 기존 플레이어가 있음을 알려준다.
			if (info.playerID != newPlayerID)
			{
				newPlayerController->FromServer_PostLogin(info.playerID, info.hand.Num(), true, true, true);
			}

			// 기존 플레이어에게 새로 들어온 플레이어가 있음을 알려준다.
			if (nullptr != info.playerController)
			{
				info.playerController->FromServer_PostLogin(newPlayerID, newPlayerInfo->hand.Num(), true, true, true);
			}
		}

		ReadyGame(newPlayerInfo);
	}
}

void ASabotageGameMode::Logout(AController* exiting)
{
	Super::Logout(exiting);

	int32 logoutPlayerID = exiting->PlayerState->GetPlayerId();
	for (auto iter = playerInfoSet.CreateIterator(); iter; ++iter)
	{
		if (iter->playerID == logoutPlayerID)
		{
			iter.RemoveCurrent();
			break;
		}
	}

	if (0 == playerInfoSet.Num())
	{
		// 방에 남은 플레이어가 없다면, 아무것도 하지 않고 맵 이동을 기다린다.
		state = eState::Wait;
		return;
	}

	for (auto& info : playerInfoSet)
	{
		if (nullptr != info.playerController)
		{
			// 남아있는 플레이어에게, 나간 플레이어가 있음을 알려준다.
			info.playerController->FromServer_Logout(logoutPlayerID);
		}
	}

	bool isEndTurn = false;
	if (eState::Play == state)
	{
		if ((0 <= turnIndex) && (logoutPlayerID == turnOrder[turnIndex]))
		{
			isEndTurn = true;
		}
	}

	turnOrder.Remove(logoutPlayerID);

	if (eState::Play == state)
	{
		if (1 == playerInfoSet.Num())
		{// 플레이어가 한 명 남았다면 승리 처리한다.
			int32 cardIdx = playerInfoSet.begin()->characterCardIdx;
			if (0 <= cardIdx)
			{
				UCharacterCard* card = Cast<UCharacterCard>(cardList[cardIdx]);
				if (nullptr != card)
				{
					EndGame(card->GetIsSaboteur());
					return;
				}
			}

			PRINT_ERROR("플레이어가 1명 남았는데, 승리 처리를 할 수 없습니다. 반드시 확인해야 합니다!! cardIdx=%d", cardIdx);
			return;
		}
		
		if (true == isEndTurn)
		{// 턴을 진행하던 플레이어가 나간 상황이면, 턴을 종료한다.
			EndTurn(nullptr);
		}
	}
}

void ASabotageGameMode::ReadyGame(FSabotagePlayerInfo* playerInfo)
{
	TArray<int32> initialBoard;
	for (auto& keyValue : board)
	{
		FIntPoint& point = keyValue.Key;
		int32& cardIdx = keyValue.Value;
		//PRINT("[%d, %d] %d", point.X, point.Y, cardIdx);

		initialBoard.Emplace(point.X);
		initialBoard.Emplace(point.Y);
		initialBoard.Emplace(cardIdx);
	}

	// 초기 보드를 공유한다.
	if (nullptr == playerInfo)
	{
		for (auto& info : playerInfoSet)
		{
			info.playerController->InitBoard(initialBoard, info.playerID);
		}
	}
	else
	{
		playerInfo->playerController->InitBoard(initialBoard, playerInfo->playerID);
	}
}

void ASabotageGameMode::SetReady(const int32& playerID)
{
	FSabotagePlayerInfo* playerInfo = playerInfoSet.Find(playerID);
	if (nullptr == playerInfo)
	{
		PRINT_ERROR("FSabotagePlayerInfo 얻을 수 없습니다. SetReady() 실패합니다. playerID=%d", playerID);
		return;
	}

	if (true == playerInfo->isReady)
	{
		PRINT_WARNING("이미 준비완료한 플레이어 입니다. UI에서도 계속 Ready하지 못하게 제한해 주세요. playerID=%d", playerID);
		return;
	}

	playerInfo->isReady = true;
	playerInfo->playerController->ClientResponseReady();
}

void ASabotageGameMode::StartGame()
{
	int32 readyCount = 0;
	for (auto& info : playerInfoSet)
	{
		if (true == info.isReady)
		{
			++readyCount;
		}
	}

	if (readyCount < playerInfoSet.Num())
	{
		for (auto& info : playerInfoSet)
		{
			// 레디 안한 플레이어가 있음을 알린다.
			info.playerController->FromServer_StartGame(false);
		}
		return;
	}

	int32 minerCount = -1;
	int32 saboteurCount = -1;
	int32 initialHandCount = -1;
	SabotageData::GetSingleton()->GetNumOfPlayerInfo(readyCount, minerCount, saboteurCount, initialHandCount);
	if (minerCount <= 0)
	{
		PRINT_WARNING("%d 명 게임의 카드 정보를 얻을 수 없습니다. ASabotageGameMode::StartGame() 실패합니다.", readyCount);
		return;
	}

	// 굴카드와 행동카드를 섞어서 덱을 만든다.
	extraCardInfo.tunnelAndActionCardIndices.Sort([this](const int32 left, const int32 right) {
		return (FMath::FRand() < 0.5f);
	});
	extraCardInfo.tunnelAndActionCardIndices.Sort([this](const int32 left, const int32 right) {
		return (FMath::FRand() < 0.7f);
	});
	tunnelAndActionCardQueue.Empty();
	for (int32& idx : extraCardInfo.tunnelAndActionCardIndices)
	{
		tunnelAndActionCardQueue.Enqueue(idx);
	}

	// 플레이어들의 초기 핸드를 만든다.
	int32 cardIdx;
	for (auto& info : playerInfoSet)
	{
		for (int i = 0; i < initialHandCount; ++i)
		{
			if (false == tunnelAndActionCardQueue.Dequeue(cardIdx))
			{
				PRINT_WARNING("%d 명 게임의 카드 갯수가 부족합니다. ASabotageGameMode::StartGame() 실패합니다.", readyCount);
				return;
			}

			info.hand.Emplace(cardIdx);
		}
	}
	
	// 금덩이카드를 섞어서 덱을 만든다.
	extraCardInfo.goldCardIndices.Sort([this](const int32 left, const int32 right) {
		return (FMath::FRand() < 0.5f);
	});

	goldCardQueue.Empty();
	for (int32& idx : extraCardInfo.goldCardIndices)
	{
		goldCardQueue.Enqueue(idx);
	}

	// 광부 카드 m 장, 방해꾼 카드 n 장을 뽑아서 섞는다.
	characterList.Reset();
	fakeCharacterList.Reset();

	extraCardInfo.minerCardIndices.Sort([this](const int32 left, const int32 right) {
		return (FMath::FRand() < 0.5f);
	});
	for (int32 i = 0; i < minerCount; ++i)
	{
		characterList.Emplace(extraCardInfo.minerCardIndices[i]);
		fakeCharacterList.Emplace(extraCardInfo.minerCardIndices[i]);
	}

	extraCardInfo.saboteurCardIndices.Sort([this](const int32 left, const int32 right) {
		return (FMath::FRand() < 0.5f);
	});
	for (int32 i = 0; i < saboteurCount; ++i)
	{
		characterList.Emplace(extraCardInfo.saboteurCardIndices[i]);
		fakeCharacterList.Emplace(extraCardInfo.saboteurCardIndices[i]);
	}

	// NOTE: characterList 만 섞는다. 서버는 characterList로 판단하지만, 클라한테는 fakeCharacterList 로 알려준다.
	// (게임 종료될때까지 서로가 어떤 캐릭터 카드를 골랐는지 모르게 하기 위해서다.)
	characterList.Sort([this](const int32 left, const int32 right) {
		return (FMath::FRand() < 0.5f);
	});

	for (auto& info : playerInfoSet)
	{
		info.characterCardIdx = -1;
		info.playerController->FromServer_StartGame(true);
		info.playerController->FromServer_StartPick(characterList.Num());
	}

	state = eState::Play;
}

void ASabotageGameMode::PickCharacter(const int32& pickIdx, const int32& playerID)
{
	FSabotagePlayerInfo* playerInfo = playerInfoSet.Find(playerID);
	if (nullptr == playerInfo)
	{
		PRINT_ERROR("FSabotagePlayerInfo 얻을 수 없습니다. ASabotageGameMode::PickCharacter() 실패합니다. playerID=%d", playerID);
		return;
	}

	if (eState::Play != state)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_IS_NOT_PLAY_STATE);
		return;
	}

	if ((characterList.Num() <= pickIdx) || (-1 == characterList[pickIdx]))
	{
		playerInfo->playerController->FromServer_PickCharacter(pickIdx, -1, -1, playerID);
		return;
	}

	if (0 <= playerInfo->characterCardIdx)
	{
		playerInfo->playerController->FromServer_PickCharacter(pickIdx, -1, -1, playerID);
		return;
	}

	// TODO Level 2s: Try Lock?
	int32 cardIdx = characterList[pickIdx];
	int32 fakeCardIdx = fakeCharacterList[pickIdx];
	characterList[pickIdx] = -1;
	playerInfo->characterCardIdx = cardIdx;

	bool isAllPick = true;
	for (auto& info : playerInfoSet)
	{
		if (playerID == info.playerID)
		{
			// 어떤 캐릭터 카드를 선택했는지 알려준다.
			info.playerController->FromServer_PickCharacter(pickIdx, cardIdx, fakeCardIdx, playerID);
		}
		else
		{
			// 다른 플레이어에게는 가짜로 알려준다.
			info.playerController->FromServer_PickCharacter(pickIdx, -1, fakeCardIdx, playerID);
		}

		if (info.characterCardIdx < 0)
		{
			isAllPick = false;
		}
	}

	// 아직 선택 안한 플레이어가 있으면, 대기한다.
	if (false == isAllPick)
	{
		return;
	}

	characterList.Empty();
	fakeCharacterList.Empty();

	TArray<int32> handArr;
	for (auto& info : playerInfoSet)
	{
		handArr.Reset();
		for (int32& handCardIdx : info.hand)
		{
			handArr.Emplace(handCardIdx);
		}

		// 각자의 초기 핸드를 알려준다.
		info.playerController->FromServer_InitHand(handArr);
		for (auto& otherInfo : playerInfoSet)
		{
			otherInfo.playerController->FromServer_UpdateHandCount(info.hand.Num(), info.playerID);
		}
	}

	// 첫 번째 턴을 시작한다.
	ResetTurnOrder();
}

void ASabotageGameMode::AddTunnelCard(const int32& cardIdx, const FIntPoint& point, const int32& playerID)
{
	//PRINT("ASabotageGameMode::AddTunnelCard() cardIdx=%d, pointX=%d, pointY=%d playerID=%d", cardIdx, point.X, point.Y, playerID);

	FSabotagePlayerInfo* playerInfo = playerInfoSet.Find(playerID);
	if (nullptr == playerInfo)
	{
		PRINT_ERROR("FSabotagePlayerInfo 얻을 수 없습니다. ASabotageGameMode::AddTunnelCard() 실패합니다. playerID=%d", playerID);
		return;
	}

	if (eState::Play != state)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_IS_NOT_PLAY_STATE);
		return;
	}
	
	if ((turnIndex < 0) || (playerID != turnOrder[turnIndex]))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_IS_NOT_YOUR_TURN);
		return;
	}


	if (true == board.Contains(point))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_FIND_CARD);
		return;
	}

	if (cardList.Num() <= cardIdx)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_FIND_CARD);
		return;
	}

	UTunnelCard* card = Cast<UTunnelCard>(cardList[cardIdx]);
	if (nullptr == card)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_USE_CARD);
		return;
	}
	
	FIntPoint neighborPoint;
	TMap<FIntPoint, int32> closedArrivalPointCard;
	bool isWin = false;

	neighborPoint.X = point.X;
	neighborPoint.Y = point.Y - 1;
	if (true == board.Contains(neighborPoint))
	{
		int32* pNeighborCardIdx = board.Find(neighborPoint);
		UTunnelCard* neighborCard = Cast<UTunnelCard>(cardList[*pNeighborCardIdx]);
		if (nullptr != neighborCard)
		{
			if (true == neighborCard->GetIsOpened())
			{
				// 한쪽은 열려있고, 한쪽은 닫혀있으면 놓을 수 없음.
				if (neighborCard->GetIsEnableBottom() != card->GetIsEnableTop())
				{
					playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_ADD_TUNNEL_CARD);
					return;
				}
			}

			if (true == neighborCard->IsA(UArrivalPointCard::StaticClass()))
			{
				UArrivalPointCard* arrivalPointCard = Cast<UArrivalPointCard>(neighborCard);
				//PRINT("위쪽 도착지 카드 isFake=%d, isOpened=%d", arrivalPointCard->GetIsFake(), arrivalPointCard->GetIsOpened());
				
				if ((false == arrivalPointCard->GetIsFake())
					&& (false == card->GetIsBlocked())
					&& (card->GetIsEnableTop() == arrivalPointCard->GetIsEnableBottom()))
				{
					// 진짜 목적지 카드와 연결되었다. 승리.
					isWin = true;
				}

				if (false == arrivalPointCard->GetIsOpened())
				{
					arrivalPointCard->SetIsOpened(true);
					closedArrivalPointCard.Emplace(neighborPoint, *pNeighborCardIdx);
					if (true == arrivalPointCard->GetIsFake())
					{
						// TODO Level 2: 가짜 목적지 카드가 처음 열리면, 주변 길과 이어지도록 조정해야 한다. (불가능하면 그대로 둔다)
					}
				}
			}
		}
	}

	neighborPoint.X = point.X;
	neighborPoint.Y = point.Y + 1;
	if (true == board.Contains(neighborPoint))
	{
		int32* pNeighborCardIdx = board.Find(neighborPoint);
		UTunnelCard* neighborCard = Cast<UTunnelCard>(cardList[*pNeighborCardIdx]);
		if (nullptr != neighborCard)
		{
			if (true == neighborCard->GetIsOpened())
			{
				// 한쪽은 열려있고, 한쪽은 닫혀있으면 놓을 수 없음.
				if (neighborCard->GetIsEnableTop() != card->GetIsEnableBottom())
				{
					playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_ADD_TUNNEL_CARD);
					return;
				}
			}

			if (true == neighborCard->IsA(UArrivalPointCard::StaticClass()))
			{
				UArrivalPointCard* arrivalPointCard = Cast<UArrivalPointCard>(neighborCard);
				//PRINT("아래쪽 도착지 카드 isFake=%d, isOpened=%d", arrivalPointCard->GetIsFake(), arrivalPointCard->GetIsOpened());
				
				if ((false == arrivalPointCard->GetIsFake())
					&& (false == card->GetIsBlocked())
					&& (card->GetIsEnableBottom() == arrivalPointCard->GetIsEnableTop()))
				{
					// 진짜 목적지 카드와 연결되었다. 승리.
					isWin = true;
				}

				if (false == arrivalPointCard->GetIsOpened())
				{
					arrivalPointCard->SetIsOpened(true);
					closedArrivalPointCard.Emplace(neighborPoint, *pNeighborCardIdx);
					if (true == arrivalPointCard->GetIsFake())
					{
						// TODO Level 2: 가짜 목적지 카드가 처음 열리면, 주변 길과 이어지도록 조정해야 한다. (불가능하면 그대로 둔다)
					}
				}
			}
		}
	}

	neighborPoint.X = point.X - 1;
	neighborPoint.Y = point.Y;
	//PRINT("왼쪽 확인, neighborPointX=%d, neighborPointY=%d", neighborPoint.X, neighborPoint.Y);
	if (true == board.Contains(neighborPoint))
	{
		//PRINT("왼쪽 카드 있음, neighborPointX=%d, neighborPointY=%d", neighborPoint.X, neighborPoint.Y);
		int32* pNeighborCardIdx = board.Find(neighborPoint);
		UTunnelCard* neighborCard = Cast<UTunnelCard>(cardList[*pNeighborCardIdx]);
		if (nullptr != neighborCard)
		{
			//PRINT("왼쪽 카드 정보도 있음, neighborCardIdx=%d, neighborCardRight=%d, cardLeft=%d", *neighborCardIdx, neighborCard->GetIsEnableRight(), card->GetIsEnableLeft());
			if (true == neighborCard->GetIsOpened())
			{
				// 한쪽은 열려있고, 한쪽은 닫혀있으면 놓을 수 없음.
				if (neighborCard->GetIsEnableRight() != card->GetIsEnableLeft())
				{
					playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_ADD_TUNNEL_CARD);
					return;
				}
			}

			if (true == neighborCard->IsA(UArrivalPointCard::StaticClass()))
			{
				UArrivalPointCard* arrivalPointCard = Cast<UArrivalPointCard>(neighborCard);
				//PRINT("왼쪽 도착지 카드 isFake=%d, isOpened=%d", arrivalPointCard->GetIsFake(), arrivalPointCard->GetIsOpened());
				
				if ((false == arrivalPointCard->GetIsFake())
					&& (false == card->GetIsBlocked())
					&& (card->GetIsEnableLeft() == arrivalPointCard->GetIsEnableRight()))
				{
					// 진짜 목적지 카드와 연결되었다. 승리.
					isWin = true;
				}

				if (false == arrivalPointCard->GetIsOpened())
				{
					arrivalPointCard->SetIsOpened(true);
					closedArrivalPointCard.Emplace(neighborPoint, *pNeighborCardIdx);
					if (true == arrivalPointCard->GetIsFake())
					{
						// TODO Level 2: 가짜 목적지 카드가 처음 열리면, 주변 길과 이어지도록 조정해야 한다. (불가능하면 그대로 둔다)
					}
				}
			}
		}
	}

	neighborPoint.X = point.X + 1;
	neighborPoint.Y = point.Y;
	if (true == board.Contains(neighborPoint))
	{
		int32* pNeighborCardIdx = board.Find(neighborPoint);
		UTunnelCard* neighborCard = Cast<UTunnelCard>(cardList[*pNeighborCardIdx]);
		if (nullptr != neighborCard)
		{
			if (true == neighborCard->GetIsOpened())
			{
				// 한쪽은 열려있고, 한쪽은 닫혀있으면 놓을 수 없음.
				if (neighborCard->GetIsEnableLeft() != card->GetIsEnableRight())
				{
					playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_ADD_TUNNEL_CARD);
					return;
				}
			}

			if (true == neighborCard->IsA(UArrivalPointCard::StaticClass()))
			{
				UArrivalPointCard* arrivalPointCard = Cast<UArrivalPointCard>(neighborCard);
				//PRINT("오른쪽 도착지 카드 isFake=%d, isOpened=%d", arrivalPointCard->GetIsFake(), arrivalPointCard->GetIsOpened());
				
				if ((false == arrivalPointCard->GetIsFake())
					&& (false == card->GetIsBlocked())
					&& (card->GetIsEnableRight() == arrivalPointCard->GetIsEnableLeft()))
				{
					// 진짜 목적지 카드와 연결되었다. 승리.
					isWin = true;
				}

				if (false == arrivalPointCard->GetIsOpened())
				{
					arrivalPointCard->SetIsOpened(true);
					closedArrivalPointCard.Emplace(neighborPoint, *pNeighborCardIdx);
					if (true == arrivalPointCard->GetIsFake())
					{
						// TODO Level 2: 가짜 목적지 카드가 처음 열리면, 주변 길과 이어지도록 조정해야 한다. (불가능하면 그대로 둔다)
					}
				}
			}
		}
	}

	// 보드에 카드를 추가하고, 핸드에서는 카드를 제거한다.
	playerInfo->hand.Remove(cardIdx);
	board.Emplace(point, cardIdx);

	card->SetIsOpened(true);
	
	if ((true == isWin) && (false == IsConnected()))
	{
		PRINT_ERROR("%s", TEXT("목적지 카드와 연결됬지만 검증을 통과하지 못했습니다. 버그가 있거나, 클라가 조작된 상황입니다. 리포트해야 합니다."));
		playerInfo->hand.Emplace(cardIdx);
		board.Remove(point);
		card->SetIsOpened(false);

		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_ADD_TUNNEL_CARD);
		return;
	}

	for (auto& info : playerInfoSet)
	{
		// 보드에 카드가 배치됬음을 알린다.
		info.playerController->FromServer_AddTunnelCard(cardIdx, point, playerID);

		// 닫혀있던 목적지 카드를 만났다면 공개한다.
		for (auto& keyValue : closedArrivalPointCard)
		{
			info.playerController->FromServer_OpenArrivalPointCard(keyValue.Value, keyValue.Key);
		}
	}

	if (true == isWin)
	{
		EndGame(false);
		return;
	}

	EndTurn(playerInfo);
}

void ASabotageGameMode::TurnOverArrivalPointCard(const int32& cardIdx, const FIntPoint& point, const int32& playerID)
{
	//PRINT("ASabotageGameMode::TurnOverArrivalPointCard() cardIdx=%d, pointX=%d, pointY=%d, playerID=%d", cardIdx, point.X, point.Y, playerID);

	FSabotagePlayerInfo* playerInfo = playerInfoSet.Find(playerID);
	if (nullptr == playerInfo)
	{
		PRINT_ERROR("FSabotagePlayerInfo 얻을 수 없습니다. ASabotageGameMode::TurnOverArrivalPointCard() 실패합니다. playerID=%d", playerID);
		return;
	}

	if (eState::Play != state)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_IS_NOT_PLAY_STATE);
		return;
	}

	if ((turnIndex < 0) || (playerID != turnOrder[turnIndex]))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_IS_NOT_YOUR_TURN);
		return;
	}

	if (false == playerInfo->hand.Contains(cardIdx))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_FIND_CARD);
		return;
	}

	if ((cardIdx < 0) || (cardList.Num() <= cardIdx))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_FIND_CARD);
		return;
	}

	if (false == cardList[cardIdx]->IsA(UMapCard::StaticClass()))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_USE_CARD);
		return;
	}

	if (false == board.Contains(point))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_FIND_CARD);
		return;
	}

	int32* pCardIdx = board.Find(point);
	if (false == cardList[*pCardIdx]->IsA(UArrivalPointCard::StaticClass()))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_USE_CARD);
		return;
	}

	playerInfo->hand.Remove(cardIdx);

	for (auto& info : playerInfoSet)
	{
		// 도착지 카드를 확인했다고 알린다.
		info.playerController->FromServer_TurnOverArrivalPointCard(cardIdx, point, playerID);
	}

	EndTurn(playerInfo);
}

void ASabotageGameMode::RemoveBoardCard(const int32& rockfallCardIdx, const FIntPoint& point, const int32& playerID)
{
	//PRINT("ASabotageGameMode::RemoveBoardCard() rockfallCardIdx=%d, pointX=%d, pointY=%d, playerID=%d", rockfallCardIdx, point.X, point.Y, playerID);

	FSabotagePlayerInfo* playerInfo = playerInfoSet.Find(playerID);
	if (nullptr == playerInfo)
	{
		PRINT_ERROR("FSabotagePlayerInfo 얻을 수 없습니다. ASabotageGameMode::RemoveBoardCard() 실패합니다. playerID=%d", playerID);
		return;
	}

	if (eState::Play != state)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_IS_NOT_PLAY_STATE);
		return;
	}

	if ((turnIndex < 0) || (playerID != turnOrder[turnIndex]))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_IS_NOT_YOUR_TURN);
		return;
	}

	if (false == board.Contains(point))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_FIND_CARD);
		return;
	}

	if (false == playerInfo->hand.Contains(rockfallCardIdx))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_FIND_CARD);
		return;
	}

	if (false == cardList[rockfallCardIdx]->IsA(URockfallCard::StaticClass()))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_USE_CARD);
		return;
	}

	int32* pCardIdx = board.Find(point);
	if ((true == cardList[*pCardIdx]->IsA(UStartingPointCard::StaticClass()))
		|| (true == cardList[*pCardIdx]->IsA(UArrivalPointCard::StaticClass())))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_USE_CARD);
		return;
	}

	board.Remove(point);
	playerInfo->hand.Remove(rockfallCardIdx);

	for (auto& info : playerInfoSet)
	{
		// 보드에서 카드가 제거됬음을 알린다.
		if (playerID == info.playerID)
		{
			info.playerController->FromServer_RemoveBoardCard(rockfallCardIdx, point);
		}
		else
		{
			info.playerController->FromServer_RemoveBoardCard(-1, point);
		}
	}

	EndTurn(playerInfo);
}

void ASabotageGameMode::RemoveHandCard(const int32& cardIdx, const int32& playerID)
{
	//PRINT("ASabotageGameMode::RemoveHandCard() cardIdx=%d, playerID=%d", cardIdx, playerID);

	FSabotagePlayerInfo* playerInfo = playerInfoSet.Find(playerID);
	if (nullptr == playerInfo)
	{
		PRINT_ERROR("FSabotagePlayerInfo 얻을 수 없습니다. ASabotageGameMode::RemoveHandCard() 실패합니다. playerID=%d", playerID);
		return;
	}

	if (eState::Play != state)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_IS_NOT_PLAY_STATE);
		return;
	}

	if ((turnIndex < 0) || (playerID != turnOrder[turnIndex]))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_IS_NOT_YOUR_TURN);
		return;
	}

	if (false == playerInfo->hand.Contains(cardIdx))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_FIND_CARD);
		return;
	}

	playerInfo->hand.Remove(cardIdx);
	playerInfo->playerController->FromServer_RemoveHandCard(cardIdx);

	EndTurn(playerInfo);
}

void ASabotageGameMode::BreakEquipment(const int32& breakCardIdx, const int32& targetPlayerID, const int32& playerID)
{
	//PRINT("ASabotageGameMode::BreakEquipment() breakCardIdx=%d, targetPlayerID=%d, playerID=%d", breakCardIdx, targetPlayerID, playerID);

	FSabotagePlayerInfo* playerInfo = playerInfoSet.Find(playerID);
	if (nullptr == playerInfo)
	{
		PRINT_ERROR("FSabotagePlayerInfo 얻을 수 없습니다. ASabotageGameMode::BreakEquipment() 실패합니다. playerID=%d", playerID);
		return;
	}

	if (eState::Play != state)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_IS_NOT_PLAY_STATE);
		return;
	}

	if ((turnIndex < 0) || (playerID != turnOrder[turnIndex]))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_IS_NOT_YOUR_TURN);
		return;
	}

	if (playerID == targetPlayerID)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_USE_CARD);
		return;
	}

	if (false == playerInfo->hand.Contains(breakCardIdx))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_FIND_CARD);
		return;
	}

	if ((breakCardIdx < 0) || (cardList.Num() <= breakCardIdx))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_FIND_CARD);
		return;
	}

	UBreakCard* breakCard = Cast<UBreakCard>(cardList[breakCardIdx]);
	if (nullptr == breakCard)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_USE_CARD);
		return;
	}

	FSabotagePlayerInfo* targetPlayerInfo = playerInfoSet.Find(targetPlayerID);
	if (nullptr == targetPlayerInfo)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_FIND_TARGET_PLAYER);
		return;
	}

	if ((true == breakCard->GetIsPick()) && (false == targetPlayerInfo->isEnablePick))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_USE_CARD);
		return;
	}

	if ((true == breakCard->GetIsLantern()) && (false == targetPlayerInfo->isEnableLantern))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_USE_CARD);
		return;
	}

	if ((true == breakCard->GetIsWagon()) && (false == targetPlayerInfo->isEnableWagon))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_USE_CARD);
		return;
	}

	playerInfo->hand.Remove(breakCardIdx);
	playerInfo->playerController->FromServer_RemoveHandCard(breakCardIdx);

	if (true == breakCard->GetIsPick())
	{
		targetPlayerInfo->isEnablePick = false;
	}
	if (true == breakCard->GetIsLantern())
	{
		targetPlayerInfo->isEnableLantern = false;
	}
	if (true == breakCard->GetIsWagon())
	{
		targetPlayerInfo->isEnableWagon = false;
	}

	for (auto& info : playerInfoSet)
	{
		// 변경된 장비 상황 알려준다.
		info.playerController->FromServer_UpdateEquipment(targetPlayerInfo->isEnablePick, targetPlayerInfo->isEnableLantern, targetPlayerInfo->isEnableWagon, targetPlayerID);
	}

	EndTurn(playerInfo);
}

void ASabotageGameMode::RepairEquipment(const int32& repairCardIdx, const int32& targetPlayerID, const int32& playerID)
{
	//PRINT("ASabotageGameMode::RepairEquipment(), repairCardIdx=%d, targetPlayerID=%d, playerID=%d", repairCardIdx, targetPlayerID, playerID);

	FSabotagePlayerInfo* playerInfo = playerInfoSet.Find(playerID);
	if (nullptr == playerInfo)
	{
		PRINT_ERROR("FSabotagePlayerInfo 얻을 수 없습니다. ASabotageGameMode::RepairEquipment() 실패합니다. playerID=%d", playerID);
		return;
	}

	if (eState::Play != state)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_IS_NOT_PLAY_STATE);
		return;
	}

	if ((turnIndex < 0) || (playerID != turnOrder[turnIndex]))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_IS_NOT_YOUR_TURN);
		return;
	}

	if (false == playerInfo->hand.Contains(repairCardIdx))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_FIND_CARD);
		return;
	}

	if ((repairCardIdx < 0) || (cardList.Num() <= repairCardIdx))
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_FIND_CARD);
		return;
	}

	URepairCard* repairCard = Cast<URepairCard>(cardList[repairCardIdx]);
	if (nullptr == repairCard)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_USE_CARD);
		return;
	}

	FSabotagePlayerInfo* targetPlayerInfo = playerInfoSet.Find(targetPlayerID);
	if (nullptr == targetPlayerInfo)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_FIND_TARGET_PLAYER);
		return;
	}

	bool isCanRepair = false;
	if ( ((true == repairCard->GetIsPick()) && (false == targetPlayerInfo->isEnablePick))
		|| ((true == repairCard->GetIsLantern()) && (false == targetPlayerInfo->isEnableLantern))
		|| ((true == repairCard->GetIsWagon()) && (false == targetPlayerInfo->isEnableWagon)) )
	{
		isCanRepair = true;
	}

	if (false == isCanRepair)
	{
		playerInfo->playerController->FromServer_SetMessage(MESSAGE_CANT_USE_CARD);
		return;
	}

	playerInfo->hand.Remove(repairCardIdx);
	playerInfo->playerController->FromServer_RemoveHandCard(repairCardIdx);

	if (true == repairCard->GetIsPick())
	{
		targetPlayerInfo->isEnablePick = true;
	}
	if (true == repairCard->GetIsLantern())
	{
		targetPlayerInfo->isEnableLantern = true;
	}
	if (true == repairCard->GetIsWagon())
	{
		targetPlayerInfo->isEnableWagon = true;
	}

	for (auto& info : playerInfoSet)
	{
		// 변경된 장비 상황 알려준다.
		info.playerController->FromServer_UpdateEquipment(targetPlayerInfo->isEnablePick, targetPlayerInfo->isEnableLantern, targetPlayerInfo->isEnableWagon, targetPlayerID);
	}

	EndTurn(playerInfo);
}

bool ASabotageGameMode::IsConnected()
{
	FIntPoint point(0, 0);
	FIntPoint tempPoint(0, 0);

	int32* pCardIdx = board.Find(point);
	int32* pTempCardIdx = nullptr;
	if (nullptr == pCardIdx)
	{
		PRINT_ERROR("%s", TEXT("출발지 카드를 찾을 수 없습니다. ASabotageGameMode::IsConnected() 실패합니다."));
		return false;
	}

	UTunnelCard* card = Cast<UTunnelCard>(cardList[*pCardIdx]);
	UTunnelCard* tempCard = nullptr;
	if (nullptr == card)
	{
		PRINT_ERROR("%s", TEXT("출발지 카드 정보를 찾을 수 없습니다. ASabotageGameMode::IsConnected() 실패합니다."));
		return false;
	}

	int32 maxVisitPointCount = board.Num() - 1;
	TSet<FIntPoint> visitPoint;					// 한번이라도 방문한 지점.
	TArray<FIntPoint> watingForVisitPoint;		// 다시 돌아가서 방문을 이어가야 하는 지점. (스택으로 사용한다)

	do
	{
		if (true == card->GetIsEnableRight())
		{
			tempPoint.X = point.X + 1;
			tempPoint.Y = point.Y;
			if (false == visitPoint.Contains(tempPoint))
			{
				pTempCardIdx = board.Find(tempPoint);
				if (nullptr != pTempCardIdx)
				{
					tempCard = Cast<UTunnelCard>(cardList[*pTempCardIdx]);
					if (nullptr != tempCard)
					{
						if (true == tempCard->IsA(UArrivalPointCard::StaticClass()))
						{
							UArrivalPointCard* arrivalPointCard = Cast<UArrivalPointCard>(tempCard);
							if (false == arrivalPointCard->GetIsFake())
							{
								return true;
							}
						}

						if (false == tempCard->GetIsBlocked())
						{
							visitPoint.Emplace(point);
							watingForVisitPoint.Emplace(point);
						
							point = tempPoint;
							card = tempCard;
							continue;
						}
					}
				}
			}
		}

		if (true == card->GetIsEnableTop())
		{
			tempPoint.X = point.X;
			tempPoint.Y = point.Y - 1;
			if (false == visitPoint.Contains(tempPoint))
			{
				pTempCardIdx = board.Find(tempPoint);
				if (nullptr != pTempCardIdx)
				{
					tempCard = Cast<UTunnelCard>(cardList[*pTempCardIdx]);
					if (nullptr != tempCard)
					{
						if (true == tempCard->IsA(UArrivalPointCard::StaticClass()))
						{
							UArrivalPointCard* arrivalPointCard = Cast<UArrivalPointCard>(tempCard);
							if (false == arrivalPointCard->GetIsFake())
							{
								return true;
							}
						}

						if (false == tempCard->GetIsBlocked())
						{
							visitPoint.Emplace(point);
							watingForVisitPoint.Emplace(point);

							point = tempPoint;
							card = tempCard;
							continue;
						}
					}
				}
			}
		}

		if (true == card->GetIsEnableBottom())
		{
			tempPoint.X = point.X;
			tempPoint.Y = point.Y + 1;
			if (false == visitPoint.Contains(tempPoint))
			{
				pTempCardIdx = board.Find(tempPoint);
				if (nullptr != pTempCardIdx)
				{
					tempCard = Cast<UTunnelCard>(cardList[*pTempCardIdx]);
					if (nullptr != tempCard)
					{
						if (true == tempCard->IsA(UArrivalPointCard::StaticClass()))
						{
							UArrivalPointCard* arrivalPointCard = Cast<UArrivalPointCard>(tempCard);
							if (false == arrivalPointCard->GetIsFake())
							{
								return true;
							}
						}

						if (false == tempCard->GetIsBlocked())
						{
							visitPoint.Emplace(point);
							watingForVisitPoint.Emplace(point);

							point = tempPoint;
							card = tempCard;
							continue;
						}
					}
				}
			}
		}

		if (true == card->GetIsEnableLeft())
		{
			tempPoint.X = point.X - 1;
			tempPoint.Y = point.Y;
			if (false == visitPoint.Contains(tempPoint))
			{
				pTempCardIdx = board.Find(tempPoint);
				if (nullptr != pTempCardIdx)
				{
					tempCard = Cast<UTunnelCard>(cardList[*pTempCardIdx]);
					if (nullptr != tempCard)
					{
						if (true == tempCard->IsA(UArrivalPointCard::StaticClass()))
						{
							UArrivalPointCard* arrivalPointCard = Cast<UArrivalPointCard>(tempCard);
							if (false == arrivalPointCard->GetIsFake())
							{
								return true;
							}
						}

						if (false == tempCard->GetIsBlocked())
						{
							visitPoint.Emplace(point);
							watingForVisitPoint.Emplace(point);

							point = tempPoint;
							card = tempCard;
							continue;
						}
					}
				}
			}
		}

		if (0 == watingForVisitPoint.Num())
		{
			return false;
		}

		visitPoint.Emplace(point);
		
		point = watingForVisitPoint.Pop();
		card = Cast<UTunnelCard>(cardList[*board.Find(point)]);
	}
	while (visitPoint.Num() < maxVisitPointCount);

	return false;
}

void ASabotageGameMode::EndTurn(FSabotagePlayerInfo* playerInfo)
{
	// 턴을 마친 플레이어에게 카드를 한장 뽑아서 준다.
	int32 drawCardIdx = -1;
	if (nullptr != playerInfo)
	{
		tunnelAndActionCardQueue.Dequeue(drawCardIdx);
		playerInfo->playerController->FromServer_AddHandCard(drawCardIdx);
	}

	if (drawCardIdx < 0)
	{
		// 덱에 남은 카드가 없고, 각 플레이어의 핸드도 말랐다면 방해꾼의 승리가 된다.
		bool isSaboteurWin = true;
		for (auto& info : playerInfoSet)
		{
			if (0 < info.hand.Num())
			{
				isSaboteurWin = false;
				break;
			}
		}

		if (true == isSaboteurWin)
		{
			EndGame(true);
			return;
		}
	}
	else
	{
		if (nullptr != playerInfo)
		{
			playerInfo->hand.Emplace(drawCardIdx);
		}
	}

	if (nullptr != playerInfo)
	{
		for (auto& info : playerInfoSet)
		{
			// 변경된 핸드 갯수를 알려준다.
			info.playerController->FromServer_UpdateHandCount(playerInfo->hand.Num(), playerInfo->playerID);
		}
	}

	// 턴을 넘긴다.
	SetNextTurn();
}

void ASabotageGameMode::EndGame(const bool isSaboteurWin)
{
	// 더 이상 진행하지 못하도록 설정한다.
	state = eState::Wait;

	// 각 플레이어의 승/패를 알린다.
	for (auto& info : playerInfoSet)
	{
		if (0 <= info.characterCardIdx)
		{
			UCharacterCard* characterCard = Cast<UCharacterCard>(cardList[info.characterCardIdx]);
			if (nullptr != characterCard)
			{
				info.playerController->FromServer_EndGame(isSaboteurWin, characterCard->GetIsSaboteur());
			}
		}
	}
}

void ASabotageGameMode::ResetTurnOrder()
{
	turnOrder.Reset();
	for (auto& info : playerInfoSet)
	{
		turnOrder.Emplace(info.playerID);
	}

	// 턴 순서를 정한다. (별다른 룰이 없으므로 랜덤으로 한다)
	turnOrder.Sort([this](const int32 left, const int32 right) {
		return (FMath::FRand() < 0.5f);
	});

	turnIndex = -1;
	SetNextTurn();
}

void ASabotageGameMode::SetNextTurn()
{
	if (0 == turnOrder.Num())
	{
		return;
	}

	++turnIndex;
	if (turnOrder.Num() <= turnIndex)
	{
		turnIndex = 0;
	}

	// 다음 턴 플레이어의 핸드가 비었다면, 다시 찾는다. (재귀 막기위해 누군가 핸드에 카드가 있는지 확인한다)
	FSabotagePlayerInfo* playerInfo = playerInfoSet.Find(turnOrder[turnIndex]);
	if (nullptr == playerInfo)
	{
		PRINT_ERROR("FSabotagePlayerInfo 얻을 수 없습니다. ASabotageGameMode::SetNextTurn() 실패합니다. playerID=%d", turnOrder[turnIndex]);
		return;
	}
	
	if (0 == playerInfo->hand.Num())
	{
		for (auto& info : playerInfoSet)
		{
			if (0 < info.hand.Num())
			{
				SetNextTurn();
				return;
			}
		}

		PRINT_ERROR("모든 플레이어의 핸드가 비었는데 턴을 바꾸려고 합니다. EndGame 체크가 안된 상황입니다. 반드시 확인해야 합니다. ASabotageGameMode::SetNextTurn() 실패합니다. turnIndex=%d", turnIndex);
		return;
	}

	playerInfo->playerController->FromServer_PlayTurn();
}