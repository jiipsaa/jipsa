﻿#include "SabotageWindow.h"

#include "../../Jipsa/Log.h"
#include "Data/SabotageData.h"
#include "SabotagePlayerController.h"

#include "Components/CanvasPanelSlot.h"
#include "Engine/Texture2D.h"
#include "Kismet/GameplayStatics.h"
//#include "Runtime/Engine/Classes/Engine/UserInterfaceSettings.h"

using namespace NSSabotage;

void USabotageWindow::Reset()
{
	int32 cardIdx;
	USabotageCard* card = nullptr;
	UAddPointCard* addPointCard = nullptr;
	USabotageCardWidget* widget = nullptr;
	for (auto& keyValue : board)
	{
		if (keyValue.Value < 0)
		{
			cardIdx = (-1 * keyValue.Value) - 1;
			if ((cardIdx < 0) || (addPointCards.Num() <= cardIdx))
			{
				continue;
			}

			addPointCard = addPointCards[cardIdx];
			if (nullptr == addPointCard)
			{
				continue;
			}

			widget = addPointCard->GetWidget();
			if (nullptr == widget)
			{
				continue;
			}
			
			EnqueueCardWidget(widget);
			EnqueueAddPointCard(cardIdx);
		}
		else
		{
			cardIdx = keyValue.Value;
			if ((cardIdx < 0) || (cardList.Num() <= cardIdx))
			{
				continue;
			}

			widget = cardList[cardIdx]->GetWidget();
			if (nullptr == widget)
			{
				continue;
			}

			EnqueueCardWidget(widget);
		}
	}
	board.Reset();

	for (auto& i : hand)
	{
		if ((i < 0) || (cardList.Num() <= i))
		{
			continue;
		}

		widget = cardList[i]->GetWidget();
		if (nullptr == widget)
		{
			continue;
		}

		EnqueueCardWidget(widget);
	}
	hand.Reset();
	selectedHandCardIdx = -1;

	for (auto& c : characterPickCards)
	{
		c->RemoveFromRoot();
	}
	characterPickCards.Reset();

	for (int32 i = characterPickPanel->GetChildrenCount() - 1; 0 <= i; --i)
	{
		widget = Cast<USabotageCardWidget>(characterPickPanel->GetChildAt(i));
		if (nullptr != widget)
		{
			//EnqueueCardWidget(widget);
			widget->RemoveFromParent();
		}
	}
	characterPickPanelRoot->SetVisibility(ESlateVisibility::Collapsed);

	for (auto& c : cardList)
	{
		c->SetIsOpened(false);
	}

	int32 num = playerInfoList.Num();
	for (int32 i = 0; i < num; ++i)
	{
		playerInfoList[i].Reset(playerInfoList[i].playerID, 0, true, true, true);

		playerCardWidgets[i]->backImage->SetVisibility(ESlateVisibility::Visible);
		playerInfoList[i].UpateTextBlock(playerInfoTexts[i], true);
	}
	myCardIdx = -1;
	myFakeCardIdx = -1;
}

void USabotageWindow::AddPlayerInfo(const int32& playerID, const int32& handCount, const bool& isEnablePick, const bool& isEnableLantern, const bool& isEnableWagon)
{
	int32 index = playerInfoList.IndexOfByKey(playerID);
	if (INDEX_NONE == index)
	{
		index = playerInfoList.IndexOfByKey(-1);
	}

	if (INDEX_NONE == index)
	{
		index = playerInfoList.Num();
		playerInfoList.Emplace(playerID, handCount, isEnablePick, isEnableLantern, isEnableWagon);
	}
	else
	{
		playerInfoList[index].Reset(playerID, handCount, isEnablePick, isEnableLantern, isEnableWagon);
	}

	playerCardWidgets[index]->backImage->SetBrushFromTexture(SabotageData::GetSingleton()->characterBackTexture);
	playerCardWidgets[index]->SetVisibility(ESlateVisibility::Visible);
	playerCardWidgets[index]->SetIsEnabled(true);

	playerInfoList[index].UpateTextBlock(playerInfoTexts[index], true);
	playerInfoTexts[index]->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
}

void USabotageWindow::RemovePlayerInfo(const int32& playerID)
{
	int32 index = playerInfoList.IndexOfByKey(playerID);
	if (INDEX_NONE == index)
	{
		PRINT_WARNING("알 수 없는 플레이어가 로그아웃 했습니다. USabotageWindow::RemovePlayerInfo() 실패합니다. playerID=%d", playerID);
		return;
	}

	playerInfoList[index].playerID = -1;

	playerCardWidgets[index]->SetIsEnabled(false);
	playerInfoTexts[index]->SetVisibility(ESlateVisibility::Collapsed);
}

void USabotageWindow::OpenCharacterCard(const int32& cardIdx, const int32& fakeCardIdx, const int32& playerID, const bool& isMine)
{
	PRINT("OpenCharacterCard, cardIdx=%d, fakeCardIdx=%d, playerID=%d, isMine=%d", cardIdx, fakeCardIdx, playerID, isMine);

	int32 index = playerInfoList.IndexOfByKey(playerID);
	if (INDEX_NONE == index)
	{
		PRINT_WARNING("알 수 없는 플레이어입니다. USabotageWindow::OpenCharacterCard() 실패합니다. playerID=%d", playerID);
		return;
	}

	if ((fakeCardIdx < 0) || (cardList.Num() <= fakeCardIdx))
	{
		PRINT_WARNING("잘못된 인덱스 입니다. USabotageWindow::OpenCharacterCard() 실패합니다. cardIdx=%d", fakeCardIdx);
		return;
	}

	if (true == isMine)
	{
		if ((cardIdx < 0) || (cardList.Num() <= cardIdx))
		{
			PRINT_WARNING("잘못된 인덱스 입니다. USabotageWindow::OpenCharacterCard() 실패합니다. cardIdx=%d", cardIdx);
			return;
		}

		if ((0 < myCardIdx) || (0 < myFakeCardIdx))
		{
			PRINT_WARNING("이미 내 캐릭터 카드가 셋팅됬는데, 다시 셋팅하려고 합니다. USabotageWindow::OpenCharacterCard() 실패합니다. myCardIdx=%d, cardIdx=%d", myCardIdx, cardIdx);
			return;
		}

		myCardIdx = cardIdx;
		myFakeCardIdx = fakeCardIdx;

		int32 num = playerInfoList.Num();
		for (int32 i = 0; i < num; ++i)
		{
			if ((index != i) && (myCardIdx == playerInfoList[i].fakeCardIdx))
			{
				// 내 진짜 캐릭터 카드가 70번 인데, 이미 다른 유저의 카드가 70번으로 셋팅되있으면, 내 가짜 캐릭터 카드로 다시 셋팅한다.
				playerInfoList[i].fakeCardIdx = myFakeCardIdx;
				cardList[myFakeCardIdx]->UpdateCardWidget(playerCardWidgets[i]);
				break;
			}
		}

		// 내껀 진짜 캐릭터 카드로 셋팅한다.
		playerInfoList[index].fakeCardIdx = myCardIdx;
		cardList[myCardIdx]->UpdateCardWidget(playerCardWidgets[index], true);
	}
	else
	{
		if (myCardIdx == fakeCardIdx)
		{
			// 내 진짜 캐릭터 카드(myCardIdx)가 70번 인데, 다른 플레이어의 가짜 캐릭터 카드가 70번 이라고 들어오면, 내 가짜 캐릭터 카드로 셋팅한다.
			playerInfoList[index].fakeCardIdx = myFakeCardIdx;
			cardList[myFakeCardIdx]->UpdateCardWidget(playerCardWidgets[index]);
		}
		else
		{
			// 다른 플레이어는 가짜 캐릭터 카드로 셋팅한다. (진짜 캐릭터 카드 번호는 게임 끝날때 서버가 알려준다)
			playerInfoList[index].fakeCardIdx = fakeCardIdx;
			cardList[fakeCardIdx]->UpdateCardWidget(playerCardWidgets[index]);
		}
	}
}

void USabotageWindow::UpdateHandCount(const int32& handCount, const int32& playerID)
{
	int32 index = playerInfoList.IndexOfByKey(playerID);
	if (INDEX_NONE == index)
	{
		PRINT_WARNING("알 수 없는 플레이어입니다. USabotageWindow::UpdateHandCount() 실패합니다. playerID=%d", playerID);
		return;
	}

	playerInfoList[index].handCount = handCount;
	playerInfoList[index].UpateTextBlock(playerInfoTexts[index]);
}

void USabotageWindow::UpdateEquipment(const bool& isEnablePick, const bool& isEnableLantern, const bool& isEnableWagon, const int32& targetPlayerID)
{
	int32 index = playerInfoList.IndexOfByKey(targetPlayerID);
	if (INDEX_NONE == index)
	{
		PRINT_WARNING("알 수 없는 플레이어입니다. USabotageWindow::UpdateEquipment() 실패합니다. targetPlayerID=%d", targetPlayerID);
		return;
	}

	FString equipments;
	bool isRepair = false;
	
	if (isEnablePick != playerInfoList[index].isEnablePick)
	{
		playerInfoList[index].isEnablePick = isEnablePick;
		
		equipments.Append(TEXT("곡괭이, "));
		
		if (true == isEnablePick)
		{
			isRepair = true;
		}
	}

	if (isEnableLantern != playerInfoList[index].isEnableLantern)
	{
		playerInfoList[index].isEnableLantern = isEnableLantern;
		
		equipments.Append(TEXT(" 랜턴, "));
		
		if (true == isEnableLantern)
		{
			isRepair = true;
		}
	}

	if (isEnableWagon != playerInfoList[index].isEnableWagon)
	{
		playerInfoList[index].isEnableWagon = isEnableWagon;
		
		equipments.Append(TEXT("수레, "));
		
		if (true == isEnableWagon)
		{
			isRepair = true;
		}
	}

	playerInfoList[index].UpateTextBlock(playerInfoTexts[index]);

	equipments.RemoveFromEnd(*FString(", "), ESearchCase::IgnoreCase);
	if (true == isRepair)
	{
		SetMessage(TCHAR_TO_UTF8(*FString::Printf(TEXT("%d 의 %s 고쳐졌습니다."), targetPlayerID, *equipments)));
	}
	else
	{
		SetMessage(TCHAR_TO_UTF8(*FString::Printf(TEXT("%d 의 %s 부서졌습니다."), targetPlayerID, *equipments)));
	}
}

// 카드가 놓이는 영역을 갱신한다.
bool USabotageWindow::IsChangedBoardSize(const int32& pointX, const int32& pointY)
{
	bool isChanged = false;

	if (pointX < minPointX)
	{
		minPointX = pointX;
		isChanged = true;
	}
	else if (maxPointX < pointX)
	{
		maxPointX = pointX;
		isChanged = true;
	}

	if (pointY < minPointY)
	{
		minPointY = pointY;
		isChanged = true;
	}
	else if (maxPointY < pointY)
	{
		maxPointY = pointY;
		isChanged = true;
	}

	return isChanged;
}

// 카드가 놓이는 영역에 맞게 Scale 과 Position 을 조정한다.
void USabotageWindow::FitBoardSize()
{
	{// 보드 위치 조정.
		UCanvasPanelSlot* boardSlot = Cast<UCanvasPanelSlot>(boardPanel->Slot);
		if (nullptr == boardSlot)
		{
			PRINT_ERROR("UCanvasPanelSlot 얻을 수 없습니다. USabotageWindow::FitBoardSize() 실패합니다. name=%s", *boardPanel->GetFName().ToString());
			return;
		}

		FVector2D position(
			(maxPointX + minPointX) * (cardSize.X * -0.5f)
			,(maxPointY + minPointY) * (cardSize.Y * -0.5f)
		);
		boardSlot->SetPosition(position);
	}

	{// 보드 크기 조정.
		float width = ((maxPointX - minPointX + 1) * cardSize.X);
		float height = ((maxPointY - minPointY + 1) * cardSize.Y);

		float newWidthScale = boardSize.X / width;
		float newHeightScale = boardSize.Y / height;
		if (newWidthScale < newHeightScale)
		{
			boardScaleBox->SetUserSpecifiedScale(newWidthScale * BOARD_SCALE_CORRECTION_VALUE);
		}
		else
		{
			boardScaleBox->SetUserSpecifiedScale(newHeightScale * BOARD_SCALE_CORRECTION_VALUE);
		}
	}
}

void USabotageWindow::InitBoard(const TArray<int32>& initialBoard)
{
	int num = initialBoard.Num();
	if (0 != (num % 3))
	{
		PRINT_WARNING("%s", TEXT("초기 배치가 pointX, pointY, cardIdx 묶음으로 오지 않았습니다. USabotageWindow::InitBoard() 실패합니다."));

		FString log;
		for (int32 i = 0; i < num; ++i)
		{
			log.Appendf(TEXT("%d, "), initialBoard[i]);
		}
		PRINT_WARNING("initialBoard: %s", *log);
		return;
	}

	int32 pointX, pointY, cardIdx;
	USabotageCard* card = nullptr;
	bool isChangedBoardSize = false;
	for (int32 i = 0; i < num;)
	{
		pointX = initialBoard[i++];
		pointY = initialBoard[i++];
		cardIdx = initialBoard[i++];
		//PRINT("[%d, %d] %d", pointX, pointY, cardIdx);

		card = cardList[cardIdx];

		if (true == IsChangedBoardSize(pointX, pointY))
		{
			isChangedBoardSize = true;
		}

		if (true == card->IsA(UStartingPointCard::StaticClass()))
		{
			AddCard(pointX, pointY, cardIdx);
		}
		else
		{
			AddCard(pointX, pointY, cardIdx, false);
		}
	}

	if (true == isChangedBoardSize)
	{
		FitBoardSize();
	}
}

void USabotageWindow::AddCard(const int32& pointX, const int32& pointY, const int32& cardIdx, const bool& isOpended)
{
	if (0 <= cardIdx)
	{
		if (cardList.Num() <= cardIdx)
		{
			PRINT_ERROR("카드 정보를 찾을 수 없습니다. USabotageWindow::AddCard() 실패합니다. cardIdx=%d, cardCnt=%d", cardIdx, cardList.Num());
			return;
		}
	}

	USabotageCardWidget* cardWidget = nullptr;

	FIntPoint point(pointX, pointY);
	if (true == board.Contains(point))
	{
		int32* pTempCardIdx = board.Find(point);
		if (0 <= *pTempCardIdx)
		{
			PRINT_ERROR("이미 카드가 배치되어 있습니다. USabotageWindow::AddCard() 실패합니다. cardIdx=%d, tempCardIdx=%d", cardIdx, *pTempCardIdx);
			return;
		}

		int32 addPointCardIdx = (-1 * (*pTempCardIdx)) - 1;
		if ((addPointCardIdx < 0) || (addPointCards.Num() <= addPointCardIdx))
		{
			PRINT_ERROR("잘못된 인덱스 입니다. USabotageWindow::AddCard() 실패합니다. idx=%d, cnt=%d", addPointCardIdx, addPointCards.Num());
			return;
		}

		UAddPointCard* addPointCard = addPointCards[addPointCardIdx];
		if (nullptr == addPointCard)
		{
			PRINT_ERROR("UAddPointCard 얻을 수 없습니다. USabotageWindow::AddCard() 실패합니다. addPointCardIdx=%d", addPointCardIdx);
			return;
		}

		cardWidget = addPointCard->GetWidget();
		if (nullptr == cardWidget)
		{
			PRINT_ERROR("USabotageCardWidget 얻을 수 없습니다. USabotageWindow::AddCard() 실패합니다. addPointCardIdx=%d", addPointCardIdx);
			return;
		}

		UTunnelCard* card = Cast<UTunnelCard>(cardList[cardIdx]);
		if (nullptr == card)
		{
			PRINT_ERROR("UTunnelCard 가 아닙니다. USabotageWindow::AddCard() 실패합니다. cardIdx=%d", cardIdx);
			return;
		}

		cardWidget->Reset();
		cardWidget->SetVisibility(ESlateVisibility::Visible);

		card->SetIsOpened(isOpended);
		card->UpdateCardWidget(cardWidget);

		*pTempCardIdx = card->GetCardIdx();
		//PRINT("바뀐 카드 인덱스: %d", *board.Find(point));
	}
	else
	{
		cardWidget = DequeueCardWidget();
		if (nullptr == cardWidget)
		{
			PRINT_ERROR("%s", TEXT("DequeueCardWidget() 가 nullptr 반환했습니다. USabotageWindow::AddCard() 실패합니다."));
			return;
		}

		if (0 <= cardIdx)
		{
			USabotageCard* card = cardList[cardIdx];
			card->SetIsOpened(isOpended);
			card->UpdateCardWidget(cardWidget);
			board.Emplace(point, card->GetCardIdx());
		}
		else
		{
			UAddPointCard* card = DequeueAddPointCard();
			card->SetIsOpened(isOpended);
			card->UpdateCardWidget(cardWidget);
			board.Emplace(point, card->GetCardIdx());
		}

		FVector2D position(
			boardCenterPos.X + (pointX * cardSize.X)
			,boardCenterPos.Y + (pointY * cardSize.Y)
		);

		UCanvasPanelSlot* cardSlot = boardPanel->AddChildToCanvas(cardWidget);
		cardSlot->SetPosition(position);
	}
}

void USabotageWindow::RemoveBoardCard(const FIntPoint& point)
{
	if (false == board.Contains(point))
	{
		PRINT_ERROR("보드에 있는 카드가 아닌데 제거하려 합니다. USabotageWindow::RemoveBoardCard() 실패합니다. pointX=%d, pointY=%d", point.X, point.Y);
		return;
	}

	int32* pCardIdx = board.Find(point);
	if ((*pCardIdx < 0) || (cardList.Num() <= *pCardIdx))
	{
		PRINT_ERROR("인덱스가 범위를 벗어납니다. USabotageWindow::RemoveBoardCard() 실패합니다. cardIdx=%d, cardCnt=%d", *pCardIdx, cardList.Num());
		return;
	}

	USabotageCard* card = cardList[*pCardIdx];
	if (nullptr == card)
	{
		PRINT_ERROR("USabotageCard 얻을 수 없습니다. USabotageWindow::RemoveBoardCard() 실패합니다. cardIdx=%d", *pCardIdx);
		return;
	}

	USabotageCardWidget* widget = card->GetWidget();
	if (nullptr == widget)
	{
		PRINT_ERROR("USabotageCardWidget 얻을 수 없습니다. USabotageWindow::RemoveBoardCard() 실패합니다. cardIdx=%d", *pCardIdx);
		return;
	}

	board.Remove(point);
	EnqueueCardWidget(widget);

	selectedHandCardIdx = -1;

	// 굴 카드가 제거되면 놓을 수 있는 위치를 없애버린다.
	TArray<FIntPoint> removePoints;
	
	int32 addPointCardIdx;
	UAddPointCard* addPointCard = nullptr;
	for (auto& keyValue : board)
	{
		if (keyValue.Value < 0)
		{
			//PRINT("보드에서 제거 시도. cardIdx=%d", keyValue.Value);

			addPointCardIdx = (-1 * keyValue.Value) - 1;
			if ((addPointCardIdx < 0) || (addPointCards.Num() <= addPointCardIdx))
			{
				continue;
			}

			addPointCard = addPointCards[addPointCardIdx];
			if (nullptr == addPointCard)
			{
				continue;
			}

			widget = addPointCard->GetWidget();
			if (nullptr == widget)
			{
				continue;
			}

			removePoints.Emplace(keyValue.Key);
			EnqueueCardWidget(widget);
			EnqueueAddPointCard(addPointCardIdx);

			//PRINT("AddPointCard 제거 성공. boardIdx=%d, addPointCardIdx=%d, cardIdx=%d", keyValue.Value, addPointCardIdx, addPointCard->GetCardIdx());
		}
	}

	for (auto& p : removePoints)
	{
		board.Remove(p);
	}
}

void USabotageWindow::TurnOverArrivalPointCard(const FIntPoint& point)
{
	if (false == board.Contains(point))
	{
		PRINT_ERROR("보드에 있는 카드가 아닙니다. USabotageWindow::TurnOverArrivalPointCard() 실패합니다. pointX=%d, pointY=%d", point.X, point.Y);
		return;
	}

	int32* pCardIdx = board.Find(point);
	if ((*pCardIdx < 0) || (cardList.Num() <= *pCardIdx))
	{
		PRINT_ERROR("잘못된 인덱스 입니다. USabotageWindow::TurnOverArrivalPointCard() 실패합니다. cardIdx=%d, cardCnt=%d", *pCardIdx, cardList.Num());
		return;
	}

	USabotageCardWidget* widget = cardList[*pCardIdx]->GetWidget();
	if (nullptr == widget)
	{
		PRINT_ERROR("USabotageCardWidget 얻을 수 없습니다. USabotageWindow::TurnOverArrivalPointCard() 실패합니다. cardIdx=%d", *pCardIdx);
		return;
	}

	widget->TurnOver();

	selectedHandCardIdx = -1;
}

void USabotageWindow::AddHandCard(const int32& cardIdx)
{
	USabotageCardWidget* cardWidget = DequeueCardWidget();
	if (nullptr == cardWidget)
	{
		PRINT_ERROR("%s", TEXT("DequeueCardWidget() 가 nullptr 반환했습니다. USabotageWindow::AddHandCard() 실패합니다."));
		return;
	}

	USabotageCard* card = cardList[cardIdx];
	card->UpdateCardWidget(cardWidget, true);

	hand.Emplace(cardIdx);

	float cardHalfWidth = cardSize.X * 0.5;
	float offsetX = (cardSize.X - handCardOverlapX) * handPanel->GetChildrenCount();
	FVector2D position(
		-cardHalfWidth + offsetX
		,0.0f
	);

	UCanvasPanelSlot* cardSlot = handPanel->AddChildToCanvas(cardWidget);
	cardSlot->SetPosition(position);

	// UI를 넘어가면 카드를 겹쳐서 놓는다.
	int32 childrenCount = handPanel->GetChildrenCount();
	float allCardWidth = (cardSize.X * childrenCount) + (handCardOverlapX * (childrenCount - 1));
	float scaledHandWidth = handWidth / handScaleBox->UserSpecifiedScale;
	if (scaledHandWidth < allCardWidth)
	{
		handCardOverlapX = cardSize.X - (scaledHandWidth - cardSize.X) / (childrenCount - 1);
		//PRINT("%d * %f vs %f -> %f",childrenCount, allCardWidth, scaledHandWidth, handCardOverlapX);

		UCanvasPanelSlot* slot = nullptr;
		for (int32 i = 0; i < childrenCount; ++i)
		{
			slot = Cast<UCanvasPanelSlot>(handPanel->GetChildAt(i)->Slot);
			if (nullptr != slot)
			{
				offsetX = (cardSize.X - handCardOverlapX) * i;
				position.Set(-cardHalfWidth + offsetX, 0.0f);

				slot->SetPosition(position);
			}
		}
	}
}

void USabotageWindow::RemoveHandCard(const int32& cardIdx)
{
	if (false == hand.Contains(cardIdx))
	{
		PRINT_ERROR("핸드에 있는 카드가 아닌데 제거하려 합니다. USabotageWindow::RemoveHandCard() 실패합니다. cardIdx=%d", cardIdx);
		return;
	}

	if ((cardIdx < 0) || (cardList.Num() <= cardIdx))
	{
		PRINT_ERROR("인덱스가 범위를 벗어납니다. USabotageWindow::RemoveHandCard() 실패합니다. cardIdx=%d, cardCnt=%d", cardIdx, cardList.Num());
		return;
	}

	USabotageCard* card = cardList[cardIdx];
	USabotageCardWidget* widget = card->GetWidget();
	if (nullptr == widget)
	{
		PRINT_ERROR("USabotageCardWidget 얻을 수 없습니다. USabotageWindow::RemoveHandCard() 실패합니다. cardIdx=%d", cardIdx);
		return;
	}

	hand.Remove(cardIdx);
	selectedHandCardIdx = -1;

	int repositionStartIdx = handPanel->GetChildIndex(widget);
	EnqueueCardWidget(widget);

	int32 childrenCount = handPanel->GetChildrenCount();
	if (0 < handCardOverlapX)
	{
		float scaledHandWidth = handWidth / handScaleBox->UserSpecifiedScale;
		float tempOverlapX = cardSize.X - (scaledHandWidth - cardSize.X) / (childrenCount - 1);
		if (tempOverlapX < handCardOverlapX)
		{
			handCardOverlapX = tempOverlapX;
			repositionStartIdx = 0;
		}
	}

	UCanvasPanelSlot* slot = nullptr;
	float offsetX;
	FVector2D position;

	float cardHalfWidth = cardSize.X * 0.5;
	for (int32 i = repositionStartIdx; i < childrenCount; ++i)
	{
		slot = Cast<UCanvasPanelSlot>(handPanel->GetChildAt(i)->Slot);
		if (nullptr != slot)
		{
			offsetX = (cardSize.X - handCardOverlapX) * i;
			position.Set(-cardHalfWidth + offsetX, 0.0f);

			slot->SetPosition(position);
		}
	}
}

void USabotageWindow::OpenArrivalPointCard(const int32& cardIdx, const FIntPoint& point)
{
	if ((cardIdx < 0) || (cardList.Num() <= cardIdx))
	{
		PRINT_ERROR("잘못된 인덱스 입니다. USabotageWindow::OpenArrivalPointCard() 실패합니다. cardIdx=%d, cardCnt=%d", cardIdx, cardList.Num());
		return;
	}

	USabotageCard* card = cardList[cardIdx];
	USabotageCardWidget* cardWidget = card->GetWidget();
	if (nullptr == cardWidget)
	{
		PRINT_ERROR("USabotageCardWidget 얻을 수 없습니다. USabotageWindow::OpenArrivalPointCard() 실패합니다. cardIdx=%d", cardIdx);
		return;
	}

	card->SetIsOpened(true);
	card->UpdateCardWidget(cardWidget);
}

void USabotageWindow::ShowCharacterPickPanel(const int32& characterCount)
{
	SabotageData* data = SabotageData::GetSingleton();

	// TODO Level 2: 배치되는 위치를 계산해야 한다. ex) 5장이면 위에 3장 아래 2장.
	float offsetX = cardSize.X * 0.5;
	float offsetY = cardSize.Y * 0.5;

	for (int32 i = 0; i < characterCount; ++i)
	{
		USabotageCardWidget* cardWidget = DequeueCardWidget();
		if (nullptr == cardWidget)
		{
			PRINT_ERROR("%s", TEXT("DequeueCardWidget() 가 nullptr 반환했습니다. USabotageWindow::ShowCharacterPickPanel() 실패합니다."));
			return;
		}

		UCharacterPickCard* characterPickCard = NewObject<UCharacterPickCard>();

		characterPickCard->FakeConstructor(i, data->characterBackTexture);
		characterPickCard->AddToRoot();
		characterPickCards.Emplace(characterPickCard);

		characterPickCard->UpdateCardWidget(cardWidget);
		cardWidget->SetBGImage(data->characterBackTexture);

		UCanvasPanelSlot* cardSlot = characterPickPanel->AddChildToCanvas(cardWidget);
		cardSlot->SetPosition(FVector2D(i * cardSize.X + offsetX, 0.0f + offsetY));
	}

	characterPickPanelRoot->SetVisibility(ESlateVisibility::Visible);
}

void USabotageWindow::OpenPickedCharacterCard(const int32& pickIdx, const int32& cardIdx, const bool& isMine)
{
	USabotageCardWidget* cardWidget = Cast<USabotageCardWidget>(characterPickPanel->GetChildAt(pickIdx));
	if (nullptr == cardWidget)
	{
		PRINT_ERROR("USabotageCardWidget 얻을 수 없습니다. USabotageWindow::OpenPickedCharacterCard() 실패합니다. *pickIdx=%d", pickIdx);
		return;
	}

	if (true == isMine)
	{
		if ((cardIdx < 0) || (cardList.Num() <= cardIdx))
		{
			PRINT_ERROR("잘못된 인덱스 입니다. USabotageWindow::OpenPickedCharacterCard() 실패합니다. *cardIdx=%d, cardCnt=%d", cardIdx, cardList.Num());
			return;
		}

		USabotageCard* card = cardList[cardIdx];
		if (nullptr == card)
		{
			PRINT_ERROR("USabotageCard 얻을 수 없습니다. USabotageWindow::OpenPickedCharacterCard() 실패합니다. *cardIdx=%d", cardIdx);
			return;
		}

		card->UpdateCardWidget(cardWidget, true);
	}
	else
	{
		//cardWidget->backImage->SetOpacity(0.6f);
		cardWidget->backImage->SetColorAndOpacity(FLinearColor(0.4f, 0.4f, 0.4f, 0.6f));
	}
}

void USabotageWindow::HideCharacterPickPanel()
{
	for (auto& card : characterPickCards)
	{
		card->RemoveFromRoot();
	}
	characterPickCards.Empty();

	FromClient_PlayHideCharacterPickPanelAnimation();
}

void USabotageWindow::SetShowAddCardPoint(const bool& isShow)
{
	int32 addPointCardIdx = -1;
	UWidget* addPointCardWidget = nullptr;

	for (auto& keyValue : board)
	{
		FIntPoint& point = keyValue.Key;
		int32& cardIdx = keyValue.Value;
		//PRINT("[%d, %d] %d", point.X, point.Y, cardIdx);

		if (0 <= cardIdx)
		{
			continue;
		}

		addPointCardIdx = (-1 * cardIdx) - 1;
		if ((addPointCardIdx < 0) || (addPointCards.Num() <= addPointCardIdx))
		{
			PRINT_ERROR("잘못된 인덱스 입니다. USabotageWindow::SetShowAddCardPoint() 실패합니다. idx=%d, cnt=%d", addPointCardIdx, addPointCards.Num());
			return;
		}

		addPointCardWidget = addPointCards[addPointCardIdx]->GetWidget();
		if (nullptr == addPointCardWidget)
		{
			PRINT_ERROR("UWidget 얻을 수 없습니다. USabotageWindow::SetShowAddCardPoint() 실패합니다. addPointCardIdx=%d", addPointCardIdx);
			return;
		}

		if (true == isShow)
		{
			addPointCardWidget->SetVisibility(ESlateVisibility::Visible);
		}
		else
		{
			addPointCardWidget->SetVisibility(ESlateVisibility::Collapsed);
		}
	}
}

//void USabotageWindow::FindAddCardPoint()
//{
//	SabotageData* data = SabotageData::GetSingleton();
//	data->InitData();
//
//	FIntPoint addCardPoint(0, 0);
//	FIntPoint neighborCardPoint(0, 0);
//
//	//TMap<FIntPoint, bool> checkedNeighborCardPoint; // TODO Level 2: 로직 속도 개선. 확인했던 위치는 가능/불가능을 기록해두자.
//	TSet<FIntPoint> addCardPointSet;
//
//	UTunnelCard* card = nullptr;
//	UTunnelCard* neighborCard = nullptr;
//	for (auto& keyValue : board)
//	{
//		FIntPoint& point = keyValue.Key;
//		int32& cardIdx = keyValue.Value;
//		//PRINT("[%d, %d] %d", point.X, point.Y, cardIdx);
//
//		if (cardIdx < 0)
//		{
//			continue;
//		}
//
//		card = Cast<UTunnelCard>(cardList[cardIdx]);
//		if (nullptr == card)
//		{
//			PRINT_WARNING("굴 카드가 아닌데 보드에 배치되어 있습니다. 디버깅 필요합니다. point=(%d,%d), cardIdx=%d", point.X, point.Y, cardIdx);
//			continue;
//		}
//
//		// (상, 하, 좌, 우) 에 놓을 수 있는 카드인가?
//		// 놓을 수 있는 카드면, 그 위치가 비었는지 확인하다.
//		// 비었으면 보드에 표시한다. (위치를 표시하는 카드를 추가할꺼야)
//		if (false == card->GetIsOpened())
//		{
//			continue;
//		}
//
//		// 단, 도착지 다른 굴 카드와 연결되어 있어야 주변에 카드를 놓을 수 있다.
//		if (true == card->IsA(UArrivalPointCard::StaticClass()))
//		{
//			bool isConnected = false;
//
//			if ((false == isConnected) && (true == card->GetIsEnableTop()))
//			{
//				addCardPoint.X = point.X;
//				addCardPoint.Y = point.Y - 1;
//
//				int32* pAddCardIdx = board.Find(addCardPoint);
//				if ((nullptr != pAddCardIdx) && (0 <= *pAddCardIdx))
//				{
//					neighborCard = Cast<UTunnelCard>(cardList[*pAddCardIdx]);
//					if ((nullptr != neighborCard) && (true == neighborCard->GetIsEnableBottom()))
//					{
//						isConnected = true;
//					}
//				}
//			}
//
//			if ((false == isConnected) && (true == card->GetIsEnableBottom()))
//			{
//				addCardPoint.X = point.X;
//				addCardPoint.Y = point.Y + 1;
//
//				int32* pAddCardIdx = board.Find(addCardPoint);
//				if ((nullptr != pAddCardIdx) && (0 <= *pAddCardIdx))
//				{
//					neighborCard = Cast<UTunnelCard>(cardList[*pAddCardIdx]);
//					if ((nullptr != neighborCard) && (true == neighborCard->GetIsEnableTop()))
//					{
//						isConnected = true;
//					}
//				}
//			}
//
//			if ((false == isConnected) && (true == card->GetIsEnableLeft()))
//			{
//				addCardPoint.X = point.X - 1;
//				addCardPoint.Y = point.Y;
//
//				int32* pAddCardIdx = board.Find(addCardPoint);
//				if ((nullptr != pAddCardIdx) && (0 <= *pAddCardIdx))
//				{
//					neighborCard = Cast<UTunnelCard>(cardList[*pAddCardIdx]);
//					if ((nullptr != neighborCard) && (true == neighborCard->GetIsEnableRight()))
//					{
//						isConnected = true;
//					}
//				}
//			}
//
//			if ((false == isConnected) && (true == card->GetIsEnableRight()))
//			{
//				addCardPoint.X = point.X + 1;
//				addCardPoint.Y = point.Y;
//
//				int32* pAddCardIdx = board.Find(addCardPoint);
//				if ((nullptr != pAddCardIdx) && (0 <= *pAddCardIdx))
//				{
//					neighborCard = Cast<UTunnelCard>(cardList[*pAddCardIdx]);
//					if ((nullptr != neighborCard) && (true == neighborCard->GetIsEnableLeft()))
//					{
//						isConnected = true;
//					}
//				}
//			}
//
//			if (false == isConnected)
//			{
//				continue;
//			}
//		}
//
//		if (true == card->GetIsEnableTop())
//		{
//			addCardPoint.X = point.X;
//			addCardPoint.Y = point.Y - 1;
//
//			int32* pAddCardIdx = board.Find(addCardPoint);
//
//			if (nullptr == pAddCardIdx)
//			{// 비어 있다.
//				addCardPointSet.Emplace(addCardPoint);
//			}
//		}
//			
//		if (true == card->GetIsEnableBottom())
//		{
//			addCardPoint.X = point.X;
//			addCardPoint.Y = point.Y + 1;
//
//			int32* pAddCardIdx = board.Find(addCardPoint);
//			if (nullptr == pAddCardIdx)
//			{// 비어 있다.
//				addCardPointSet.Emplace(addCardPoint);
//			}
//		}
//			
//		if (true == card->GetIsEnableLeft())
//		{
//			addCardPoint.X = point.X - 1;
//			addCardPoint.Y = point.Y;
//
//			int32* pAddCardIdx = board.Find(addCardPoint);
//			if (nullptr == pAddCardIdx)
//			{// 비어 있다.
//				addCardPointSet.Emplace(addCardPoint);
//			}
//		}
//
//		if (true == card->GetIsEnableRight())
//		{
//			addCardPoint.X = point.X + 1;
//			addCardPoint.Y = point.Y;
//
//			int32* pAddCardIdx = board.Find(addCardPoint);
//			if (nullptr == pAddCardIdx)
//			{// 비어 있다.
//				addCardPointSet.Emplace(addCardPoint);
//			}
//		}
//	}
//
//	bool isChangedBoardSize = false;
//	for (auto& point : addCardPointSet)
//	{
//		if (true == IsChangedBoardSize(point.X, point.Y))
//		{
//			isChangedBoardSize = true;
//		}
//		
//		AddCard(point.X, point.Y, -1);
//	}
//
//	if (true == isChangedBoardSize)
//	{
//		FitBoardSize();
//	}
//}

void USabotageWindow::FindAddCardPoint()
{
	FIntPoint point(0, 0);
	FIntPoint tempPoint(0, 0);

	int32* pCardIdx = board.Find(point);
	int32* pTempCardIdx = nullptr;
	if (nullptr == pCardIdx)
	{
		PRINT_ERROR("%s", TEXT("출발지 카드를 찾을 수 없습니다. USabotageWindow::FindAddCardPoint() 실패합니다."));
		return;
	}

	UTunnelCard* card = Cast<UTunnelCard>(cardList[*pCardIdx]);
	UTunnelCard* tempCard = nullptr;
	if (nullptr == card)
	{
		PRINT_ERROR("%s", TEXT("출발지 카드 정보를 찾을 수 없습니다. USabotageWindow::FindAddCardPoint() 실패합니다."));
		return;
	}

	int32 maxVisitPointCount = board.Num() - 1;
	TSet<FIntPoint> visitPoint;					// 한번이라도 방문한 지점.
	TArray<FIntPoint> watingForVisitPoint;		// 다시 돌아가서 방문을 이어가야 하는 지점. (스택으로 사용한다)

	TSet<FIntPoint> addCardPointSet;			// 놓을 위치를 표시해야 하는 지점.

	do
	{
		if (true == card->GetIsEnableRight())
		{
			tempPoint.X = point.X + 1;
			tempPoint.Y = point.Y;
			if (false == visitPoint.Contains(tempPoint))
			{
				// 보드에 카드가 배치되어 있는가?
				pTempCardIdx = board.Find(tempPoint);
				if (nullptr != pTempCardIdx)	// 보드에 카드가 배치되어 있는가?
				{
					// 굴 카드인가?
					if (0 <= *pTempCardIdx)
					{
						tempCard = Cast<UTunnelCard>(cardList[*pTempCardIdx]);
						if (nullptr != tempCard)
						{
							bool isDestination = false;
							if (true == tempCard->IsA(UArrivalPointCard::StaticClass()))
							{
								UArrivalPointCard* arrivalPointCard = Cast<UArrivalPointCard>(tempCard);
								if (false == arrivalPointCard->GetIsFake())
								{
									isDestination = true;
								}
							}

							// 막혀있는가?
							if ((false == isDestination) && (false == tempCard->GetIsBlocked()))
							{
								visitPoint.Emplace(point);
								watingForVisitPoint.Emplace(point);

								point = tempPoint;
								card = tempCard;
								continue;
							}
						}
					}
				}
				else
				{
					addCardPointSet.Emplace(tempPoint);
				}
			}
		}

		if (true == card->GetIsEnableTop())
		{
			tempPoint.X = point.X;
			tempPoint.Y = point.Y - 1;
			if (false == visitPoint.Contains(tempPoint))
			{
				// 보드에 카드가 배치되어 있는가?
				pTempCardIdx = board.Find(tempPoint);
				if (nullptr != pTempCardIdx)	// 보드에 카드가 배치되어 있는가?
				{
					// 굴 카드인가?
					if (0 <= *pTempCardIdx)
					{
						tempCard = Cast<UTunnelCard>(cardList[*pTempCardIdx]);
						if (nullptr != tempCard)
						{
							bool isDestination = false;
							if (true == tempCard->IsA(UArrivalPointCard::StaticClass()))
							{
								UArrivalPointCard* arrivalPointCard = Cast<UArrivalPointCard>(tempCard);
								if (false == arrivalPointCard->GetIsFake())
								{
									isDestination = true;
								}
							}

							// 막혀있는가?
							if ((false == isDestination) && (false == tempCard->GetIsBlocked()))
							{
								visitPoint.Emplace(point);
								watingForVisitPoint.Emplace(point);

								point = tempPoint;
								card = tempCard;
								continue;
							}
						}
					}
				}
				else
				{
					addCardPointSet.Emplace(tempPoint);
				}
			}
		}

		if (true == card->GetIsEnableBottom())
		{
			tempPoint.X = point.X;
			tempPoint.Y = point.Y + 1;
			if (false == visitPoint.Contains(tempPoint))
			{
				// 보드에 카드가 배치되어 있는가?
				pTempCardIdx = board.Find(tempPoint);
				if (nullptr != pTempCardIdx)	// 보드에 카드가 배치되어 있는가?
				{
					// 굴 카드인가?
					if (0 <= *pTempCardIdx)
					{
						tempCard = Cast<UTunnelCard>(cardList[*pTempCardIdx]);
						if (nullptr != tempCard)
						{
							bool isDestination = false;
							if (true == tempCard->IsA(UArrivalPointCard::StaticClass()))
							{
								UArrivalPointCard* arrivalPointCard = Cast<UArrivalPointCard>(tempCard);
								if (false == arrivalPointCard->GetIsFake())
								{
									isDestination = true;
								}
							}

							// 막혀있는가?
							if ((false == isDestination) && (false == tempCard->GetIsBlocked()))
							{
								visitPoint.Emplace(point);
								watingForVisitPoint.Emplace(point);

								point = tempPoint;
								card = tempCard;
								continue;
							}
						}
					}
				}
				else
				{
					addCardPointSet.Emplace(tempPoint);
				}
			}
		}

		if (true == card->GetIsEnableLeft())
		{
			tempPoint.X = point.X - 1;
			tempPoint.Y = point.Y;
			if (false == visitPoint.Contains(tempPoint))
			{
				// 보드에 카드가 배치되어 있는가?
				pTempCardIdx = board.Find(tempPoint);
				if (nullptr != pTempCardIdx)	// 보드에 카드가 배치되어 있는가?
				{
					// 굴 카드인가?
					if (0 <= *pTempCardIdx)
					{
						tempCard = Cast<UTunnelCard>(cardList[*pTempCardIdx]);
						if (nullptr != tempCard)
						{
							bool isDestination = false;
							if (true == tempCard->IsA(UArrivalPointCard::StaticClass()))
							{
								UArrivalPointCard* arrivalPointCard = Cast<UArrivalPointCard>(tempCard);
								if (false == arrivalPointCard->GetIsFake())
								{
									isDestination = true;
								}
							}

							// 막혀있는가?
							if ((false == isDestination) && (false == tempCard->GetIsBlocked()))
							{
								visitPoint.Emplace(point);
								watingForVisitPoint.Emplace(point);

								point = tempPoint;
								card = tempCard;
								continue;
							}
						}
					}
				}
				else
				{
					addCardPointSet.Emplace(tempPoint);
				}
			}
		}

		if (0 == watingForVisitPoint.Num())
		{
			break;
		}

		visitPoint.Emplace(point);

		point = watingForVisitPoint.Pop();
		card = Cast<UTunnelCard>(cardList[*board.Find(point)]);
	} while (visitPoint.Num() < maxVisitPointCount);


	bool isChangedBoardSize = false;
	for (auto& p : addCardPointSet)
	{
		if (true == IsChangedBoardSize(p.X, p.Y))
		{
			isChangedBoardSize = true;
		}
		
		AddCard(p.X, p.Y, -1);
	}

	if (true == isChangedBoardSize)
	{
		FitBoardSize();
	}
}

void USabotageWindow::OnClickedHandCard(const int32& cardIdx)
{
	//PRINT("USabotageWindow::OnClickedHandCard, cardIdx=%d", cardIdx);

	if (false == hand.Contains(cardIdx))
	{
		PRINT_WARNING("핸드에 있을때만 사용할 수 있습니다. cardIdx=%d", cardIdx);
		return;
	}

	if ((cardIdx < 0) || (cardList.Num() <= cardIdx))
	{
		PRINT_ERROR("잘못된 인덱스입니다. cardIdx=%d, cardCnt=%d", cardIdx, cardList.Num());
		return;
	}

	if (0 < selectedHandCardIdx)
	{
		if (cardIdx == selectedHandCardIdx)
		{
			return;
		}

		USabotageCardWidget* widget = cardList[selectedHandCardIdx]->GetWidget();
		if (nullptr == widget)
		{
			PRINT_ERROR("USabotageCardWidget 얻을 수 없습니다. selectedHandCardIdx=%d", selectedHandCardIdx);
			return;
		}

		UCanvasPanelSlot* slot = Cast<UCanvasPanelSlot>(widget->Slot);
		if (nullptr == slot)
		{
			PRINT_ERROR("UCanvasPanelSlot 얻을 수 없습니다. selectedHandCardIdx=%d", selectedHandCardIdx);
			return;
		}

		//widget->SetShowSelectedEffect(false);
		slot->SetPosition(FVector2D(slot->GetPosition().X, 0.0f));
	}

	USabotageCardWidget* widget = cardList[cardIdx]->GetWidget();
	if (nullptr == widget)
	{
		PRINT_ERROR("USabotageCardWidget 얻을 수 없습니다. cardIdx=%d", cardIdx);
		return;
	}

	UCanvasPanelSlot* slot = Cast<UCanvasPanelSlot>(widget->Slot);
	if (nullptr == slot)
	{
		PRINT_ERROR("UCanvasPanelSlot 얻을 수 없습니다. cardIdx=%d", cardIdx);
		return;
	}

	// TODO Level 2: 선택된 핸드 카드 강조.
	//widget->SetShowSelectedEffect(true);
	slot->SetPosition(FVector2D(slot->GetPosition().X, -30.0f));

	selectedHandCardIdx = cardIdx;
}

void USabotageWindow::OnClickedTunnelCard(const int32& cardIdx) 
{
	PRINT("USabotageWindow::OnClickedTunnelCard, cardIdx=%d", cardIdx);

	if ((cardIdx < 0) || (cardList.Num() <= cardIdx))
	{
		PRINT_ERROR("잘못된 인덱스입니다. USabotageWindow::OnClickedTunnelCard() 실패합니다. cardIdx=%d, cardCnt=%d", cardIdx, cardList.Num());
		return;
	}

	if (true == hand.Contains(cardIdx))
	{// 핸드 카드를 선택한다.
		OnClickedHandCard(cardIdx);
	}
	else
	{// 보드에 있는 굴 카드를 선택한다.
		if (0 < selectedHandCardIdx)
		{
			if (true == cardList[selectedHandCardIdx]->IsA(UActionCard::StaticClass()))
			{
				if (true == cardList[selectedHandCardIdx]->IsA(URockfallCard::StaticClass()))
				{
					if (true == cardList[cardIdx]->IsA(UStartingPointCard::StaticClass()))
					{
						SetMessage("출발지 카드는 제거할 수 없습니다.");
						return;
					}

					if (true == cardList[cardIdx]->IsA(UArrivalPointCard::StaticClass()))
					{
						SetMessage("도착지 카드는 제거할 수 없습니다.");
						return;
					}
					
					// TODO Level 2: 카드키로 보드좌표 얻는 함수로 변경.
					for (auto& keyValue : board)
					{
						if (keyValue.Value == cardIdx)
						{
							ASabotagePlayerController* playerController = Cast<ASabotagePlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
							if (nullptr == playerController)
							{
								PRINT_ERROR("%s", TEXT("ASabotagePlayerController 얻을 수 없습니다. USabotageWindow::OnClickedTunnelCard() 실패합니다."));
								return;
							}

							playerController->ToServer_RemoveBoardCard(selectedHandCardIdx, keyValue.Key);
							return;
						}
					}

					PRINT_WARNING("보드에서 굴 카드를 찾을 수 없습니다. cardIdx=%d", cardIdx);
				}
				else if (true == cardList[selectedHandCardIdx]->IsA(UMapCard::StaticClass()))
				{
					if (false == cardList[cardIdx]->IsA(UArrivalPointCard::StaticClass()))
					{
						SetMessage("도착지 카드만 확인할 수 있습니다.");
						return;
					}

					if (true == cardList[cardIdx]->GetIsOpened())
					{
						SetMessage("이미 공개된 도착지 카드입니다.");
						return;
					}

					// TODO Level 2: 카드키로 보드좌표 얻는 함수로 변경.
					for (auto& keyValue : board)
					{
						if (keyValue.Value == cardIdx)
						{
							ASabotagePlayerController* playerController = Cast<ASabotagePlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
							if (nullptr == playerController)
							{
								PRINT_ERROR("%s", TEXT("ASabotagePlayerController 얻을 수 없습니다. USabotageWindow::OnClickedTunnelCard() 실패합니다."));
								return;
							}

							playerController->ToServer_TurnOverArrivalPointCard(selectedHandCardIdx, keyValue.Key);
							return;
						}
					}

					PRINT_WARNING("보드에서 도착지 카드를 찾을 수 없습니다. cardIdx=%d", cardIdx);
				}
			}

			SetMessage("놓을 수 없는 위치입니다.");
		}
		else
		{
			SetMessage("핸드에 있는 카드를 먼저 선택하세요.");
		}
	}
}

void USabotageWindow::OnClickedAddPointCard(const int32& cardIdx)
{
	PRINT("USabotageWindow::OnClickedAddPointCard, cardIdx=%d", cardIdx);

	if (0 < selectedHandCardIdx)
	{
		USabotageCard* selectedHandCard = cardList[selectedHandCardIdx];
		if (true == selectedHandCard->IsA(UTunnelCard::StaticClass()))
		{
			for (auto& keyValue : board)
			{
				if (keyValue.Value == cardIdx)
				{
					ASabotagePlayerController* playerController = Cast<ASabotagePlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
					if (nullptr == playerController)
					{
						PRINT_ERROR("%s", TEXT("ASabotagePlayerController 얻을 수 없습니다. USabotageWindow::OnClickedAddPointCard() 실패합니다."));
						return;
					}

					FSabotagePlayerUIInfo* playerInfo = playerInfoList.FindByKey(playerController->PlayerState->GetPlayerId());
					if (nullptr == playerInfo)
					{
						PRINT_ERROR("%s", TEXT("FSabotagePlayerUIInfo 얻을 수 없습니다. USabotageWindow::OnClickedAddPointCard() 실패합니다."));
						return;
					}

					if ((false == playerInfo->isEnablePick) || (false == playerInfo->isEnableLantern) || (false == playerInfo->isEnableWagon))
					{
						SetMessage("장비가 부서져서 굴을 팔 수 없습니다.");
						return;
					}

					//PRINT("USabotageWindow::OnClickedAddPointCard, ToServer_RequestAddTunnelCard() 호출");
					playerController->ToServer_RequestAddTunnelCard(selectedHandCardIdx, keyValue.Key);
					return;
				}
			}

			PRINT_WARNING("보드에서 AddPointCard를 찾을 수 없습니다. cardIdx=%d", cardIdx);
		}
		else
		{
			SetMessage("놓을 수 없는 위치입니다.");
		}
	}
	else
	{
		SetMessage("핸드에 있는 카드를 먼저 선택하세요.");
	}
}

void USabotageWindow::OnClickedCharacterCard(const int32& cardIdx)
{
	PRINT("USabotageWindow::OnClickedCharacterCard, cardIdx=%d", cardIdx);

	if (0 < selectedHandCardIdx)
	{
		if (true == cardList[selectedHandCardIdx]->IsA(UBreakCard::StaticClass()))
		{
			if (cardIdx == myCardIdx)
			{
				SetMessage("내 장비를 고장낼 수 없습니다.");
				return;
			}

			UBreakCard* breakCard = Cast<UBreakCard>(cardList[selectedHandCardIdx]);
			int32 num = playerInfoList.Num();
			for (int32 i = 0; i < num; ++i)
			{
				if (cardIdx == playerInfoList[i].fakeCardIdx)
				{
					if ((true == breakCard->GetIsPick()) && (false == playerInfoList[i].isEnablePick))
					{
						SetMessage("이미 곡괭이가 고장나 있으면 사용할 수 없습니다.");
						return;
					}

					if ((true == breakCard->GetIsLantern()) && (false == playerInfoList[i].isEnableLantern))
					{
						SetMessage("이미 랜턴이 고장나 있으면 사용할 수 없습니다.");
						return;
					}

					if ((true == breakCard->GetIsWagon()) && (false == playerInfoList[i].isEnableWagon))
					{
						SetMessage("이미 수레가 고장나 있으면 사용할 수 없습니다.");
						return;
					}

					ASabotagePlayerController* playerController = Cast<ASabotagePlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
					if (nullptr == playerController)
					{
						PRINT_ERROR("%s", TEXT("ASabotagePlayerController 얻을 수 없습니다. USabotageWindow::OnClickedCharacterCard() 실패합니다."));
						return;
					}

					playerController->ToServer_BreakEquipment(selectedHandCardIdx, playerInfoList[i].playerID);
					return;
				}
			}

			PRINT_WARNING("PlayerInfo 찾을 수 없습니다. USabotageWindow::OnClickedCharacterCard() 실패합니다. cardIdx=%d", cardIdx);
		}
		else if (true == cardList[selectedHandCardIdx]->IsA(URepairCard::StaticClass()))
		{
			URepairCard* repairCard = Cast<URepairCard>(cardList[selectedHandCardIdx]);
			int32 num = playerInfoList.Num();
			for (int32 i = 0; i < num; ++i)
			{
				if (cardIdx == playerInfoList[i].fakeCardIdx)
				{
					if ( ((true == repairCard->GetIsPick()) && (false == playerInfoList[i].isEnablePick))
						|| ((true == repairCard->GetIsLantern()) && (false == playerInfoList[i].isEnableLantern))
						|| ((true == repairCard->GetIsWagon()) && (false == playerInfoList[i].isEnableWagon)))
					{
						ASabotagePlayerController* playerController = Cast<ASabotagePlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
						if (nullptr == playerController)
						{
							PRINT_ERROR("%s", TEXT("ASabotagePlayerController 얻을 수 없습니다. USabotageWindow::OnClickedCharacterCard() 실패합니다."));
							return;
						}

						playerController->ToServer_RepairEquipment(selectedHandCardIdx, playerInfoList[i].playerID);
						return;
					}

					if ((false == playerInfoList[i].isEnablePick) || (false == playerInfoList[i].isEnableLantern) || (false == playerInfoList[i].isEnableWagon))
					{
						SetMessage("고칠 수 없습니다.");
					}
					else
					{
						SetMessage("고장난 장비가 없습니다.");
					}
					return;
				}
			}

			PRINT_WARNING("PlayerInfo 찾을 수 없습니다. USabotageWindow::OnClickedCharacterCard() 실패합니다. cardIdx=%d", cardIdx);
		}
		else
		{
			SetMessage("놓을 수 없는 위치입니다.");
		}
	}
	else
	{
		SetMessage("핸드에 있는 카드를 먼저 선택하세요.");
	}
}

void USabotageWindow::OnClickedTrashCan()
{
	if (0 < selectedHandCardIdx)
	{
		ASabotagePlayerController* playerController = Cast<ASabotagePlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		if (nullptr == playerController)
		{
			PRINT_ERROR("%s", TEXT("ASabotagePlayerController 얻을 수 없습니다. USabotageWindow::OnClickedTrashCan() 실패합니다."));
			return;
		}

		playerController->ToServer_RemoveHandCard(selectedHandCardIdx);
	}
	else
	{
		SetMessage("핸드에 있는 카드를 먼저 선택하세요.");
	}
}

void USabotageWindow::SetEnableStartBtn(const bool& isEnable)
{
	startBtn->SetIsEnabled(isEnable);
}

void USabotageWindow::SetEnableRestartBtn(const bool& isEnable)
{
	restartBtn->SetIsEnabled(isEnable);
}

void USabotageWindow::SetMessage(const char* message)
{
	messageText->SetText(FText::FromString(UTF8_TO_TCHAR(message)));
}

void USabotageWindow::NativeConstruct()
{
	Super::NativeConstruct();

	SabotageData::GetSingleton()->LoadCardList(cardList);
	
	{// 카드와 보드판의 크기를 구해둔다.
		USabotageCardWidget* cardWidget = DequeueCardWidget();
		if (nullptr == cardWidget)
		{
			PRINT_ERROR("%s", TEXT("DequeueCardWidget() 가 nullptr 반환했습니다. USabotageWindow::NativeConstruct() 실패합니다."));
			return;
		}

		cardSize.Set(cardWidget->GetSizeBoxWidth(), cardWidget->GetSizeBoxHeight());
		EnqueueCardWidget(cardWidget);

		UCanvasPanelSlot* slot = Cast<UCanvasPanelSlot>(boardScaleBox->Slot);
		if (nullptr == slot)
		{
			PRINT_ERROR("UCanvasPanelSlot 얻을 수 없습니다. USabotageWindow::NativeConstruct() 실패합니다. name=%s", *boardScaleBox->GetFName().ToString());
			return;
		}

		boardSize = slot->GetSize();
		boardCenterPos.Set((boardSize.X - cardSize.X) * 0.5f, (boardSize.Y - cardSize.Y) * 0.5f);

		minPointX = maxPointX = 0;
		minPointY = maxPointY = 0;

		//PRINT("보드판(%f, %f), 카드(%f, %f), 보드판 중앙(%f, %f)", boardSize.X, boardSize.Y, cardSize.X, cardSize.Y, boardCenterPos.Y, boardCenterPos.Y);
	}

	{
		UCanvasPanelSlot* slot = Cast<UCanvasPanelSlot>(handScaleBox->Slot);
		if (nullptr == slot)
		{
			PRINT_ERROR("UCanvasPanelSlot 얻을 수 없습니다. USabotageWindow::NativeConstruct() 실패합니다. name=%s", *handScaleBox->GetFName().ToString());
			return;
		}

		handWidth = slot->GetSize().X;
		handCardOverlapX = 0.0f;
	}

	{
		trashCanBtn->OnClicked.AddDynamic(this, &USabotageWindow::OnClickedTrashCan);
	}

	{// 방장 UI를 셋팅한다.
		ASabotagePlayerController* playerController = Cast<ASabotagePlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		if (nullptr == playerController)
		{
			PRINT_ERROR("%s", TEXT("ASabotagePlayerController 얻을 수 없습니다. USabotageWindow::NativeConstruct() 실패합니다."));
			return;
		}

		if (true == playerController->HasAuthority())
		{
			startBtn->OnClicked.AddDynamic(playerController, &ASabotagePlayerController::ToServer_StartGame);
			restartBtn->OnClicked.AddDynamic(playerController, &ASabotagePlayerController::ToServer_ResetGame);
		}
	}

	{
		if ((0 == playerCardWidgets.Num()) || (0 == playerInfoTexts.Num()))
		{
			PRINT_WARNING("SabotageWindowBP 에서 playerCardWidgets 과 playerInfoTexts 을 10개씩 셋팅해야 합니다. playerCardWidgetCount=%d, playerInfoTextCount=%d", playerCardWidgets.Num(), playerInfoTexts.Num());
			return;
		}

		for (auto& widget : playerCardWidgets)
		{
			widget->SetVisibility(ESlateVisibility::Collapsed);
		}

		for (auto& text : playerInfoTexts)
		{
			text->SetVisibility(ESlateVisibility::Collapsed);
		}
	}

	//// Slot (Canvas Panel Slot) / Size X, Size Y
	//PRINT("Size, %f %f", slot->GetSize().X, slot->GetSize().Y);
 
	//// Slot (Canvas Panel Slot) / Offset Left, Offset Top, Offset Right, Offset Bottom
	//FMargin offsets = slot->GetOffsets();
	//PRINT("Offset, %f %f %f %f", offsets.Left, offsets.Right, offsets.Top, offsets.Bottom);

	//// 화면 사이즈.
	//FVector2D viewportSize = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());
	//PRINT("ViewportSize, %f %f", viewportSize.X, viewportSize.Y);

	//// 화면 사이즈 - 방법2.
	//FVector2D viewportSize2;
	//GEngine->GameViewport->GetViewportSize(viewportSize2);
	//
	//// 화면 해상도.
	//FVector2D Resolution = FVector2D(GSystemResolution.ResX, GSystemResolution.ResY);
	//PRINT("Resolution, %f %f", Resolution.X, Resolution.Y); //640, 480
	//
	//// 디자인 화면 사이즈. // Project Settings / Engine / User Interface / Scale To Fit Rule / Design Screen Size
	//// UI 위치 및 크기 값의 기준이 된다. UI를 가로로 300px 옮겼다면 DesignTimeSize.X(기본값은 1920px 이다) 에서 300px 만큼 옮겼다는 의미이다.
	//// ex) 양쪽으로 300px씩 띄어서 중앙에 CanvasPanel을 올렸다면. 640x480 인 경우엔 300px 보다 작게 계산된다.
	//PRINT("DesignTimeSize, %f %f", DesignTimeSize.X, DesignTimeSize.Y); //1920, 1080

	//// DPI Scale. // Project Settings / Engine / User Interface / DPI Scaling / DPI Curve
	//int32 X = FGenericPlatformMath::FloorToInt(viewportSize.X);
	//int32 Y = FGenericPlatformMath::FloorToInt(viewportSize.Y);
	//float dpiScale = GetDefault<UUserInterfaceSettings>(UUserInterfaceSettings::StaticClass())->GetDPIScaleBasedOnSize(FIntPoint(X, Y));
	//PRINT("DPIScale: %f", dpiScale);

	//// 앵커를 동적으로 설정했을때 위젯의 가로, 세로를 계산하는 방법. (일단 비율 필요한 경우만 쓰자)
	//// 버그가 있다. 비율은 맞게 계산되는데, 실제 크기와는 또 다르다;;;; 왜.. 크기 커지냐고.. DPI 말고 영향주는 계산이 있는것 같은데.. 못찾았음.
	//// CanvasPanel / Get Cached Geometry / Get Local Size 값이 정확하다. 그치만 렌더링 되야 계산된 값이 나오는 문제가 있다.
	//// TODO: 어? DPI Scale Rule을 Horizontal 로 바꿔볼까?
	//int32 tempX = DesignTimeSize.X - offsets.Left - offsets.Right;
	//int32 tempY = DesignTimeSize.Y - offsets.Top - offsets.Bottom;
	//float tempDPIScale = GetDefault<UUserInterfaceSettings>(UUserInterfaceSettings::StaticClass())->GetDPIScaleBasedOnSize(FIntPoint(tempX, tempY));
	//PRINT("tempDPIScale, %f", dpiScale); // ~=0.67
	//float boardWidth = viewportSize.X - ((offsets.Left + offsets.Right) * dpiScale);
	//float boardHeight = viewportSize.Y - ((offsets.Top + offsets.Bottom) * dpiScale);
	//PRINT("Board Size: %f x %f", boardWidth, boardHeight);
}

USabotageWindow::~USabotageWindow()
{
	//PRINT("USabotageWindow 소멸자 호출");

	for (auto& card : cardList)
	{
		card->RemoveFromRoot();
	}
	cardList.Empty();

	for (auto& card : characterPickCards)
	{
		card->RemoveFromRoot();
	}
	characterPickCards.Empty();

	for (auto& card : addPointCards)
	{
		card->RemoveFromRoot();
	}
	addPointCards.Empty();
}

void USabotageWindow::EnqueueAddPointCard(const int32& addPointCardIdx)
{
	if ((addPointCardIdx < 0) || (addPointCards.Num() <= addPointCardIdx))
	{
		PRINT_ERROR("잘못된 인덱스 입니다. 반드시 확인해야 합니다!! 메모리 누수 발생합니다! USabotageWindow::EnqueueAddPointCard() 실패합니다. idx=%d, cnt=%d", addPointCardIdx, addPointCards.Num());
		return;
	}

	addPointCardIdxQueue.Enqueue(addPointCardIdx);
}

UAddPointCard* USabotageWindow::DequeueAddPointCard()
{
	if (false == addPointCardIdxQueue.IsEmpty())
	{
		int32 idx;
		if (false == addPointCardIdxQueue.Dequeue(idx))
		{
			PRINT_WARNING("%s", TEXT("Dequeue 실패했습니다. USabotageWindow::DequeueAddPointCard() 실패합니다."));
			return nullptr;
		}

		return addPointCards[idx];
	}
	else
	{
		UAddPointCard* card = NewObject<UAddPointCard>();
		card->AddToRoot();
		card->FakeConstructor(-1 * (addPointCards.Num() + 1)); // TODO Level 2: 하.. 다른 방법을 찾아보자. 이거 누가 알아볼꺼야.. (1/2)
		addPointCards.Emplace(card);
		return card;
	}

	return nullptr;
}

void USabotageWindow::EnqueueCardWidget(USabotageCardWidget* cardWidget)
{
	if (nullptr != cardWidget)
	{
		if (cardWidgetPool->GetChildrenCount() < CARD_WIDGET_POOL_LIMIT)
		{
			cardWidget->Reset();
			cardWidgetPool->AddChildToCanvas(cardWidget);
		}
		else
		{
			cardWidget->RemoveFromParent();
		}
	}
}

USabotageCardWidget* USabotageWindow::DequeueCardWidget()
{
	USabotageCardWidget* cardWidget = nullptr;
	
	if (0 < cardWidgetPool->GetChildrenCount())
	{
		cardWidget = Cast<USabotageCardWidget>(cardWidgetPool->GetChildAt(0));
		if (nullptr == cardWidget)
		{
			PRINT_ERROR("%s", TEXT("CardWidgetPool 에 USabotageCardWidget 아닌 위젯이 들어있습니다. USabotageWindow::DequeueCardWidget() 실패합니다."));
			return nullptr;
		}

		cardWidget->SetVisibility(ESlateVisibility::Visible);
	}
	else
	{
		UWorld* world = GetWorld();
		if (nullptr == world)
		{
			PRINT_ERROR("%s", TEXT("UWorld 얻을 수 없습니다. USabotageWindow::DequeueCardWidget() 실패합니다."));
			return nullptr;
		}

		TSubclassOf<UUserWidget> widgetClass = LoadClass<UUserWidget>(world, *CARD_WIDGET_PATH);
		if (nullptr == widgetClass)
		{
			PRINT_ERROR("TSubclassOf<UUserWidget> 얻을 수 없습니다. USabotageWindow::DequeueCardWidget() 실패합니다. path=%s", *CARD_WIDGET_PATH);
			return nullptr;
		}

		//cardWidget = CreateWidget<USabotageCardWidget>(this, USabotageCardWidget::StaticClass());
		cardWidget = CreateWidget<USabotageCardWidget>(world, widgetClass);
		if (nullptr == cardWidget)
		{
			PRINT_ERROR("USabotageCardWidget 얻을 수 없습니다. USabotageWindow::DequeueCardWidget() 실패합니다. path=%s", *CARD_WIDGET_PATH);
			return nullptr;
		}

		cardWidget->Reset();
	}

	return cardWidget;
}

void USabotageWindow::ToClient_EndPlayHideCharacterPickPanelAnimation()
{
	characterPickPanelRoot->SetVisibility(ESlateVisibility::Collapsed);

	USabotageCardWidget* cardWidget = nullptr;
	for (int32 i = characterPickPanel->GetChildrenCount() - 1; 0 <= i; --i)
	{
		cardWidget = Cast<USabotageCardWidget>(characterPickPanel->GetChildAt(i));
		if (nullptr != cardWidget)
		{
			//characterPickPanel->RemoveChildAt(i);
			//EnqueueCardWidget(cardWidget);
			cardWidget->RemoveFromParent();
		}
	}
}