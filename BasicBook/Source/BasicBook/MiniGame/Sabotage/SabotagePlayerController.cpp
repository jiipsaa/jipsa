﻿#include "SabotagePlayerController.h"

#include "../../Jipsa/Messagemanager.h"
#include "../../Jipsa/PopupManager.h"
#include "SabotageGameMode.h"

#include "Kismet/GameplayStatics.h"

void ASabotagePlayerController::InitPlayerState()
{
	Super::InitPlayerState();
}

void ASabotagePlayerController::InitBoard_Implementation(const TArray<int32>& initialBoard, int32 const& playerID)
{
	if (nullptr == window)
	{
		PRINT_WARNING("%s", TEXT("UI 가 없습니다. ASabotagePlayerController::InitBoard() 실패합니다."));
		return;
	}

	window->Reset();
	window->InitBoard(initialBoard);

	if (true == HasAuthority())
	{
		window->SetEnableStartBtn(true);
		window->SetEnableRestartBtn(false);
	}

	// TODO Level 2: PlayerState 가 널로 바꿘 원인을 알아야한다. 분명 InitPlayerState에서 생성 됬었는데,
	// 그 후에 GameMode::PostLogin -> ClientInit 으로 돌아오니까 PlayerState 없어졌다. 하.. 멀티에서는 컨트롤러부터 아예 다시 만들어서 교체 할것 같은데..
	// if (nullptr != PlayerState) { ServerRequestReady(playerState->GetIsReady()); }
	ServerRequestReady(playerID);
}

void ASabotagePlayerController::FromServer_PostLogin_Implementation(const int32& playerID, const int32& handCount, bool isEnablePick, bool isEnableLantern, bool isEnableWagon)
{
	if (nullptr == window)
	{
		UUserWidget* userWidget = PopupManager::GetSingleton()->OpenWindow(GetWorld(), "SabotageWindowBP");
		if (nullptr == userWidget)
		{
			return;
		}

		window = Cast<USabotageWindow>(userWidget);
	}

	window->AddPlayerInfo(playerID, handCount, isEnablePick, isEnableLantern, isEnableWagon);
	
	// 다른 유저가 입장했다.
	FString message = FString::Printf(TEXT("플레이어 %d 참가합니다."), playerID);
	MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), message);
}

void ASabotagePlayerController::FromServer_Logout_Implementation(const int32& playerID)
{
	if (nullptr == window)
	{
		PRINT_WARNING("%s", TEXT("UI 가 없습니다. ASabotagePlayerController::FromServer_Logout() 실패합니다."));
		return;
	}

	window->RemovePlayerInfo(playerID);

	// 다른 유저가 퇴장했다.
	FString message = FString::Printf(TEXT("플레이어 %d 방을 나갔습니다."), playerID);
	MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), message);
}

void ASabotagePlayerController::ServerRequestReady_Implementation(const int32& playerID)
{
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ServerReady() 호출할 수 없습니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	ASabotageGameMode* gameMode = Cast<ASabotageGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("ASabotageGameMode 를 얻을 수 없습니다. 준비 상태로 변경할 수 없습니다."));
		return;
	}

	gameMode->SetReady(playerID);
}

void ASabotagePlayerController::ClientResponseReady_Implementation()
{
	// 준비 완료.
}

void ASabotagePlayerController::ToServer_StartGame_Implementation()
{
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ToServer_StartGame() 실패합니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	ASabotageGameMode* gameMode = Cast<ASabotageGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("ASabotageGameMode 얻을 수 없습니다. ToServer_StartGame() 실패합니다."));
		return;
	}

	gameMode->StartGame();
}

void ASabotagePlayerController::FromServer_StartGame_Implementation(const bool isAllReady)
{
	if (nullptr == window)
	{
		PRINT_WARNING("%s", TEXT("UI 가 없습니다. ASabotagePlayerController::FromServer_StartGame() 실패합니다."));
		return;
	}

	if (true == isAllReady)
	{
		FString message = TEXT("게임이 시작됬습니다. 캐릭터 카드를 선택해주세요.");
		MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), message);

		window->SetMessage("캐릭터 카드를 선택해주세요.");

		if (true == HasAuthority())
		{
			window->SetEnableStartBtn(false);
			window->SetEnableRestartBtn(true);
		}
	}
	else
	{
		if (true == HasAuthority())
		{
			FString message = TEXT("아직 준비되지 않은 플레이어가 있습니다.");
			MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), message);
		}
		else
		{
			FString message = TEXT("준비 해주세요~");
			MessageManager::GetSingleton()->ShowSimpleMessage(GetWorld(), message);
		}
	}
}

void ASabotagePlayerController::FromServer_StartPick_Implementation(const int32& characterCount)
{
	if (nullptr != window)
	{
		window->ShowCharacterPickPanel(characterCount);
	}
}

void ASabotagePlayerController::ToServer_PickCharacter_Implementation(const int32& pickIdx, const int32& playerID)
{
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ServerStarGame() 호출할 수 없습니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	ASabotageGameMode* gameMode = Cast<ASabotageGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("ASabotageGameMode 얻을 수 없습니다. 게임을 시작할 수 없습니다."));
		return;
	}

	gameMode->PickCharacter(pickIdx, playerID);
}

void ASabotagePlayerController::FromServer_PickCharacter_Implementation(const int32& pickIdx, const int32& cardIdx, const int32& fakeCardIdx, const int32& playerID)
{
	if (nullptr == window)
	{
		PRINT_WARNING("%s", TEXT("UI 가 없습니다. ASabotagePlayerController::FromServer_PickCharacter() 실패합니다."));
		return;
	}

	bool isMine = (playerID == PlayerState->GetPlayerId());
	if ((-1 == cardIdx) && (true == isMine))
	{
		window->SetMessage("이 카드는 고를 수 없습니다. 다시 시도해주세요.");
		return;
	}

	window->OpenCharacterCard(cardIdx, fakeCardIdx, playerID, isMine);
	window->OpenPickedCharacterCard(pickIdx, cardIdx, isMine);
}

void ASabotagePlayerController::FromServer_InitHand_Implementation(const TArray<int32>& tunnelAndActionCardArr)
{
	if (nullptr == window)
	{
		PRINT_WARNING("%s", TEXT("UI 가 없습니다. ASabotagePlayerController::FromServer_GiveInitialCards() 실패합니다."));
		return;
	}

	window->SetMessage("모든 플레이어가 카드를 선택했습니다. 덱 뭉치에서 카드가 분배됩니다.");
	window->HideCharacterPickPanel();

	for (auto iter = tunnelAndActionCardArr.CreateConstIterator(); iter; ++iter)
	{
		window->AddHandCard(*iter);
	}
}

void ASabotagePlayerController::FromServer_UpdateHandCount_Implementation(const int32& handCount, const int32& playerID)
{
	if (nullptr == window)
	{
		PRINT_WARNING("%s", TEXT("UI 가 없습니다. ASabotagePlayerController::FromServer_UpdateHandCount() 실패합니다."));
		return;
	}

	window->UpdateHandCount(handCount, playerID);
}

void ASabotagePlayerController::FromServer_PlayTurn_Implementation()
{
	if (nullptr == window)
	{
		PRINT_WARNING("%s", TEXT("UI 가 없습니다. ASabotagePlayerController::FromServer_PlayTurn() 실패합니다."));
		return;
	}

	// 보드에 카드를 놓을 수 있는 위치를 표시한다.
	window->FindAddCardPoint();
	window->SetShowAddCardPoint(true);
	window->SetMessage("당신의 턴입니다.");
}

void ASabotagePlayerController::OnClickedHandCard(const int32& cardIdx)
{
	if (nullptr != window)
	{
		window->OnClickedHandCard(cardIdx);
	}
}

void ASabotagePlayerController::OnClickedTunnelCard(const int32& cardIdx)
{
	//PRINT("PlayerId=%d", PlayerState->GetPlayerId());
	if (nullptr != window)
	{
		window->OnClickedTunnelCard(cardIdx);
	}
}

void ASabotagePlayerController::OnClickedAddPointCard(const int32& cardIdx)
{
	if (nullptr != window)
	{
		window->OnClickedAddPointCard(cardIdx);
	}
}

void ASabotagePlayerController::OnClickedCharacterCard(const int32& cardIdx)
{
	if (nullptr != window)
	{
		window->OnClickedCharacterCard(cardIdx);
	}
}

void ASabotagePlayerController::ToServer_RequestAddTunnelCard_Implementation(const int32& cardIdx, const FIntPoint& point)
{
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ServerStarGame() 호출할 수 없습니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	ASabotageGameMode* gameMode = Cast<ASabotageGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("ASabotageGameMode 얻을 수 없습니다. 게임을 시작할 수 없습니다."));
		return;
	}

	gameMode->AddTunnelCard(cardIdx, point, PlayerState->GetPlayerId());
}

void ASabotagePlayerController::FromServer_AddTunnelCard_Implementation(const int32& cardIdx, const FIntPoint& point, const int32& playerID)
{
	PRINT("ASabotagePlayerController::FromServer_AddTunnelCard() cardIdx=%d, pointX=%d, pointY=%d, playerId=%d", cardIdx, point.X, point.Y, PlayerState->GetPlayerId());

	if (nullptr != window)
	{
		if (playerID == PlayerState->GetPlayerId())
		{
			window->RemoveHandCard(cardIdx);
			window->AddCard(point.X, point.Y, cardIdx);
			window->SetShowAddCardPoint(false);
			window->SetMessage("굴 카드가 배치됬습니다.");
		}
		else
		{
			window->AddCard(point.X, point.Y, cardIdx);
		}
	}
}

void ASabotagePlayerController::FromServer_OpenArrivalPointCard_Implementation(const int32& cardIdx, const FIntPoint& point)
{
	//PRINT("FromServer_OpenArrivalPointCard, (%d,%d)", point.X, point.Y);
	if (nullptr == window)
	{
		PRINT_WARNING("%s", TEXT("UI 가 없습니다. ASabotagePlayerController::FromServer_OpenArrivalPointCard() 실패합니다."));
		return;
	}

	window->OpenArrivalPointCard(cardIdx, point);
}

void ASabotagePlayerController::ToServer_TurnOverArrivalPointCard_Implementation(const int32& cardIdx, const FIntPoint& point)
{
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ToServer_RemoveTunnelCard() 실패합니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	ASabotageGameMode* gameMode = Cast<ASabotageGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("ASabotageGameMode 얻을 수 없습니다. ToServer_RemoveTunnelCard() 실패합니다."));
		return;
	}

	gameMode->TurnOverArrivalPointCard(cardIdx, point, PlayerState->GetPlayerId());
}

void ASabotagePlayerController::FromServer_TurnOverArrivalPointCard_Implementation(const int32& cardIdx, const FIntPoint& point, const int32& playerID)
{
	PRINT("ASabotagePlayerController::FromServer_TurnOverArrivalPointCard(), cardIdx=%d, pointX=%d, pointY=%d", cardIdx, point.X, point.Y);

	if (nullptr == window)
	{
		PRINT_WARNING("%s", TEXT("UI 가 없습니다. ASabotagePlayerController::FromServer_RemoveTunnelCard() 실패합니다."));
		return;
	}

	if (playerID == PlayerState->GetPlayerId())
	{
		window->RemoveHandCard(cardIdx);
		window->TurnOverArrivalPointCard(point);
		window->SetShowAddCardPoint(false);
	}

	window->SetMessage(TCHAR_TO_UTF8(*FString::Printf(TEXT("%d 도착지 카드 1장을 뒤집어 확인합니다."), playerID)));
}

void ASabotagePlayerController::ToServer_RemoveBoardCard_Implementation(const int32& rockfallCardIdx, const FIntPoint& point)
{
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ToServer_RemoveTunnelCard() 실패합니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	ASabotageGameMode* gameMode = Cast<ASabotageGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("ASabotageGameMode 얻을 수 없습니다. ToServer_RemoveTunnelCard() 실패합니다."));
		return;
	}

	gameMode->RemoveBoardCard(rockfallCardIdx, point, PlayerState->GetPlayerId());
}

void ASabotagePlayerController::FromServer_RemoveBoardCard_Implementation(const int32& rockfallCardIdx, const FIntPoint& point)
{
	PRINT("ASabotagePlayerController::FromServer_RemoveTunnelCard() rockfallCardIdx=%d, pointX=%d, pointY=%d", rockfallCardIdx, point.X, point.Y);

	if (nullptr == window)
	{
		PRINT_WARNING("%s", TEXT("UI 가 없습니다. ASabotagePlayerController::FromServer_RemoveTunnelCard() 실패합니다."));
		return;
	}

	if (0 <= rockfallCardIdx)
	{
		window->RemoveHandCard(rockfallCardIdx);
	}
	window->RemoveBoardCard(point);
}

void ASabotagePlayerController::FromServer_AddHandCard_Implementation(const int32& cardIdx)
{
	if (nullptr == window)
	{
		PRINT_WARNING("%s", TEXT("UI 가 없습니다. ASabotagePlayerController::FromServer_AddHandCard() 실패합니다."));
		return;
	}

	if (cardIdx < 0)
	{
		window->SetMessage("덱에 카드가 없습니다.");
		return;	
	}

	window->AddHandCard(cardIdx);
}

void ASabotagePlayerController::ToServer_RemoveHandCard_Implementation(const int32& cardIdx)
{
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ASabotagePlayerController::ToServer_RemoveHandCard() 실패합니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	ASabotageGameMode* gameMode = Cast<ASabotageGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("ASabotageGameMode 얻을 수 없습니다. ASabotagePlayerController::ToServer_RemoveHandCard() 실패합니다."));
		return;
	}

	gameMode->RemoveHandCard(cardIdx, PlayerState->GetPlayerId());
}

void ASabotagePlayerController::FromServer_RemoveHandCard_Implementation(const int32& cardIdx)
{
	if (nullptr == window)
	{
		PRINT_WARNING("%s", TEXT("UI 가 없습니다. ASabotagePlayerController::FromServer_RemoveTunnelCard() 실패합니다."));
		return;
	}

	window->RemoveHandCard(cardIdx);
	window->SetShowAddCardPoint(false);
	window->SetMessage("카드를 버렸습니다.");
}

void ASabotagePlayerController::ToServer_BreakEquipment_Implementation(const int32& breakCardIdx, const int32& targetPlayerID)
{
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ToServer_BreakEquipment() 실패합니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	ASabotageGameMode* gameMode = Cast<ASabotageGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("ASabotageGameMode 얻을 수 없습니다. ToServer_BreakEquipment() 실패합니다."));
		return;
	}

	gameMode->BreakEquipment(breakCardIdx, targetPlayerID, PlayerState->GetPlayerId());
}

void ASabotagePlayerController::ToServer_RepairEquipment_Implementation(const int32& breakCardIdx, const int32& targetPlayerID)
{
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ToServer_RepairEquipment() 실패합니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	ASabotageGameMode* gameMode = Cast<ASabotageGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("ASabotageGameMode 얻을 수 없습니다. ToServer_RepairEquipment() 실패합니다."));
		return;
	}

	gameMode->RepairEquipment(breakCardIdx, targetPlayerID, PlayerState->GetPlayerId());
}

void ASabotagePlayerController::FromServer_UpdateEquipment_Implementation(const bool isEnablePick, const bool isEnableLantern, const bool isEnableWagon, const int32& targetPlayerID)
{
	if (nullptr == window)
	{
		PRINT_WARNING("%s", TEXT("UI 가 없습니다. ASabotagePlayerController::FromServer_UpdateEquipment() 실패합니다."));
		return;
	}

	window->UpdateEquipment(isEnablePick, isEnableLantern, isEnableWagon, targetPlayerID);
}

void ASabotagePlayerController::FromServer_EndGame_Implementation(const bool isSaboteurWin, const bool isSaboteur)
{
	if (nullptr == window)
	{
		PRINT_WARNING("%s", TEXT("UI 가 없습니다. ASabotagePlayerController::FromServer_EndGame() 실패합니다."));
		return;
	}

	if (isSaboteurWin == isSaboteur)
	{
		if (true == isSaboteur)
		{
			window->SetMessage("축하합니다~!!! 방해꾼의 승리입니다.");
		}
		else
		{
			window->SetMessage("축하합니다~!!! 광부의 승리입니다.");
		}
		return;
	}
	window->SetMessage("아쉽네요. 다시 도전해보세요.");
}

void ASabotagePlayerController::ToServer_ResetGame_Implementation()
{
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ToServer_ResetGame() 실패합니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	ASabotageGameMode* gameMode = Cast<ASabotageGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("ASabotageGameMode 얻을 수 없습니다. ToServer_ResetGame() 실패합니다."));
		return;
	}

	gameMode->ResetGame(false);
}

void ASabotagePlayerController::FromServer_SetMessage_Implementation(const FString& message)
{
	if (nullptr != window)
	{
		window->SetMessage(TCHAR_TO_UTF8(*message));
	}
}