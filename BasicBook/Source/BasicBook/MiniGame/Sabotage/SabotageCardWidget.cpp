﻿#include "SabotageCardWidget.h"

float USabotageCardWidget::GetSizeBoxWidth()
{
	return cardSizeBox->WidthOverride;
}

float USabotageCardWidget::GetSizeBoxHeight()
{
	return cardSizeBox->HeightOverride;
}

void USabotageCardWidget::Reset()
{
	bgImage->SetVisibility(ESlateVisibility::Collapsed);

	top->SetVisibility(ESlateVisibility::Collapsed);
	bottom->SetVisibility(ESlateVisibility::Collapsed);
	left->SetVisibility(ESlateVisibility::Collapsed);
	right->SetVisibility(ESlateVisibility::Collapsed);

	pick->SetVisibility(ESlateVisibility::Collapsed);
	lantern->SetVisibility(ESlateVisibility::Collapsed);
	wagon->SetVisibility(ESlateVisibility::Collapsed);

	backImage->SetVisibility(ESlateVisibility::Collapsed);

	addCardEffectImage->SetVisibility(ESlateVisibility::Collapsed);

	if (true == onClickDelegate.IsBound())
	{
		onClickDelegate.~TDelegate();
	}
}

void USabotageCardWidget::SetBGImage(UTexture2D* texture)
{
	if (nullptr != texture)
	{
		bgImage->SetBrushFromTexture(texture);
		bgImage->SetVisibility(ESlateVisibility::Visible);
	}
}

void USabotageCardWidget::TurnOver()
{
	FromClient_PlayTurnOverAnimation();
}

void USabotageCardWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (false == touchButton->OnClicked.IsBound())
	{
		touchButton->OnClicked.AddDynamic(this, &USabotageCardWidget::OnClickedCard);
	}
}

void USabotageCardWidget::OnClickedCard()
{
	if (true == onClickDelegate.IsBound())
	{
		onClickDelegate.Execute();
		//onClickDelegate.~TDelegate();
	}
}