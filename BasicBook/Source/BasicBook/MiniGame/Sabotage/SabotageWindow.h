﻿#pragma once

#include "SabotageCardWidget.h"

#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/CanvasPanel.h"
#include "Components/ScaleBox.h"
#include "Components/TextBlock.h"
//#include "Components/TileView.h"
#include "Containers/Queue.h"
#include "CoreMinimal.h"

#include "SabotageWindow.generated.h"

namespace NSSabotage
{
	#define CARD_WIDGET_PATH FString("/Game/MiniGame/Sabotage/SabotageCardWidgetBP.SabotageCardWidgetBP_C")
	
	// 95% 만 확대시켜서 5% 만큼을 여유 공간으로 갖는다.
	#define BOARD_SCALE_CORRECTION_VALUE 0.95f

	// 카드 위젯 재활용은 10개만 한다. 나머지는 GC에 맡긴다.
	#define CARD_WIDGET_POOL_LIMIT 10
}

USTRUCT()
struct FSabotagePlayerUIInfo
{
	GENERATED_BODY()

	int32 playerID;
	int32 handCount;
	bool isEnablePick;
	bool isEnableLantern;
	bool isEnableWagon;

	int32 fakeCardIdx;

	FSabotagePlayerUIInfo()
		: playerID(-1), handCount(0), isEnablePick(true), isEnableLantern(true), isEnableWagon(true), fakeCardIdx(-1) {}

	FSabotagePlayerUIInfo(const int32& _playerID, const int32& _handCount, const bool& _isEnablePick, const bool& _isEnableLantern, const bool& _isEnableWagon)
		: playerID(_playerID), handCount(_handCount), isEnablePick(_isEnablePick), isEnableLantern(_isEnableLantern), isEnableWagon(_isEnableWagon), fakeCardIdx(-1) {}

	void Reset(const int32& _playerID, const int32& _handCount, const bool& _isEnablePick, const bool& _isEnableLantern, const bool& _isEnableWagon)
	{
		playerID = _playerID;
		handCount = _handCount;
		isEnablePick = _isEnablePick;
		isEnableLantern = _isEnableLantern;
		isEnableWagon = _isEnableWagon;
		fakeCardIdx = -1;
	}

	void UpateTextBlock(UTextBlock* text, const bool isOnlyPlayerID = false)
	{
		if (nullptr != text)
		{
			FString info = FString::Printf(TEXT("%d"), playerID);

			if (false == isOnlyPlayerID)
			{
				info.Appendf(TEXT("\nH : %d"), handCount);

				if (false == isEnablePick)
				{
					info.Append("\n!!Pick");
				}

				if (false == isEnableLantern)
				{
					info.Append("\n!!Lantern");
				}

				if (false == isEnableWagon)
				{
					info.Append("\n!!Wagon");
				}
			}

			text->SetText(FText::FromString(info));
		}
	}

	bool operator==(const int32& rhsPlayerID) const
	{
		return (playerID == rhsPlayerID);
	}

	//bool operator==(const FSabotagePlayerUIInfo& RHS) const
	//{
	//	return (playerID == RHS.playerID);
	//}
	
	friend bool operator==(const FSabotagePlayerUIInfo& LHS, const FSabotagePlayerUIInfo& RHS)
	{
		return (LHS.playerID == RHS.playerID);
	}

	friend uint32 GetTypeHash(const FSabotagePlayerUIInfo& RHS)
	{
		return GetTypeHash(RHS.playerID);
	}
};

UCLASS()
class BASICBOOK_API USabotageWindow : public UUserWidget
{
	GENERATED_BODY()

protected:
	//UPROPERTY(meta=(BindWidget)) UTileView* cardTileView; // TODO Level 2: 카드 일람 메뉴 추가
	UPROPERTY(meta=(BindWidget)) UScaleBox* boardScaleBox;
	UPROPERTY(BlueprintReadOnly, meta=(BindWidget)) UCanvasPanel* boardPanel;

	UPROPERTY(meta = (BindWidget)) UScaleBox* handScaleBox;
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget)) UCanvasPanel* handPanel;

	UPROPERTY(BlueprintReadOnly, meta=(BindWidget)) UCanvasPanel* characterPickPanelRoot;
	UPROPERTY(BlueprintReadOnly, meta=(BindWidget)) UCanvasPanel* characterPickPanel;

	UPROPERTY(BlueprintReadWrite) TArray<USabotageCardWidget*> playerCardWidgets;
	UPROPERTY(BlueprintReadWrite) TArray<UTextBlock*> playerInfoTexts;
	TArray<FSabotagePlayerUIInfo> playerInfoList;
	int32 myCardIdx;
	int32 myFakeCardIdx;

	UPROPERTY(meta = (BindWidget)) UButton* trashCanBtn;

	UPROPERTY(meta=(BindWidget)) UButton* startBtn;
	UPROPERTY(meta=(BindWidget)) UButton* restartBtn;

	UPROPERTY(meta=(BindWidget)) class UTextBlock* messageText;

	// NOTE: 위젯에 AddToRoot() 하면, 에디터 Stop 시마다 크래시 발생.
	// - UObjectBaseUtility::MarkPendingKill() 에서 IsRooted() 인 상황이면 문제가 된다.
	// - 위젯을 돌려쓰는 더 나은 방법이 있다면 수정하고 싶다.
	UPROPERTY(meta = (BindWidget)) UCanvasPanel* cardWidgetPool;

	TMap<FIntPoint, int32> board;
	TSet<int32> hand;
	int32 selectedHandCardIdx;

	FVector2D boardSize;
	FVector2D cardSize;

	// 카드가 배치된 영역을 계산하기 위한 값.
	FVector2D boardCenterPos;
	int32 minPointX, maxPointX;
	int32 minPointY, maxPointY;

	float handWidth;
	float handCardOverlapX;

	// 전체 카드 정보.
	TArray<class USabotageCard*> cardList;
	
	TArray<class UCharacterPickCard*> characterPickCards;

	TArray<class UAddPointCard*> addPointCards;
	TQueue<int32> addPointCardIdxQueue;

public:
	void Reset();

	void AddPlayerInfo(const int32& playerID, const int32& handCount, const bool& isEnablePick, const bool& isEnableLantern, const bool& isEnableWagon);
	void RemovePlayerInfo(const int32& playerID);
	void OpenCharacterCard(const int32& cardIdx, const int32& fakeCardIdx, const int32& playerID, const bool& isMine);
	void UpdateHandCount(const int32& handCount, const int32& playerID);
	void UpdateEquipment(const bool& isEnablePick, const bool& isEnableLantern, const bool& isEnableWagon, const int32& targetPlayerID);

	bool IsChangedBoardSize(const int32& pointX, const int32& pointY);
	void FitBoardSize();

	void InitBoard(const TArray<int32>& initialBoard);
	void AddCard(const int32& pointX, const int32& pointY, const int32& cardIdx, const bool& isOpended = true);
	void RemoveBoardCard(const FIntPoint& point);
	void TurnOverArrivalPointCard(const FIntPoint& point);

	void AddHandCard(const int32& cardIdx);
	void RemoveHandCard(const int32& cardIdx);
	void OpenArrivalPointCard(const int32& cardIdx, const FIntPoint& point);

	void ShowCharacterPickPanel(const int32& characterCount);
	void OpenPickedCharacterCard(const int32& pickIdx, const int32& cardIdx, const bool& isMine);
	void HideCharacterPickPanel();

	void FindAddCardPoint();
	void SetShowAddCardPoint(const bool& isShow);

	void OnClickedHandCard(const int32& cardIdx);
	void OnClickedTunnelCard(const int32& cardIdx);
	void OnClickedAddPointCard(const int32& cardIdx);
	void OnClickedCharacterCard(const int32& cardIdx);

	UFUNCTION()
	void OnClickedTrashCan();

	void SetEnableStartBtn(const bool& isEnable);
	void SetEnableRestartBtn(const bool& isEnable);

	void SetMessage(const char* message);

protected:
	void NativeConstruct() override;
	~USabotageWindow();

	void EnqueueAddPointCard(const int32& idx);
	UAddPointCard* DequeueAddPointCard();

	void EnqueueCardWidget(USabotageCardWidget* cardWidget);
	USabotageCardWidget* DequeueCardWidget();

	UFUNCTION(BlueprintImplementableEvent, Category = "FromClient")
	void FromClient_PlayHideCharacterPickPanelAnimation();

	UFUNCTION(BlueprintCallable, Category = "ToClient")
	void ToClient_EndPlayHideCharacterPickPanelAnimation();
};