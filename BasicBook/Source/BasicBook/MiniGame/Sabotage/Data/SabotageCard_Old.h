﻿//#pragma once
//
//namespace NSSabotage
//{
//	//UENUM()
//	//enum class eCategory : uint8
//	//{
//	//	Tunnel
//	//	,Action
//	//	,Character
//	//	,Gold
//	//};
//
//	//UENUM()
//	//enum class eBreakableTarget : uint8
//	//{
//	//	Pick
//	//	,Lantern
//	//	,Wagon
//	//};
//
//	class Card
//	{
//	protected:
//		FString textureName;
//
//	public:
//		Card(FString _textureName = TEXT("")) : textureName(_textureName) {}
//
//		const FString& GetTextureName() { return textureName; }
//	};
//
//	/// <summary>
//	/// 굴 카드
//	/// </summary>
//	class TunnelCard : public Card
//	{
//	protected:
//		bool isEnableTop;
//		bool isEnableBottom;
//		bool isEnableLeft;
//		bool isEnableRight;
//
//		bool isBlocked;
//
//	public:
//		TunnelCard(bool _isEnableTop = false, bool _isEnableBottom = false, bool _isEnableLeft = false, bool _isEnableRight = false, FString _textureName = TEXT(""), bool _isBlocked = false)
//			: Card(_textureName), isEnableTop(_isEnableTop), isEnableBottom(_isEnableBottom), isEnableLeft(_isEnableLeft), isEnableRight(_isEnableRight), isBlocked(_isBlocked) {}
//
//		const bool& GetIsEnableTop() { return isEnableTop; }
//		const bool& GetIsEnableBottom() { return isEnableBottom; }
//		const bool& GetIsEnableLeft() { return isEnableLeft; }
//		const bool& GetIsEnableRight() { return isEnableRight; }
//		const bool& GetIsBlocked() { return isBlocked; }
//	};
//
//	class StartingPointCard : public TunnelCard
//	{
//	public:
//		StartingPointCard(bool _isEnableTop = false, bool _isEnableBottom = false, bool _isEnableLeft = false, bool _isEnableRight = false, FString _textureName = TEXT(""))
//			: TunnelCard(_isEnableTop, _isEnableBottom, _isEnableLeft, _isEnableRight, _textureName) {}
//	};
//
//	class ArrivalPointCard : public TunnelCard
//	{
//	protected:
//		bool isFake;
//
//	public:
//		ArrivalPointCard(bool _isFake, bool _isEnableTop = false, bool _isEnableBottom = false, bool _isEnableLeft = false, bool _isEnableRight = false, FString _textureName = TEXT(""), bool _isBlocked = false)
//			: TunnelCard(_isEnableTop, _isEnableBottom, _isEnableLeft, _isEnableRight, _textureName, _isBlocked), isFake(_isFake) {}
//
//		const bool& GetIsFake() { return isFake; }
//	};
//
//	/// <summary>
//	/// 행동 카드
//	/// </summary>
//	class ActionCard : public Card
//	{
//	public:
//		ActionCard(FString _textureName = TEXT(""))
//			: Card(_textureName) {}
//	};
//
//	class MapCard : public ActionCard
//	{
//	public:
//		MapCard(FString _textureName = TEXT(""))
//			: ActionCard(_textureName) {}
//	};
//
//	class RockfallCard : public ActionCard
//	{
//	public:
//		RockfallCard(FString _textureName = TEXT(""))
//			: ActionCard(_textureName) {}
//	};
//
//	class BreakCard : public ActionCard
//	{
//	protected:
//		bool isPick;
//		bool isLantern;
//		bool isWagon;
//
//	public:
//		BreakCard(bool _isPick = false, bool _isLantern = false, bool _isWagon = false, FString _textureName = TEXT(""))
//			: ActionCard(_textureName), isPick(_isPick), isLantern(_isLantern), isWagon(_isWagon) {}
//
//		const bool& GetIsPick() { return isPick; }
//		const bool& GetIsLantern() { return isLantern; }
//		const bool& GetIsWagon() { return isWagon; }
//	};
//
//	class RepairCard : public ActionCard
//	{
//	protected:
//		bool isPick;
//		bool isLantern;
//		bool isWagon;
//
//	public:
//		RepairCard(bool _isPick = false, bool _isLantern = false, bool _isWagon = false, FString _textureName = TEXT(""))
//			: ActionCard(_textureName), isPick(_isPick), isLantern(_isLantern), isWagon(_isWagon) {}
//
//		const bool& GetIsPick() { return isPick; }
//		const bool& GetIsLantern() { return isLantern; }
//		const bool& GetIsWagon() { return isWagon; }
//	};
//
//	/// <summary>
//	/// 캐릭터 카드
//	/// </summary>
//	class CharacterCard : public Card
//	{
//	protected:
//		bool isSaboteur;
//
//	public:
//		CharacterCard(bool _isSaboteur, FString _textureName = TEXT(""))
//			: Card(_textureName), isSaboteur(_isSaboteur) {}
//
//		const bool& GetIsSaboteur() { return isSaboteur; }
//	};
//
//	/// <summary>
//	/// 금덩이 카드
//	/// </summary>
//	class GoldCard : public Card
//	{
//	protected:
//		int32 count;
//
//	public:
//		GoldCard(int32 _count, FString _textureName = TEXT(""))
//			: Card(_textureName), count(_count) {}
//
//		const int32& GetCount() { return count; }
//	};
//}