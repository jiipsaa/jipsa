﻿#pragma once

#include "../../../Jipsa/Log.h"
#include "../SabotageCardWidget.h"
#include "../SabotagePlayerController.h"

#include "CoreMinimal.h"
#include "Engine/Texture2D.h"
#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/NoExportTypes.h"

#include "SabotageCard.generated.h"

UCLASS()
class BASICBOOK_API USabotageCard : public UObject
{
	GENERATED_BODY()

protected:
	// 변경되지 않는 정보.
	int32 cardIdx;
	FString textureName;
	UTexture2D* backTexture;

	// 변경되는 정보.
	USabotageCardWidget* widget;
	ASabotagePlayerController* playerController;
	bool isOpened;					// 공개된 카드인가?

public:
	void FakeConstructor(const int32& _cardIdx, const FString& _textureName, UTexture2D* _backTexture)
	{
		cardIdx = _cardIdx;
		textureName = _textureName;
		backTexture = _backTexture;

		widget = nullptr;
		playerController = nullptr;
		isOpened = false;
	}

	// 변경되지 않는 정보.
	const int32& GetCardIdx() { return cardIdx; }
	const FString& GetTextureName() { return textureName; }

	// 변경되는 정보.
	USabotageCardWidget* GetWidget()
	{
		return widget;
	}
	
	const bool& GetIsOpened() { return isOpened; }
	void SetIsOpened(const bool& _isOpened)
	{
		isOpened = _isOpened;
	}

	virtual void UpdateCardWidget(USabotageCardWidget* cardWidget, const bool& isForceShow = false)
	{
		//PRINT("USabotageCard::UpdateCardWidget(), cardIdx=%d, isForceShow=%d", cardIdx, isForceShow);

		if (false == textureName.IsEmpty())
		{
			UTexture2D* texture = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), nullptr, *textureName));
			cardWidget->bgImage->SetBrushFromTexture(texture);
			cardWidget->bgImage->SetVisibility(ESlateVisibility::Visible);
		}

		if (nullptr != backTexture)
		{
			cardWidget->backImage->SetBrushFromTexture(backTexture);
		}

		if ((true == isOpened) || (true == isForceShow))
		{
			cardWidget->backImage->SetVisibility(ESlateVisibility::Collapsed);
		}
		else
		{
			cardWidget->backImage->SetVisibility(ESlateVisibility::Visible);
		}

		if (true == cardWidget->onClickDelegate.IsBound())
		{
			cardWidget->onClickDelegate.~TDelegate();
		}
		cardWidget->onClickDelegate.BindUFunction(this, FName("OnClicked"));

		widget = cardWidget;
		playerController = Cast<ASabotagePlayerController>(UGameplayStatics::GetPlayerController(widget->GetWorld(), 0));
	}
	
	UFUNCTION()
	virtual void OnClicked() {}
};

/// <summary>
/// 굴 카드
/// </summary>
UCLASS()
class UTunnelCard : public USabotageCard
{
	GENERATED_BODY()
	
protected:
	bool isEnableTop;
	bool isEnableBottom;
	bool isEnableLeft;
	bool isEnableRight;

	bool isBlocked;					// 바위로 막혀있는 굴 카드인가?

public:
	void FakeConstructor(const int32& _cardIdx, const FString& _textureName, UTexture2D* _backTexture
		,const bool& _isEnableTop, const bool& _isEnableBottom, const bool& _isEnableLeft, const bool& _isEnableRight
		,const bool& _isBlocked)
	{
		Super::FakeConstructor(_cardIdx, _textureName, _backTexture);
		isEnableTop = _isEnableTop;
		isEnableBottom = _isEnableBottom;
		isEnableLeft = _isEnableLeft;
		isEnableRight = _isEnableRight;
		isBlocked = _isBlocked;
	}

	const bool& GetIsEnableTop() { return isEnableTop; }
	const bool& GetIsEnableBottom() { return isEnableBottom; }
	const bool& GetIsEnableLeft() { return isEnableLeft; }
	const bool& GetIsEnableRight() { return isEnableRight; }
	const bool& GetIsBlocked() { return isBlocked; }

	void UpdateCardWidget(USabotageCardWidget* cardWidget, const bool& isForceShow = false) override
	{
		Super::UpdateCardWidget(cardWidget, isForceShow);

		cardWidget->top->SetVisibility((true == isEnableTop) ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
		cardWidget->bottom->SetVisibility((true == isEnableBottom) ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
		cardWidget->left->SetVisibility((true == isEnableLeft) ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
		cardWidget->right->SetVisibility((true == isEnableRight) ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
	}

	void OnClicked() override
	{
		if (nullptr == playerController)
		{
			PRINT_ERROR("%s", TEXT("ASabotagePlayerController 얻을 수 없습니다. UTunnelCard::OnClicked() 실패합니다."));
			return;
		}

		playerController->OnClickedTunnelCard(cardIdx);
	}
};

UCLASS()
class UStartingPointCard : public UTunnelCard
{
	GENERATED_BODY()

public:
	void FakeConstructor(const int32& _cardIdx, const FString& _textureName, UTexture2D* _backTexture
		,const bool& _isEnableTop, const bool& _isEnableBottom, const bool& _isEnableLeft, const bool& _isEnableRight)
	{
		Super::FakeConstructor(_cardIdx, _textureName, _backTexture, _isEnableTop, _isEnableBottom, _isEnableLeft, _isEnableRight, false);
	}
};

UCLASS()
class UArrivalPointCard : public UTunnelCard
{
	GENERATED_BODY()

protected:
	bool isFake;

public:
	void FakeConstructor(const int32& _cardIdx, const FString& _textureName, UTexture2D* _backTexture, const bool& _isFake
		,const bool& _isEnableTop, const bool& _isEnableBottom, const bool& _isEnableLeft, const bool& _isEnableRight, const bool& _isBlocked)
	{
		Super::FakeConstructor(_cardIdx, _textureName, _backTexture, _isEnableTop, _isEnableBottom, _isEnableLeft, _isEnableRight, _isBlocked);
		isFake = _isFake;
	}

	const bool& GetIsFake() { return isFake; }
};

UCLASS()
class UAddPointCard : public UTunnelCard
{
	GENERATED_BODY()

public:
	void FakeConstructor(const int32& _cardIdx)
	{
		Super::FakeConstructor(_cardIdx, TEXT(""), nullptr, false, false, false, false, false);
	}

	void UpdateCardWidget(USabotageCardWidget* cardWidget, const bool& isForceShow = false) override
	{
		Super::UpdateCardWidget(cardWidget, isForceShow);

		cardWidget->addCardEffectImage->SetVisibility(ESlateVisibility::Visible);
	}

	void OnClicked() override
	{
		if (nullptr == playerController)
		{
			PRINT_ERROR("%s", TEXT("ASabotagePlayerController 얻을 수 없습니다. UTunnelCard::OnClicked() 실패합니다."));
			return;
		}

		playerController->OnClickedAddPointCard(cardIdx);
	}
};

/// <summary>
/// 행동 카드
/// </summary>
UCLASS()
class UActionCard : public USabotageCard
{
	GENERATED_BODY()

public:
//	void FakeConstructor(const int32& _cardIdx, const FString& _textureName, UTexture2D* _backTexture)
//	{
//		Super::FakeConstructor(_cardIdx, _textureName, _backTexture);
//	}

	void UpdateCardWidget(USabotageCardWidget* cardWidget, const bool& isForceShow = false) override
	{
		Super::UpdateCardWidget(cardWidget, isForceShow);

		cardWidget->top->SetVisibility(ESlateVisibility::Collapsed);
		cardWidget->bottom->SetVisibility(ESlateVisibility::Collapsed);
		cardWidget->left->SetVisibility(ESlateVisibility::Collapsed);
		cardWidget->right->SetVisibility(ESlateVisibility::Collapsed);
	}

	void OnClicked() override
	{
		if (nullptr == playerController)
		{
			PRINT_ERROR("%s", TEXT("ASabotagePlayerController 얻을 수 없습니다. UMapCard::OnClicked() 실패합니다."));
			return;
		}

		playerController->OnClickedHandCard(cardIdx);
	}
};

UCLASS()
class UMapCard : public UActionCard
{
	GENERATED_BODY()

//public:
//	void FakeConstructor(const int32& _cardIdx, const FString& _textureName, UTexture2D* _backTexture)
//	{
//		Super::FakeConstructor(_cardIdx, _textureName, _backTexture);
//	}
};

UCLASS()
class URockfallCard : public UActionCard
{
	GENERATED_BODY()

//public:
//	void FakeConstructor(const int32& _cardIdx, const FString& _textureName, UTexture2D* _backTexture)
//	{
//		Super::FakeConstructor(_cardIdx, _textureName, _backTexture);
//	}
};

UCLASS()
class UBreakCard : public UActionCard
{
	GENERATED_BODY()

protected:
	bool isPick;
	bool isLantern;
	bool isWagon;

public:
	void FakeConstructor(const int32& _cardIdx, const FString& _textureName, UTexture2D* _backTexture
		,const bool& _isPick, const bool& _isLantern, const bool& _isWagon)
	{
		Super::FakeConstructor(_cardIdx, _textureName, _backTexture);
		isPick = _isPick;
		isLantern = _isLantern;
		isWagon = _isWagon;
	}

	const bool& GetIsPick() { return isPick; }
	const bool& GetIsLantern() { return isLantern; }
	const bool& GetIsWagon() { return isWagon; }

	void UpdateCardWidget(USabotageCardWidget* cardWidget, const bool& isForceShow = false) override
	{
		Super::UpdateCardWidget(cardWidget, isForceShow);

		cardWidget->pick->SetVisibility((true == isPick) ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
		cardWidget->lantern->SetVisibility((true == isLantern) ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
		cardWidget->wagon->SetVisibility((true == isWagon) ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
	}
};

UCLASS()
class URepairCard : public UActionCard
{
	GENERATED_BODY()

protected:
	bool isPick;
	bool isLantern;
	bool isWagon;

public:
	void FakeConstructor(const int32& _cardIdx, const FString& _textureName, UTexture2D* _backTexture
		,const bool& _isPick, const bool& _isLantern, const bool& _isWagon)
	{
		Super::FakeConstructor(_cardIdx, _textureName, _backTexture);
		isPick = _isPick;
		isLantern = _isLantern;
		isWagon = _isWagon;
	}

	const bool& GetIsPick() { return isPick; }
	const bool& GetIsLantern() { return isLantern; }
	const bool& GetIsWagon() { return isWagon; }

	void UpdateCardWidget(USabotageCardWidget* cardWidget, const bool& isForceShow = false) override
	{
		Super::UpdateCardWidget(cardWidget, isForceShow);

		cardWidget->pick->SetVisibility((true == isPick) ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
		cardWidget->lantern->SetVisibility((true == isLantern) ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
		cardWidget->wagon->SetVisibility((true == isWagon) ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
	}
};

/// <summary>
/// 캐릭터 카드
/// </summary>
UCLASS()
class UCharacterCard : public USabotageCard
{
	GENERATED_BODY()

protected:
	bool isSaboteur;

public:
	void FakeConstructor(const int32& _cardIdx, const FString& _textureName, UTexture2D* _backTexture, const bool& _isSaboteur)
	{
		Super::FakeConstructor(_cardIdx, _textureName, _backTexture);
		isSaboteur = _isSaboteur;
	}

	const bool& GetIsSaboteur() { return isSaboteur; }

	void UpdateCardWidget(USabotageCardWidget* cardWidget, const bool& isForceShow = false) override
	{
		Super::UpdateCardWidget(cardWidget, isForceShow);

		cardWidget->top->SetVisibility(ESlateVisibility::Collapsed);
		cardWidget->bottom->SetVisibility(ESlateVisibility::Collapsed);
		cardWidget->left->SetVisibility(ESlateVisibility::Collapsed);
		cardWidget->right->SetVisibility(ESlateVisibility::Collapsed);

		cardWidget->pick->SetVisibility(ESlateVisibility::Collapsed);
		cardWidget->lantern->SetVisibility(ESlateVisibility::Collapsed);
		cardWidget->wagon->SetVisibility(ESlateVisibility::Collapsed);
	}

	void OnClicked() override
	{
		if (nullptr == playerController)
		{
			PRINT_ERROR("%s", TEXT("ASabotagePlayerController 얻을 수 없습니다. UCharacterCard::OnClicked() 실패합니다."));
			return;
		}

		playerController->OnClickedCharacterCard(cardIdx);
	}
};

UCLASS()
class UCharacterPickCard : public UCharacterCard
{
	GENERATED_BODY()

public:
	void FakeConstructor(const int32& _pickIdx, UTexture2D* _backTexture)
	{
		Super::FakeConstructor(_pickIdx, TEXT(""), _backTexture, false);
	}

	void OnClicked() override
	{
		if (nullptr == playerController)
		{
			PRINT_ERROR("%s", TEXT("ASabotagePlayerController 얻을 수 없습니다. UCharacterPickCard::OnClicked() 실패합니다."));
			return;
		}

		playerController->ToServer_PickCharacter(cardIdx, playerController->PlayerState->GetPlayerId());
	}
};

/// <summary>
/// 금덩이 카드
/// </summary>
UCLASS()
class UGoldCard : public USabotageCard
{
	GENERATED_BODY()

protected:
	int32 count;

public:
	void FakeConstructor(const int32& _cardIdx, const FString& _textureName, UTexture2D* _backTexture, const int32& _count)
	{
		Super::FakeConstructor(_cardIdx, _textureName, _backTexture);
		count = _count;
	}

	const int32& GetCount() { return count; }

	void UpdateCardWidget(USabotageCardWidget* cardWidget, const bool& isForceShow = false) override
	{
		Super::UpdateCardWidget(cardWidget, isForceShow);

		cardWidget->top->SetVisibility(ESlateVisibility::Collapsed);
		cardWidget->bottom->SetVisibility(ESlateVisibility::Collapsed);
		cardWidget->left->SetVisibility(ESlateVisibility::Collapsed);
		cardWidget->right->SetVisibility(ESlateVisibility::Collapsed);

		cardWidget->pick->SetVisibility(ESlateVisibility::Collapsed);
		cardWidget->lantern->SetVisibility(ESlateVisibility::Collapsed);
		cardWidget->wagon->SetVisibility(ESlateVisibility::Collapsed);
	}
};