﻿#pragma once

#include "../../../Jipsa/Singleton.h"
#include "SabotageCard.h"

#include "Engine/Texture2D.h"

namespace NSSabotage
{
	#define CARD_DATA_PATH FString("Data/Sabotage.xml")
	#define NUM_OF_PLAYER_DATA_PATH FString("Data/SabotageNumOfPlayer.xml")

	struct ExtraCardInfo
	{
		TArray<int32> startingPointCardIndices;
		TArray<int32> arrivalPointCardIndices;

		TArray<int32> tunnelAndActionCardIndices;

		TArray<int32> minerCardIndices;
		TArray<int32> saboteurCardIndices;

		TArray<int32> goldCardIndices;
	};

	class SabotageData : public Singleton<SabotageData>
	{
	public:
		UTexture2D* defaultBackTexture = nullptr;
		UTexture2D* arrivalPointBackTexture = nullptr;
		UTexture2D* characterBackTexture = nullptr;
		UTexture2D* goldBackTexture = nullptr;

	public:
		void OnLoadComplete() override;

		// TODO: LoadBinary() 추가. LoadXml() 에서 파싱하면서 Binary 파일 만들고, 실제 빌드했을때는 Binary 파일 읽도록 해야한다.
		void LoadCardList(TArray<USabotageCard*>& cardList, ExtraCardInfo* pExtraCardInfo = nullptr);
		void GetNumOfPlayerInfo(const int32& playerCount, int32& minerCount, int32& saboteurCount, int32& initialHandCount);
	};
}