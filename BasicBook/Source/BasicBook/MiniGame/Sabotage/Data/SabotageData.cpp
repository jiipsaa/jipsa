﻿#include "SabotageData.h"

#include "../../../Jipsa/Log.h"

#include "Runtime/XmlParser/Public/XmlFile.h"
#include "Runtime/XmlParser/Public/XmlNode.h"

using namespace NSSabotage;

void SabotageData::OnLoadComplete()
{
//#if !WITH_EDITOR
//	// NOTE: 에디터 환경에서는 서버와 클라들이 싱글턴을 공유한다.
//	// 서버가 만든 방에 클라가 들어오면, 클라가 싱글턴에 정의된 변수를 초기화하면서 문제가 발생한다.
//	// 클라가 방을 나갈때도, 싱글턴에 정의된 변수를 날리기 때문에 방에있던 유저들에게 문제가 생긴다.
//	// -> 에디터에서는, 맵 이동시 EmptyData()를 하지 않고, GameMode 에서 InitGame(), Logout() 시점에 명시적으로 호출한다.
//	EmptyData();
//#endif
}

void SabotageData::LoadCardList(TArray<USabotageCard*>& cardList, ExtraCardInfo* pExtraCardInfo)
{
	if (0 < cardList.Num())
	{
		PRINT_WARNING("이미 cardList 초기화 되어 있습니다. cardListCount=%d", cardList.Num());
		return;
	}

	FString inFile = FString::Printf(TEXT("%s%s"), *FPaths::ProjectContentDir(), *CARD_DATA_PATH);
	FXmlFile xmlFile(inFile);
	if (false == xmlFile.IsValid())
	{
		PRINT_ERROR("%s", *xmlFile.GetLastError());
		return;
	}

	FXmlNode* rootNode = xmlFile.GetRootNode();
	if (nullptr == rootNode)
	{
		PRINT_ERROR("RootNode 얻을 수 없습니다. filePath=%s", *inFile);
		
		xmlFile.Clear();
		return;
	}

	FString category;
	FString texture;
	FString feature;
	FString direction;
	bool isEnableTop = false;
	bool isEnableBottom = false;
	bool isEnableLeft = false;
	bool isEnableRight = false;
	TArray<FString> parsedArr;
	const TCHAR DELIMITER[] = TEXT("|");

	const FXmlNode* childNode = rootNode->GetFirstChildNode();
	while (nullptr != childNode)
	{
		//FString category = childNode->GetAttribute(TEXT("Category"));
		//FString feature = childNode->GetAttribute(TEXT("Feature"));
		//FString direction = childNode->GetAttribute(TEXT("Direction"));
		//FString texture = childNode->GetAttribute(TEXT("Texture"));
		//PRINT("%s/%s/%s/%s", *category, *feature, *direction, *texture);

		category = childNode->GetAttribute(TEXT("Category"));
		if (true == category.IsEmpty()) { PRINT_ERROR("%s", TEXT("Category 가 있어야 합니다. ")); return; } // TODO: assert() 로 변경.

		texture = childNode->GetAttribute(TEXT("Texture"));
		if (true == texture.IsEmpty()) { PRINT_ERROR("%s", TEXT("Texture 가 있어야 합니다. ")); return; } // TODO: assert() 로 변경.

		/// <summary>
		/// 뒷면 이미지
		/// </summary>
		if (true == category.Equals("DefaultBack", ESearchCase::IgnoreCase))
		{
			defaultBackTexture = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), nullptr, *texture));
		}
		else if (true == category.Equals("ArrivalPointBack", ESearchCase::IgnoreCase))
		{
			arrivalPointBackTexture = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), nullptr, *texture));
		}
		else if (true == category.Equals("CharacterBack", ESearchCase::IgnoreCase))
		{
			characterBackTexture = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), nullptr, *texture));
		}
		else if (true == category.Equals("GoldBack", ESearchCase::IgnoreCase))
		{
			goldBackTexture = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), nullptr, *texture));
		}
		/// <summary>
		/// 굴 카드
		/// </summary>
		else if (true == category.Equals("Tunnel", ESearchCase::IgnoreCase))
		{
			direction = childNode->GetAttribute(TEXT("Direction"));
			if (true == direction.IsEmpty()) { PRINT_ERROR("%s", TEXT("Category 가 Tunnel 이면, Direction 이 있어야 합니다. ")); } // TODO: assert() 로 변경.
			if (false == direction.IsEmpty())
			{
				int32 count = direction.ParseIntoArray(parsedArr, DELIMITER, true);
				if (0 == count) { PRINT_ERROR("%s Direction={%s}", TEXT("Category 가 Tunnel 이면, Direction 이 있어야 합니다."), *direction); } // TODO: assert() 로 변경.
				if (0 < count)
				{
					isEnableTop = parsedArr.Contains("T");
					isEnableBottom = parsedArr.Contains("B");
					isEnableLeft = parsedArr.Contains("L");
					isEnableRight = parsedArr.Contains("R");
				}
			}

			feature = childNode->GetAttribute(TEXT("Feature"));
			int32 count = feature.ParseIntoArray(parsedArr, DELIMITER, true);
			if (true == parsedArr.Contains("StartingPoint"))
			{
				UStartingPointCard* startingPointCard = NewObject<UStartingPointCard>();
				startingPointCard->AddToRoot();
				startingPointCard->FakeConstructor(cardList.Num(), texture, defaultBackTexture, isEnableTop, isEnableBottom, isEnableLeft, isEnableRight);
				cardList.Emplace(startingPointCard);

				if (nullptr != pExtraCardInfo)
				{
					pExtraCardInfo->startingPointCardIndices.Emplace(startingPointCard->GetCardIdx());
				}
			}
			else if (true == parsedArr.Contains("ArrivalPoint"))
			{
				bool isFake = parsedArr.Contains("Fake");
				bool isBlocked = parsedArr.Contains("Blocked");
				
				UArrivalPointCard* arrivalPointCard = NewObject<UArrivalPointCard>();
				arrivalPointCard->AddToRoot();
				arrivalPointCard->FakeConstructor(cardList.Num(), texture, arrivalPointBackTexture, isFake, isEnableTop, isEnableBottom, isEnableLeft, isEnableRight, isBlocked);
				cardList.Emplace(arrivalPointCard);

				if (nullptr != pExtraCardInfo)
				{
					pExtraCardInfo->arrivalPointCardIndices.Emplace(arrivalPointCard->GetCardIdx());
				}
			}
			else
			{
				bool isBlocked = parsedArr.Contains("Blocked");
				
				UTunnelCard* tunnelCard = NewObject<UTunnelCard>();
				tunnelCard->AddToRoot();
				tunnelCard->FakeConstructor(cardList.Num(), texture, defaultBackTexture, isEnableTop, isEnableBottom, isEnableLeft, isEnableRight, isBlocked);
				cardList.Emplace(tunnelCard);

				if (nullptr != pExtraCardInfo)
				{
					pExtraCardInfo->tunnelAndActionCardIndices.Emplace(tunnelCard->GetCardIdx());
				}
			}
		}
		/// <summary>
		/// 행동 카드
		/// </summary>
		else if (true == category.Equals("Action", ESearchCase::IgnoreCase))
		{
			feature = childNode->GetAttribute(TEXT("Feature"));
			if (true == feature.IsEmpty()) { PRINT_ERROR("%s", TEXT("Category 가 Action 이면, Feature 가 있어야 합니다. ")); } // TODO: assert() 로 변경.
			if (false == feature.IsEmpty())
			{
				int32 count = feature.ParseIntoArray(parsedArr, DELIMITER, true);
				if (0 == count) { PRINT_ERROR("%s Feature={%s}", TEXT("Category 가 Action 이면, Feature 가 있어야 합니다."), *feature); } // TODO: assert() 로 변경.
				if (0 < count)
				{
					if (nullptr != pExtraCardInfo)
					{
						pExtraCardInfo->tunnelAndActionCardIndices.Emplace(cardList.Num());
					}

					if (true == parsedArr.Contains("Map"))
					{
						UMapCard* mapCard = NewObject<UMapCard>();
						mapCard->AddToRoot();
						mapCard->FakeConstructor(cardList.Num(), texture, defaultBackTexture);
						cardList.Emplace(mapCard);
					}
					else if (true == parsedArr.Contains("Rockfall"))
					{
						URockfallCard* rockfallCard = NewObject<URockfallCard>();
						rockfallCard->AddToRoot();
						rockfallCard->FakeConstructor(cardList.Num(), texture, defaultBackTexture);
						cardList.Emplace(rockfallCard);
					}
					else
					{
						bool isPick = parsedArr.Contains("Pick");
						bool isLantern = parsedArr.Contains("Lantern");
						bool isWagon = parsedArr.Contains("Wagon");
						if ((false == isPick) && (false == isLantern) && (false == isWagon)) { PRINT_ERROR("%s Feature={%s}", TEXT("Feature 에 Pick/Lantern/Wagon 중 최소한 하나는 있어야 합니다."), *feature); } // TODO: assert() 로 변경.

						if (true == parsedArr.Contains("Break"))
						{
							UBreakCard* breakCard = NewObject<UBreakCard>();
							breakCard->AddToRoot();
							breakCard->FakeConstructor(cardList.Num(), texture, defaultBackTexture, isPick, isLantern, isWagon);
							cardList.Emplace(breakCard);
						}
						else
						{
							URepairCard* repairCard = NewObject<URepairCard>();
							repairCard->AddToRoot();
							repairCard->FakeConstructor(cardList.Num(), texture, defaultBackTexture, isPick, isLantern, isWagon);
							cardList.Emplace(repairCard);
						}
					}
				}
			}
		}
		/// <summary>
		/// 캐릭터 카드
		/// </summary>
		else if (true == category.Equals("Character", ESearchCase::IgnoreCase))
		{
			feature = childNode->GetAttribute(TEXT("Feature"));

			bool isSaboteur = feature.Equals("Saboteur", ESearchCase::IgnoreCase);

			UCharacterCard* characterCard = NewObject<UCharacterCard>();
			characterCard->AddToRoot();
			characterCard->FakeConstructor(cardList.Num(), texture, characterBackTexture, isSaboteur);
			cardList.Emplace(characterCard);

			if (nullptr != pExtraCardInfo)
			{
				if (true == isSaboteur)
				{
					pExtraCardInfo->saboteurCardIndices.Emplace(characterCard->GetCardIdx());
				}
				else
				{
					pExtraCardInfo->minerCardIndices.Emplace(characterCard->GetCardIdx());
				}
			}
		}
		else if (true == category.Equals("Gold", ESearchCase::IgnoreCase))
		{
			feature = childNode->GetAttribute(TEXT("Feature"));
			if (true == feature.IsEmpty()) { PRINT_ERROR("%s", TEXT("Category 가 Gold 면, Feature 가 있어야 합니다. ")); } // TODO: assert() 로 변경.
			if (false == feature.IsEmpty())
			{
				int32 count = feature.ParseIntoArray(parsedArr, DELIMITER, true);
				if (count < 2) { PRINT_ERROR("%s Feature={%s}", TEXT("Category 가 Gold 면, Feature 가 2개 있어야 합니다."), *feature); } // TODO: assert() 로 변경.
				if (2 <= count)
				{
					int32 goldCount = FCString::Atoi(*parsedArr[0]);
					int32 goldCardCount = FCString::Atoi(*parsedArr[1]);

					UGoldCard* goldCard = NewObject<UGoldCard>();
					goldCard->AddToRoot();
					goldCard->FakeConstructor(cardList.Num(), texture, goldBackTexture, goldCount);

					// NOTE: 만약에 금 카드가 단일 특성을 가지면, goldCardCount 만큼 새로 생성해야 합니다.
					for (int i = 0; i < goldCardCount; ++i)
					{
						if (nullptr != pExtraCardInfo)
						{
							pExtraCardInfo->goldCardIndices.Emplace(cardList.Num());
						}

						cardList.Emplace(goldCard);
					}
				}
			}
		}
		else
		{
			PRINT_WARNING("알 수 없는 Category 입니다. Category={%s}", *category);
		}

		childNode = childNode->GetNextNode();
	}

	xmlFile.Clear();
}

void SabotageData::GetNumOfPlayerInfo(const int32& playerCount, int32& minerCount, int32& saboteurCount, int32& initialHandCount)
{
	FString inFile = FString::Printf(TEXT("%s%s"), *FPaths::ProjectContentDir(), *NUM_OF_PLAYER_DATA_PATH);
	FXmlFile xmlFile(inFile);
	if (false == xmlFile.IsValid())
	{
		PRINT_ERROR("%s", *xmlFile.GetLastError());
		return;
	}

	FXmlNode* rootNode = xmlFile.GetRootNode();
	if (nullptr == rootNode)
	{
		PRINT_ERROR("RootNode 얻을 수 없습니다. filePath=%s", *inFile);
		return;
	}

	const FXmlNode* childNode = rootNode->GetFirstChildNode();
	while (nullptr != childNode)
	{
		int32 player = FCString::Atoi(*childNode->GetAttribute(TEXT("Player")));
		if (player == playerCount)
		{
			int32 minerCard = FCString::Atoi(*childNode->GetAttribute(TEXT("MinerCard")));
			int32 saboteurCard = FCString::Atoi(*childNode->GetAttribute(TEXT("SaboteurCard")));
			int32 initialHandCard = FCString::Atoi(*childNode->GetAttribute(TEXT("InitialHandCard")));
			if ((0 < minerCard) && (0 < saboteurCard) && (0 < initialHandCard))
			{
				minerCount = minerCard;
				saboteurCount = saboteurCard;
				initialHandCount = initialHandCard;
				
				xmlFile.Clear();
				return;
			}

			xmlFile.Clear();
			PRINT_ERROR("%d 명 게임에서 사용할 카드 갯수가 이상합니다. 광부=%d, 방해꾼=%d, 최초핸드=%d", player, minerCard, saboteurCard, initialHandCard);
			return;
		}

		childNode = childNode->GetNextNode();
	}

	xmlFile.Clear();
	PRINT_ERROR("%d 명 게임의 정보는 존재하지 않습니다.", playerCount);
}