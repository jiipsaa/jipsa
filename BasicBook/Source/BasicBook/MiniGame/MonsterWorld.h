﻿#pragma once

#include "Data/MonsterWorldRanking.h"
#include "Monster.h"
#include "MonsterWorldCanvas.h"
#include "MonsterWorldPlayer.h"

using namespace NSMonsterWorld;

// TODO:
// 키 조합으로 AutoPlay 호출하게 바꾸자.
// AutoPlay 중지하면, 종료조건 아닐때 Play로 바꾸고 수동 진행할 수 있게 변경.
// 결과창 확인 후 Wait 상태로 변경해야함.
// 턴 베이스 버전: 유저가 한 턴 움직이면, 몬스터들도 한 턴 움직인다.
// 실시간 버전: 몬스터들은 계속 턴을 진행하고, 유저는 입력이 있을때만 턴을 진행한다.
// 리얼타임 플레이 -> 유저는 직접입력. 몬스터 자동
// 자동 플레이 -> 유저 자동. 몬스터도 자동. (의미 있나?)
// 턴 베이스 플레이 -> 유저 입력되면 몬스터도 이동.
// 유저 입력을 변수에 넣고(맨처음 입력한걸로 고정? 아님 바꿔줘?), PlayNextTurn 호출될때 넣었던 입력을 가져오고, 변수에 넣던 입력을 비우면.. 몬스터 턴이랑 같이 동작할것 같네. 해보자.
// Player 1p, 2p를 멀티로 구현한다?
// Player 1p, 2p를 로컬로 구현한다?
// 강시는 주기적으로 Horizontal, Vertical 기준을 변경할 수 있게 하자.

enum class eMonsterWorldState : uint8
{
	Wait
	,Ready
	,Play
	,AutoPlay
	,EndPlay
};

inline bool CompareItemCount(Monster* first, Monster* second)
{
	return (second->GetItemCount() < first->GetItemCount());
}

class MonsterWorld
{
private:
	vector<vector<int32>> worldMatrix;
	int32 maxX, maxY;

	vector<Monster*> monsters;
	MonsterWorldPlayer* player;

	const int32 MAX_MOVE_COUNT = 50;
	int32 moveCount;

	MonsterWorldCanvas canvas;

	eMonsterWorldState currentState;

	const UWorld* world;
	FTimerHandle autoPlayTimerHandle;

public:
	MonsterWorld(const UWorld* _world, const int32& width, const int32& height)
		: worldMatrix(height), maxX(width), maxY(height), player(nullptr), canvas(width, height), currentState(eMonsterWorldState::Wait), world(_world)
	{
		for (int y = 0; y < maxY; ++y)
		{
			// TODO: 1이상: 아이템 있음, 0: 아이템 없음 -> enum 같은걸로 변경.
			worldMatrix[y] = vector<int32>(width, 1);
		}

		//for (int y = 0; y < maxY; ++y)
		//{
		//	for (int x = 0; x < maxX; ++x)
		//	{
		//		worldMatrix[y].push_back(1);
		//	}
		//}

		moveCount = 0;
	}
	
	~MonsterWorld()
	{
		if (nullptr != player)
		{
			delete player;
		}

		for (int32 i = 0; i < monsters.size(); ++i)
		{
			delete monsters[i];
		}
	}

	void AddMonster(Monster* monster)
	{
		if (eMonsterWorldState::Wait != currentState)
		{
			delete monster;
			PRINT_WARNING("%s", TEXT("Wait 상태에서만 몬스터 추가가 가능합니다."));
			return;
		}

		monsters.push_back(monster);
	}

	void AddPlayer(MonsterWorldPlayer* _player)
	{
		if (eMonsterWorldState::Wait != currentState)
		{
			delete _player;
			PRINT_WARNING("%s", TEXT("Wait 상태에서만 MonsterWorldPlayer 추가가 가능합니다."));
			return;
		}

		if (nullptr != player)
		{
			delete _player;
			PRINT_WARNING("%s", TEXT("이미 MonsterWorldPlayer 가 추가되어 있습니다."));
			return;
		}

		player = _player;
	}

	void Ready()
	{
		if (eMonsterWorldState::Wait != currentState)
		{
			PRINT_WARNING("%s", TEXT("Wait 상태에서만 레디가 가능합니다."));
			return;
		}

		if (monsters.size() <= 0)
		{
			PRINT_WARNING("%s", TEXT("몬스터가 추가되지 않았습니다. AddMonster()를 호출해 주세요."));
			return;
		}

		currentState = eMonsterWorldState::Ready;

		Print();
		PRINT("엔터를 눌러 플레이하세요..");
	}

	void Play()
	{
		if ((eMonsterWorldState::Ready != currentState) && (eMonsterWorldState::Play != currentState))
		{
			PRINT_WARNING("플레이 할 수 없는 상태입니다. currentState=%d", currentState);
			return;
		}

		if (eMonsterWorldState::Ready == currentState)
		{
			currentState = eMonsterWorldState::Play;
		}

		PlayNextTurn(false);
	}

	void AutoPlay()
	{
		if (eMonsterWorldState::Ready != currentState)
		{
			PRINT_WARNING("Ready 상태가 아닙니다. 오토 플레이 할 수 없습니다. currentState=%d", currentState);
			return;
		}

		if (false == IsValid(world))
		{
			PRINT_WARNING("%s", TEXT("UWorld* 가 비정상입니다. 오토 플레이 할 수 없습니다."));
			return;
		}

		if (eMonsterWorldState::Ready == currentState)
		{
			currentState = eMonsterWorldState::AutoPlay;
		}

		PlayNextTurn(true);
	}

	void PlayNextTurn(const bool& isAutoPlay = false)
	{
		for (int32 i = 0; i < monsters.size(); ++i)
		{
			monsters[i]->Move(worldMatrix, maxX, maxY);
		}
		++moveCount;

		Print();

		if (true == IsDone())
		{
			EndPlay();
			return;
		}

		if (true == isAutoPlay)
		{
			if (false == IsValid(world))
			{
				PRINT_WARNING("%s", TEXT("UWorld* 가 비정상입니다. 자동 진행을 할 수 없습니다."));
				return;
			}

			world->GetTimerManager().SetTimer(
				autoPlayTimerHandle
				,[&]() {
					PlayNextTurn(true);
				}
				,0.2f
				,false
			);
		}
	}

	void Input(const eMonsterWorldDirection& direction)
	{
		if ((eMonsterWorldState::Play != currentState) && (eMonsterWorldState::AutoPlay != currentState))
		{
			PRINT_WARNING("유저가 조작을 할 수 없는 상태입니다. currentState=%d", currentState);
			return;
		}

		if (nullptr == player)
		{
			PRINT_WARNING("%s", TEXT("생성된 MonsterWorldPlayer 가 없습니다."));
			return;
		}

		player->Input(direction, worldMatrix, maxX, maxY);

		Print();

		if (true == IsDone())
		{
			PRINT("유저가 막타~");
			EndPlay();
			return;
		}
	}

private:
	void EndPlay()
	{
		// TODO: 결과창 확인 후 Wait 상태로 변경해야함.
		currentState = eMonsterWorldState::EndPlay;

		if (nullptr != player)
		{
			MonsterWorldRanking ranking;

			//try
			//{
				ranking.Load();
			//}
			//catch (FileException fileException)
			//{
			//	if (true == fileException.isReadMode)
			//	{
			//		PRINT_WARNING("파일 읽기에 실패했습니다. 랭킹을 강제로 초기화 합니다. path=%s", *fileException.filePath);
			//		ranking.Reset();
			//	}
			//	else
			//	{
			//		PRINT_WARNING("파일 읽기를 시도했는데 예외는 쓰기 모드라고 나옵니다. 구현 확인이 필요합니다. path=%s", *fileException.filePath);
			//	}
			//}
			//catch (...)
			//{
			//	PRINT_WARNING("%s", TEXT("알 수 없는 오류가 발생했습니다. 디버깅이 필요합니다."));
			//}

			//try
			//{
				ranking.Add(player->GetItemCount(), player->GetItemCountPerTotalDistance());
				ranking.Save();
			//}
			//catch (FileException fileException)
			//{
			//	if (true == fileException.isReadMode)
			//	{
			//		PRINT_WARNING("파일 쓰기를 시도했는데 예외는 읽기 모드라고 나옵니다. 구현 확인이 필요합니다. path=%s", *fileException.filePath);
			//	}
			//	else
			//	{
			//		PRINT_WARNING("파일 쓰기에 실패했습니다. 랭킹이 저장되지 않습니다. path=%s", *fileException.filePath);
			//	}
			//}
			//catch (...)
			//{
			//	PRINT_WARNING("%s", TEXT("알 수 없는 오류가 발생했습니다. 디버깅이 필요합니다."));
			//}

			ranking.Print();
		}
	}

	int32& Map(const int32& x, const int32& y)
	{
		return worldMatrix[y][x];
	}

	bool IsDone()
	{
		return ((MAX_MOVE_COUNT <= moveCount) || (GetItemCount() <= 0));
	}

	int32 GetItemCount()
	{
		int32 itemCount = 0;
		for (int32 y = 0; y < maxY; ++y)
		{
			itemCount += count(worldMatrix[y].begin(), worldMatrix[y].end(), 1);

			//for (int32 x = 0; x < maxX; ++x)
			//{
			//	if (0 < Map(x, y))
			//	{
			//		++itemCount;
			//	}
			//}
		}
		return itemCount;
	}

	void Print()
	{
		canvas.Clear();

		for (int32 y = 0; y < maxY; ++y)
		{
			for (int32 x = 0; x < maxX; ++x)
			{
				if (0 < Map(x, y))
				{
					canvas.Draw(x, y, 'I');
				}
			}
		}

		for (int32 i = 0; i < monsters.size(); ++i)
		{
			monsters[i]->Draw(canvas);
		}
		if (nullptr != player)
		{
			player->Draw(canvas);
		}

		canvas.Print("[ Monster World (Dynamic World) ]");

		PRINT("전체 이동 횟수 = %d", moveCount);
		PRINT("남은 아이템 수 = %d", GetItemCount());
		
		// 획득한 아이템으로 내림차순 정렬한다.
		sort(monsters.begin(), monsters.end(), CompareItemCount);
		
		int32 playerItemCount = -1;
		if (nullptr != player)
		{
			playerItemCount = player->GetItemCount();
		}

		for (int32 i = 0; i < monsters.size(); ++i)
		{
			// TODO: player가 monsters에 들어가게 구현 안해놔서 sort 한번으로 끝내지 못했다.
			// player를 monsters에 넣자. 몬스터와 유저의 구분 방법은 virtual bool IsPlayer() 를 Monster에 추가하는걸로 해결하자.
			if ((-1 != playerItemCount) && (monsters[i]->GetItemCount() <= playerItemCount))
			{
				if (nullptr != player)
				{
					playerItemCount = -1;
					player->Print();
				}
			}

			monsters[i]->Print();
		}

		if (nullptr != player)
		{
			if (-1 != playerItemCount)
			{
				player->Print();
			}
		}
	}
};