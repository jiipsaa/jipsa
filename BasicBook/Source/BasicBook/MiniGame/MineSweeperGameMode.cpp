﻿#include "MineSweeperGameMode.h"

#include "../Jipsa/Log.h" // test
#include "MiniGameGameInstance.h"

#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"
//#include "UObject/Class.h"

void AMineSweeperGameMode::InitGame(const FString& mapName, const FString& options, FString& errorMessage)
{
	Super::InitGame(mapName, options, errorMessage);

	playerInfoSet.Reset();
}

void AMineSweeperGameMode::PostLogin(APlayerController* newPlayer)
{
	Super::PostLogin(newPlayer);

	UMiniGameGameInstance* gameInstance = Cast<UMiniGameGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	if (nullptr == gameInstance)
	{
		// TODO Level 2: 로비로 튕겨내거나 게임을 종료시켜야 한다.
		PRINT_WARNING("UMiniGameGameInstance 를 얻을 수 없습니다.. %d", MAX_PLAYER_COUNT);
		return;
	}

	//FString temp = StaticEnum<eMiniGamePlayMode>()->GetValueAsString(gameInstance->GetPlayMode());
	//PRINT("현재 게임 모드: %d, %s", gameInstance->GetPlayMode(), *temp);

	// TODO Level 2: assert 추가. eMiniGamePlayMode::MultiGuest 인데 GameMode 에 접근했습니다. 엔진 구조상 불가능합니다. 반드시 클라 로직 확인해야 합니다.
	if (eMiniGamePlayMode::Single == gameInstance->GetPlayMode())
	{
		// TODO Level 2: UI 위치도 조정하고 싶다. 다른 초기화 함수를 사용하자.
		AMineSweeperPlayerController* newPlayerController = Cast<AMineSweeperPlayerController>(newPlayer);
		newPlayerController->ClientInitMap();
	}
	else if (eMiniGamePlayMode::MultiHost == gameInstance->GetPlayMode())
	{
		if (MAX_PLAYER_COUNT <= playerInfoSet.Num())
		{
			// TODO Level 2: PreLogin() 에서, 반려하거나, 관전자로 만들어야 한다.
			PRINT_WARNING("최대 인원수를 넘어섰습니다. %d", MAX_PLAYER_COUNT);
			return;
		}

		AMineSweeperPlayerController* newPlayerController = Cast<AMineSweeperPlayerController>(newPlayer);
		int32 newPlayerID = newPlayerController->PlayerState->GetPlayerId();
	
		AMineSweeperPlayerController* otherPlayerController = nullptr;
		FMineSweeperPlayerInfo otherPlayerInfo;
		for (auto iter = playerInfoSet.CreateIterator(); iter; ++iter)
		{
			otherPlayerInfo = *iter;

			// 새로 들어온 플레이어에게 기존 플레이어가 있음을 알려준다.
			newPlayerController->ClientInitOtherPlayerMap(otherPlayerInfo.playerID);

			// 기존 플레이어에게 새로 들어온 플레이어가 있음을 알려준다.
			otherPlayerController = GetPlayerControllerByPlayerID(otherPlayerInfo.playerID);
			if (nullptr != otherPlayerController)
			{
				otherPlayerController->ClientInitOtherPlayerMap(newPlayerID);
			}
		}

		playerInfoSet.Emplace(FMineSweeperPlayerInfo(newPlayerID, false));
		newPlayerController->ClientInitMap();
	}
}

void AMineSweeperGameMode::StartGame()
{
	// TODO Level 2: TSet에서 has playerID 체크해서 Loop를 한번만 돌게 로직 수정하자.
	AMineSweeperPlayerController* playerController = nullptr;
	FMineSweeperPlayerInfo playerInfo = -1;
	for (auto iter = playerInfoSet.CreateIterator(); iter; ++iter)
	{
		playerInfo = *iter;

		playerController = GetPlayerControllerByPlayerID(playerInfo.playerID);
		if (nullptr != playerController)
		{
			playerController->ClientStartGame();
		}
	}
}

void AMineSweeperGameMode::ShowMineImage(const int32& x, const int32& y, const int32& playerID)
{
	AMineSweeperPlayerController* playerController = nullptr;
	FMineSweeperPlayerInfo playerInfo;
	for (auto iter = playerInfoSet.CreateIterator(); iter; ++iter)
	{
		playerInfo = *iter;

		playerController = GetPlayerControllerByPlayerID(playerInfo.playerID);
		if (nullptr != playerController)
		{
			playerController->ClientShowMineImage(x, y, playerID);
		}
	}
}

void AMineSweeperGameMode::ShowEmptyImage(const int32& x, const int32& y, const int32& nearMineCount, const int32& playerID)
{
	AMineSweeperPlayerController* playerController = nullptr;
	FMineSweeperPlayerInfo playerInfo;
	for (auto iter = playerInfoSet.CreateIterator(); iter; ++iter)
	{
		playerInfo = *iter;

		playerController = GetPlayerControllerByPlayerID(playerInfo.playerID);
		if (nullptr != playerController)
		{
			playerController->ClientShowEmptyImage(x, y, nearMineCount, playerID);
		}
	}
}

void AMineSweeperGameMode::FailGame(const int32& playerID)
{
	int32 failPlayerCount = 0;
	for (auto iter = playerInfoSet.CreateIterator(); iter; ++iter)
	{
		if (playerID == iter->playerID)
		{
			if (true == iter->isFail)
			{
				PRINT_WARNING("이미 폭탄이 터진 플레이어 입니다. FailGame() 실패합니다. playerID=%d", playerID);
				return;
			}

			iter->isFail = true;
		}

		if (true == iter->isFail)
		{
			++failPlayerCount;
		}
	}

	if (playerInfoSet.Num() <= failPlayerCount)
	{// 모든 플레이어가 폭탄을 터트렸다. 비김 판정을 한다.
		AMineSweeperPlayerController* playerController = nullptr;
		for (auto iter = playerInfoSet.CreateIterator(); iter; ++iter)
		{
			playerController = GetPlayerControllerByPlayerID(iter->playerID);
			if (nullptr != playerController)
			{
				playerController->ClientDrawGame();
			}
		}
	}
	else
	{// 누군가의 폭탄이 터졌다는 소식을 전한다.
		AMineSweeperPlayerController* playerController = nullptr;
		for (auto iter = playerInfoSet.CreateIterator(); iter; ++iter)
		{
			playerController = GetPlayerControllerByPlayerID(iter->playerID);
			if (nullptr != playerController)
			{
				playerController->ClientFailGame(playerID);
			}
		}
	}
}

void AMineSweeperGameMode::ClearGame(const int32& playerID)
{
	AMineSweeperPlayerController* playerController = nullptr;
	FMineSweeperPlayerInfo playerInfo;
	for (auto iter = playerInfoSet.CreateIterator(); iter; ++iter)
	{
		playerInfo = *iter;

		playerController = GetPlayerControllerByPlayerID(playerInfo.playerID);
		if (nullptr != playerController)
		{
			playerController->ClientClearGame(playerID);
		}
	}
}

void AMineSweeperGameMode::Logout(AController* exiting)
{
	Super::Logout(exiting);

	AMineSweeperPlayerController* playerController = Cast<AMineSweeperPlayerController>(exiting);
	if (nullptr != playerController)
	{
		if (nullptr != playerController->PlayerState)
		{
			int32 logoutPlayerID = playerController->PlayerState->GetPlayerId();

			FMineSweeperPlayerInfo otherPlayerInfo;
			for (auto iter = playerInfoSet.CreateIterator(); iter; ++iter)
			{
				otherPlayerInfo = *iter;

				if (logoutPlayerID == otherPlayerInfo.playerID)
				{
					iter.RemoveCurrent();
					break;
				}
			}

			for (auto iter = playerInfoSet.CreateIterator(); iter; ++iter)
			{
				otherPlayerInfo = *iter;

				// 기존 플레이어에게 나간 플레이어가 있음을 알려준다.
				playerController = GetPlayerControllerByPlayerID(otherPlayerInfo.playerID);
				if (nullptr != playerController)
				{
					// TODO: 로그아웃시 UI 처리.
					//playerController->ClientLogoutOtherPlayer(logoutPlayerID);
				}
			}
		}
	}
}

AMineSweeperPlayerController* AMineSweeperGameMode::GetPlayerControllerByPlayerID(const int32& playerID)
{
	UWorld* world = GetWorld();
	if (nullptr == world)
	{
		return nullptr;
	}

	APlayerController* playerController = nullptr;
	for (FConstPlayerControllerIterator iter = world->GetPlayerControllerIterator(); iter; ++iter)
	{
		playerController = iter->Get();
		if ((nullptr == playerController) || (nullptr == playerController->PlayerState))
		{
			continue;
		}

		if (true == MustSpectate(playerController))
		{
			continue;
		}

		if (playerID == playerController->PlayerState->GetPlayerId())
		{
			return Cast<AMineSweeperPlayerController>(playerController);
		}
	}

	return nullptr;
}