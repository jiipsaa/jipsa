#pragma once

#define MOST_VALUABLE_PLAY_COUNT 5

struct PuzzlePlayInfo
{
	FString name;
	int32 moveCount;
	double playTime;
};

class PuzzleRanking
{
private:
	PuzzlePlayInfo mostValuablePlayInfo[MOST_VALUABLE_PLAY_COUNT];

public:
	void Load();
	void Save();
	void Print();
	int32 Add(int32 moveCount, double playTime);

private:
	void Init();
	FString GetSubPath();
};