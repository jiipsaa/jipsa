﻿#pragma once

struct FileException
{
	FString filePath;
	bool isReadMode;
	FileException(const FString& _filePath, const bool& _isReadMode) : filePath(_filePath), isReadMode(_isReadMode) {}
};