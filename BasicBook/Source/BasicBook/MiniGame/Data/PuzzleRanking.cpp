﻿#include "PuzzleRanking.h"
#include "../../Jipsa/Log.h"

void PuzzleRanking::Load()
{
	FString filePath = FString::Printf(TEXT("%s%s"), *FPaths::ProjectContentDir(), *GetSubPath());
	FArchive* fileReader = IFileManager::Get().CreateFileReader(*filePath);
	if (NULL != fileReader)
	{
		for (int32 i = 0; i < MOST_VALUABLE_PLAY_COUNT; ++i)
		{
			*fileReader << mostValuablePlayInfo[i].name << mostValuablePlayInfo[i].moveCount << mostValuablePlayInfo[i].playTime;
		}
		fileReader->Close();

		PRINT("Success read");
	}
	else
	{
		Init();

		PRINT("Fail read -> init ranking");
	}
}

void PuzzleRanking::Save()
{
	FString filePath = FString::Printf(TEXT("%s%s"), *FPaths::ProjectContentDir(), *GetSubPath());
	FArchive* fileWriter = IFileManager::Get().CreateFileWriter(*filePath);
	if (NULL != fileWriter)
	{
		for (int32 i = 0; i < MOST_VALUABLE_PLAY_COUNT; ++i)
		{
			// FString int32 double
			*fileWriter << mostValuablePlayInfo[i].name << mostValuablePlayInfo[i].moveCount << mostValuablePlayInfo[i].playTime;
		}
		fileWriter->Close();

		PRINT("Success write");
	}
	else
	{
		PRINT("Fail write");
	}
}

void PuzzleRanking::Print()
{
	FString log;
	for (int32 i = 0; i < MOST_VALUABLE_PLAY_COUNT; ++i)
	{
		log.Appendf(TEXT("[%2d위] %4d %-16s %5.1lf\n"), i + 1, mostValuablePlayInfo[i].moveCount, *mostValuablePlayInfo[i].name, mostValuablePlayInfo[i].playTime);
	}
	PRINT("%s", *log);
}

int32 PuzzleRanking::Add(int32 moveCount, double playTime)
{
	if (moveCount < mostValuablePlayInfo[MOST_VALUABLE_PLAY_COUNT - 1].moveCount)
	{
		int32 index = MOST_VALUABLE_PLAY_COUNT - 1;
		for (; 0 < index; --index)
		{
			if (mostValuablePlayInfo[index - 1].moveCount <= moveCount)
			{
				break;
			}
			mostValuablePlayInfo[index] = mostValuablePlayInfo[index - 1];
		}

		mostValuablePlayInfo[index].name = TEXT("이름을 입력해주세요.");
		mostValuablePlayInfo[index].moveCount = moveCount;
		mostValuablePlayInfo[index].playTime = playTime;
		return index + 1;
	}

	return 0;
}

void PuzzleRanking::Init()
{
	PuzzlePlayInfo defaultPlayInfo;
	defaultPlayInfo.name = TEXT("알 수 없는 플레이어");
	defaultPlayInfo.moveCount = 1000;
	defaultPlayInfo.playTime = 1000.0;

	for (int32 i = 0; i < MOST_VALUABLE_PLAY_COUNT; ++i)
	{
		mostValuablePlayInfo[i] = defaultPlayInfo;
	}
}

FString PuzzleRanking::GetSubPath()
{
	return UTF8_TO_TCHAR("Data/PuzzleRanking.txt");
}