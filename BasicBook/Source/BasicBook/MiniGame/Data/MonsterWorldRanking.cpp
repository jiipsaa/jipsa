﻿#include "MonsterWorldRanking.h"
using namespace NSMonsterWorld;

#include "../../Jipsa/Log.h"

void MonsterWorldRanking::Load()
{
	FString filePath = FString::Printf(TEXT("%s%s"), *FPaths::ProjectContentDir(), *GetSubPath());
	FArchive* fileReader = IFileManager::Get().CreateFileReader(*filePath);
	if (NULL == fileReader)
	{
		//throw(FileException(filePath, true));
		return;
	}

	for (int32 i = 0; i < MOST_VALUABLE_PLAY_COUNT; ++i)
	{
		*fileReader << mostValuablePlayInfo[i].name << mostValuablePlayInfo[i].itemCount << mostValuablePlayInfo[i].itemCountPerTotalDistance;
	}
	fileReader->Close();
}

void MonsterWorldRanking::Save()
{
	FString filePath = FString::Printf(TEXT("%s%s"), *FPaths::ProjectContentDir(), *GetSubPath());
	FArchive* fileWriter = IFileManager::Get().CreateFileWriter(*filePath);
	if (NULL == fileWriter)
	{
		//throw(FileException(filePath, false));
		return;
	}
	
	for (int32 i = 0; i < MOST_VALUABLE_PLAY_COUNT; ++i)
	{
		*fileWriter << mostValuablePlayInfo[i].name << mostValuablePlayInfo[i].itemCount << mostValuablePlayInfo[i].itemCountPerTotalDistance;
	}
	fileWriter->Close();
}

int32 MonsterWorldRanking::Add(const int32& itemCount, const float& itemCountPerTotalDistance)
{
	if (mostValuablePlayInfo[MOST_VALUABLE_PLAY_COUNT - 1].itemCount < itemCount)
	{
		int32 index = MOST_VALUABLE_PLAY_COUNT - 1;
		for (; 0 < index; --index)
		{
			if (itemCount <= mostValuablePlayInfo[index - 1].itemCount)
			{
				break;
			}
			mostValuablePlayInfo[index] = mostValuablePlayInfo[index - 1];
		}

		mostValuablePlayInfo[index].name = TEXT("이름을 입력해주세요.");
		mostValuablePlayInfo[index].itemCount = itemCount;
		mostValuablePlayInfo[index].itemCountPerTotalDistance = itemCountPerTotalDistance;
		return index + 1;
	}

	return 0;
}

void MonsterWorldRanking::Reset()
{
	for (int32 i = 0; i < MOST_VALUABLE_PLAY_COUNT; ++i)
	{
		mostValuablePlayInfo[i] = PlayInfo();
	}
}

void MonsterWorldRanking::Print()
{
	FString log;
	for (int32 i = 0; i < MOST_VALUABLE_PLAY_COUNT; ++i)
	{
		log.Appendf(TEXT("[%2d위] %4d %-16s %5.1f\n"), i + 1, mostValuablePlayInfo[i].itemCount, *mostValuablePlayInfo[i].name, mostValuablePlayInfo[i].itemCountPerTotalDistance);
	}
	PRINT("%s", *log);
}

FString MonsterWorldRanking::GetSubPath()
{
	return UTF8_TO_TCHAR("Data/MonsterWorldRanking.txt");
}