﻿#pragma once

#include "FileException.h"

#define MOST_VALUABLE_PLAY_COUNT 5

namespace NSMonsterWorld
{
	struct PlayInfo
	{
		FString name;
		int32 itemCount;
		float itemCountPerTotalDistance;

		PlayInfo(const FString& _name = TEXT("알 수 없는 유저"), const int32& _itemCount = 0, const float& _itemCountPerTotalDistance = 0.0f)
			: name(_name), itemCount(_itemCount), itemCountPerTotalDistance(_itemCountPerTotalDistance) {}
	};

	class MonsterWorldRanking
	{
	private:
		PlayInfo mostValuablePlayInfo[MOST_VALUABLE_PLAY_COUNT];

	public:
		void Load();
		void Save();		
		int32 Add(const int32& itemCount, const float& itemCountPerTotalDistance);
		
		void Reset();
		void Print();

	private:
		FString GetSubPath();
	};
}