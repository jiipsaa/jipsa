﻿#pragma once

#include "Misc/FileHelper.h"

class HangmanProgress
{
public:
	bool Load(TArray<FString>& progressStrings, const int32& totalProgressLineCount)
	{
		progressStrings.Empty();

		FString filePath = FString::Printf(TEXT("%s%s"), *FPaths::ProjectContentDir(), *GetSubPath());
		bool isLoaded = FFileHelper::LoadANSITextFileToStrings(*filePath, NULL, progressStrings);
		if (true == isLoaded)
		{
			int32 lineCount = progressStrings.Num();
			if (totalProgressLineCount != lineCount)
			{
				PRINT("Fail read.. 총 %d줄 이어야 합니다. (현재 %d줄)", totalProgressLineCount, lineCount);
				return false;
			}

			//// 한 줄을 띄어쓰기로 파싱해서 값을 얻는 예시.
			//TArray<FString> parsedWords;
			//outStrings[0].ParseIntoArray(parsedWords, TEXT(" "), true);
			//if (1 < parsedWords.Num())
			//{
			//	int32 firstNum = FCString::Atoi(*parsedWords[0]);
			//	int32 secondNum = FCString::Atoi(*parsedWords[1]);
			//}

			PRINT("Success read. lineCount=%d", lineCount);
			return true;
		}
		else
		{
			PRINT("Fail read");
			return false;
		}
	}

private:
	FString GetSubPath()
	{
		return UTF8_TO_TCHAR("Data/HangmanProgress.txt");
	}
};