﻿#pragma once

#include "MonsterWorldCanvas.h"
#include "MonsterWorldPoint.h"

#define MONSTER_WORLD_DIM 40 // Dimension

class Monster
{
protected:
	string name;
	char icon;

	int32 itemCount;

	MonsterWorldPoint prevPos, currPos;
	float totalDistance;
	float distance;

	const float DISTANCE_INTERVAL = 20.0f;
	const int32 MAX_SLEEP_COUNT = 10;
	int32 remainSleepCount = 0;

public:
	Monster(const string& _name = "괴물", const char& _icon = 'M', const int32& _x = 0, const int32& _y = 0)
		: name(_name), icon(_icon), itemCount(0), prevPos(_x, _y), currPos(_x, _y), totalDistance(0.0f), distance(0.0f), remainSleepCount(0) {}
	
	virtual ~Monster()
	{
		PRINT("[%c]%s 물러갑니다.", icon, UTF8_TO_TCHAR(name.c_str()));
	}

	void Draw(MonsterWorldCanvas& canvas)
	{
		canvas.Draw(currPos.x, currPos.y, icon);
	}

	//void Move(int32 (&map)[MONSTER_WORLD_DIM][MONSTER_WORLD_DIM], const int32& maxX, const int32& maxY)
	virtual void Move(vector<vector<int32>>& map, const int32& maxX, const int32& maxY)
	{
		if (true == IsSteelSleeping())
		{
			return;
		}

		int32 randomDirection = FMath::RandRange(1, 8);
		switch (randomDirection)
		{
		case 1:
			--currPos.y;
			break;
		case 2:
			++currPos.x; --currPos.y;
			break;
		case 3:
			++currPos.x;
			break;
		case 4:
			++currPos.x; ++currPos.y;
			break;
		case 5:
			++currPos.y;
			break;
		case 6:
			--currPos.x; ++currPos.y;
			break;
		case 7:
			--currPos.x;
			break;
		case 8:
			--currPos.x; --currPos.y;
			break;
		default:
			PRINT_WARNING("잘못된 방향입니다. 디버깅 필요합니다. randomDirection=%d", randomDirection);
			break;
		}

		Clip(maxX, maxY);
		Eat(map);
	}

	void Print()
	{
		PRINT("[%c]%s: item=%d, remainSleepCount=%d", icon, UTF8_TO_TCHAR(name.c_str()), itemCount, remainSleepCount);
	}

	int32 GetItemCount()
	{
		return itemCount;
	}

	float GetItemCountPerTotalDistance()
	{
		return itemCount / totalDistance;
	}

protected:
	void Clip(const int32& maxX, const int32& maxY)
	{
		currPos(maxX, maxY);
		//currPos.Clip(maxX, maxY);
	}

	//void Eat(int32 (&map)[MONSTER_WORLD_DIM][MONSTER_WORLD_DIM])
	void Eat(vector<vector<int32>>& map)
	{
		// TODO: 1이상: 아이템 있음, 0: 아이템 없음 -> enum 같은걸로 변경.
		if (1 <= map[currPos.y][currPos.x])
		{
			map[currPos.y][currPos.x] = 0;
			++itemCount;
		}

		float hypotenuse = (float)(currPos - prevPos);
		//float hypotenuse = (currPos - prevPos).GetHypotenuse();

		distance += hypotenuse;;
		totalDistance += hypotenuse;

		prevPos = currPos;

		if (DISTANCE_INTERVAL < distance)
		{
			distance = 0;
			remainSleepCount = MAX_SLEEP_COUNT;
		}
	}

	bool IsSteelSleeping()
	{
		if (0 < remainSleepCount)
		{
			--remainSleepCount;
			return true;
		}
		return false;
	}
};