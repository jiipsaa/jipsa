﻿#pragma once

#include "Monster.h"

enum class eMonsterWorldDirection : uint8
{
	Left
	,Right
	,Up
	,Down
	,Count
};

class MonsterWorldPlayer : public Monster
{
protected:

public:
	MonsterWorldPlayer(const string& _name = "유저", const char& _icon = 'U', const int32& _x = 0, const int32& _y = 0)
		: Monster(_name, _icon, _x, _y) {}

	void Input(const eMonsterWorldDirection& direction, vector<vector<int32>>& map, const int32& maxX, const int32& maxY)
	{
		if (eMonsterWorldDirection::Left == direction)
		{
			if (0 < currPos.x)
			{
				--currPos.x;
			}
		}
		else if (eMonsterWorldDirection::Right == direction)
		{
			if (currPos.x < (maxX - 1))
			{
				++currPos.x;
			}
		}
		else if (eMonsterWorldDirection::Up == direction)
		{
			if (0 < currPos.y)
			{
				--currPos.y;
			}
		}
		else if (eMonsterWorldDirection::Down == direction)
		{
			if (currPos.y < (maxY - 1))
			{
				++currPos.y;
			}
		}
		else
		{
			PRINT_WARNING("%s", TEXT("처리 할 수 없는 입력입니다. eMonsterWorldDirection=%d"), direction);
			return;
		}

		Move(map, maxX, maxY);
	}

protected:
	void Move(vector<vector<int32>>& map, const int32& maxX, const int32& maxY)
	{
		Clip(maxX, maxY);
		Eat(map);
	}
};