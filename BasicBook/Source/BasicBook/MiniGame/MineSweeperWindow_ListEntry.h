#pragma once

#include "CoreMinimal.h"
#include "Blueprint/IUserObjectListEntry.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "MineSweeperWindow_ListEntry.generated.h"

UCLASS()
class BASICBOOK_API UMineSweeperWindow_ListEntry : public UUserWidget, public IUserObjectListEntry
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* nearMineCountText;

	UPROPERTY(meta = (BindWidget))
	class UImage* flagImage;

	UPROPERTY(meta = (BindWidget))
	class UImage* mineImage;

	UPROPERTY(meta = (BindWidget))
	class UImage* emptyImage;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UButton* button;

public:
	void Reset();
	void SetEnableTouch(const bool& isEnable);
	void ShowMineImage();
	void ShowEmptyImage(const int32& nearMineCount);

protected:
	void NativeOnListItemObjectSet(UObject* ListItemObject) override;

	UFUNCTION(BlueprintCallable, Category = "ToClient")
	void ToClient_HoldLeftMouseButton();

	UFUNCTION(BlueprintCallable, Category = "ToClient")
	void ToClient_ClickLeftMouseButton();
};