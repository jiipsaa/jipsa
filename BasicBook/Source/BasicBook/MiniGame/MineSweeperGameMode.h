﻿#pragma once

#include "MineSweeperPlayerController.h"
#include "MineSweeperWindow.h"

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MineSweeperGameMode.generated.h"

// TODO:
// 가로 세로 갯수를 변경할 수 있도록 수정하고 싶다.
// - 가로 세로 변경할 수 있도록 옵션팝업 추가.
// - UMineSweeperWindow::Init() 에서,
// -> width, height 변경되면 목록 더 만들거나 지워야 하는지 확인.
// -> tileView 의 크기도 조정 필요.
// - MAX_MINE_COUNT는 가로*세로의 일정 비율로 조정한다. 난이도 있으면 비율도 변경한다.

USTRUCT()
struct FMineSweeperPlayerInfo
{
	GENERATED_BODY()

	int32 playerID;
	bool isFail;

	FMineSweeperPlayerInfo(int32 _playerID = -1, bool _isFail = false) : playerID(_playerID), isFail(_isFail) {}

	//bool operator==(const FMineSweeperPlayerInfo& RHS)
	//{
	//	return (playerID == RHS.playerID);
	//}

	friend bool operator==(const FMineSweeperPlayerInfo& LHS, const FMineSweeperPlayerInfo& RHS)
	{
		return (LHS.playerID == RHS.playerID);
	}

	friend uint32 GetTypeHash(const FMineSweeperPlayerInfo& RHS)
	{
		return GetTypeHash(RHS.playerID);
	}
};

UCLASS()
class BASICBOOK_API AMineSweeperGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	const int32 MAX_PLAYER_COUNT = 3;
	TSet<FMineSweeperPlayerInfo> playerInfoSet;

public:
	void InitGame(const FString& mapName, const FString& options, FString& errorMessage) override;
	void PostLogin(APlayerController* newPlayer) override;
	void StartGame();
	void ShowMineImage(const int32& x, const int32& y, const int32& playerID);
	void ShowEmptyImage(const int32& x, const int32& y, const int32& nearMineCount, const int32& playerID);
	void FailGame(const int32& playerID);
	void ClearGame(const int32& playerID);
	void Logout(AController* exiting) override;

protected:
	AMineSweeperPlayerController* GetPlayerControllerByPlayerID(int32 const& playerID);
};