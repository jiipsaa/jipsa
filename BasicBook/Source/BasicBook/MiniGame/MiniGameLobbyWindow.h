﻿#pragma once

#include "../Jipsa/LobbyWindow.h"
#include "MiniGameEnum.h"

#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"
#include "Components/WrapBox.h"
#include "HttpModule.h"

#include "MiniGameLobbyWindow.generated.h"

UCLASS()
class BASICBOOK_API UMiniGameLobbyWindow : public ULobbyWindow
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta=(BindWidget))
	UButton* singleBtn;

	UPROPERTY(meta=(BindWidget))
	UButton* hostBtn;

	UPROPERTY(meta=(BindWidget))
	UButton* guestBtn;

	UPROPERTY(meta=(BindWidget))
	UTextBlock* myIPText;

	UPROPERTY(meta=(BindWidget))
	UEditableTextBox* friendIPEditableTextBox;

	UPROPERTY(meta = (BindWidget))
	UButton* joinBtn;

	UPROPERTY(meta = (BindWidget))
	UWrapBox* enterBtnWrapBox;

	UPROPERTY(BlueprintReadOnly, meta=(BindWidget))
	UTextBlock* guideText;

	UPROPERTY(meta = (BindWidget))
	UButton* refreshExternalIPBtn;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UButton* destroySessionBtn;

public:
	void NativeConstruct();

protected:
	void ToClient_AsyncLoadMap(const TSoftObjectPtr<UWorld> map);

	UFUNCTION(BlueprintImplementableEvent, Category="FromClient")
	void FromClient_TryCreateSession(const TSoftObjectPtr<UWorld>& map);

	UFUNCTION(BlueprintImplementableEvent, Category = "FromClient")
	void FromClient_TryJoinSession(const FString& ipAddress);

	UFUNCTION(BlueprintImplementableEvent, Category = "FromClient")
	void FromClient_TryDestroySession();

private:
	void SetPlayMode(const eMiniGamePlayMode& _playMode);
	UFUNCTION() void SetPlayModeToSingle();
	UFUNCTION() void SetPlayModeToMultiHost();
	UFUNCTION() void SetPlayModeToMultiGuest();

	UFUNCTION() void JoinSessionByIPAddress();
	UFUNCTION() void DestroySession();

	UFUNCTION() void RequestExternalIPAddress();
	void ResponseExternalIPAddress(FHttpRequestPtr request, FHttpResponsePtr response, bool bConnectedSuccessfully);
};