﻿#pragma once

#include "../Jipsa/Log.h"

class UpAndDownGame
{
public:

	// TODO:
	// 입력시 비번처럼 표시될 수 있게 하자.
	// 랜덤 Gen도 가능하게 하자.
	// 이거 멀티로직 붙여서 테스트하기 좋은 볼륨이네.
	// 예측값 입력시 갱신된 min, max로 검사.
	// 중간에 점수 갱신.

	void Play(int32 answer)
	{
		int32 min = 1, max = 99;
		if (answer < min)
		{
			PRINT_WARNING("%d 는 최소값보다 작습니다. %d 로 변환합니다.", answer, min);
			answer = min;
		}
		else if (max < answer)
		{
			PRINT_WARNING("%d 는 최대값보다 큽니다. %d 로 변환합니다.", answer, max);
			answer = max;
		}

		int32 i;
		int32 predictedValue;
		const int32 LIMIT_COUNT = 10;
		for (i = 0; i < LIMIT_COUNT; ++i)
		{
			predictedValue = FMath::RandRange(min, max);
			PRINT("[%2d회] %2d ~ %2d 사이의 값 예측 => %d", i + 1, min, max, predictedValue);

			if (predictedValue == answer)
			{
				break;
			}
			else if (predictedValue < answer)
			{
				PRINT("더 큰 숫자입니다!");
				min = predictedValue + 1;
			}
			else
			{
				PRINT("더 작은 숫자입니다!");
				max = predictedValue - 1;
			}
		}

		PRINT("\n%s !!! 정답은 %d", (predictedValue == answer) ? TEXT("성공") : TEXT("실패"), answer);
		PRINT("최종 점수 = %d", 10 * (LIMIT_COUNT - i));
	}
};