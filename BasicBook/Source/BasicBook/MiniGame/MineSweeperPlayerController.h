#pragma once

#include "MineSweeperWindow.h"

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "MineSweeperPlayerController.generated.h"

#define WIDTH 9
#define HEIGHT 9
#define MAX_OTHER_PLAYER_COUNT 2

UENUM()
enum class ePlayMapState : uint8
{
	Unkown
	,Empty
	,Mine
	,Flag
};

UCLASS()
class BASICBOOK_API AMineSweeperPlayerController : public APlayerController
{
	GENERATED_BODY()

protected:
	const int32 MAX_MINE_COUNT = 10;

	const int32 EMPTY_AROUND_TILE = 0;
	const int32 MINE_TILE = 9;
	int32 map[HEIGHT][WIDTH];

	ePlayMapState playMap[HEIGHT][WIDTH];

	int32 otherPlayerIDList[MAX_OTHER_PLAYER_COUNT];
	ePlayMapState otherPlayMap[MAX_OTHER_PLAYER_COUNT][HEIGHT][WIDTH];

	bool isPlay = false;

	class UMineSweeperWindow* window = nullptr;

public:
	AMineSweeperPlayerController(const FObjectInitializer& objectInitializer = FObjectInitializer::Get());

	UFUNCTION(Client, Reliable)
	void ClientInitMap();

	UFUNCTION(Client, Reliable)
	void ClientInitOtherPlayerMap(int32 const& playerID);

	UFUNCTION(Server, Reliable)
	void ServerStartGame();

	UFUNCTION(Client, Reliable)
	void ClientStartGame();

	UFUNCTION()
	void Dig(const int32& x, const int32& y, const bool isNested = false);

	UFUNCTION(Server, Reliable)
	void ServerShowMineImage(const int32& x, const int32& y, const int32& playerID);

	UFUNCTION(Client, Reliable)
	void ClientShowMineImage(const int32& x, const int32& y, const int32& playerID);

	UFUNCTION(Server, Reliable)
	void ServerShowEmptyImage(const int32& x, const int32& y, const int32& nearMineCount, const int32& playerID);

	UFUNCTION(Client, Reliable)
	void ClientShowEmptyImage(const int32& x, const int32& y, const int32& nearMineCount, const int32& playerID);
	
	UFUNCTION()
	ePlayMapState Flag(const int32& x, const int32& y);

	UFUNCTION(Server, Reliable)
	void ServerFailGame(const int32& playerID);

	UFUNCTION(Client, Reliable)
	void ClientFailGame(const int32& playerID);

	UFUNCTION(Client, Reliable)
	void ClientDrawGame();

	UFUNCTION(Server, Reliable)
	void ServerClearGame(const int32& playerID);

	UFUNCTION(Client, Reliable)
	void ClientClearGame(const int32& playerID);

private:
	bool IsValidPosition(const int32& x, const int32& y);
	void CheckEndPlay();
};
