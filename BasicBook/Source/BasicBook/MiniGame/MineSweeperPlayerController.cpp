﻿#include "MineSweeperPlayerController.h"

#include "../Jipsa/Log.h"
#include "../Jipsa/PopupManager.h"
#include "MineSweeperGameMode.h"

#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"

AMineSweeperPlayerController::AMineSweeperPlayerController(const FObjectInitializer& objectInitializer)
	: APlayerController(objectInitializer)
{
	for (int32 i = 0; i < MAX_OTHER_PLAYER_COUNT; ++i)
	{
		otherPlayerIDList[i] = -1;
	}
}

void AMineSweeperPlayerController::ClientInitMap_Implementation()
{
	isPlay = false;

	for (int32 y = 0; y < HEIGHT; ++y)
	{
		for (int32 x = 0; x < WIDTH; ++x)
		{
			map[y][x] = EMPTY_AROUND_TILE;
			playMap[y][x] = ePlayMapState::Unkown;
		}
	}

	int32 randomX, randomY;
	for (int32 i = 0; i < MAX_MINE_COUNT; ++i)
	{
		do
		{
			randomX = FMath::RandRange(0, WIDTH - 1);
			randomY = FMath::RandRange(0, HEIGHT - 1);
		} while (MINE_TILE == map[randomY][randomX]);

		map[randomY][randomX] = MINE_TILE;
	}

	//{// NOTE: TEST 로직.
	//	for (int32 y = 0; y < HEIGHT; ++y)
	//	{
	//		for (int32 x = 0; x < WIDTH; ++x)
	//		{
	//			map[y][x] = EMPTY_AROUND_TILE;
	//		}
	//	}
	//	map[2][1] = MINE_TILE;
	//	map[2][2] = MINE_TILE;
	//}

	for (int32 y = 0; y < HEIGHT; ++y)
	{
		for (int32 x = 0; x < WIDTH; ++x)
		{
			if (MINE_TILE != map[y][x])
			{
				int32 nearMineCount = 0;
				for (int32 yy = y - 1; yy <= y + 1; ++yy)
				{
					for (int32 xx = x - 1; xx <= x + 1; ++xx)
					{
						if ((true == IsValidPosition(xx, yy)) && (MINE_TILE == map[yy][xx]))
						{
							++nearMineCount;
						}
					}
				}
				map[y][x] = nearMineCount;
			}
		}
	}

	if (nullptr == window)
	{
		UUserWidget* userWidget = PopupManager::GetSingleton()->OpenWindow(GetWorld(), "MineSweeperWindowBP");
		if (nullptr == userWidget)
		{
			return;
		}

		window = Cast<UMineSweeperWindow>(userWidget);
	}
	window->Init(WIDTH, HEIGHT);
	window->SetMessage("지뢰 찾기");

	isPlay = true;
}

void AMineSweeperPlayerController::ClientInitOtherPlayerMap_Implementation(int32 const& playerID)
{
	int32 index = -1;
	for (int32 i = 0; i < MAX_OTHER_PLAYER_COUNT; ++i)
	{
		if (-1 == otherPlayerIDList[i])
		{
			index = i;
			break;
		}
	}

	if (-1 == index)
	{
		PRINT_WARNING("다른 플레이어의 맵을 초기화 할 수 없습니다. 이미 2명분의 맵이 셋팅됬습니다. otherPlayerID=%d", playerID);
		if (nullptr != window)
		{
			window->SetMessage("다른 플레이어의 맵을 초기화 할 수 없습니다. 이미 2명분의 맵이 셋팅됬습니다.");
		}
		return;
	}

	otherPlayerIDList[index] = playerID;

	for (int32 y = 0; y < HEIGHT; ++y)
	{
		for (int32 x = 0; x < WIDTH; ++x)
		{
			otherPlayMap[index][y][x] = ePlayMapState::Unkown;
		}
	}

	if (nullptr == window)
	{
		UUserWidget* userWidget = PopupManager::GetSingleton()->OpenWindow(GetWorld(), "MineSweeperWindowBP");
		if (nullptr == userWidget)
		{
			return;
		}

		window = Cast<UMineSweeperWindow>(userWidget);
	}
	window->InitOtherTileView(WIDTH, HEIGHT, index);
}

void AMineSweeperPlayerController::ServerStartGame_Implementation()
{
	// TODO Level 2: assert로 바꾸기.
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ServerStarGame() 호출할 수 없습니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	AMineSweeperGameMode* gameMode = Cast<AMineSweeperGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("AMineSweeperGameMode 를 얻을 수 없습니다. 게임을 시작할 수 없습니다."));
		return;
	}

	gameMode->StartGame();
}

void AMineSweeperPlayerController::ClientStartGame_Implementation()
{
	// TODO Level 2: 함수로 바꾸기
	if (nullptr == window)
	{
		UUserWidget* userWidget = PopupManager::GetSingleton()->OpenWindow(GetWorld(), "MineSweeperWindowBP");
		if (nullptr == userWidget)
		{
			return;
		}

		window = Cast<UMineSweeperWindow>(userWidget);
	}
	
	window->SetEnableTouch(true);
	
	if (true == HasAuthority())
	{
		window->SetEnableStartBtn(false);
		window->SetEnableRestartBtn(true);
	}
}

void AMineSweeperPlayerController::Dig(const int32& x, const int32& y, const bool isNested)
{
	//PRINT("Dig (%d, %d), %d", x, y, (x + y * HEIGHT));

	if (false == isPlay)
	{
		return;
	}

	if (false == isNested)
	{
		if (false == IsValidPosition(x, y))
		{
			PRINT_WARNING("잘못된 위치입니다. Dig() 실패합니다. x=%d, y=%d", x, y);
			return;
		}

		if (ePlayMapState::Unkown != playMap[y][x])
		{
			return;
		}
	}

	if (MINE_TILE == map[y][x])
	{
		playMap[y][x] = ePlayMapState::Mine;
		//window->ShowMineImage(x + y * HEIGHT);
		//PRINT("지뢰 (%d, %d), %d", x, y, (x + y * HEIGHT));

		ServerShowMineImage(x, y, PlayerState->GetPlayerId());
	}
	else if (EMPTY_AROUND_TILE == map[y][x])
	{
		for (int32 yy = y - 1; yy <= y + 1; ++yy)
		{
			for (int32 xx = x - 1; xx <= x + 1; ++xx)
			{
				if (true == IsValidPosition(xx, yy))
				{
					if (ePlayMapState::Empty != playMap[yy][xx])
					{
						playMap[yy][xx] = ePlayMapState::Empty;
						//window->ShowEmptyImage(xx + yy * HEIGHT, map[yy][xx]);
						//PRINT("빈 칸(1) (%d, %d), %d", xx, yy, (xx + yy * HEIGHT));

						ServerShowEmptyImage(xx, yy, map[yy][xx], PlayerState->GetPlayerId());

						//PRINT("재귀 조건 xx=%d, x=%d, yy=%d, y=%d, map=%d", xx, x, yy, y, map[yy][xx]);
						if (EMPTY_AROUND_TILE == map[yy][xx])
						{
							if ((xx != x) || (yy != y))
							{
								Dig(xx, yy, true);
							}
						}
					}
				}
			}
		}
	}
	else
	{
		playMap[y][x] = ePlayMapState::Empty;
		//window->ShowEmptyImage(x + y * HEIGHT, map[y][x]);
		//PRINT("빈 칸(2) (%d, %d), %d", x, y, (x + y * HEIGHT));

		ServerShowEmptyImage(x, y, map[y][x], PlayerState->GetPlayerId());
	}

	if (false == isNested)
	{
		CheckEndPlay();
	}
}

void AMineSweeperPlayerController::ServerShowMineImage_Implementation(const int32& x, const int32& y, const int32& playerID)
{
	// TODO Level 2: assert로 바꾸기.
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ServerShowMineImage() 호출할 수 없습니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	AMineSweeperGameMode* gameMode = Cast<AMineSweeperGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("AMineSweeperGameMode 를 얻을 수 없습니다. ServerShowMineImage() 실패합니다."));
		return;
	}

	gameMode->ShowMineImage(x, y, playerID);
}

void AMineSweeperPlayerController::ClientShowMineImage_Implementation(const int32& x, const int32& y, const int32& playerID)
{
	if (nullptr == window)
	{
		return;
	}

	if (playerID == PlayerState->GetPlayerId())
	{
		window->ShowMineImage(x + y * HEIGHT);
	}
	else
	{
		int32 index = -1;
		for (int32 i = 0; i < MAX_OTHER_PLAYER_COUNT; ++i)
		{
			if (playerID == otherPlayerIDList[i])
			{
				index = i;
				break;
			}
		}

		if (-1 == index)
		{
			PRINT_WARNING("다른 플레이어의 맵을 찾을 수 없습니다. ClientShowMineImage() 실패합니다. otherPlayerID=%d", playerID);
			return;
		}

		otherPlayMap[index][y][x] = ePlayMapState::Mine;
		window->ShowOtherMineImage(x + y * HEIGHT, index);
	}
}

void AMineSweeperPlayerController::ServerShowEmptyImage_Implementation(const int32& x, const int32& y, const int32& nearMineCount, const int32& playerID)
{
	// TODO Level 2: assert로 바꾸기.
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ServerShowEmptyImage() 호출할 수 없습니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	AMineSweeperGameMode* gameMode = Cast<AMineSweeperGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("AMineSweeperGameMode 를 얻을 수 없습니다. ServerShowEmptyImage() 실패합니다."));
		return;
	}

	gameMode->ShowEmptyImage(x, y, nearMineCount, playerID);
}

void AMineSweeperPlayerController::ClientShowEmptyImage_Implementation(const int32& x, const int32& y, const int32& nearMineCount, const int32& playerID)
{
	if (nullptr == window)
	{
		return;
	}

	if (playerID == PlayerState->GetPlayerId())
	{
		window->ShowEmptyImage(x + y * HEIGHT, nearMineCount);
	}
	else
	{
		int32 index = -1;
		for (int32 i = 0; i < MAX_OTHER_PLAYER_COUNT; ++i)
		{
			if (playerID == otherPlayerIDList[i])
			{
				index = i;
				break;
			}
		}

		if (-1 == index)
		{
			PRINT_WARNING("다른 플레이어의 맵을 찾을 수 없습니다. ClientShowEmptyImage() 실패합니다. otherPlayerID=%d", playerID);
			return;
		}

		otherPlayMap[index][y][x] = ePlayMapState::Empty;
		window->ShowOtherEmptyImage(x + y * HEIGHT, nearMineCount, index);
	}
}

ePlayMapState AMineSweeperPlayerController::Flag(const int32& x, const int32& y)
{
	if (false == IsValidPosition(x, y))
	{
		PRINT_WARNING("잘못된 위치입니다. Flag() 실패합니다. x=%d, y=%d", x, y);
		return ePlayMapState::Unkown;
	}

	if (false == isPlay)
	{
		return playMap[y][x];
	}

	if (ePlayMapState::Unkown == playMap[y][x])
	{
		playMap[y][x] = ePlayMapState::Flag;
	}
	else if (ePlayMapState::Flag == playMap[y][x])
	{
		playMap[y][x] = ePlayMapState::Unkown;
	}

	return playMap[y][x];
}

void AMineSweeperPlayerController::ServerFailGame_Implementation(const int32& playerID)
{
	// TODO Level 2: assert로 바꾸기.
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ServerFailGame() 호출할 수 없습니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	AMineSweeperGameMode* gameMode = Cast<AMineSweeperGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("AMineSweeperGameMode 를 얻을 수 없습니다. ServerFailGame() 실패합니다."));
		return;
	}

	gameMode->FailGame(playerID);
}

void AMineSweeperPlayerController::ClientFailGame_Implementation(const int32& playerID)
{
	if (nullptr == window)
	{
		return;
	}

	if (playerID != PlayerState->GetPlayerId())
	{		
		window->SetMessage("다른 플레이어의 폭탄이 터졌습니다~!");
	}
}

void AMineSweeperPlayerController::ClientDrawGame_Implementation()
{
	if (nullptr == window)
	{
		return;
	}

	isPlay = false;
	window->SetMessage("모두가 폭탄을 터트렸습니다. 비겼습니다!");
}

void AMineSweeperPlayerController::ServerClearGame_Implementation(const int32& playerID)
{
	// TODO Level 2: assert로 바꾸기.
	if (false == HasAuthority())
	{
		PRINT_WARNING("권한이 없는 플레이어 입니다. ServerFailGame() 호출할 수 없습니다. playerID=%d", PlayerState->GetPlayerId());
		return;
	}

	AMineSweeperGameMode* gameMode = Cast<AMineSweeperGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (nullptr == gameMode)
	{
		PRINT_WARNING("%s", TEXT("AMineSweeperGameMode 를 얻을 수 없습니다. ServerFailGame() 실패합니다."));
		return;
	}

	gameMode->ClearGame(playerID);
}

void AMineSweeperPlayerController::ClientClearGame_Implementation(const int32& playerID)
{
	if (nullptr == window)
	{
		return;
	}

	isPlay = false;
	
	if (playerID == PlayerState->GetPlayerId())
	{
		window->SetMessage("축하합니다! 모든 폭탄을 찾았습니다.");
	}
	else
	{
		window->SetMessage("아쉽네요. 다음 기회를 노려봅시다.");
	}
}

bool AMineSweeperPlayerController::IsValidPosition(const int32& x, const int32& y)
{
	return ((0 <= x) && (x < WIDTH) && (0 <= y) && (y < HEIGHT));
}

void AMineSweeperPlayerController::CheckEndPlay()
{
	int32 expectedMineCount = 0;
	for (int32 yy = 0; yy < HEIGHT; ++yy)
	{
		for (int32 xx = 0; xx < WIDTH; ++xx)
		{
			if (ePlayMapState::Mine == playMap[yy][xx])
			{
				isPlay = false;
				window->SetMessage("폭탄이 터졌습니다.");

				ServerFailGame(PlayerState->GetPlayerId());
				return;
			}

			if (ePlayMapState::Flag == playMap[yy][xx])
			{
				if (MINE_TILE != map[yy][xx])
				{
					// 계속 진행. 깃발 제거가 필요함.
					return;
				}

				++expectedMineCount;
			}
			else if (ePlayMapState::Unkown == playMap[yy][xx])
			{
				++expectedMineCount;
			}
		}
	}

	if (MAX_MINE_COUNT == expectedMineCount)
	{
		isPlay = false;
		//window->SetMessage("축하합니다. 모든 폭탄을 찾았습니다.");

		ServerClearGame(PlayerState->GetPlayerId());
	}
}