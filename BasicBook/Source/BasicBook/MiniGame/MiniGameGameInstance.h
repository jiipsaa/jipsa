﻿#pragma once

#include "../Jipsa/JipsaGameInstance.h"
#include "MiniGameEnum.h"

#include "MiniGameGameInstance.generated.h"

UCLASS()
class BASICBOOK_API UMiniGameGameInstance : public UJipsaGameInstance
{
	GENERATED_BODY()

private:
	eMiniGamePlayMode playMode;

public:
	void Init() override;

	bool SetPlayMode(const eMiniGamePlayMode& _playMode);
	eMiniGamePlayMode GetPlayMode();

protected:
	// TODO Level 2: 네트워크 에러를 C++로 표시.
	UFUNCTION(BlueprintCallable, Category="ToClient")
	void ToClient_ShowSimpleMessage(FString message);
};