﻿#pragma once

#include <ctime>

#include "../Jipsa/Log.h"

class SpeedGugu
{
	// TODO:
	// 양쪽에서 입력받을 수 있게 UI 구성한다. (로컬 플레이는 번갈아서 진행, 멀티 플레이는 동시에 진행)
	// 번갈아가면서 한 문제씩 풀고, 상단에 각 유저의 소요 시간을 갱신해준다. 턴이 넘어가면 시간 일시 정지.

private:
	int32 tryCount = 0;
	int32 winCount = 0;
	double score = 0;
	double playTime = 0;
	const double AVERAGE_SOLVE_TIME = 1; // 한 문제당 풀이 시간 기준을 1초로 둔다.

public:

	void Play()
	{
		clock_t startClock = clock();

		int totalTryCount = 10;
		for (int i = 0; i < totalTryCount; ++i)
		{
			int firstNumber = FMath::RandRange(2, 9);
			int secondNumber = FMath::RandRange(2, 9);
		
			++tryCount;
			PRINT("[문제 %d] %2d x %2d = ", tryCount, firstNumber, secondNumber);

			int inputNumber = FMath::RandRange(2, 9) * FMath::RandRange(2, 9);
			if (inputNumber == (firstNumber * secondNumber))
			{
				++winCount;
				PRINT("정답~!");
			}
			else
			{
				PRINT("틀렸습니다~");
			}
		}

		clock_t endClock = clock();

		playTime = (double)(endClock - startClock) / CLOCKS_PER_SEC;
		if (playTime < 1)
		{
			playTime = 1;
		}

		score = (((double)winCount / tryCount) * 100) * ((tryCount * AVERAGE_SOLVE_TIME) / playTime);

		PRINT("%4.1lf점 입니다. (정답: %d/%d, 소요 시간: %4.1lf)", score, winCount, tryCount, playTime);
	}
};