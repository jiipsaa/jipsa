// Copyright Epic Games, Inc. All Rights Reserved.

#include "BasicBookGameMode.h"
#include "BasicBookPlayerController.h"
#include "BasicBookPawn.h"

ABasicBookGameMode::ABasicBookGameMode()
{
	// no pawn by default
	DefaultPawnClass = ABasicBookPawn::StaticClass();
	// use our own player controller class
	PlayerControllerClass = ABasicBookPlayerController::StaticClass();
}
