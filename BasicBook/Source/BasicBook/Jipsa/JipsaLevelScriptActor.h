﻿#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "JipsaLevelScriptActor.generated.h"

UCLASS()
class BASICBOOK_API AJipsaLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()

protected:
	UFUNCTION(BlueprintCallable, Category = "ToClient")
	void ToClient_OpenWindow(const FString windowBlueprintName);

	UFUNCTION(BlueprintCallable, Category = "ToClient")
	void ToClient_OpenPopup(const FString popupBlueprintName, const bool isToggle = false, const bool isStackPrevPopup = false);

	//UFUNCTION(BlueprintCallable, Category = "ToClient")
	//void ToClient_ShowPopupManagerMember();
};