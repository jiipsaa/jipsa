﻿#include "MessageManager.h"

void MessageManager::OnLoadComplete()
{
	simpleMessageBPClass = nullptr;
	currentMessage = nullptr;
	while (false == simpleMessageQueue.empty())
	{
		simpleMessageQueue.pop();
	}
}

void MessageManager::HideSimpleMessage()
{
	if (false == IsValid(currentMessage))
	{
		PRINT_WARNING("%s", TEXT("HideSimpleMessage() 실패했습니다. currentMessage 비정상입니다."));
		return;
	}

	if (0 < simpleMessageQueue.size())
	{
		currentMessage->Show(simpleMessageQueue.front(), Z_ORDER_MESSAGE);
		simpleMessageQueue.pop();
		return;
	}

	if (true == currentMessage->IsInViewport())
	{
		currentMessage->RemoveFromViewport();
	}
	currentMessage = nullptr;
}

//void MessageManager::ShowSimpleMessage(UWorld* world, char* message)
//{
//	FString temp = FString(UTF8_TO_TCHAR(message));
//	ShowSimpleMessage(world, temp);
//}

void MessageManager::ShowSimpleMessage(UWorld* world, FString& message)
{
	if ((nullptr != currentMessage) && (true == currentMessage->IsInViewport()))
	{
		if (MAX_QUEUE_SIZE < simpleMessageQueue.size())
		{
			simpleMessageQueue.pop();
		}

		simpleMessageQueue.push(message);
		return;
	}

	if (false == IsValid(simpleMessageBPClass))
	{
		simpleMessageBPClass = LoadClass<UMessage>(world, TEXT("/Game/UI/Message/SimpleMessageBP.SimpleMessageBP_C"));
	}

	if (true == IsValid(simpleMessageBPClass))
	{
		currentMessage = CreateWidget<UMessage>(world, simpleMessageBPClass);
		if (nullptr != currentMessage)
		{
			currentMessage->Show(message, Z_ORDER_MESSAGE);
		}
		else
		{
			PRINT_WARNING("%s", TEXT("ShowSimpleMessage() 실패했습니다. CreateWidget() 요청했지만 null 반환됬습니다."));
		}
	}
	else
	{
		PRINT_WARNING("%s", TEXT("ShowSimpleMessage() 실패했습니다. simpleMessageBPClass 비정상입니다."));
	}
}