﻿#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LobbyWindow.generated.h"

UCLASS()
class BASICBOOK_API ULobbyWindow : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UFUNCTION(BlueprintCallable, Category = "ToClient")
	virtual void ToClient_AsyncLoadMap(const TSoftObjectPtr<UWorld> map);

	UFUNCTION(BlueprintImplementableEvent, Category = "FromClient")
	void FromClient_SuccessAsyncLoadMap();

	UFUNCTION(BlueprintImplementableEvent, Category = "FromClient")
	void FromClient_FailAsyncLoadMap();

	UFUNCTION(BlueprintImplementableEvent, Category = "FromClient")
	void FromClient_CancelAsyncLoadMap();
};