﻿#include "PopupManager.h"
#include "Log.h"

void PopupManager::OnLoadComplete()
{
	currentPopup = nullptr;
	while (false == collapsedPopupStack.empty())
	{
		collapsedPopupStack.pop();
	}

	currentResultPopup = nullptr;
}

UUserWidget* PopupManager::OpenWindow(UWorld* world, const char* windowBlueprintName)
{
	return OpenWindow(world, UTF8_TO_TCHAR(windowBlueprintName));
}

UUserWidget* PopupManager::OpenWindow(UWorld* world, FString windowBlueprintName)
{
	TSubclassOf<UUserWidget> windowBPClass = LoadClass<UUserWidget>(world, *FString::Printf(TEXT("/Game/UI/Window/%s.%s_C"), *windowBlueprintName, *windowBlueprintName));
	if (true == IsValid(windowBPClass))
	{
		UUserWidget* currentWindow = CreateWidget(world, windowBPClass);
		if (nullptr != currentWindow)
		{
			currentWindow->AddToViewport(Z_ORDER_WINDOW);

			APlayerController* playerController = world->GetFirstPlayerController();
			playerController->SetInputMode(FInputModeGameAndUI());
			playerController->bShowMouseCursor = true;
			
			return currentWindow;
		}
		else
		{
			PRINT_WARNING("%s", TEXT("OpenWindow() 실패했습니다. CreateWidget() 요청했지만 null 반환됬습니다."));
			return nullptr;
		}
	}
	else
	{
		PRINT_WARNING("OpenWindow() 실패했습니다. LoadClass() 요청했지만 null 반환됬습니다. blueprintName=%s", *windowBlueprintName);
		return nullptr;
	}
}

void PopupManager::ClosePopup(UPopup* targetPopup)
{
	if (false == IsValid(targetPopup))
	{
		PRINT_WARNING("%s", TEXT("ClosePopup() 실패했습니다. targetPopup 비정상입니다."));
		return;
	}

	if (true == targetPopup->IsInViewport())
	{
		targetPopup->RemoveFromViewport();
	}

	if (targetPopup == currentResultPopup)
	{
		if (false == IsValid(currentResultPopup))
		{
			PRINT_WARNING("%s", TEXT("ClosePopup() 실패했습니다. currentResultPopup 비정상입니다."));
			return;
		}

		currentResultPopup = nullptr;
	}
	else if (targetPopup == currentPopup)
	{
		if (false == IsValid(currentPopup))
		{
			PRINT_WARNING("%s", TEXT("ClosePopup() 실패했습니다. currentPopup 비정상입니다."));
			return;
		}

		currentPopup = nullptr;

		if (0 < collapsedPopupStack.size())
		{
			UPopup* prevPopup = collapsedPopupStack.top();
			collapsedPopupStack.pop();

			if (true == IsValid(prevPopup))
			{
				prevPopup->Open(Z_ORDER_POPUP);
				currentPopup = prevPopup;
			}
		}
	}
}

void PopupManager::OpenPopup(UWorld* world, const char* popupBlueprintName, const bool isToggle, const bool isStackPrevPopup)
{
	OpenPopup(world, UTF8_TO_TCHAR(popupBlueprintName), isToggle, isStackPrevPopup);
}

void PopupManager::OpenPopup(UWorld* world, const FString popupBlueprintName, const bool isToggle, const bool isStackPrevPopup)
{
	// TODO: 경로가 제한되는구나.. 풀패스 안쓰려고 이름만 받았었는데.. 경로를 바꾸고 싶어서 Parameter를 추가하면.. 불편할듯? 차라리 풀패스 쓰는게...
	TSubclassOf<UPopup> popupBPClass = LoadClass<UPopup>(world, *FString::Printf(TEXT("/Game/UI/Popup/%s.%s_C"), *popupBlueprintName, *popupBlueprintName));
	if (true == IsValid(popupBPClass))
	{
		if (nullptr != currentPopup)
		{
			if (true == currentPopup->IsA(popupBPClass))
			{
				if (true == isToggle)
				{// 이미 팝업이 열려있으면, Hide 한다.
					// TODO:
					// 로직에 문제있음. 열려있는 팝업이 ConfirmPopup이고, 새로 여는 팝업이 ConfirmPopup인 경우엔 두 개의 팝업이 같은지 확신할 수 없다. (2/3)
					currentPopup->Close();
					return;
				}
				else
				{// 이미 팝업이 열려있으면, Open 하지 않는다.
					// TODO:
					// 로직에 문제있음. 열려있는 팝업이 ConfirmPopup이고, 새로 여는 팝업이 ConfirmPopup인 경우엔 두 개의 팝업이 같은지 확신할 수 없다. (1/3)
					// -> ConfirmPopup은 쌓지 못하도록 제한한다. ex) 다음 팝업이 확인 팝업 쌓으려고 하면, 안됨 하고 개발용 경고 띄우자.
					// -> ConfirmPopup은 토글도 제한하자.
					// -> ConfirmPopup인 경우는 IsA 비교에 더해서 파라미터 비교도 한다. (ex. 메시지 내용이 같은지 확인한다. 아.. 문자열 비교 하기 싫은데.. 번역 문자열 쓸텐데.. 키 비교 있나)
					// -> 일단 써본다. 동일한 팝업인지 구분할 방법을 고민을 좀 더 해봐야 되겠다.
					return;
				}
			}
			else
			{
				if (true == isStackPrevPopup)
				{// 이전 팝업은 가린 상태로 Stack에 쌓는다. 상단 팝업이 닫힐때 stack에 있는 팝업을 다시 Visible 한다.
					currentPopup->SetVisibility(ESlateVisibility::Collapsed);
					collapsedPopupStack.push(currentPopup);
					currentPopup = nullptr;
				}
				else
				{// 이전 팝업은 닫는다.
					if (0 < collapsedPopupStack.size())
					{
						UPopup* prevPopup = collapsedPopupStack.top();
						if (true == prevPopup->IsA(popupBPClass))
						{
							// TODO:
							// 로직에 문제있음. 쌓여있는 팝업이 ConfirmPopup이고, 새로 여는 팝업이 ConfirmPopup인 경우엔 두 개의 팝업이 같은지 확신할 수 없다. (3/3)
							currentPopup->Close();
							return;
						}
						else
						{
							currentPopup->SetVisibility(ESlateVisibility::Collapsed);
							collapsedPopupStack.push(currentPopup);
							currentPopup = nullptr;
						}
					}
					else
					{
						currentPopup->Close();
					}
				}
			}
		}

		currentPopup = CreateWidget<UPopup>(world, popupBPClass);
		if (nullptr != currentPopup)
		{
			// TODO: 인자 전달이 가능해야 한다.
			currentPopup->Open(Z_ORDER_POPUP);
		}
		else
		{
			PRINT_WARNING("%s", TEXT("OpenPopup() 실패했습니다. CreateWidget() 요청했지만 null 반환됬습니다."));
		}
	}
	else
	{
		PRINT_WARNING("OpenPopup() 실패했습니다. LoadClass() 요청했지만 null 반환됬습니다. blueprintName=%s", *popupBlueprintName);
	}
}

UUserWidget* PopupManager::OpenResultPopup(UWorld* world, const char* popupBlueprintName)
{
	if (nullptr != currentResultPopup)
	{
		PRINT_WARNING("%s", TEXT("결과 팝업이 이미 열려 있습니다. 화면에 결과 팝업이 안떠있으면 디버깅 해야합니다."));
		return nullptr;
	}

	// TODO: 경로 ResultPopup 따로 두자.
	TCHAR* blueprintName = UTF8_TO_TCHAR(popupBlueprintName);
	TSubclassOf<UPopup> popupBPClass = LoadClass<UPopup>(world, *FString::Printf(TEXT("/Game/UI/Popup/%s.%s_C"), blueprintName, blueprintName));
	if (true == IsValid(popupBPClass))
	{
		currentResultPopup = CreateWidget<UPopup>(world, popupBPClass);
		if (nullptr != currentResultPopup)
		{
			currentResultPopup->Open(Z_ORDER_RESULT_POPUP);
			return currentResultPopup;
		}
		else
		{
			PRINT_WARNING("%s", TEXT("OpenResultPopup() 실패했습니다. CreateWidget() 요청했지만 null 반환됬습니다."));
			return nullptr;
		}
	}
	else
	{
		PRINT_WARNING("OpenResultPopup() 실패했습니다. LoadClass() 요청했지만 null 반환됬습니다. blueprintName=%s", blueprintName);
		return nullptr;
	}
}

//void PopupManager::ShowPopupManagerMember()
//{
//	PRINT("currentPopup is null: %d, stack size: %d", (nullptr == currentPopup), collapsedPopupStack.size());
//}