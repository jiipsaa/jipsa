﻿#pragma once

#pragma region 로그용 매크로
// ex)
// PRINT("3 3.0 삼 쓰리");
// PRINT("%i %f %c %s", 3, 3.0, TEXT('삼'), TEXT("쓰리"));
// PRINT_WARNING("%i %f %c %s", 3, 3.0, TEXT('삼'), TEXT("쓰리"));
// PRINT_ERROR("%i %f %c %s", 3, 3.0, TEXT('삼'), TEXT("쓰리"));

// 변환 명세.
// %d: int		// ex) %3d => 3자리, 
// %f: float	// ex) %5.1f => 소수점 포함해서 5자리 + 소숫점은 첫째자리까지, 
// %lf: double
// %c: char
// %s: string, char*
// %0x: 메모리 주소

// 확장 특수문자(escape sequence).
// \n: 줄 바꿈
// \t: 탭
// \v: 수직 탭
// \r: 캐리지 리턴(=현재 위치를 나타내는 커서 를 맨 앞으로 이동)
// \b: 백스페이스
// \0: 공백 문자
// \a: 비프음
// \\: 역슬래시
// \': 작은 따옴표
// \": 큰 따옴표

#if PLATFORM_ANDROID
	#ifndef PRINT
	#define PRINT(Format, ...) \
		UE_LOG(LogTemp, Display, TEXT("%s"), *FString::Printf(TEXT(Format), ##__VA_ARGS__)); \
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, FString::Printf(TEXT(Format), ##__VA_ARGS__), false);
	#endif

	#ifndef PRINT_WARNING
	#define PRINT_WARNING(Format, ...) \
		UE_LOG(LogTemp, Warning, TEXT("%s"), *FString::Printf(TEXT(Format), ##__VA_ARGS__)); \
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::Printf(TEXT(Format), ##__VA_ARGS__), false);
	#endif

	#ifndef PRINT_ERROR
	#define PRINT_ERROR(Format, ...) \
		UE_LOG(LogTemp, Error, TEXT("%s"), *FString::Printf(TEXT(Format), ##__VA_ARGS__)); \
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT(Format), ##__VA_ARGS__), false);
	#endif
#else
	#ifndef GET_TIME
	#define GET_TIME FString(__TIME__)
	#endif

	#ifndef GET_CALL_INFO
	#define GET_CALL_INFO FString::Printf(TEXT("function: %s(), line: %d, file: %s"), TEXT(__FUNCTION__), __LINE__, TEXT(__FILE__))
	#endif

	#ifndef PRINT
	#define PRINT(Format, ...) \
		UE_LOG(LogTemp, Display, TEXT("[%s] %s"), *GET_TIME, *FString::Printf(TEXT(Format), ##__VA_ARGS__)); \
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::White, FString::Printf(TEXT(Format), ##__VA_ARGS__), false);
	#endif

	#ifndef PRINT_WARNING
	#define PRINT_WARNING(Format, ...) \
		UE_LOG(LogTemp, Warning, TEXT("[%s] %s *%s"), *GET_TIME, *FString::Printf(TEXT(Format), ##__VA_ARGS__), *GET_CALL_INFO); \
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::Printf(TEXT(Format), ##__VA_ARGS__), false);
	#endif

	#ifndef PRINT_ERROR
	#define PRINT_ERROR(Format, ...) \
		UE_LOG(LogTemp, Error, TEXT("[%s] %s *%s"), *GET_TIME, *FString::Printf(TEXT(Format), ##__VA_ARGS__), *GET_CALL_INFO); \
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT(Format), ##__VA_ARGS__), false);
	#endif
#endif
#pragma endregion

#pragma region 어썰트 매크로
// ex)
// ASSERT(true == b, "문제가 있다.");
// ASSERT(true == b, "문제가 있다. i=%d, b=%d", i, b);
#ifndef ASSERT
#define ASSERT(InExpression, Format, ...) if (false == ensureAlwaysMsgf(InExpression, TEXT(Format), ##__VA_ARGS__)) \
	{ GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Emerald, FString::Printf(TEXT(Format), ##__VA_ARGS__), false);  return; }
#endif
#pragma endregion