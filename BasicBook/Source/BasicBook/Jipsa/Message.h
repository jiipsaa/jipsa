#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "Message.generated.h"

UCLASS()
class BASICBOOK_API UMessage : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* messageText;

//private:
//	FTimerHandle timerHandle;
//
//protected:
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Jipsa")
//	float showTime = 3;

public:
	void Show(const FString& message, const int32 ZOrder);

protected:
	virtual void NativeConstruct() override
	{
		Super::NativeConstruct();

		//GetWorld()->GetTimerManager().SetTimer(
		//	timerHandle
		//	,[&]() {
		//		MessageManager::GetSingleton()->HideSimpleMessage(GetWorld());
		//	}
		//	,showTime
		//	,false
		//);
	}

	virtual void NativeDestruct() override
	{
		Super::NativeDestruct();

		//FTimerManager& timerManger = GetWorld()->GetTimerManager();
		//if (true == timerManger.IsTimerActive(timerHandle))
		//{
		//	timerManger.ClearTimer(timerHandle);
		//}
	}

	UFUNCTION(BlueprintImplementableEvent, Category = "FromClient")
	void FromClient_ShowMessage();

	UFUNCTION(BlueprintCallable, Category = "ToClient")
	void ToClient_HideMessage();
};