﻿#include "LobbyWindow.h"

#include "Log.h"

#include "Kismet/GameplayStatics.h"
#include "UObject/UObjectGlobals.h"

void ULobbyWindow::ToClient_AsyncLoadMap(const TSoftObjectPtr<UWorld> map)
{
	LoadPackageAsync(
		FPackageName::ObjectPathToPackageName(map.ToString())
		,FLoadPackageAsyncDelegate::CreateLambda(
			[=](const FName& packageName, UPackage* loadedPackage, EAsyncLoadingResult::Type result)
			{
				if (EAsyncLoadingResult::Succeeded == result)
				{
					UGameplayStatics::OpenLevelBySoftObjectPtr(this, map);
					FromClient_SuccessAsyncLoadMap();
				}
				else
				{
					if (EAsyncLoadingResult::Failed == result)
					{
						PRINT_WARNING("맵 로딩 실패했습니다. *mapName=%s", *map.ToString());
						FromClient_FailAsyncLoadMap();
					}
					else if (EAsyncLoadingResult::Canceled == result)
					{
						PRINT("맵 로딩 취소됬습니다. *mapName=%s", *map.ToString());
						FromClient_CancelAsyncLoadMap();
					}
					else
					{
						PRINT_WARNING("알 수 없는 EAsyncLoadingResult 입니다. 새로운 타입이 생겼는지 확인해야 합니다. *EAsyncLoadingResult=%d, mapName=%s", result, *map.ToString());
						FromClient_FailAsyncLoadMap();
					}
				}
			}
		)
		,0
		,PKG_ContainsMap
	);

	// TODO: 로딩 UI에 연결할때 Timer에서 GetAsyncLoadPercentage() 수치를 보여주도록 작업하자.
	FName packageName = FName(*FPackageName::ObjectPathToPackageName(map.ToString()));
	float loadPercentage = -1;
	do
	{
		loadPercentage = GetAsyncLoadPercentage(packageName);
		PRINT("TODO: 로딩 진행상황 표시: %f", loadPercentage);
	} while (-1 != loadPercentage);
}