﻿#pragma once

#include "Popup.h"
#include "Singleton.h"

#include <stack>
using namespace std;

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"

class PopupManager : public Singleton<PopupManager> {
private:
	const int32 Z_ORDER_WINDOW = 1;
	const int32 Z_ORDER_POPUP = 2;
	const int32 Z_ORDER_RESULT_POPUP = 3;

	//stack<TSubclassOf<UUserWidget>> windowStack;
	
	UPopup* currentPopup;
	stack<UPopup*> collapsedPopupStack;

	UPopup* currentResultPopup = nullptr;

	//typedef void(PopupManager::*FunctionPtrType)(void);
	//FunctionPtrType temp;

public:
	PopupManager() : currentPopup(nullptr) {}

	void OnLoadComplete() override;

	void CloseAllWindow()
	{
	}

	UUserWidget* OpenWindow(UWorld* world, const char* windowBlueprintName);
	UUserWidget* OpenWindow(UWorld* world, const FString windowBlueprintName);

	void SwapTopWindow(char* windowBlueprintName)
	{
	}

	void CloseAllPopup()
	{
	}

	void ClosePopup(UPopup* targetPopup);
	void OpenPopup(UWorld* world, const char* popupBlueprintName, const bool isToggle = false, const bool isStackPrevPopup = false);
	void OpenPopup(UWorld* world, const FString popupBlueprintName, const bool isToggle = false, const bool isStackPrevPopup = false);

	// 람다나 어노니머스 함수 넣을 수 있냐?
	UUserWidget* OpenResultPopup(UWorld* world, const char* popupBlueprintName);

	//void ShowPopupManagerMember();
};