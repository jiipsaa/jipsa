#include "Popup.h"
#include "Log.h"
#include "PopupManager.h"

void UPopup::Open(const int32 ZOrder)
{
	if (false == IsInViewport())
	{
		AddToViewport(ZOrder);
	}

	SetVisibility(ESlateVisibility::Visible);
	closeButton->SetIsEnabled(true);

	FromClient_OpenPopup();
}

void UPopup::Close()
{
	closeButton->SetIsEnabled(false);

	FromClient_BeforeClosePopup();
}

void UPopup::NativeConstruct()
{
	Super::NativeConstruct();

	closeButton->OnClicked.AddDynamic(this, &UPopup::OnClickedCloseButton);
}

void UPopup::ToClient_ClosePopup()
{
	if (true == onCloseDelegate.IsBound())
	{
		onCloseDelegate.Execute();
		onCloseDelegate.~TDelegate();
	}

	PopupManager::GetSingleton()->ClosePopup(this);
}

void UPopup::OnClickedCloseButton()
{
	Close();
}