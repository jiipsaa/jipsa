﻿#pragma once
#include "MessageManager.h"
#include "PopupManager.h"
#include "../Data/AsciiCode.h"
#include "../MiniGame/Sabotage/Data/SabotageData.h"

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include "JipsaGameInstance.generated.h"

UCLASS()
class BASICBOOK_API UJipsaGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	void LoadComplete(const float LoadTime, const FString& MapName) override
	{
		Super::LoadComplete(LoadTime, MapName);
		
		// 맵을 이동하면 메시지와 팝업을 제거한다.
		MessageManager::GetSingleton()->OnLoadComplete();
		PopupManager::GetSingleton()->OnLoadComplete();
		NSSabotage::SabotageData::GetSingleton()->OnLoadComplete();
	}

	void Shutdown() override
	{
		Super::Shutdown();
		// NOTE: 강제로 Singleton 클래스를 해제한다.
		// - 에디터 재시작할때 static variable 초기화가 안되는 것 같은 증상을 보임. (-> 언리얼 생명 주기에서 빌드 됬을때 초기화하는 것과, 플레이 종료시 초기화하는 것이 다름을 확인할 수 있다.)
		// - 이전에 시작했을때의 T* singleton 을 다시 사용하고 있었음. 두번째로 시작하면 생성자도 호출이 안되는걸 확인할 수 있음.
		// - ex) PopupManager에서 열려있던 팝업을 닫는 로직이 있는데, 이전 시작했을때의 팝업 pointer가 남아서 크래시나는 이슈가 발생했음.
		// TODO: 일일이 OnShutdown() 호출하지 않도록 개선 필요. Singleton.h 수정을 해야하는데..
		MessageManager::OnShutdown();
		PopupManager::OnShutdown();
		AsciiCodeList::OnShutdown();
		NSSabotage::SabotageData::OnShutdown();
	}
};