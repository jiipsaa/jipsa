#include "JipsaLevelScriptActor.h"
#include "PopupManager.h"

void AJipsaLevelScriptActor::ToClient_OpenWindow(const FString windowBlueprintName)
{
	PopupManager::GetSingleton()->OpenWindow(GetWorld(), windowBlueprintName);
}

void AJipsaLevelScriptActor::ToClient_OpenPopup(const FString popupBlueprintName, const bool isToggle, const bool isStackPrevPopup)
{
	PopupManager::GetSingleton()->OpenPopup(GetWorld(), popupBlueprintName, isToggle, isStackPrevPopup);
}

//void AJipsaLevelScriptActor::ToClient_ShowPopupManagerMember()
//{
//	PopupManager::GetSingleton()->ShowPopupManagerMember();
//}