﻿#pragma once

#include "Log.h"
#include "Message.h"
#include "Singleton.h"
class UMessage;

#include <queue>
using namespace std;

#include "CoreMinimal.h"

class MessageManager : public Singleton<MessageManager>
{
private:
	const int32 Z_ORDER_MESSAGE = 3;

	const int32 MAX_QUEUE_SIZE = 5; // NOTE: 5개 넘으면 먼저 들어왔던 메시지는 삭제한다.
	queue<FString> simpleMessageQueue;

	TSubclassOf<UMessage> simpleMessageBPClass;
	UMessage* currentMessage;

public:
	MessageManager() : currentMessage(nullptr) {}

	void OnLoadComplete() override;

	// TODO: UObject 로 만들면 GetWorld 가능할까?
	void HideSimpleMessage();
	//void ShowSimpleMessage(UWorld* world, char* message);
	void ShowSimpleMessage(UWorld* world, FString& message);
};