#pragma once

template <typename T>
class Singleton
{
private:
	static T* singleton;

public:
	Singleton() {}
	virtual ~Singleton()
	{
		//if (nullptr != singleton)
		//{
		//	delete singleton;
		//}
		//singleton = nullptr;
	}
	
	static T* GetSingleton()
	{
		if (nullptr == singleton)
		{
			singleton = new T;
		}

		return singleton;
	}

	static void OnShutdown()
	{
		if (nullptr != singleton)
		{
			delete singleton;
		}
		singleton = nullptr;
	}

	virtual void OnLoadComplete() {}
};

template <typename T> T* Singleton<T> ::singleton = nullptr;