﻿#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Popup.generated.h"

DECLARE_DELEGATE(OnCloseDelegate)

UCLASS()
class BASICBOOK_API UPopup : public UUserWidget
{
	GENERATED_BODY()

public:
	OnCloseDelegate onCloseDelegate;

protected:
	UPROPERTY(meta = (BindWidget))
	class UButton* closeButton;

public:
	void Open(const int32 ZOrder);
	void Close();

protected:
	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintImplementableEvent, Category = "FromClient")
	void FromClient_OpenPopup();

	UFUNCTION(BlueprintImplementableEvent, Category = "FromClient")
	void FromClient_BeforeClosePopup();

	UFUNCTION(BlueprintCallable, Category = "ToClient")
	void ToClient_ClosePopup();

private:
	UFUNCTION()
	void OnClickedCloseButton();
};