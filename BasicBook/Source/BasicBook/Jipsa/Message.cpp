﻿#include "Message.h"
#include "MessageManager.h"

void UMessage::Show(const FString& message, const int32 ZOrder)
{
	messageText->SetText(FText::FromString(message));

	if (false == IsInViewport())
	{
		AddToViewport(ZOrder);
	}

	FromClient_ShowMessage();
}

void UMessage::ToClient_HideMessage()
{
	MessageManager::GetSingleton()->HideSimpleMessage();
}