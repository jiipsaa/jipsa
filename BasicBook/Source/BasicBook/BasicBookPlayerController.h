// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BasicBookPlayerController.generated.h"

/** PlayerController class used to enable cursor */
UCLASS()
class ABasicBookPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ABasicBookPlayerController();
};


