// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BasicBookGameMode.generated.h"

/** GameMode class to specify pawn and playercontroller */
UCLASS(minimalapi)
class ABasicBookGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABasicBookGameMode();
};



