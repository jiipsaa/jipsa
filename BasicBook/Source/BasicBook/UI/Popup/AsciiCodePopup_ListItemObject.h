#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "AsciiCodePopup_ListItemObject.generated.h"

// NOTE: BP에서 UserObjectListEntry를 사용하지 않으므로, CLASS(BlueprintType) 와 UPROPERTY(BlueprintReadWrite) 역시 선언하지 않아도 된다.
// *BlueprintType: 클래스를 블루프린트에서 변수로 사용할 수 있는 유형으로 노출시킵니다.

// UCLASS(BlueprintType)
UCLASS()
class BASICBOOK_API UAsciiCodePopup_ListItemObject : public UObject
{
	GENERATED_BODY()

public:
	//UPROPERTY(BlueprintReadWrite)
	int32 decimal;
	
	//UPROPERTY(BlueprintReadWrite)
	FString character;
	
	//UPROPERTY(BlueprintReadWrite)
	FString meaning;
};