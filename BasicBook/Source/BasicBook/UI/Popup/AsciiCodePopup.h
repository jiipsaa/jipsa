﻿#pragma once

#include "../../Data/AsciiCode.h"
#include "../../Jipsa/Popup.h"
#include "AsciiCodePopup_ListItemObject.h"

#include "Components/ListView.h"
#include "AsciiCodePopup.generated.h"

// NOTE: 디자이너가 BP에서 "List Entries/Entry Widget Class" 셋팅해줘야 한다. (EntryWidget도 BP를 디자이너가 만들기 때문에 디자이너가 연결하는게 맞다.)

UCLASS()
class BASICBOOK_API UAsciiCodePopup : public UPopup
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	class UListView* asciiCodeListView;

protected:
	virtual void NativeConstruct() override
	{
		Super::NativeConstruct();

		const TArray<AsciiCode>& asciiCodeList = AsciiCodeList::GetSingleton()->GetData();

		UAsciiCodePopup_ListItemObject* listItemObject = nullptr;

		int32 num = asciiCodeList.Num();
		for (int32 i = 0; i < num; ++i)
		{
			listItemObject = NewObject<UAsciiCodePopup_ListItemObject>();
			listItemObject->decimal = asciiCodeList[i].decimal;
			listItemObject->character = asciiCodeList[i].character;
			listItemObject->meaning = asciiCodeList[i].meaning;

			asciiCodeListView->AddItem(listItemObject);
		}

		AsciiCodeList::GetSingleton()->EmptyData();
	}
};