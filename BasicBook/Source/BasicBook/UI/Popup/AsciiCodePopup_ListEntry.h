﻿#pragma once

#include "AsciiCodePopup_ListItemObject.h"

#include "CoreMinimal.h"
#include "Blueprint/IUserObjectListEntry.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "AsciiCodePopup_ListEntry.generated.h"

UCLASS()
class BASICBOOK_API UAsciiCodePopup_ListEntry : public UUserWidget, public IUserObjectListEntry
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* decimalText;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* meaningText;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* characterText;

protected:
	virtual void NativeOnListItemObjectSet(UObject* ListItemObject) override
	{
		UAsciiCodePopup_ListItemObject* listItemObject = Cast<UAsciiCodePopup_ListItemObject>(ListItemObject);
		decimalText->SetText(FText::FromString(FString::FromInt(listItemObject->decimal)));
		meaningText->SetText(FText::FromString(listItemObject->character));
		characterText->SetText(FText::FromString(listItemObject->meaning));
	}
};