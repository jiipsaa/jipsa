// Copyright Epic Games, Inc. All Rights Reserved.

#include "BasicBookPlayerController.h"

ABasicBookPlayerController::ABasicBookPlayerController()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}
