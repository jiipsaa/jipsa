﻿using UnrealBuildTool;

public class TestModule : ModuleRules
{
	public TestModule(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		// TODO: 같은 내용 inject할 방법좀 찾아보자. MyModuleRules 클래스를 만들 수 있나?
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine" });
	}
}
