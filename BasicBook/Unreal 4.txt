[레벨 스트리밍]
- Persistant 레벨 하나와 Streaming 레벨 다수로 구성됨. Streaming 레벨을 상황에 따라 로드/언로드 해서 사용함. (Visibility만 제어할 수도 있음)
ex)
원경, 하늘 -> Persistant Level
특정 상황, 특정 구역 -> Streaming Level

방법1. 명시적 제어
- Levels 탭 / Change Streaming Method / Blueprint: 블루프린트(또는 C++)에서 Load Stream Level, Unload Stream Level 을 명시적으로 호출해서 제어. (Make Visible After Load: true, Should Block on Load: true)
- Levels 탭 / Change Streaming Method / Always Loaded: 항상 로드됨. (Persistant Level 이 변경될때만 언로드됨)

방법2. 레벨 스트리밍 볼륨
- Persistant 레벨에 설치. Camera가 볼륨에 진입하면 레벨을 로드하고, 볼륨을 빠져나가면 레벨을 언로드함.
- 볼륨을 겹쳐서 설치하면 Seamless 맵을 구현할 수 있음.
- 자연스럽게 레벨이 연결됨.
- Levels 탭 / Change Streaming Method / Blueprint 으로 설정되야함.
- 에디터에서 실행하는 경우 전부 로딩된 상태로 시작함. 빌드해서 테스트 해봐야함.

특별한 방법. 월드 컴포지션
https://docs.unrealengine.com/4.27/ko/BuildingWorlds/LevelStreaming/WorldBrowser/
- 평면형 그리드에 레벨을 배치해 두고, 플레이어가 다가가면 레벨을 로드함. (Persistant 레벨에 스트리밍 정보가 저장되지 않도록 만든 시스템이다)
- World Settings 탭 / World / Enable World Composition 을 true 로 설정하여 활성화 할 수 있음.
- 활성화되면, Levels 탭 / Summons world composition 버튼이 활성화됨. 그리드 편집이 가능함.
- 멀티플레이 지원되지 않음. 별도의 서버로직을 구현하거나, 언리얼 데디케이트 서버에 레이어를 추가 구현해야함.

